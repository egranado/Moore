# Contributing to Moore

Contributions to Moore are accepted in the form of merge requests. Code should
conform to the C++ and Python formatting standards define by the CI. The
checker will run automatically when you push new commits to your branch, and
will generate a patch you can apply if the checks fail.

To simplify the workflow, you can format and lint locally using

```shell
lb-format path/to/file
flake8 path/to/file
```

Check [Git4LHCb](https://twiki.cern.ch/twiki/bin/view/LHCb/Git4LHCb) for more details.

Opening a merge request (MR) is a good way to discuss your changes with others.
If your code is not yet ready to be reviewed by a Moore responsible, you can
[mark it 'work in
progress'](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html)
by starting the title with `WIP: `. This can be useful for when you want to ask
others about specific details your unsure of. Non-WIP MRs will be reviewed
following the [RTA shifter guidelines][shifterpage].

Each MR should have a short title summarising the changes you've made, such as
"Add two-body prompt D0 HLT2 lines" or "Fix HLT2 forward tracking bug", and a
description with more detail. It is useful to see [what Moore responsibles look
for in a MR before you open your first one][shifterpage].

## Code owners

The [`CODEOWNERS` file](./CODEOWNERS) lists 'owners' of files and folders.
These are people who might be interested in changes to those objects. If
you're adding new files, please make sure to put yourself, and perhaps
others, down as owners in the owners file. You can also update the entries if
you notice things are out of date.

## Branches

The `master` branch is used for Run 3 development and data-taking. This is the
usual branch to open merge requests against.

Some other branches track code from previous campaigns. Open an issue if you're
unsure what branch to use.

[shifterpage]: https://twiki.cern.ch/twiki/bin/viewauth/LHCb/RealTimeAnalysisShifterChecklist