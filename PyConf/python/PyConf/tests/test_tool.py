###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import pytest
import re

from PyConf import ConfigurationError
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer as Producer
from PyConf.Tools import MyTool, Gaudi__Examples__FloatTool as FloatTool


def test_init():
    # Fail on nonexistent properties
    with pytest.raises(ConfigurationError) as e:
        MyTool(NotAProp=1)
    assert re.search(r'NotAProp.*not propert.* of.*MyTool', str(e))

    with pytest.raises(ConfigurationError) as e:
        FloatTool()
    assert re.match(r'.*provide all inputs.*Input.*', str(e))

    data = Producer()
    t = FloatTool(Input=data)
    assert len(t.inputs) == 1
    assert t.private

    # initializing an algorithm twice should give the same instance
    t2 = FloatTool(Input=data)
    assert t is t2, "tool instantiation doesn't seem to be cached correctly"

    tp = FloatTool(public=True, Input=data)
    assert tp.public
    assert tp != t, "public and private tool instance must be different"

    with pytest.raises(ConfigurationError):
        # immutable after instantiation
        t.OutputLevel = 2
