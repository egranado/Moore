###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import inspect
import re

import pytest

from PyConf.tonic import (configurable, bound_parameters, debug, forced)

# TODO add pre-test func that clears global state in tonic.py?


def test_supported_signatures():
    configurable(lambda: None)
    configurable(lambda a=1: None)
    configurable(lambda **kwargs: None)
    configurable(lambda a=1, **kwargs: None)
    configurable(lambda a: None)
    configurable(lambda a, **kwargs: None)


def test_configurable_wrapping():
    """The configurable wrapper should preserve all arguments."""
    f = lambda a=1, b=2: None
    decorated = configurable(f)

    try:
        f_spec = inspect.getfullargspec(f)
        decorated_spec = inspect.getfullargspec(decorated)
    except AttributeError:
        # Python 2 doesn't have getfullargspec
        f_spec = inspect.getargspec(f)
        decorated_spec = inspect.getargspec(decorated)

    assert f_spec.args == decorated_spec.args
    assert f_spec.keywords == decorated_spec.keywords


def test_bind():
    @configurable
    def func(param='default'):
        return param

    with func.bind(param='override value'):
        assert bound_parameters(func)['param'] == 'override value'

        with func.bind(param='deeper override'):
            # TODO test for override warning in bind (atm warning is raised only below)
            with pytest.warns(UserWarning):
                assert bound_parameters(func)['param'] == 'override value'

            # Mark func called so we don't see 'unused bound function' warnings
            func._called = True

    with func.bind():  # test a noop bind
        func()

    # bind outside `with` does not affect calls
    func.bind(param='x')
    assert func() == 'default'


def test_global_bind():
    @configurable
    def func(param='default'):
        return param

    # check that you cannot do global bind inside a bind
    with func.bind(param='scoped'):
        func()  # use function to avoid 'unused bound function' warnings
        with pytest.raises(RuntimeError):
            func.global_bind(param='this line raises')

    # a working global bind
    func.global_bind(param='global')
    assert func() == 'global'

    # a second global bind
    with pytest.warns(UserWarning):
        func.global_bind(param=forced('override'))
        assert func() == 'override'


def test_bind_within_functions():
    """Bind should work when scoped within functions."""

    @configurable
    def f(a=1):
        return a

    @configurable
    def g():
        with f.bind(a=2):
            return f()

    @configurable
    def h():
        with f.bind(a=3):
            return f()

    assert f() == 1
    assert g() == 2
    # The bind in `g` should not affect f
    assert f() == 1
    assert h() == 3
    assert f() == 1


def test_precedence():
    @configurable
    def func(param='default value'):
        assert param == 'override value'

    # normally higher-level bind takes precedence
    with debug():
        lineno = inspect.getframeinfo(inspect.currentframe()).lineno
        with func.bind(param='override value'):
            with pytest.warns(UserWarning) as w:
                func(param='call site value')
        assert len(w) == 1  # exactly one warning
        assert ("higher-level 'override value' (test_tonic.py:{}) "
                "shadows 'call site value' (test_tonic.py:{})".format(
                    lineno + 1, lineno + 3)) in w[0].message.args[0]

        # using `forced` in a bind overrides higher-level binds
        lineno = inspect.getframeinfo(inspect.currentframe()).lineno
        with func.bind(param='value'):
            with func.bind(param=forced('override value')):
                with pytest.warns(UserWarning) as w:
                    func()
        assert len(w) == 1  # exactly one warning
        assert (
            "higher-level 'value' (test_tonic.py:{}) "
            "overridden by forced 'override value' (test_tonic.py:{})".format(
                lineno + 1, lineno + 2)) in w[0].message.args[0]

        # using `forced` at the call site overrides higher-level binds
        lineno = inspect.getframeinfo(inspect.currentframe()).lineno
        with func.bind(param='value'):
            with pytest.warns(UserWarning) as w:
                func(param=forced('override value'))
        assert len(w) == 1  # exactly one warning
        assert (
            "higher-level 'value' (test_tonic.py:{}) "
            "overridden by forced 'override value' (test_tonic.py:{})".format(
                lineno + 1, lineno + 3)) in w[0].message.args[0]


def test_positional_arguments():
    @configurable
    def func(param='default value'):
        assert param == 'override value'

    func('override value')
    with pytest.raises(TypeError):  #multiple values for single argument error
        func('override value', param='other value')
    with pytest.raises(TypeError):  #too many arguments error
        func('override value', 'other value')


def test_arguments_without_default():
    @configurable
    def func(param, param2='default value'):
        assert param == 'value'
        assert param2 == 'override value'

    func('value', 'override value')
    func('value', param2='override value')  #positional call
    func(param='value', param2='override value')  #kw call
    with pytest.raises(TypeError):
        func(param2='override value')  #no value was given for param
    with func.bind(param='value'):
        func(param2='override value')
        func('value', param2='override value')  # same value, no warning


def _remove_test_method_reference(s):
    """Return string with the reference to the test_debug method removed."""
    # Check that the function name and line number are indeed present
    assert re.match(r'.*test_debug:\d+', s)
    return re.sub(r'test_debug:\d+', 'TEST_MODULE', s)


def test_debug(capsys):
    """Parameter values should be print in the `debug` context."""

    @configurable
    def func(param1='default value', param2=123):
        pass

    func()
    captured = capsys.readouterr()
    assert captured.out == ''

    with debug():
        func()
        captured = capsys.readouterr()
        assert _remove_test_method_reference(captured.out) == '\n'.join([
            'Calling @configurable `func` from TEST_MODULE with non-default parameters: NONE',
        ]) + '\n'

        with func.bind(param1='non-default value'):
            func()
            captured = capsys.readouterr()
            assert _remove_test_method_reference(captured.out) == '\n'.join([
                'Calling @configurable `func` from TEST_MODULE with non-default parameters:',
                '    param1 = non-default value (bound)',
            ]) + '\n'

            func(param2=321)
            captured = capsys.readouterr()
            assert _remove_test_method_reference(captured.out) == '\n'.join([
                'Calling @configurable `func` from TEST_MODULE with non-default parameters:',
                '    param1 = non-default value (bound)',
                '    param2 = 321 (given)'
            ]) + '\n'


def test_unused_bind_warning():
    @configurable
    def func(param='default value', param2='default value 2'):
        pass

    # Binding but not calling should raise a warning
    with pytest.warns(UserWarning):
        with func.bind(param='override value'):
            pass

    with pytest.warns(UserWarning):
        with func.bind(param='override value'):
            with pytest.warns(UserWarning):
                with func.bind(param2='override value 2'):
                    pass

    # Warning shouldn't occur for nested binds
    with func.bind(param='override value'):
        with func.bind(param2='override value 2'):
            func()


def test_substitute():
    @configurable
    def func(a=123):
        return a

    def replacement():
        return 456

    def alternative():
        return 789

    with func.substitute(replacement):
        assert func() == 456

    assert func() == 123

    # Like with bind, the substitution higher in the stack always wins, and we
    # warn if this precedence has to be enforced
    with func.substitute(replacement):
        with pytest.warns(UserWarning):
            with func.substitute(replacement):
                assert func() == 456

    assert func() == 123

    # Warn if the original function has already had `bind` called
    with func.bind(a=0):
        # Prevent 'uncalled bound function' warning
        func()
        with pytest.warns(UserWarning):
            with func.substitute(replacement):
                assert func() == 456

    # Warn if the original function has `bind` called after substitution
    with func.substitute(replacement):
        with pytest.warns(UserWarning):
            with func.bind(a=0):
                assert func() == 456

    # Raise if the substitution is a configurable
    with pytest.raises(ValueError):
        with func.substitute(func):
            pass
