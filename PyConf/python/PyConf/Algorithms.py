###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import sys


class AlgorithmImportModule(object):
    """Magic module that wraps retrieved objects in PyConf.components.Algorithm.

    Requested objects are imported from <_packageName>. Every algorithm that
    you can import from <_packageName>, you can also import from this module.

        >>> from PyConf.Algorithms import Gaudi__Examples__IntDataProducer

    Importing objects that are not algorithms, like tools, will fail.

    See PyConf.Tools for importing wrapped algorithms.
    """
    _packageName = "Configurables"
    _original__file__ = __file__
    _original__name__ = __name__
    _imported_algorithms = dict()

    def __getattr__(self, name):
        retval = None
        if name == "__all__":
            retval = getattr(__import__(self._packageName), '__all__')
        elif name == "__path__":
            raise AttributeError("'module' object has no attribute '__path__'")
        elif name == "__file__":
            retval = self._original__file__
        elif name == "__name__":
            retval = self._original__name__
        else:
            try:
                retval = self._imported_algorithms[name]
            except KeyError:
                retval = self._wrap_algorithm(
                    getattr(
                        __import__(self._packageName, fromlist=[name]), name))
                self._imported_algorithms[name] = retval
        return retval

    @staticmethod
    def _wrap_algorithm(alg_type, **defaults):
        """Return the Configurable class `alg_type` wrapped as a `PyConf.components.Algorithm`.

        See the `PyConf.components.Algorithm.__new__` method for details on the
        keyword arguments.
        """
        from PyConf import configurable
        from PyConf.components import Algorithm, _is_configurable_algorithm

        if not _is_configurable_algorithm(alg_type):
            raise TypeError(
                'cannot declare an Algorithm with {}, which is of type {}'.
                format(alg_type, alg_type.getGaudiType()))

        @configurable
        def wrapped(**kwargs):
            props = dict(defaults, **kwargs)
            return Algorithm(alg_type, **props)

        wrapped.getDefaultProperties = alg_type.getDefaultProperties

        return wrapped


sys.modules[__name__] = AlgorithmImportModule()
