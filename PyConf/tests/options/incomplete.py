###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options that do not specify all properties, raising an exception."""
from PyConf.application import ApplicationOptions, configure
from PyConf.control_flow import CompositeNode

cf = CompositeNode("top", children=[], forceOrder=True)

options = ApplicationOptions(_enabled=False)

config = configure(options, cf)
