###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Define HLT2 line for ``Lambda_b0 -> Lambda_c+ pi+``.

With ``Lambda_c+ -> p+ K- pi+``.

An example input file and Moore configuration is also given at the bottom of
this file, so that it can be run as-is.
"""
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
# For code inside files under Hlt2Conf/python/lines you should reference these
# two modules using relative imports:
#
#     from ..standard_particles import make_has_rich_long_kaons
from Hlt2Conf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
)
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs

all_lines = {}


def filter_protons(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9,
                   dllp_min=5):
    code = require_all('PT > {pt_min}', 'MIPCHI2DV(PRIMARY) > {mipchi2_min}',
                       'PIDp > {dllp_min}').format(
                           pt_min=pt_min,
                           mipchi2_min=mipchi2_min,
                           dllp_min=dllp_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def filter_kaons(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, dllk_min=5):
    code = require_all('PT > {pt_min}', 'MIPCHI2DV(PRIMARY) > {mipchi2_min}',
                       'PIDK > {dllk_min}').format(
                           pt_min=pt_min,
                           mipchi2_min=mipchi2_min,
                           dllk_min=dllk_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def filter_pions(particles, pvs, pt_min=0.5 * GeV, mipchi2_min=9, dllk_max=5):
    code = require_all('PT > {pt_min}', 'MIPCHI2DV(PRIMARY) > {mipchi2_min}',
                       'PIDK < {dllk_max}').format(
                           pt_min=pt_min,
                           mipchi2_min=mipchi2_min,
                           dllk_max=dllk_max)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_lambdacs(protons,
                  kaons,
                  pions,
                  pvs,
                  am_min=2080 * MeV,
                  am_max=2480 * MeV,
                  apt_min=2000 * MeV,
                  amindoca_max=0.1 * mm,
                  vchi2pdof_max=10,
                  bpvvdchi2_min=25):
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                              "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        particles=[protons, kaons, pions],
        pvs=pvs,
        DecayDescriptors=["[Lambda_c+ -> p+ K- pi+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_lambdabs(lcs,
                  pions,
                  pvs,
                  am_min=5000 * MeV,
                  am_max=7000 * MeV,
                  apt_min=4000 * MeV,
                  amindoca_max=0.1 * mm,
                  vchi2pdof_max=10,
                  bpvvdchi2_min=25):
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}",
                              "BPVVDCHI2() > {bpvvdchi2_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min)

    return ParticleCombinerWithPVs(
        particles=[lcs, pions],
        pvs=pvs,
        DecayDescriptors=["[Lambda_b0 -> Lambda_c+ pi-]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@register_line_builder(all_lines)
def lbtolcpi_lctopkpi_line(name="Hlt2LbToLcpPim_LcToPpKmPipLine", prescale=1):
    pvs = make_pvs()
    protons = filter_protons(make_has_rich_long_protons(), pvs)
    kaons = filter_kaons(make_has_rich_long_kaons(), pvs)
    pions = filter_pions(make_has_rich_long_pions(), pvs)
    lcs = make_lambdacs(protons, kaons, pions, pvs)
    lbs = make_lambdabs(lcs, pions, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [lbs],
        prescale=prescale,
    )


# Moore configuration
from Moore import options, run_moore

# In a normal options file, we would import the line from Hlt2Conf where it is
# defined
# from Hlt2Conf.lines.LbToLcPi import lbtolcpi_lctopkpi_line

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom
public_tools = [stateProvider_with_simplified_geom()]


def all_lines():
    return [lbtolcpi_lctopkpi_line()]


options.set_conds_from_testfiledb('Upgrade_MinBias_LDST')
options.set_input_from_testfiledb('Upgrade_MinBias_LDST')
options.input_raw_format = 4.3
options.evt_max = 100
options.control_flow_file = 'control_flow.gv'
options.data_flow_file = 'data_flow.gv'

run_moore(options, all_lines, public_tools)
