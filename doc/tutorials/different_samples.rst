Running on different input samples
==================================

The LHCb software has different data formats for different stages in the simulation or the data collection and reconstruction.


Running on (L)DST
-----------------

The options which need to be changed are ``input_type`` and ``input_raw_format``.
The ``input_type`` should be set to ``ROOT`` and the ``input_raw_format`` depends on the settings of the previous reconstruction step.
The different options are documented at `RawEventFormat <https://gitlab.cern.ch/lhcb-datapkg/RawEventFormat/-/blob/master/python/RawEventFormat/__init__.py>`_.
If the file has been processed by Brunel, it should usually be set to ``4.3``.
In summary, the following lines have to be added to your script if your input file is a (L)DST::

    from Moore import options
    options.input_type = "ROOT"
    options.input_raw_format = 4.3

An example how to run on LDST is also given by the test `RecoConf.hlt2_reco_test_ldst_input`.
You can execute it with::

    make Moore/test ARGS='-R RecoConf.hlt2_reco_test_ldst_input'


FT decoding version
-------------------

The detector simulation is constantly changing to reflect the latest developments. One part which is still in development is the format in which sub-detectors save their data. The detector output is part of the simulation and, thus, samples from different simulation campaigns can have different formats. One example is the decoding version of the SciFi-Tracker (FT). If you encounter an error connected to ``FTRawBankDecoder``, try change the decoding version.
This is done by adding the following lines to your options file before the ``run_moore`` or ``run_reconstruction`` call::

    from RecoConf.hlt1_tracking import default_ft_decoding_version
    ft_decoding_version=2 #4,6
    default_ft_decoding_version.global_bind(value=ft_decoding_version)

Alternatively, one can include the files ``ft_decoding_v2.py`` or ``ft_decoding_v6`` from `Moore options <https://gitlab.cern.ch/lhcb/Moore/-/tree/master/Hlt/Moore/options>`_.

A general rule of thumb is that for MC that was produced before 2018, ``2`` should be the version used. For MC produced between 2018 and early 2019, version ``4`` is generally correct, and for MC produced since 2019, version ``6`` is the most appropriate. There is more information about FT raw event formats and decoding `here <https://indico.cern.ch/event/785185/contributions/3269815/attachments/1781336/2898050/rta-180119.pdf>`_.



