Studying HLT efficiencies and rates
===================================

.. |jpsiPhi-decay| replace:: :math:`B_{s}^{0} \to J/\psi\phi`
.. |PhiPhi-decay| replace:: :math:`B_{s}^{0} \to \phi\phi`
.. |pt| replace:: :math:`p_{\textrm{T}}`

Whether you are a line developer or you're just interested in the current status of LHCb's
high level trigger, you might like to study the efficiencies and rates of a particular line
or subset of lines. ``HltEfficiencyChecker`` is a package designed to help you do just that.

In earlier sections of this documentation, you've seen how to run ``Moore`` on some MC samples.
``HltEfficiencyChecker`` extends this functionality and writes the trigger decisions to a ROOT ntuple.
The package then also provides configurable analysis scripts that take the tuple and give you information
on the rates and efficiencies of the line(s) you're interested in. In the case of efficiencies, some MC truth
information of the kinematics of each event will have also been saved, allowing a configurable
set of plots, console and text file output to be created that characterise the response of the trigger line(s)
that were run as a function of the decay's kinematics. There is also functionality to calculate the inclusive
and exclusive rates of multiple lines in one run.

In the same way that you would provide an options file to ``gaudirun.py`` when running ``Moore``, you interface
with ``HltEfficiencyChecker`` by providing a configuration file (what lines you're interested in, what MC input
files to use, whether you want to see rates or efficiencies, what plots to make etc.). You can then run the trigger
and tupling sequence from the command line in a similar way that you would :doc:`run Moore <running>`.

In this tutorial we will first see how to set-up the package in addition to the usual LHCb software stack. This
is followed by a set of examples, which increase in complexity. All of these examples are closely based on scripts
that live in the directory ``MooreAnalysis/HltEfficiencyChecker/options/``.

.. note::

    Since late July 2020, ``HltEfficiencyChecker`` uses ``Allen`` as the default HLT1 option, to reflect the trigger
    we will use in Run 3. This is achieved by compiling ``Allen`` for CPU and running it within ``Moore`` as a Gaudi
    algorithm. Don't worry if this sounds a bit technical - running ``Allen`` like this requires no extra work from the user and gives rates
    and efficiencies that are representative of the ``Allen`` trigger that will run online on GPUs (just not as fast)!
    Running ``Moore``'s HLT1 is still supported as well, although users should be aware that this is not the trigger that
    will run in Run 3.

Setup instructions
------------------

``HltEfficiencyChecker`` lives within the `MooreAnalysis <https://gitlab.cern.ch/lhcb/MooreAnalysis>`_ software package,
because it is dependent on ``Moore`` and requires *tuple tools* to save trigger and kinematic information, and these tools
live in the ``Analysis`` project.

First of all, you'll need a working build of the LHCb software stack that you can develop. If you don't have
this yet, follow the instructions on the :doc:`developing` page. Once you have the stack, you need to get ``MooreAnalysis`` as well.
From your top-level ``stack/`` directory do::

    make MooreAnalysis

which will checkout the ``master`` branch of ``MooreAnalysis`` and build it for you.

.. note::

    In the following, when particular file paths are given, they will always be relative to the top-level directory of
    the software stack. For example, let's say you built your stack in ``/home/user/stack/`` and the path
    ``MooreAnalysis/HltEfficiencyChecker/options/hlt1_eff_example.py`` is referred to: the full path would be
    ``/home/user/stack/MooreAnalysis/HltEfficiencyChecker/options/hlt1_eff_example.py``.

.. _necessary_info:

How to interact with HltEfficiencyChecker
-----------------------------------------

As mentioned in the introduction, ``HltEfficiencyChecker`` requires that you provide it with a configuration file, i.e. a set of
information for the tool to run in an expected format. The necessary information needed for the trigger to run is

    * The input simulation files to run over, either as a ``TestFileDB`` key or as a list of LFNs,
    * Details of the simulation e.g. CondDB tags, data format etc. (not needed if TestFileDB key specified),
    * The level of the trigger (HLT1 or HLT2),
    * Which lines to run.

and the necessary information about the results you want to see is

    * If you want rates or efficiencies,
    * If efficiencies, you then need to provide an "annotated decay descriptor" and the names of the
      final-state (stable) particles in the decay (more below).

There are also a variety of optional features to customize the results you get. All this will be explained in more detail when
we get to the examples.

The configuration can be provided in two ways. The first - which we call using the tool's "wizard" - involves writing a short text file
in ``yaml`` format. This is designed to be as readable and self-explanatory as possible and is recommended if you are not yet very
experienced with running the trigger and writing Gaudi options files (the wizard writes the options file for you behind-the-scenes). If you
are an experienced line developer who needs greater flexibility, or you don't like any kind of behind-the-scenes "magic", you can write
your options files and call the analysis scripts yourself. Both workflows will be covered in this tutorial.

Let's go ahead and get started, and write our first configuration file.

Example: my first config file using the ``HltEfficiencyChecker`` wizard
-----------------------------------------------------------------------

``yaml`` text files are similar to ``json`` and python dictionaries: you specify key-value pairs and values are accessed by asking for
their key (usually a string). The ``HltEfficiencyChecker`` "wizard" script
(``MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py``) parses the ``yaml``, generates a Gaudi options file from it, then runs
it and runs analysis scripts on the output. Thus, the ``yaml`` *keys* you provide must be known and must follow a basic structure. Let's
say we are interested in measuring some HLT1 efficiencies on |PhiPhi-decay|. Here is a suitable ``yaml`` config file:

.. code-block:: yaml

    annotated_decay_descriptor:
        "[${B_s0}B_s0 => ( phi(1020) => ${Kplus0}K+ ${Kminus0}K-) ( phi(1020) => ${Kplus1}K+ ${Kminus1}K-) ]CC"
    ntuple_path: &NTUPLE eff_ntuple.root
    job:
        trigger_level: 1
        evt_max: 100
        testfiledb_key: Upgrade_BsPhiPhi_MD_FTv4_DIGI
        options:
            # - $HLTEFFICIENCYCHECKERROOT/options/hlt1_moore_lines_example.py  # Allen lines are specified internally to Allen
            - $HLTEFFICIENCYCHECKERROOT/options/options_template.py.jinja  # first rendered with jinja2
    analysis:
        script: $HLTEFFICIENCYCHECKERROOT/scripts/hlt_line_efficiencies.py
        args:
            input: *NTUPLE
            level: Hlt1
            reconstructible_children: Kplus0,Kminus0,Kplus1,Kminus1
            legend_header: "B^{0}_{s} #rightarrow #phi#phi"
            make_plots: true
            vars: "PT,Kplus0:PT"
            lines: Hlt1TwoTrackMVADecision,Hlt1TrackMVADecision

The first options are needed globally. They are the ``ntuple_path``, which will be the name of the tuple that is written out, and the
``annotated_decay_descriptor``, which is an MC decay descriptor with an important difference.
``MCDecayTreeTuple`` - which writes the MC truth information to the tuple - needs a decay descriptor to find the signal decays in the MC.
Given that, we'll get branches in the tuple of various kinematic properties of the different particles in the decay. In the same way that
in ``DaVinci`` you need to put a `^` to the left of the particles you want to make branches for, here you must specify the branch prefix
as ``${prefix}`` to the left of the particles of interest. For example, the tuple written from this config file will have branches for the
:math:`B_{s}^{0}` and the four charged kaons, and these branches will be prefixed
with ``B_s0, Kplus0, Kminus0, Kplus1`` and ``Kminus1`` respectively, e.g. ``B_s0_TRUEPT`` or ``Kminus0_TRUEETA``.

.. note::

    The branch prefixes does the same job as `^`, but also must be specified so that the analysis scripts can work out
    exactly what branches to expect in the tuple. This knowledge is necessary to construct different efficiency
    *denominator* requirements, which are explained :ref:`below <denominators>`.


The ``job`` options are just needed to run the trigger. ``trigger_level`` (1 or 2) selects HLT1 or HLT2. As mentioned at the start, the
default HLT1 is ``Allen``, but you can use the ``Moore`` HLT1 if you really want to by adding the line ``use_moore_as_hlt1: True`` here.
``evt_max`` is the number of events to run over (not the number of events that fired the trigger -
bear this in mind if you expect a low efficiency). The next thing to specify is the input MC files, and here we have done the
simplest thing and specified the key of an entry in the `TestFileDB`_. A TestFileDB entry holds all the information needed to run over
a set of input files, including the tags that it was created with and the LFNs. Fortunately, we have here a recent
entry for our decay of interest.

.. note::

    If the TestFileDB doesn't have a sample that is sufficient for your testing needs - which we assume will be the case
    for most users of this tool - there are instructions in the following examples to use any LFNs you like in the wizard with a
    few more lines of code.

Finally in the ``job`` options is the ``options`` key. These are paths to scripts in ``MooreAnalysis`` and the helpful environment
variable ``$HLTEFFICIENCYCHECKERROOT`` makes sure they are correct on any system. The only one we need here is ``.../options_template.py.jinja``,
which is the script that translates the ``yaml`` into options that can be passed to Gaudi. You don't normally need to edit this file.

If you remember the :ref:`list of necessary information <necessary_info>` that ``HltEfficiencyChecker`` needs, you may have noticed that we haven't
yet *configured* the trigger i.e. told it what HLT1 lines to run and what thresholds to run with. This is done internally to ``Allen`` and a
bit of guidance on that is given in the `selections README`_.  If we were running the ``Moore`` HLT1, we would need to provide a short
options file to do this. An example of this (``.../hlt1_moore_lines_example.py``) is commented out to show you where you would add it.
``.../hlt1_moore_lines_example.py`` should look like this:

.. code-block:: python

    from Moore import options
    from Hlt1Conf.settings import all_lines
    from Hlt1Conf.lines.high_pt_muon import one_track_muon_highpt_line
    from Hlt1Conf.lines.gec_passthrough import gec_passthrough_line

    def make_lines():
        return all_lines() + [one_track_muon_highpt_line(), gec_passthrough_line()]

    options.lines_maker = make_lines

That is all for the trigger's needs. Next up are the ``analysis`` options, which configure what results you'd like to see and the trigger doesn't need to know.
``script`` specifies the analysis script to run. We want efficiencies, so we ask for the ``hlt_line_efficiencies.py`` script
(we'll see a rates example later). The ``args`` are the set of python ``ArgumentParser`` args that are parsed to this script.
It needs to know the path to the tuple that will be made, which is handled by ``input: *NTUPLE``, and the trigger
level. Optionally, we've provided a pretty LaTeX header with ``legend_header``.
We hinted above that, in order to construct efficiency denominators, the analysis script needs to know what branches to expect.
The ``reconstructible_children`` key specifies this as a comma-separated list of **stable, reconstructible, final-state particles of the decay**.
This list *must* be a subset of the branch prefixes in the annotated decay descriptor. The ``vars`` keyword accepts a
comma-separated list of kinematic variables that you'd like to plot efficiencies against. By default, it is assumed that you're interested in
the parent kinematics (by default, the parent is interpreted as the leftmost branch prefix in the decay descriptor), but you can specify a
different particle in the decay before a colon, as shown for the :math:`K^{+}`. Finally, we've picked two physics lines to see the results
from with the ``lines`` keyword, which again takes a comma-separated list. These two lines should fire on our decay of interest.
There are a variety of other args that the analysis script can take; take a look at the script ``hlt_line_efficiencies.py`` if you'd like
to see what they are. We'll use some others in the next examples.

.. note::

    There is nothing stopping you running ``hlt_line_efficiencies.py``, with these args, on its own, and we will see how to later on.
    However, a bit of re-formatting is needed to transform the ``yaml`` key-value pairs into command-line arguments,
    as you can see from ``hlt_line_efficiencies.py --help``. In short, those keys that have
    the value ``true`` correspond to positional arguments e.g. for ``make_plots: true`` you'd simply pass ``--make-plots`` to the script.
    The rest are keyword arguments e.g. ``level: Hlt1`` becomes ``--level Hlt1``. Finally, ``input: *NTUPLE`` is a special case, and it
    corresponds to just the value of ``NTUPLE`` as a string e.g. ``eff_ntuple.root``. This transformation is all done in
    ``MooreAnalysis/HltEfficiencyChecker/python/HltEfficiencyChecker/wizard.py``. In the rest of this tutorial, we will interchange between
    passing args via the ``yaml`` and passing them directly to the analysis scripts.

Running the tool on a ``yaml`` config file
------------------------------------------

Assuming the config file we just wrote lived at the path ``MooreAnalysis/HltEfficiencyChecker/options/hlt1_eff_example.yaml``,
then we just need to do (from the ``stack/`` directory)::

    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py MooreAnalysis/HltEfficiencyChecker/options/hlt1_eff_example.yaml

In general, the command follows the pattern::

    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py <path/to/config/file> <OPTIONS>

where ``hlt_eff_checker.py`` is the "wizard" script that runs the trigger and then the analysis script according to the configuration in
``<path/to/config/file>``. The ``<OPTIONS>`` you can pass to ``hlt_eff_checker.py`` are explained
with the ``--help`` argument::

    (LHCb Env) [user@users-computer stack]$ MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py --help
    usage: hlt_eff_checker.py [-h] [-o OUTPUT] [-s OUTPUT_SUFFIX] [--force] [-n]
                              config

    Script that extracts and plots efficiencies and rates from the high level
    trigger.

    positional arguments:
      config                YAML configuration file

    optional arguments:
      -h, --help            show this help message and exit
      -o OUTPUT, --output OUTPUT
                            Output directory prefix. See also --output-suffix.
      -s OUTPUT_SUFFIX, --output-suffix OUTPUT_SUFFIX
                            Output directory suffix
      --force               Do not fail when output directory exists
      -n, --dry-run         Only print the commands needed to run from stack/
                            directory.

By default, like the ``Moore`` tests (see :doc:`developing`), the results will go into a new directory with the current time in
the title e.g. ``checker-20200604-093835``. Let's say you'd prefer the results to live in your directory ``checker-hlt1-tests``,
which already exists and has results in it already from the last time you ran ``HltEfficiencyChecker``, which you'd like to
overwrite. Then you'd do::

    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py <path/to/config/file> -o checker-hlt1 -s tests --force

Finally, there is also the ``--dry-run`` feature, which interprets your ``yaml`` and writes the options that will configure the job, but
then stops just before running, and instead prints the set of commands that it was about to execute::

    (LHCb Env) [user@users-computer stack]$ MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py MooreAnalysis/HltEfficiencyChecker/options/hlt1_eff_example.yaml -o checker-hlt1 -s tests --force --dry-run
    The commands to run are...
    cd 'checker-hlt1tests'
    '/home/user/stack/MooreAnalysis/run' 'gaudirun.py' '.hlt_eff_checker_options_58B6C937.py'
    '/home/user/stack/MooreAnalysis/run' '/home/user/stack/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py' 'eff_ntuple.root' '--level=Hlt1' '--vars=PT,Kplus0:PT' '--reconstructible-children=Kplus0,Kminus0,Kplus1,Kminus1' '--make-plots' '--legend-header=B^{0}_{s} #rightarrow #phi#phi'

You are then free to execute these commands at your own leisure! This can be particularly useful if you just want to tweak your analysis
options, e.g. you'd like to see plots against a different variable or a different denominator and remake the plots. This obviously doesn't
require running the trigger and making the tuple again (which can take a long time!). So you can just copy and paste the first and last of
these commands to re-run only the analysis step, which is typically fast.

Results
^^^^^^^

When all of the steps of the tool have ran, we should be looking at a console output that ends like this:

.. code-block:: text

    ------------------------------------------------------------------------------------
    INFO:	 Starting /home/user/stack/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py...
    ------------------------------------------------------------------------------------
    Hlt1 integrated efficiencies for the lines with denominator: CanRecoChildren
    ------------------------------------------------------------------------------------
    Line:	 Hlt1TwoTrackMVADecision	 Efficiency:	 0.500 +/- 0.098
    Line:	 Hlt1TrackMVADecision   	 Efficiency:	 0.385 +/- 0.095
    ------------------------------------------------------------------------------------
    Finished printing Hlt1 integrated efficiencies for denominator: CanRecoChildren
    ------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------
    INFO:	 Making plots...
    ------------------------------------------------------------------------------------
    Info in <TCanvas::MakeDefCanvas>:  created default TCanvas with name c1
    INFO:	 --xtitle not specified. Using branch name and attempting to figure out the units...
    INFO:	 Found unit "MeV" for variable "PT". Placing it on the x-axis title...
    Info in <TCanvas::Print>: pdf file Efficiency__AllLines__AllDenoms__logPT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__AllLines__AllDenoms__PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TwoTrackMVADecision__AllDenoms__logPT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TwoTrackMVADecision__AllDenoms__PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TrackMVADecision__AllDenoms__logPT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TrackMVADecision__AllDenoms__PT.pdf has been created
    INFO:	 --xtitle not specified. Using branch name and attempting to figure out the units...
    INFO:	 Found unit "MeV" for variable "Kplus0:PT". Placing it on the x-axis title...
    Info in <TCanvas::Print>: pdf file Efficiency__AllLines__AllDenoms__logKplus0_PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__AllLines__AllDenoms__Kplus0_PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TwoTrackMVADecision__AllDenoms__logKplus0_PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TwoTrackMVADecision__AllDenoms__Kplus0_PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TrackMVADecision__AllDenoms__logKplus0_PT.pdf has been created
    Info in <TCanvas::Print>: pdf file Efficiency__Hlt1TrackMVADecision__AllDenoms__Kplus0_PT.pdf has been created
    ------------------------------------------------------------------------------------
    INFO	 Finished making plots. Goodbye.
    ------------------------------------------------------------------------------------

We can take a quick look at some of the plots too! For example, ``Efficiency__Hlt1TwoTrackMVADecision__AllDenoms__PT.pdf`` should look
something like this (note that the plots have been placed in a temporary directory e.g. ``checker-20200604-093835``):

.. image:: hltefficiencychecker_plots/hlteffchecker_example_lowstats.png
    :width: 600
    :align: center
    :alt: Plot of the efficiency of the Hlt1TwoTrackMVALine for all denominators (just *CanRecoChildren* was asked for) plotted against
          the parent's |pt|.

Although we could clearly do with more statistics - of the 100 events we ran over, just 25 passed the denominator requirement -
we see efficiencies as a function of the parent's |pt| for the *CanRecoChildren* denominator (abbreviated to C.R.C). If we instead set
``evt_max: 5000`` and use the ``xtitle`` key to make a pretty LaTeX label for the x-axis, the run will take quite a bit longer, but things
look a lot better:

.. image:: hltefficiencychecker_plots/hlteffchecker_example_highstats.png
    :width: 600
    :align: center
    :alt: Plot of the efficiency of the Hlt1TwoTrackMVALine for all denominators (just *CanRecoChildren* was asked for) plotted against
          the parent's |pt|, this time with 50x more statistics.

.. _hlt2_rate:

Example: using the wizard to calculate HLT2 rates
-------------------------------------------------

The efficiencies of HLT1 serve as a simple introduction to the tool, but we expect that the main use case of this tool will be
physics analysts writing HLT2 lines. This next example therefore shows how to use the tool to get the rate of a specific HLT2 line.
We'll this time follow the example decay of |jpsiPhi-decay|, which has a dedicated HLT2 line, ``Hlt2BsToJpsiPhiLine``. Here is the
``yaml`` config file that we'll need:

.. code-block:: yaml

    # HltEfficiencyChecker "wizard" example for Hlt2 rates
    ntuple_path: &NTUPLE rate_ntuple.root
    job:
        trigger_level: 2
        evt_max: 100
        testfiledb_key: upgrade_minbias_hlt1_filtered
        input_raw_format: 4.3
        lines_from: Hlt2Conf.lines.Bs2JpsiPhi
        run_reconstruction: True
        ft_decoding_version: 2
        options:
            - $HLTEFFICIENCYCHECKERROOT/options/options_template.py.jinja  # first rendered with jinja2
    analysis:
        script: $HLTEFFICIENCYCHECKERROOT/scripts/hlt_calculate_rates.py
        args:
            input: *NTUPLE
            level: Hlt2
            json: Hlt2_rates.json

The first thing you might notice is the absence of the ``annotated_decay_descriptor``. We are interested in estimating the rate that a
given line will fire on real data coming out of HLT1, which we simulate with HLT1-filtered minimum bias simulation. In minimum bias, all known,
kinematically-accessible physics processes are possible, all of which can fire our line. The rate comes from counting all triggered events,
all of which could potentially be a different process, which would all need a different decay descriptor! Given this, we can see that
putting the MC truth information into a single tuple would be very difficult, and not very useful. Therefore, we don't try to save it and
thus don't need a decay descriptor.

This time, we need to specify a non-default ``input_raw_format``, and instead of listing a script that configures the lines, we use the
``lines_from`` key, which is only supported for HLT2 and does the same job. ``run_reconstruction`` asks that the current HLT2 reconstruction
be run (currently not the default behaviour, more on this below), which also required that the ``ft_decoding_version`` be set.
Finally, ``script`` now gives the path to the script that calculates rates, and ``json`` is the name of a ``.json`` to which the rate
will be saved.

.. note::

    ``input_raw_format`` needed to be set to ``4.3`` because the ``upgrade_minbias_hlt1_filtered`` files are in ``LDST`` format, which
    means they store reconstruction-level information that we can make selections on. This stored reconstruction was provided by
    running the samples through the offline reconstruction application ``Brunel`` when they were produced. There is further guidance on
    raw formats :doc:`here <different_samples>`.

    In data-taking, we will make selections based on the real-time, full HLT2 reconstruction, which is
    `under development <https://gitlab.cern.ch/lhcb/Moore/-/milestones/25>`_ but available now. Thus there are two ways to make selections
    on reco-level information. The default is to use that which is stored in the ``LDST`` file, or you can run the state-of-the-art HLT2
    reconstruction and make your selections from it with the ``run_reconstruction`` key.

    Here we also had to specify the ``default_ft_decoding_version``. If we use the wrong decoding version, ``Moore`` may break or we may get
    slightly different results. A general rule of thumb is that for MC that is 3+ years old, ``2`` should be the version used. For 1–3 years old,
    version ``4`` is generally correct, and for MC produced in the last year or so, version ``6`` is the most appropriate. There is more
    information about FT raw event formats and decoding `here <https://indico.cern.ch/event/785185/contributions/3269815/attachments/1781336/2898050/rta-180119.pdf>`_.

If the above code is put into a script with the path ``MooreAnalysis/HltEfficiencyChecker/options/hlt2_rate_example.yaml``, we can run as
before::

    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_eff_checker.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_rate_example.yaml

we'll see the printout::

     ----------------------------------------------------------------------------------------------------
     INFO:	 Starting /home/user/stack/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py...
     ----------------------------------------------------------------------------------------------------
     INFO:	 No lines specified. Defaulting to all...
     Hlt2 rates:
     ----------------------------------------------------------------------------------------------------
     Line:	 Hlt2BsToJpsiPhiLineDecision	 Incl: 20.0 +/- 14.0 kHz, Excl: 20.0 +/- 14.0 kHz
     ----------------------------------------------------------------------------------------------------
     Hlt2 Total:                             Rate:	 20 +/- 14.0 kHz
     ----------------------------------------------------------------------------------------------------
     Finished printing Hlt2 rates!

Note that both the *inclusive* rate (rate of that line firing agnostic to other lines) is returned as well as the *exclusive* rate (rate of
firing on events that **only** this line fired for). There is also the total rate (rate that one or more lines fired on an event). Because
we only use one line, these three quantities are the same, but will generally differ if a set of lines is asked for.
If we go into the temporary directory that has been auto-created (in this case it is called ``checker-20200617-123354``) we'll see::

    (LHCb Env) [user@users-computer checker-20200617-123354]$ ls
    control_flow.gv  data_flow.gv  Hlt2_rates.json  rate_ntuple.root

.. _denominators:

How we define an efficiency and a rate
--------------------------------------

In practice, the efficiency of a trigger line is defined as

.. math::

   \varepsilon = \frac{N_{\textrm{Triggered}}}{N_{\textrm{you might expect to trigger on}}} \, ,

where the denominator of this equation is subjective. One obvious choice is the total number of simulated events that you ran the
trigger over. However, it is not guaranteed in these events that all of the final state particles made tracks inside the
detector's fiducial acceptance, and you may not wish to count such events towards your efficiency. Furthermore, you may wish to see
the effect of "offline" selection cuts on your trigger efficiency.

Both of these issues motivate the ability to make a set of selection cuts on MC truth information; to ask questions like
"What is the efficiency of my shiny new line, *given* all the children fall inside the detector?". Such a set of cuts is called a
*denominator* requirement, because the denominator of the above equation is the total number of events that pass it.
``HltEfficiencyChecker`` has a pre-defined set of four such *denominators*:

    * **AllEvents**: total number of events that the trigger ran over.
    * **CanRecoChildren**: all final state children left charged, long tracks within :math:`2 < \eta < 5`.
    * **CanRecoChildrenParentCut**: in addition to **CanRecoChildren**, the decay parent is also required to have a true decay time greater than
      0.2 ps, and a minimum |pt| of 2 GeV.
    * **CanRecoChildrenAndChildPt**: in addition to **CanRecoChildren**, all final state children have :math:`p_{\textrm{T}} > 250` MeV.

By default, efficiencies will be computed and plotted for the CanRecoChildren denominator. This can be changed using the `denoms` key.
If none of these denominators suit your needs, you can add a new one - see :ref:`new-denoms`.

On the other hand, the rate of a trigger line is a much less subjective concept. It is simply the number of times per second that the
line fires when it is running on real data. This is an important quantity to know because both levels of the HLT are constrained by their
total output rate. We might like to loosen the cuts in our lines to be more efficient at selecting signal, but this will in turn increase
the rate of that line. For HLT1, we estimate a line's rate on real data by first calculating the efficiency of that line when run over
minimum bias, and then multiplying by the LHCb :math:`pp` collision rate, which will be 30 MHz in Run 3. For HLT2, which will only run on
the output of HLT1, the relevant rate to multiply by is the HLT1 output rate. This is
`assumed to be 1 MHz <https://indico.cern.ch/event/921771/contributions/3882467/attachments/2053448/3442508/ProjectOrg_ForLHCbWeek_09062020.pdf>`_
by default, but can be adjusted with the ``hlt1_output_rate`` analysis key.

.. note::

    All the efficiencies that the tool currently computes are **decision** efficiencies. They reflect whether the line fired on the *event*
    or not. In analysis, we are often interested in what part of the event fired the trigger; was it the signal candidate for instance (TOS)?
    The ability to compute TOS efficiencies is envisaged and will be added to the tool in the future.

Beyond the wizard: writing and running your own options file
------------------------------------------------------------

If you have some experience with writing options files for running ``Moore``, and you already have some options file that you've written
e.g. to test your new line, then you'll be happy to learn that with a few small changes, you can use the same options file with
``HltEfficiencyChecker``. Continuing with |jpsiPhi-decay|, you'll first need to write a short script to configure your line as we saw above

.. code-block:: python

    from Moore import options
    from Hlt2Conf.lines.Bs2JpsiPhi import all_lines

    def make_lines():
        return [builder() for builder in all_lines.values()]

    options.lines_maker = make_lines

and an options file defining the input:

.. code-block:: python

    from Moore import options
    from HltEfficiencyChecker.config import run_moore_with_tuples
    from RecoConf.reconstruction_objects import reconstruction
    from RecoConf.protoparticles import make_charged_protoparticles

    decay = (
        "${Bs}[B_s0 => ( J/psi(1S) => ${mup}mu+ ${mum}mu- ) ( phi(1020) => ${Kp}K+ ${Km}K- )]CC"
    )
    options.input_files = [
        # HLT1-filtered
        # Bs2JpsiPhi, 13144011
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/13144011/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000005_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000064_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000068_1.ldst",
    ]
    options.input_type = 'ROOT'
    options.input_raw_format = 4.3
    options.evt_max = 100

    options.simulation = True
    options.data_type = 'Upgrade'
    options.dddb_tag = 'dddb-20171126'
    options.conddb_tag = 'sim-20171127-vc-md100'
    options.ntuple_file = "eff_ntuple.root"

    # needed to run over FTv2 data
    from RecoConf.hlt1_tracking import default_ft_decoding_version
    default_ft_decoding_version.global_bind(value=2)

    from RecoConf.global_tools import stateProvider_with_simplified_geom
    with reconstruction.bind(from_file=False), make_charged_protoparticles.bind(
            enable_muon_id=True):
        run_moore_with_tuples(
            options, decay, public_tools=[stateProvider_with_simplified_geom()])

These two scripts could be put together, but we keep them separate for modularity.

If you've written an options file before, and you've followed the "wizard" examples, then hopefully you see that we're providing all the
same information, it is just specified in slightly different syntax. The
most important difference is that, instead of calling ``run_moore()`` at the end of the script, we instead call ``run_moore_with_tuples()``,
which simply adds the tuple-creating tools into the control flow directly after ``Moore`` has run. The latter requires
the annotated decay descriptor, as was motivated earlier. The ``run_reconstruction`` flag we used earlier corresponds to wrapping the
``run_moore_with_tuples`` call in ``with reconstruction.bind(from_file=False), make_charged_protoparticles.bind(enable_muon_id=True)``.

Assuming that the former of these scripts lives at ``MooreAnalysis/HltEfficiencyChecker/options/hlt2_lines_example.py`` and the second at
``MooreAnalysis/HltEfficiencyChecker/options/hlt2_eff_example.py``, we can then pass both of these options files to ``gaudirun.py``,
in the same way you would to run ``Moore`` alone::

    MooreAnalysis/run gaudirun.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_lines_example.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_eff_example.py

which will produce the tuple ``eff_ntuple.root`` as before. Note that we haven't used the "wizard" script ``hlt_eff_checker.py``, so this
tuple will not appear in an auto-generated temporary directory as before -  it will appear in your current directory. You can then run the
analysis script on the tuple when it is produced::

    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py eff_ntuple.root --level Hlt2 --reconstructible-children=Kp,Km,mup,mum --legend-header="B_{s} #rightarrow J/#psi #phi" --make-plots

Where we see for the first time the difference between the  ``yaml`` keys and the command line arguments. This example should yield a plot
that looks like this:

.. image:: hltefficiencychecker_plots/hlteffchecker_hlt2_example.png
    :width: 600
    :align: center
    :alt: Plot of the efficiency of the Hlt2BsToJpsiPhiLine for all denominators (just *CanRecoChildren* was asked for) plotted against
          the parent's |pt|.

The rate of this line can be calculated by first configuring a minimum bias run

.. code-block:: python

    from Moore import options
    from HltEfficiencyChecker.config import run_moore_with_tuples
    from RecoConf.reconstruction_objects import reconstruction
    from RecoConf.protoparticles import make_charged_protoparticles

    options.set_input_from_testfiledb('upgrade_minbias_hlt1_filtered')
    options.input_raw_format = 4.3
    options.evt_max = 100
    options.set_conds_from_testfiledb('upgrade_minbias_hlt1_filtered')

    options.ntuple_file = "rate_ntuple.root"

    from RecoConf.hlt1_tracking import default_ft_decoding_version
    default_ft_decoding_version.global_bind(value=2)

    from RecoConf.global_tools import stateProvider_with_simplified_geom
    with reconstruction.bind(from_file=False),\
         make_charged_protoparticles.bind(enable_muon_id=True):
        run_moore_with_tuples(
            options, public_tools=[stateProvider_with_simplified_geom()])

and then running these options (``MooreAnalysis/HltEfficiencyChecker/options/hlt2_rate_example.py``) and then the rate-calculating script
``hlt_calculate_rates.py`` on ``rate_ntuple.root``::

    MooreAnalysis/run gaudirun.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_lines_example.py MooreAnalysis/HltEfficiencyChecker/options/hlt2_rate_example.py
    MooreAnalysis/run MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py --level Hlt2 --json Hlt2_rates.json rate_ntuple.root

which gives the output::

    ----------------------------------------------------------------------------------------------------
    INFO:	 Starting /home/user/stack/MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py...
    ----------------------------------------------------------------------------------------------------
    INFO:	 No lines specified. Defaulting to all...
    Hlt2 rates:
    ----------------------------------------------------------------------------------------------------
    Line:	 Hlt2BsToJpsiPhiLineDecision	 Incl: 20.0 +/- 14.0 kHz, Excl: 20.0 +/- 14.0 kHz
    ----------------------------------------------------------------------------------------------------
    Hlt2 Total:                             Rate:	 20 +/- 14.0 kHz
    ----------------------------------------------------------------------------------------------------
    Finished printing Hlt2 rates!

which is identical to what we calculated with the "wizard" earlier, reflecting that the two configurations are just two interfaces that
run the same code under-the-hood.

Customizing your rate/efficiency results
----------------------------------------

Up until this point, we have looked at the basic features that are built-in to ``HltEfficiencyChecker``. In this subsection, we take a
look at some of the features that we hope will be useful to line developers. In addition to the features listed here, take a look at the
two analysis scripts ``MooreAnalysis/HltEfficiencyChecker/scripts/hlt_line_efficiencies.py`` and
``MooreAnalysis/HltEfficiencyChecker/scripts/hlt_calculate_rates.py``, in particular the arguments that they can accept. If you think an
important feature is missing, feel free to get involved and add it in a merge request to ``MooreAnalysis``!

.. _new-denoms:

Different denominators
^^^^^^^^^^^^^^^^^^^^^^

It was mentioned above that the "number of events that you might expect to trigger on" is a very subjective quantity, and thus the
pre-defined denominators may not be enough. You can simply add a new one to the python dictionary in the ``get_denoms`` function in
``MooreAnalysis/HltEfficiencyChecker/python/HltEfficiencyChecker/utils.py``.
For example, adding the line::

    efficiency_denominators["ParentPtCutOnly"] = make_cut_string([parent_name + '_TRUEPT > 2000'])

to ``utils.py`` will define the new denominator. In the "wizard" case, use the the ``denoms`` key in the set of
``analysis`` options to specify its use e.g.:

.. code-block:: yaml

    analysis:
        script: $HLTEFFICIENCYCHECKERROOT/scripts/hlt_line_efficiencies.py
        args:
            input: *NTUPLE
            level: Hlt1
            reconstructible_children: muplus,muminus,Kplus,Kminus
            legend_header: "B^{0}_{s} #rightarrow J/#psi #phi"
            make_plots: true
            denoms: ParentPtCutOnly

or if calling ``hlt_line_efficiencies.py`` directly, use the ``--denoms`` argument.

Testing the inclusive rates of a group of lines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In the HLT2 rate examples, we saw that ``hlt_calculate_rates.py`` gives information on both the inclusive and exclusive rate
of a line, as well as a total rate of the group of all the lines that were specified. You can also define further groups of lines and the
script will give an inclusive rate of those groups, using the ``--rates-groups`` argument. For instance, two groups of lines could be
specified as ``--rates-groups my_group1:line1_name,line2_name my_group2:line3_name,line4_name,line5_name``.

With this feature it is hoped that line authors can get a feel for the overlap of lines; to see where 2 lines are perhaps doing the same job.
In the future we hope to add some correlation matrices to the output of the analysis scripts, so that such this overlap can be easily
visualised between every possible pair of lines.

Tweaking the parameters of your line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

While you are developing your line, we expect that you'll want to see the effect of changing the thresholds. Instructions on how you
configure a line with modified thresholds are provided in the **Modifying thresholds** section of
:doc:`Writing an HLT2 line <hlt2_line>`. In this way, if you configure several lines with slightly different thresholds and slightly
different names, you will be able to extract rates and efficiencies of each of these new "lines" with the ``--lines`` argument of either
analysis script. As of July 2020, configuring lines likes this in your options file isn't possible with ``Allen``, so any threshold tweaking
must be done in ``Allen``, but work is ongoing to provide this extra flexibility.

.. Internal note and talks - add once a talk is given introducing this documentation
.. -----------------------

.. _TestFileDB: https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/blob/master/python/PRConfig/TestFileDB.py
.. _HLT1 technology choice study: https://indico.cern.ch/event/906032/contributions/3811935/attachments/2023722/3384799/rta_tuesday_meeting_comparison.pdf
.. _selections README: https://gitlab.cern.ch/lhcb/Allen/-/blob/master/selections.md