Running with Ganga
==================

Running Moore by hand on your machine is good for small runs over a file or
two, but can quickly become two slow as the number of input files you need to
process increases.

Running Moore on the Grid lets you run multiple instances simultaneously.
Ganga is the interface we use for submitting jobs to the Grid, and Moore is
compatible with Ganga.

.. note::

    This page assumes you are already familiar with Ganga and how to submit
    Ganga jobs using other Gaudi-based LHCb applications, such as DaVinci.
    The `Starterkit lesson on Ganga`_ is a good place to start if you're
    unsure.

Configuration
-------------

Let's begin with a typical configuration of Moore for running HLT2 on some
Monte Carlo DST files::

    options.input_files = ["xrootd://...", "xrootd://..."]
    options.input_type = "ROOT"
    options.input_raw_format = 4.3
    options.data_type = "Upgrade"
    options.dddb_tag = "dddb-20171126"
    options.conddb_tag = "sim-20171127-vc-md100"
    options.simulation = True

    # Somewhere later
    run_moore(options, lines_maker)

We specify the paths to the input data, the file format (ROOT or MDF), the
raw event format, the data type, the tags, and that we're running over MC.

Because Ganga specifies the *locations* of the input data for us, we don't
need to give ``options.input_files``::

    # Don't need this when using Ganga
    # options.input_files = ["xrootd://...", "xrootd://..."]
    options.input_type = "ROOT"
    options.input_raw_format = 4.3
    options.data_type = "Upgrade"
    options.dddb_tag = "dddb-20171126"
    options.conddb_tag = "sim-20171127-vc-md100"
    options.simulation = True

    # Somewhere later
    run_moore(options, lines_maker)

That one line is all we need to change.

Build
-----

Ganga bundles up a local build of Moore, which is then downloaded and used by
each Grid worker node. This build is different from the
``lb-stack-setup``-based build we normally use, as in :doc:`developing`. We
must use an ``lb-dev``-based build instead.

.. note::

    If you want to submit a version of Moore that includes your own changes,
    which are not yet part of the master branch, you must first **push your
    changes to a branch** in Moore.

    You can read more about working with ``lb-dev`` on the `Starterkit lesson`_.

At the moment, we recommend that you create an ``lb-dev`` project using a
version of Moore from the nightlies:

.. code-block:: bash

    $ lb-dev --platform x86_64-centos7-gcc9-opt --nightly lhcb-head/Today Moore/HEAD
    $ cd ./MooreDev_HEAD

It might be that the most recent nightly, ``Today``, didn't build
successfully, for whatever reason. If this is the case then you can try a
different day, such as ``Mon`` or ``Sat``.

If you want to modify packages, you can now checkout those packages, for
example:

.. code-block:: bash

    $ git lb-use Moore
    $ git lb-checkout Moore/master Hlt

If you want to use existing modifications in your own branch, use your own
branch name above rather than ``master``.

Finally, run the build:

.. code-block:: bash

    $ make

Job definition
--------------

You can now define the job as usual. The Ganga job application type should be
``GaudiExec``, as for any other Gaudi-based LHCb application, like DaVinci::

    # Inside a Ganga prompt
    In [1]: app = GaudiExec(
       ...:     directory="/path/to/your/MooreDev_HEAD",
       ...:     options=["/path/to/your/hlt2_example.py"],
       ...:     platform=["x86_64-centos7-gcc9-opt"],
       ...: )

    In [2]: j = Job(name="MooreJobXYZ", application=app)

Because we took a build of Moore from the nightlies, the Grid jobs must have
access to the ``/cvmfs/lhcbdev.cern.ch`` CVMFS repository (this is where
nightly builds are installed). The only Grid site guaranteed to provided this
is CERN, so we must only run over data available at CERN.

Here is an example of fetching a dataset from the bookkeeping, filtering out
files not available at CERN::

    # Inside a Ganga prompt, after setting up our Job object `j`
    In [3]: bkq = BKQuery("/MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/27163002/LDST")

    In [4]: ds = bkq.getDataset(SE="CERN_MC-DST-EOS")

    In [5]: j.inputdata = ds

Then we just need to make sure Dirac always sends the jobs to CERN::

    In [6]: j.backend = Dirac()

    In [7]: j.backend.settings["Site"] = "LCG.CERN.cern"

And that's it. Configure the rest of the `Job` properties as you normally
would, such as `j.splitter`. Your studies await!

.. _Starterkit lesson on Ganga: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/davinci-grid.html
.. _Starterkit lesson: https://lhcb.github.io/starterkit-lessons/first-analysis-steps/lhcb-dev.html
