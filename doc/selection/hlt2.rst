HLT2 overview
=============

HLT2 selection.

.. _hlt2_standard_makers:

Standard particle makers
------------------------

.. automodule:: Hlt2Conf.standard_particles
  :members:
  :undoc-members:

Standard primary vertex makers
------------------------------

.. automodule:: RecoConf.reconstruction_objects
  :members: make_pvs
  :undoc-members: make_pvs

Helpers
-------

.. automodule:: Hlt2Conf.algorithms
  :members: require_all
