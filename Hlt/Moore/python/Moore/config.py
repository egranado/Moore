###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re, os, json, logging, inspect
from collections import namedtuple
from Configurables import (LumiCounterMerger, ApplicationMgr, DumpUTGeometry,
                           DumpFTGeometry, DumpMuonTable, DumpMuonGeometry,
                           DumpVPGeometry, DumpMagneticField, DumpBeamline,
                           DumpUTLookupTables, AllenUpdater)
from PyConf import configurable
from PyConf.Algorithms import (bankKiller, DeterministicPrescaler,
                               ExecutionReportsWriter, HltDecReportsWriter,
                               HltSelReportsWriter, Hlt__RoutingBitsWriter,
                               HltLumiWriter)
from PyConf.components import Algorithm, force_location, setup_component
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.dataflow import DataHandle
from PyConf.application import (ApplicationOptions, output_writers, make_odin,
                                default_raw_event)

from routingbits import get_default_routing_bits

# "forward" some useful functions from PyConf.application
from PyConf.application import configure_input, configure

from .selreports import make_selreports
from .persistency import clone_candidates
from RecoConf.hlt1_allen import make_allen_dec_reports

log = logging.getLogger(__name__)

# TODO move the options global to a separate module such that functions
#      defined here cannot access it
# FIXME _enabled is a workaround for using ConfigurableUser
#: Global ApplicationOptions instance holding the options for Moore
options = ApplicationOptions(_enabled=False)


def _format_location(l):
    """Return the TES location `l` formatted for an output writer.

    Arguments:
        l (str or DataHandle)
    """
    # Accept `l` as a str or a DataHandle
    if isinstance(l, DataHandle):
        l = "{}#1".format(l.location)
    # Ensure the location ends with the depth specification `#N` or `#*`
    assert re.match(r".*#(\d+|\*)", l), l
    return l


class HltLine(namedtuple('HltLine', ['node', 'extra_outputs'])):  # noqa
    """Immutable object fully qualifiying an HLT line.

    Attributes:
        node (CompositeNode): the control flow node of the line

    """
    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, algs, prescale=1., extra_outputs=None):
        """Initialize a HltLine from name, algs and prescale.

        Creates a control flow `CompositeNode` with the given `algs`
        combined with `LAZY_AND` logic. A `DeterministicPrescaler` is
        always inserted even if the prescale is 1.

        Args:
            name (str): name of the line
            algs: iterable of algorithms
            prescale (float): accept fraction of the prescaler
            extra_outputs (iterable of 2-tuple): List of (name, DataHandle) pairs.
        """
        # Line names ending with "Decision" would be confusing, so forbid it
        if name.endswith("Decision"):
            raise ValueError(
                "line name ({}) should not end with Decision".format(name))
        prescaler = DeterministicPrescaler(
            AcceptFraction=prescale,
            SeedName=name + "Prescaler",
            ODINLocation=make_odin())
        node = CompositeNode(
            name, [prescaler] + algs,
            combineLogic=NodeLogic.LAZY_AND,
            forceOrder=True)
        if extra_outputs is None:
            extra_outputs = []
        return super(HltLine, cls).__new__(cls, node, frozenset(extra_outputs))

    @property
    def name(self):
        """Line name"""
        return self.node.name

    @property
    def decision_name(self):
        """Decision name"""
        return self.name + "Decision"

    @property
    def output_producer(self):
        """Return the producer that defines the output of this line.

        The producer is defined as the last child in the control flow node,
        i.e. the last item passed as the `algs` argument to the `HltLine`
        constructor.

        If the producer creates no output, None is returned.
        """
        children = self.node.children
        last = children[-1]
        # Could in principle have control node here; will deal with this use
        # case if it arises
        assert isinstance(last, Algorithm)
        # If the last algorithm produces nothing, there is no 'producer'
        return last if last.outputs else None

    def produces_output(self):
        """Return True if this line produces output."""
        return self.output_producer is not None


class Reconstruction(namedtuple('Reconstruction', ['node'])):  # noqa
    """Immutable object fully qualifiying the output of a reconstruction data flow with prefilters.

    Attributes:
        node (CompositeNode): the control flow of the reconstruction.

    """
    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, data_producers, filters=None):
        """Initialize a Reconstruction from name, data_producers and filters.

        Creates two control flow `CompositeNode` with the given `data`
        combined with `NONLAZY_OR` to execute  and a `CompositeNode`.

        Args:
            name (str): name of the reconstruction
            data_producers (list): iterable list of algorithms to produce data
            filters (list): list of filters to apply before running reconstruction
        """
        data_producers_node = CompositeNode(
            name + "_data",
            data_producers,
            combineLogic=NodeLogic.NONLAZY_OR,
            forceOrder=True)
        if filters is None:
            filters = []
        control_flow = filters + [data_producers_node]
        cf_node = CompositeNode(
            name + "_decision",
            control_flow,
            combineLogic=NodeLogic.LAZY_AND,
            forceOrder=True)

        return super(Reconstruction, cls).__new__(cls, cf_node)

    @property
    def name(self):
        """Reconstruction name"""
        return self.node.name


@configurable
def report_writers_node(lines,
                        kill_existing,
                        get_routing_bits=get_default_routing_bits):
    """Return the control flow node and locations to persist of the default reports writers."""
    # Hack to support writing of objects created in HLT2 jobs
    locations_to_persist = []

    # Sort the lines by name for consistency between runs
    lines = sorted(lines, key=lambda line: line.name)

    algs = []
    # We will write the reports to raw banks at these locations
    report_banks_to_write = ['HltDecReports', 'HltSelReports']
    routing_bits_banks_to_write = ['HltRoutingBits']
    lumi_banks_to_write = ['HltLumiSummary']
    reports_raw_event = default_raw_event(report_banks_to_write)
    routing_bits_raw_event = default_raw_event(routing_bits_banks_to_write)
    lumi_raw_event = default_raw_event(lumi_banks_to_write)
    # TODO we probably shouldn't write in Trigger/RawEvent when
    # running on reconstructed data. Instead, we should create a
    # RawEvent at DAQ/RawEvent, which would make HltDecReportsWriter
    # happy, or make raw events/banks const and adapt everything.
    if kill_existing:
        all_banks_to_write = (report_banks_to_write + lumi_banks_to_write +
                              routing_bits_banks_to_write)
        algs.append(
            bankKiller(
                RawEventLocations=[reports_raw_event, lumi_raw_event],
                BankTypes=all_banks_to_write))

    erw = ExecutionReportsWriter(
        Persist=[line.name for line in lines],
        ANNSvcKey="Hlt1SelectionID",
    )
    drw = HltDecReportsWriter(
        InputHltDecReportsLocation=erw.DecReportsLocation,
        outputs=dict(
            OutputRawEventLocation=force_location(reports_raw_event.location)),
    )
    algs.extend([erw, drw])
    locations_to_persist.append(drw.OutputRawEventLocation)

    Hlt1SelectionIDs = {
        line.decision_name: n
        for n, line in enumerate(lines, 1)
    }

    hlt_ann_svc = setup_component(
        "HltANNSvc",
        # Zero is an invalid DecReport ID, so start from 1
        Hlt1SelectionID={
            line.decision_name: n
            for n, line in enumerate(lines, 1)
        })

    lines_with_output = [l for l in lines if l.produces_output()]
    lumi_lines = [
        l for l in lines_with_output
        if l.output_producer.type == LumiCounterMerger
    ]
    physics_lines = [l for l in lines_with_output if l not in lumi_lines]

    # TODO(AP) move this somewhere more general, e.g. run_moore
    hlt1 = all(l.name.startswith('Hlt1') for l in lines)
    hlt2 = all(l.name.startswith('Hlt2') for l in lines)
    assert hlt1 ^ hlt2, 'Expected exclusively all Hlt1 or all Hlt2 lines'
    if hlt1:
        srm = make_selreports(physics_lines, erw)
        algs.append(srm)
        srw = HltSelReportsWriter(
            DecReports=erw.DecReportsLocation,
            SelReports=srm.SelReports,
            ObjectSummaries=srm.ObjectSummaries,
            # Algorithm uses this property as a both input and output
            RawEvent=reports_raw_event)
        algs.append(srw)
        locations_to_persist.append(reports_raw_event)
    elif hlt2:
        cloner_cf, cloner_locations = clone_candidates(physics_lines,
                                                       erw.DecReportsLocation)
        algs.extend(cloner_cf)
        locations_to_persist.extend(cloner_locations)

    # We can handle multiple lines by using a LumiCounterMerger algorithm, but
    # for now simplify the logic by assuming there's a single line
    assert len(lumi_lines) <= 1, 'Found multiple lumi lines'
    if len(lumi_lines) == 1:
        lumi_line, = lumi_lines
        lumi_encoder = HltLumiWriter(
            InputBank=lumi_line.output_producer,
            outputs=dict(
                RawEventLocation=force_location(lumi_raw_event.location)),
        )
        algs.append(lumi_encoder)
        locations_to_persist.append(lumi_encoder.RawEventLocation)

    rbw = Hlt__RoutingBitsWriter(
        RoutingBits=get_routing_bits([line.decision_name for line in lines]),
        DecReports=erw.DecReportsLocation,
        ODIN=make_odin(),
        outputs=dict(
            RawEventLocation=force_location(routing_bits_raw_event.location)),
    )
    algs.append(rbw)
    locations_to_persist.append(rbw.RawEventLocation)

    node = CompositeNode(
        'report_writers',
        combineLogic=NodeLogic.NONLAZY_OR,
        children=algs,
        forceOrder=True)

    # Transform to a list of unique str
    locations_to_persist = list(
        set(map(_format_location, locations_to_persist)))

    return node, locations_to_persist


def moore_control_flow(options, lines):
    """Return the Moore application control flow node.

    Combines the lines with `NONLAZY_OR` logic in a global decision
    control flow node. This is `LAZY_AND`-ed with the output control
    flow, which consists of Moore-specific report makers/writers and
    generic persistency.

    Args:
        options (ApplicationOptions): holder of application options
        lines: control flow nodes of lines

    """
    options.finalize()
    # TODO pass kill_existing from options
    rw_node, rw_outputs = report_writers_node(lines, kill_existing=True)
    writers = [rw_node]
    writers.extend(output_writers(options, rw_outputs))

    dec = CompositeNode(
        'hlt_decision',
        combineLogic=NodeLogic.NONLAZY_OR,
        children=[line.node for line in lines],
        forceOrder=False)

    return CompositeNode(
        'moore',
        combineLogic=NodeLogic.LAZY_AND,
        children=[dec] + writers,
        forceOrder=True)


def run_moore(options, make_lines=None, public_tools=[]):
    """Configure Moore's entire control and data flow.

    Convenience function that configures all services, creates the
    standard Moore control flow and builds the the data flow (by
    calling the global lines maker).

    Args:
        options (ApplicationOptions): holder of application options
        make_lines: function returning a list of `HltLine` objects
        public_tools (list): list of public `Tool` instances to configure

    """
    # First call configure_input for its important side effect of
    # changing the default values of default_raw_event's arguments.
    # The latter is the deepest part of the call stack of make_lines.
    config = configure_input(options)
    # Then create the data (and control) flow for all lines.
    lines = (make_lines or options.lines_maker)()
    # Combine all lines and output in a global control flow.
    top_cf_node = moore_control_flow(options, lines)
    config.update(configure(options, top_cf_node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def setup_allen_non_event_data_service(dump_binaries=False):
    """Setup Allen non-event data

    An ExtSvc is added to the ApplicationMgr to provide the Allen non-event data (geometries etc.)
    """
    producers = [
        p(DumpToFile=dump_binaries)
        for p in (DumpVPGeometry, DumpUTGeometry, DumpFTGeometry,
                  DumpMuonGeometry, DumpMuonTable, DumpMagneticField,
                  DumpBeamline, DumpUTLookupTables)
    ]
    ApplicationMgr().ExtSvc += [
        AllenUpdater(OutputLevel=2),
    ] + producers


def get_allen_hlt1_lines():
    """Read Allen HLT1 lines from json configuration file

    """
    Hlt1SelectionIDs = {}
    config_file_path = "$ALLEN_INSTALL_DIR/constants/Sequence.json"
    try:
        with open(os.path.expandvars(config_file_path)) as config_file:
            config = (json.load(config_file))
    except:
        log.fatal(
            "Could not find Allen sequence file %s; make sure you have built Allen",
            config_file_path)

    # lines = []
    # for line in config["configured_lines"]:
    #     lines.append("Hlt1" + str(line) + "Decision")
    # return {
    #     lines: n
    #     for n, line in enumerate(lines, 1)
    # }

    index = 1
    for line in config["configured_lines"]:
        line_name = "Hlt1" + str(line) + "Decision"
        Hlt1SelectionIDs[line_name] = index
        index += 1

    return Hlt1SelectionIDs


def setup_allen_Hlt1ANN():
    """Configure HltANN service for Allen

    """
    Hlt1SelectionIDs = get_allen_hlt1_lines()
    hlt_ann_svc = setup_component(
        "HltANNSvc", Hlt1SelectionID=Hlt1SelectionIDs)

    return Hlt1SelectionIDs


def allen_control_flow(options):
    options.finalize()

    setup_allen_non_event_data_service()

    # Write DecReports raw banks
    allen_dec_reports = make_allen_dec_reports()

    # We will write the reports to raw banks at these locations
    algs = []
    locations_to_persist = []
    report_banks_to_write = ['HltDecReports']
    reports_raw_event = default_raw_event(report_banks_to_write)

    # TODO we probably shouldn't write in Trigger/RawEvent when
    # running on reconstructed data. Instead, we should create a
    # RawEvent at DAQ/RawEvent, which would make HltDecReportsWriter
    # happy, or make raw events/banks const and adapt everything.
    kill_existing = True
    if kill_existing:
        all_banks_to_write = (report_banks_to_write)
        algs.append(
            bankKiller(
                RawEventLocations=[reports_raw_event],
                BankTypes=all_banks_to_write))

    drw = HltDecReportsWriter(
        InputHltDecReportsLocation=allen_dec_reports,
        outputs=dict(
            OutputRawEventLocation=force_location(reports_raw_event.location)),
    )

    algs.extend([drw])
    locations_to_persist.append(drw.OutputRawEventLocation)

    Hlt1SelectionIDs = setup_allen_Hlt1ANN()

    locations_to_persist.append(reports_raw_event)

    report_writers_node = CompositeNode(
        'report_writers',
        combineLogic=NodeLogic.NONLAZY_OR,
        children=algs,
        forceOrder=True)

    # Transform to a list of unique str
    locations_to_persist = list(
        set(map(_format_location, locations_to_persist)))

    writers = [report_writers_node]
    writers.extend(output_writers(options, locations_to_persist))

    return CompositeNode(
        'allen',
        combineLogic=NodeLogic.NONLAZY_OR,
        children=[make_allen_dec_reports()] + writers,
        forceOrder=True)


def run_allen(options):
    """Configure Allen within Mooore.

    Convenience function that sets up an Allen node and sets up services

    Args:
        options (ApplicationOptions): holder of application options

    """
    config = configure_input(options)

    top_cf_node = allen_control_flow(options)

    config.update(configure(options, top_cf_node))
    # TODO pass config to gaudi explicitly when that is supported
    return config


def run_allen_reconstruction(options,
                             make_reconstruction,
                             dump_binaries=False,
                             public_tools=[]):
    """Configure the Allen reconstruction data flow

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    setup_allen_non_event_data_service(dump_binaries=dump_binaries)
    return run_reconstruction(
        options, make_reconstruction, public_tools=public_tools)


def run_reconstruction(options, make_reconstruction, public_tools=[]):
    """Configure the reconstruction data flow with a simple control flow.

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    config = configure_input(options)
    reconstruction = make_reconstruction()
    config.update(
        configure(options, reconstruction.node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config


#: Regular expression (compiled) defining the valid line names
HLT_LINE_NAME_PATTERN = re.compile(r'^Hlt[12][A-Za-z0-9_]+Line$')


def valid_name(name):
    """Return True if name follows the HLT line name conventions."""
    try:
        return HLT_LINE_NAME_PATTERN.match(name) is not None
    except TypeError:
        return False


def _get_arg_default(function, name):
    """Return the default value of a function argument.

    Raises TypeError if ``function`` has no default keyword argument
    called ``name``.
    """
    spec = inspect.getargspec(function)
    try:
        i = spec.args.index(name)  # ValueError if not found
        return spec.defaults[i - len(spec.args)]  # IndexError if not keyword
    except (ValueError, IndexError):
        raise TypeError('{!r} has no keyword argument {!r}'.format(
            function, name))


def add_line_to_registry(registry, name, maker):
    """Add a line maker to a registry, ensuring no name collisions."""
    if name in registry:
        raise ValueError('{} already names an HLT line maker: '
                         '{}'.format(name, registry[name]))
    registry[name] = maker


def register_line_builder(registry):
    """Decorator to register a named HLT line.

    The decorated function must have keyword argument `name`. Its
    default value is used as the key in `registry`, under which the
    line builder (maker) is registered.

    Usage:

        >>> from PyConf.tonic import configurable
        ...
        >>> all_lines = {}
        >>> @register_line_builder(all_lines)
        ... @configurable
        ... def the_line_definition(name='Hlt2TheNameOfTheLine'):
        ...     # ...
        ...     return HltLine(name=name, algs=[])  # filled with control flow
        ...
        >>> 'Hlt2TheNameOfTheLine' in all_lines
        True

    """

    def wrapper(wrapped):
        name = _get_arg_default(wrapped, 'name')
        if not valid_name(name):
            raise ValueError('{!r} is not a valid HLT line name'.format(name))
        add_line_to_registry(registry, name, wrapped)
        # TODO return a wrapped function that checks the return type is HltLine
        return wrapped

    return wrapper
