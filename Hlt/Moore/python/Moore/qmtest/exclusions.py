###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiTesting.BaseTest import LineSkipper
from GaudiConf.QMTest.LHCbTest import GroupMessages, BlockSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

remove_known_warnings = LineSkipper(regexps=[
    # expected WARNINGs from the data broker
    r"HiveDataBrokerSvc +WARNING non-reentrant algorithm: .*",
    # expected WARNINGs from MuonIDHlt1Alg due to lhcb/Rec#79
    r"MuonID.* +WARNING CondDB {X,Y}FOIParameters member "
    r"size is 20, geometry expects 16",
    r"ToolSvc.CommonMuonTool.* +WARNING CondDB {X,Y}FOIParameters member "
    r"size is 20, geometry expects 16",
    # expected WARNING when JIT-compiling functors
    (r".*WARNING Suppressing message: 'Stack:SSE|(AVX(2|256|512)), Functor:Scalar, instruction"
     r" set mismatch \(Best requested\). ROOT/cling was not compiled with the same options as the"
     r" stack, try the functor cache'"),

    # expected WARNINGs due to SPD/PRS in the condition database
    # see https://gitlab.cern.ch/lhcb/Rec/issues/107
    r"(ToolSvc)?.*(Calo|Photon|Electron).* WARNING  o Type [a-z]+C? is "
    r"not registered",
])

preprocessor = remove_known_warnings + LineSkipper(regexps=[
    # output from the scheduler that is expected to differ run-by-run
    r"HLTControlFlowMgr\s+INFO ---> End of Initialization. This took [0-9\.]+ ms",
    r"HLTControlFlowMgr\s+INFO ---> Loop over \d+ Events Finished -  WSS [0-9\.]+, timed \d+ Events: [0-9\.]+ ms, Evts/s = [0-9\.]+",
    r"HLTControlFlowMgr\s+INFO  o Number of events slots: .*",
    r"HLTControlFlowMgr\s+INFO  o TBB thread pool size:  'ThreadPoolSize':.*"
])

# In most cases everything interesting happens at finalize: counters,
# PRChecker tables, etc., so pick everything starting with finalize
# until the end of the file.
only_finalize = BlockSkipper("# --> Including file",
                             " INFO Application Manager Stopped successfully")

# In case you do not want to check what is printed in initialize.
skip_initialize = BlockSkipper(
    " INFO Application Manager Configured successfully",
    " Reading Event record 1. Record number within stream 1: 1")

# In case you do not want to check counters.
remove_finalize = BlockSkipper(
    " INFO Application Manager Stopped successfully",
    " INFO Application Manager Terminated successfully")

# Skip everything from the HLTControlFlowMgr (incl. the table with
# number of executions/passes).
skip_scheduler = GroupMessages() + LineSkipper(strings=["HLTControlFlowMgr"])

ref_preprocessor = only_finalize + LHCbPreprocessor + skip_scheduler
