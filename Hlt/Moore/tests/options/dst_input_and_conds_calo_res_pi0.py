###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from Moore import options
from Configurables import ApplicationMgr
from Configurables import NTupleSvc

options.evt_max = 100
# for meaningfull resolution plot larger statistics is needed: comment out the line above (the test will process ~110K events and take a few hours)

options.set_input_from_testfiledb('Upgrade_Bd_pi+pi-pi0_LDST')

options.input_type = 'ROOT'
options.input_raw_format = 4.3

options.histo_file = 'histofile.root'
options.ntuple_file = 'outputfile_calo_res_pi0.root'

execfile(os.path.expandvars('$MOOREROOT/tests/options/default_conds.py'))
