###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Maker functions for Particle definitions common across HLT2.

The Run 2 code makes the sensible choice of creating Particle objects first,
and then filtering these with FilterDesktop instances. Because the
FunctionalParticleMaker can apply LoKi cut strings directly to Track and
ProtoParticle objects, we just do the one step.
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from PyConf.Algorithms import FunctionalParticleMaker, Proto2ChargedBasic
from PyConf.Tools import (LoKi__Hybrid__ProtoParticleFilter as
                          ProtoParticleFilter, LoKi__Hybrid__TrackSelector as
                          TrackSelector)

from PyConf import configurable

from .algorithms import (require_all, ParticleFilter, ParticleFilterWithPVs,
                         ParticleCombiner, ParticleCombinerWithPVs,
                         PhotonFilter, ResolvedPi0Filter, MergedPi0Filter)

from .hacks import patched_hybrid_tool
from RecoConf.reconstruction_objects import (
    make_charged_protoparticles as _make_charged_protoparticles, make_pvs as
    _make_pvs, make_neutral_protoparticles as _make_neutral_protoparticles)

_KAON0_M = 497.611 * MeV  # +/- 0.013, PDG, PR D98, 030001 and 2019 update
_LAMBDA_M = 1115.683 * MeV  # +/- 0.006, PDG, PR D98, 030001 and 2019 update


@configurable
def get_all_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=Code, **kwargs)


@configurable
def get_long_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrLONG', Code), **kwargs)


@configurable
def get_down_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrDOWNSTREAM', Code), **kwargs)


@configurable
def get_upstream_track_selector(Code='TrALL', **kwargs):
    return TrackSelector(Code=require_all('TrUPSTREAM', Code), **kwargs)


@configurable
def standard_protoparticle_filter(Code='PP_ALL', **kwargs):
    return ProtoParticleFilter(
        Code=Code, Factory=patched_hybrid_tool('PPFactory'), **kwargs)


@configurable
def _make_particles(species,
                    make_protoparticles=_make_charged_protoparticles,
                    get_track_selector=get_long_track_selector,
                    make_protoparticle_filter=standard_protoparticle_filter):
    """ creates LHCb::Particles from LHCb::ProtoParticles """
    particles = FunctionalParticleMaker(
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        TrackSelector=get_track_selector(),
        ProtoParticleFilter=make_protoparticle_filter()).Particles
    return particles


@configurable
def _make_ChargedBasics(
        species,
        make_protoparticles=_make_charged_protoparticles,
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter):
    """ creates LHCb::v2::ChargedBasics from LHCb::ProtoParticles """
    particles = Proto2ChargedBasic(
        InputProtoParticles=make_protoparticles(),
        ParticleID=species,
        TrackSelector=get_track_selector(),
        ProtoParticleFilter=make_protoparticle_filter()).Particles
    return particles


@configurable
def _make_all_ChargedBasics(species):
    return _make_ChargedBasics(
        species=species,
        get_track_selector=get_all_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def _make_long_ChargedBasics(species):
    return _make_ChargedBasics(
        species=species,
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_cb_electrons():
    return _make_long_ChargedBasics('electron')


def make_long_cb_muons():
    return _make_long_ChargedBasics('muon')


def make_long_cb_protons():
    return _make_long_ChargedBasics('proton')


def make_long_cb_kaons():
    return _make_long_ChargedBasics('kaon')


def make_long_cb_pions():
    return _make_long_ChargedBasics('pion')


def make_all_cb_electrons():
    return _make_all_ChargedBasics('electron')


def make_all_cb_muons():
    return _make_all_ChargedBasics('muon')


def make_all_cb_protons():
    return _make_all_ChargedBasics('proton')


def make_all_cb_kaons():
    return _make_all_ChargedBasics('kaon')


def make_all_cb_pions():
    return _make_all_ChargedBasics('pion')


@configurable
def make_photons(species='Gamma',
                 make_neutral_protoparticles=_make_neutral_protoparticles,
                 **kwargs):
    return PhotonFilter(make_neutral_protoparticles(), species, **kwargs)


@configurable
def make_resolved_pi0s(
        species='Pi0',
        mass_window=30. * MeV,
        make_neutral_protoparticles=_make_neutral_protoparticles,
        photon_args={'PtCut': 200. * MeV},
        **kwargs):
    respi0s = ResolvedPi0Filter(make_neutral_protoparticles(), species,
                                2 * mass_window, photon_args, **kwargs)
    code = "ADMASS('pi0') < {mass_window}".format(mass_window=mass_window)
    return ParticleFilter(respi0s, Code=code)


@configurable
def make_merged_pi0s(species='Pi0',
                     mass_window=60. * MeV,
                     make_neutral_protoparticles=_make_neutral_protoparticles,
                     **kwargs):
    return MergedPi0Filter(make_neutral_protoparticles(), species, mass_window,
                           **kwargs)


#Long particles
def make_long_electrons_no_brem():
    return _make_particles(
        species="electron",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_pions():
    return _make_particles(
        species="pion",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_kaons():
    return _make_particles(
        species="kaon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_protons():
    return _make_particles(
        species="proton",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_long_muons():
    return _make_particles(
        species="muon",
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


#Down particles
def make_down_pions():
    return _make_particles(
        species="pion",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_kaons():
    return _make_particles(
        species="kaon",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


def make_down_protons():
    return _make_particles(
        species="proton",
        get_track_selector=get_down_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter)


@configurable
def make_phi2kk(am_max=1100. * MeV, adoca_chi2=30, vchi2=25.0):
    kaons = make_long_kaons()
    descriptors = ['phi(1020) -> K+ K-']
    combination_code = require_all("AM < {am_max}",
                                   "ADOCACHI2CUT({adoca_chi2}, '')").format(
                                       am_max=am_max, adoca_chi2=adoca_chi2)
    vertex_code = "(VFASPF(VCHI2) < {vchi2})".format(vchi2=vchi2)
    return ParticleCombiner(
        particles=kaons,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# Make V0s
def _make_long_for_V0(particles, pvs):
    code = require_all("BPVVALID()", "MIPCHI2DV(PRIMARY)>36")
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def _make_down_for_V0(particles):
    code = require_all("P>3000*MeV", "PT > 175.*MeV")
    return ParticleFilter(particles, Code=code)


def make_long_pions_for_V0():
    return _make_long_for_V0(make_long_pions(), _make_pvs())


def make_long_protons_for_V0():
    return _make_long_for_V0(make_long_protons(), _make_pvs())


def make_down_pions_for_V0():
    return _make_down_for_V0(make_down_pions())


def make_down_protons_for_V0():
    return _make_down_for_V0(make_down_protons())


@configurable
def _make_V0LL(particles,
               descriptors,
               pname,
               pvs,
               am_dmass=50 * MeV,
               m_dmass=35 * MeV,
               vchi2pdof_max=30,
               bpvltime_min=2.0 * picosecond):
    """Make long-long V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    combination_code = require_all("ADAMASS('{pname}') < {am_dmass}").format(
        pname=pname, am_dmass=am_dmass)
    vertex_code = require_all("ADMASS('{pname}')<{m_dmass}",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVLTIME() > {bpvltime_min}").format(
                                  pname=pname,
                                  m_dmass=m_dmass,
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvltime_min=bpvltime_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def _make_V0DD(particles,
               descriptors,
               pvs,
               am_min=_KAON0_M - 80 * MeV,
               am_max=_KAON0_M + 80 * MeV,
               m_min=_KAON0_M - 64 * MeV,
               m_max=_KAON0_M + 64 * MeV,
               vchi2pdof_max=30,
               bpvvdz_min=400 * mm):
    """Make down-down V0 -> h+ h'- candidates
    Initial implementation a replication of the old Hlt2SharedParticles
    """
    combination_code = require_all("in_range({am_min},  AM, {am_max})").format(
        am_min=am_min, am_max=am_max)
    vertex_code = require_all("in_range({m_min},  M, {m_max})",
                              "CHI2VXNDOF<{vchi2pdof_max}",
                              "BPVVDZ() > {bpvvdz_min}").format(
                                  m_min=m_min,
                                  m_max=m_max,
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdz_min=bpvvdz_min)
    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_KsLL():
    pions = make_long_pions_for_V0()
    descriptors = ["KS0 -> pi+ pi-"]
    return _make_V0LL(
        particles=[pions],
        descriptors=descriptors,
        pname='KS0',
        pvs=_make_pvs())


def make_KsDD():
    pions = make_down_pions_for_V0()
    descriptors = ["KS0 -> pi+ pi-"]
    return _make_V0DD(
        particles=[pions], descriptors=descriptors, pvs=_make_pvs())


def make_LambdaLL():
    pions = make_long_pions_for_V0()
    protons = make_long_protons_for_V0()
    descriptors = ["[Lambda0 -> p+ pi-]cc"]
    return _make_V0LL(
        particles=[pions, protons],
        descriptors=descriptors,
        pname='Lambda0',
        pvs=_make_pvs(),
        am_dmass=50 * MeV,
        m_dmass=20 * MeV,
        vchi2pdof_max=30,
        bpvltime_min=2.0 * picosecond)


@configurable
def make_LambdaDD():
    pions = make_down_pions_for_V0()
    protons = make_down_protons_for_V0()
    descriptors = ["[Lambda0 -> p+ pi-]cc"]
    return _make_V0DD(
        particles=[pions, protons],
        descriptors=descriptors,
        pvs=_make_pvs(),
        am_min=_LAMBDA_M - 80 * MeV,
        am_max=_LAMBDA_M + 80 * MeV,
        m_min=_LAMBDA_M - 21 * MeV,
        m_max=_LAMBDA_M + 24 * MeV,
        vchi2pdof_max=30,
        bpvvdz_min=400 * mm)


# Make pions
@configurable
def make_has_rich_long_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_pions()


@configurable
def make_has_rich_down_pions():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_pions()


# Make kaons
@configurable
def make_has_rich_long_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_kaons()


@configurable
def make_has_rich_down_kaons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_kaons()


# Make protons
@configurable
def make_has_rich_long_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_long_protons()


@configurable
def make_has_rich_down_protons():
    with standard_protoparticle_filter.bind(Code='PP_HASRICH'):
        return make_down_protons()


@configurable
def make_detached_mumu(probnn_mu=0.2,
                       pt_mu=0. * GeV,
                       minipchi2=9.,
                       trghostprob=0.25,
                       adocachi2cut=30,
                       bpvvdchi2=30,
                       vfaspfchi2ndof=10):
    #def make_detached_mumu(probnn_mu=-0.2, pt_mu=0.*GeV, minipchi2=0., trghostprob=0.925, adocachi2cut=30, bpvvdchi2=30, vfaspfchi2ndof=10):
    muons = make_long_muons()
    descriptors = ['J/psi(1S) -> mu+ mu-', '[J/psi(1S) -> mu+ mu+]cc']
    daughters_code = {
        'mu+':
        '(PROBNNmu > {probnn_mu}) & (PT > {pt_mu}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_mu=probnn_mu,
            pt_mu=pt_mu,
            minipchi2=minipchi2,
            trghostprob=trghostprob),
        'mu-':
        '(PROBNNmu > {probnn_mu}) & (PT > {pt_mu}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_mu=probnn_mu,
            pt_mu=pt_mu,
            minipchi2=minipchi2,
            trghostprob=trghostprob)
    }
    combination_code = "ADOCACHI2CUT({adocachi2cut}, '')".format(
        adocachi2cut=adocachi2cut)
    vertex_code = require_all(
        "(VFASPF(VCHI2/VDOF) < {vfaspfchi2ndof}) & (BPVVDCHI2() > {bpvvdchi2})"
    ).format(
        vfaspfchi2ndof=vfaspfchi2ndof, bpvvdchi2=bpvvdchi2)

    return ParticleCombinerWithPVs(
        particles=muons,
        pvs=_make_pvs(),
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


#Update to ProbNNe once the variables are ready
@configurable
def make_detached_ee(probnn_e=2,
                     pt_e=0.25 * GeV,
                     minipchi2=9.,
                     trghostprob=0.25,
                     adocachi2cut=30,
                     bpvvdchi2=30,
                     vfaspfchi2ndof=10):
    electrons = make_long_electrons_no_brem()
    descriptors = ['J/psi(1S) -> e+ e-', '[J/psi(1S) -> e+ e+]cc']
    daughters_code = {
        'e+':
        '(PIDe > {probnn_e}) & (PT > {pt_e}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_e=probnn_e,
            pt_e=pt_e,
            minipchi2=minipchi2,
            trghostprob=trghostprob),
        'e-':
        '(PIDe > {probnn_e}) & (PT > {pt_e}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_e=probnn_e,
            pt_e=pt_e,
            minipchi2=minipchi2,
            trghostprob=trghostprob)
    }
    combination_code = require_all("ADOCACHI2CUT({adocachi2cut}, '')").format(
        adocachi2cut=adocachi2cut)
    vertex_code = require_all(
        "(VFASPF(VCHI2/VDOF) < {vfaspfchi2ndof}) & (BPVVDCHI2() > {bpvvdchi2})"
    ).format(
        vfaspfchi2ndof=vfaspfchi2ndof, bpvvdchi2=bpvvdchi2)
    return ParticleCombinerWithPVs(
        particles=electrons,
        pvs=_make_pvs(),
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_detached_mue(probnn_mu=0.2,
                      pt_mu=0. * GeV,
                      probnn_e=2,
                      pt_e=0.25 * GeV,
                      minipchi2=9.,
                      trghostprob=0.25,
                      adocachi2cut=30,
                      bpvvdchi2=30,
                      vfaspfchi2ndof=10):
    muons = make_long_muons()
    electrons = make_long_electrons_no_brem()
    descriptors = ['J/psi(1S) -> mu+ e-', '[J/psi(1S) -> mu+ e+]cc']
    daughters_code = {
        'mu+':
        '(PROBNNmu > {probnn_mu}) & (PT > {pt_mu}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_mu=probnn_mu,
            pt_mu=pt_mu,
            minipchi2=minipchi2,
            trghostprob=trghostprob),
        'mu-':
        '(PROBNNmu > {probnn_mu}) & (PT > {pt_mu}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_mu=probnn_mu,
            pt_mu=pt_mu,
            minipchi2=minipchi2,
            trghostprob=trghostprob),
        'e+':
        '(PIDe > {probnn_e}) & (PT > {pt_e}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_e=probnn_e,
            pt_e=pt_e,
            minipchi2=minipchi2,
            trghostprob=trghostprob),
        'e-':
        '(PIDe > {probnn_e}) & (PT > {pt_e}) & (MIPCHI2DV(PRIMARY) > {minipchi2}) & (TRGHOSTPROB < {trghostprob})'
        .format(
            probnn_e=probnn_e,
            pt_e=pt_e,
            minipchi2=minipchi2,
            trghostprob=trghostprob)
    }
    combination_code = require_all("ADOCACHI2CUT({adocachi2cut}, '')").format(
        adocachi2cut=adocachi2cut)
    vertex_code = require_all(
        "(VFASPF(VCHI2/VDOF) < {vfaspfchi2ndof}) & (BPVVDCHI2() > {bpvvdchi2})"
    ).format(
        vfaspfchi2ndof=vfaspfchi2ndof, bpvvdchi2=bpvvdchi2)
    return ParticleCombinerWithPVs(
        particles=[muons, electrons],
        pvs=_make_pvs(),
        DecayDescriptors=descriptors,
        DaughtersCuts=daughters_code,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# Make muons
@configurable
def make_ismuon_long_muon():
    with standard_protoparticle_filter.bind(Code='PP_ISMUON'):
        return make_long_muons()


@configurable
def make_dimuon_base(name='DiMuonBaseCombiner', maxVCHI2PDOF=25):
    """Basic dimuon without any requirements but common vertex
    Please DO NOT add pt requirements here:
    a dedicated (tighter) dimuon filter is implemented in the dimuon module.
    """

    # get the long muons
    muons = make_ismuon_long_muon()

    # require that the muons come from the same vertex
    mother_code = require_all("VFASPF(VCHI2PDOF) < {vchi2}").format(
        vchi2=maxVCHI2PDOF)

    return ParticleCombiner(
        name=name,
        particles=muons,
        DecayDescriptors=['J/psi(1S) -> mu+ mu-'],
        CombinationCut='AALL',
        MotherCut=mother_code)


@configurable
def make_mass_constrained_jpsi2mumu(name='MassConstrJpsi2MuMuMaker',
                                    jpsi_maker=make_dimuon_base,
                                    pid_mu=0,
                                    pt_mu=0.5 * GeV,
                                    admass=250. * MeV,
                                    adoca_chi2=20,
                                    vchi2=16):
    """Make the Jpsi, starting from dimuons"""

    # get the dimuons with basic cuts (only vertexing)
    # note that the make_dimuon_base combiner uses vertexChi2/ndof < 25,
    # which is looser than the vertexChi2 < 16 required here
    dimuons = jpsi_maker()

    code = require_all(
        'ADMASS("J/psi(1S)") < {admass}',
        'DOCACHI2MAX < {adoca_chi2}',
        'VFASPF(VCHI2) < {vchi2}',
        'INTREE(("mu+" == ABSID)  & (PIDmu > {pid_mu}))',
        'INTREE(("mu+" == ABSID)  & (PT > {pt_mu}))',
        #'MFIT',  # not really needed
    ).format(
        admass=admass,
        adoca_chi2=adoca_chi2,
        vchi2=vchi2,
        pid_mu=pid_mu,
        pt_mu=pt_mu,
    )

    return ParticleFilter(dimuons, name=name, Code=code)
