###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Load data from files and set up unpackers.

There are two things we have to deal with:

1. Loading the data from the file in to the TES, done by
   Gaudi::Hive::FetchDataFromFile.
2. Unpacking and preparing packed containers, if the 'reconstruction' is
   defined as the objects already present in the file.

In most LHCb applications, step 2 is done for you behind the scenes. The
DataOnDemandSvc is configured in LHCb/GaudiConf/DstConf.py to unpack containers
when they are requested. It also configures adding RICH, MUON, and combined PID
information to ProtoParticles when they're unpacked. Because we don't have the
DataOnDemandSvc here, we have to do this by hand.

The interesting 'user-facing' exports of this module are:

* [reco,mc]_from_file(): Dict of names to locations that can be loaded from a file.
* [reco,mc]_unpackers(): Dict from unpacked object name to Algorithm that produces a
  container of those objects.
* pid_algorithms(): Dict from PID type to Algorithm that adds that PID
  information to existing ProtoParticles (yes, mutating the objects in the TES).
"""
from __future__ import absolute_import, division, print_function
import collections

from Gaudi.Configuration import ERROR
from Configurables import (
    ChargedProtoCombineDLLsAlg, ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddRichInfo, DataPacking__Unpack_LHCb__MuonPIDPacker_,
    DataPacking__Unpack_LHCb__RichPIDPacker_, UnpackCaloHypo,
    UnpackProtoParticle, UnpackRecVertex, UnpackTrackFunctional,
    UnpackMCParticle, UnpackMCVertex, DataPacking__Unpack_LHCb__MCVPHitPacker_
    as UnpackMCVPHit, DataPacking__Unpack_LHCb__MCUTHitPacker_ as
    UnpackMCUTHit, DataPacking__Unpack_LHCb__MCFTHitPacker_ as UnpackMCFTHit,
    DataPacking__Unpack_LHCb__MCRichHitPacker_ as UnpackMCRichHit,
    DataPacking__Unpack_LHCb__MCEcalHitPacker_ as UnpackMCEcalHit,
    DataPacking__Unpack_LHCb__MCHcalHitPacker_ as UnpackMCHcalHit,
    DataPacking__Unpack_LHCb__MCMuonHitPacker_ as UnpackMCMuonHit,
    DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack)

from PyConf.components import Algorithm, force_location
from PyConf.application import make_data_with_FetchDataFromFile


def packed_reco_from_file():
    return {
        'PackedPVs': '/Event/pRec/Vertex/Primary',
        'PackedCaloElectrons': '/Event/pRec/Calo/Electrons',
        'PackedCaloPhotons': '/Event/pRec/Calo/Photons',
        'PackedCaloMergedPi0s': '/Event/pRec/Calo/MergedPi0s',
        'PackedCaloSplitPhotons': '/Event/pRec/Calo/SplitPhotons',
        'PackedMuonPIDs': '/Event/pRec/Muon/MuonPID',
        'PackedRichPIDs': '/Event/pRec/Rich/PIDs',
        'PackedTracks': '/Event/pRec/Track/Best',
        'PackedMuonTracks': '/Event/pRec/Track/Muon',
        'PackedNeutralProtos': '/Event/pRec/ProtoP/Neutrals',
        'PackedChargedProtos': '/Event/pRec/ProtoP/Charged',
    }


def packed_mc_from_file():
    return {
        'PackedMCParticles': '/Event/pSim/MCParticles',
        'PackedMCVertices': '/Event/pSim/MCVertices',
        'PackedMCVPHits': '/Event/pSim/VP/Hits',
        'PackedMCUTHits': '/Event/pSim/UT/Hits',
        'PackedMCFTHits': '/Event/pSim/FT/Hits',
        'PackedMCRichHits': '/Event/pSim/Rich/Hits',
        'PackedMCEcalHits': '/Event/pSim/Ecal/Hits',
        'PackedMCHcalHits': '/Event/pSim/Hcal/Hits',
        'PackedMCMuonHits': '/Event/pSim/Muon/Hits',
        'PackedMCRichDigitSummaries': '/Event/pSim/Rich/DigitSummaries',
    }


def unpacked_reco_locations():
    # If the structure is not like this, pointers point to to the wrong place...
    # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
    locations = {
        k: v.replace('pRec', 'Rec')
        for k, v in packed_reco_from_file().items()
    }
    return locations


def unpacked_mc_locations():
    # If the structure is not like this, pointers point to to the wrong place...
    # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
    return {
        'PackedMCParticles': '/Event/MC/Particles',
        'PackedMCVertices': '/Event/MC/Vertices',
        'PackedMCVPHits': '/Event/MC/VP/Hits',
        'PackedMCUTHits': '/Event/MC/UT/Hits',
        'PackedMCFTHits': '/Event/MC/FT/Hits',
        'PackedMCRichHits': '/Event/MC/Rich/Hits',
        'PackedMCEcalHits': '/Event/MC/Ecal/Hits',
        'PackedMCHcalHits': '/Event/MC/Hcal/Hits',
        'PackedMCMuonHits': '/Event/MC/Muon/Hits',
        'PackedMCRichDigitSummaries': '/Event/MC/Rich/DigitSummaries',
    }


def reco_from_file():
    # TODO(AP) should only add the packed data if we're running on Upgrade MC
    # where Brunel has already been run
    packed_data = packed_reco_from_file()
    # raw_event = raw_event_from_file()
    # We don't want any keys accidentally overwriting each other
    # assert set(packed_data.keys()).intersection(set(raw_event.keys())) == set()
    # return dict(list(packed_data.items()) + list(raw_event.items()))
    return packed_data


def mc_from_file():
    # TODO(AP) should only add the packed data if we're running on Upgrade MC
    # where Brunel has already been run
    packed_data = packed_mc_from_file()
    return packed_data


def reco_unpacker(key, configurable, name, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    packed_loc = reco_from_file()[key]
    unpacked_loc = unpacked_reco_locations()[key]
    alg = Algorithm(
        configurable,
        name=name,
        outputs={'OutputName': force_location(unpacked_loc)},
        InputName=make_data_with_FetchDataFromFile(packed_loc),
        **kwargs)
    return alg


def mc_unpacker(key, configurable, name, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    packed_loc = mc_from_file()[key]
    unpacked_loc = unpacked_mc_locations()[key]
    alg = Algorithm(
        configurable,
        name=name,
        outputs={'OutputName': force_location(unpacked_loc)},
        InputName=make_data_with_FetchDataFromFile(packed_loc),
        **kwargs)
    return alg


def make_mc_track_info():
    return make_data_with_FetchDataFromFile('/Event/MC/TrackInfo')


def reco_unpackers():
    # Ordered so that dependents are unpacked first
    d = collections.OrderedDict([
        ('PVs', reco_unpacker('PackedPVs', UnpackRecVertex,
                              'UnpackRecVertices')),
        ('CaloElectrons',
         reco_unpacker('PackedCaloElectrons', UnpackCaloHypo,
                       'UnpackCaloElectrons')),
        ('CaloPhotons',
         reco_unpacker('PackedCaloPhotons', UnpackCaloHypo,
                       'UnpackCaloPhotons')),
        ('CaloMergedPi0s',
         reco_unpacker('PackedCaloMergedPi0s', UnpackCaloHypo,
                       'UnpackCaloMergedPi0s')),
        ('CaloSplitPhotons',
         reco_unpacker('PackedCaloSplitPhotons', UnpackCaloHypo,
                       'UnpackCaloSplitPhotons')),
        ('MuonPIDs',
         reco_unpacker('PackedMuonPIDs',
                       DataPacking__Unpack_LHCb__MuonPIDPacker_,
                       'UnpackMuonPIDs')),
        ('RichPIDs',
         reco_unpacker(
             'PackedRichPIDs',
             DataPacking__Unpack_LHCb__RichPIDPacker_,
             'UnpackRichPIDs',
             OutputLevel=ERROR)),
        # The OutputLevel above suppresses the following useless warnings (plus more?)
        # WARNING DataPacking::Unpack<LHCb::RichPIDPacker>:: Incorrect data version 0 for packing version > 3. Correcting data to version 2.
        ('Tracks',
         reco_unpacker('PackedTracks', UnpackTrackFunctional,
                       'UnpackBestTracks')),
        ('MuonTracks',
         reco_unpacker('PackedMuonTracks', UnpackTrackFunctional,
                       'UnpackMuonTracks')),
        ('NeutralProtos',
         reco_unpacker('PackedNeutralProtos', UnpackProtoParticle,
                       'UnpackNeutralProtos')),
        ('ChargedProtos',
         reco_unpacker('PackedChargedProtos', UnpackProtoParticle,
                       'UnpackChargedProtos')),
    ])

    # Make sure we have consistent names, and that we're unpacking everything
    # we load from the file
    assert set(['Packed' + k for k in d.keys()]) - set(
        packed_reco_from_file().keys()) == set()

    return d


def mc_unpackers():
    # Ordered so that dependents are unpacked first
    mc_vertices = mc_unpacker('PackedMCVertices', UnpackMCVertex,
                              'UnpackMCVertices')
    # Make sure that MC particles and MC vertices are unpacked together,
    # see https://gitlab.cern.ch/lhcb/LHCb/issues/57 for details.
    mc_particles = mc_unpacker(
        'PackedMCParticles',
        UnpackMCParticle,
        'UnpackMCParticles',
        ExtraInputs=[mc_vertices])

    mc_vp_hits = mc_unpacker('PackedMCVPHits', UnpackMCVPHit, 'UnpackMCVPHits')
    mc_ut_hits = mc_unpacker('PackedMCUTHits', UnpackMCUTHit, 'UnpackMCUTHits')
    mc_ft_hits = mc_unpacker('PackedMCFTHits', UnpackMCFTHit, 'UnpackMCFTHits')
    mc_rich_hits = mc_unpacker('PackedMCRichHits', UnpackMCRichHit,
                               'UnpackMCRichHits')
    mc_ecal_hits = mc_unpacker('PackedMCEcalHits', UnpackMCEcalHit,
                               'UnpackMCEcalHits')
    mc_hcal_hits = mc_unpacker('PackedMCHcalHits', UnpackMCHcalHit,
                               'UnpackMCHcalHits')
    mc_muon_hits = mc_unpacker('PackedMCMuonHits', UnpackMCMuonHit,
                               'UnpackMCMuonHits')

    # RICH Digit summaries
    mc_rich_digit_sums = mc_unpacker('PackedMCRichDigitSummaries',
                                     RichSumUnPack, "RichSumUnPack")

    d = collections.OrderedDict([
        ('MCRichDigitSummaries', mc_rich_digit_sums),
        ('MCParticles', mc_particles),
        ('MCVertices', mc_vertices),
        ('MCVPHits', mc_vp_hits),
        ('MCUTHits', mc_ut_hits),
        ('MCFTHits', mc_ft_hits),
        ('MCRichHits', mc_rich_hits),
        ('MCEcalHits', mc_ecal_hits),
        ('MCHcalHits', mc_hcal_hits),
        ('MCMuonHits', mc_muon_hits),
    ])

    # Make sure we have consistent names, and that we're unpacking everything
    # we load from the file
    assert set(['Packed' + k for k in d.keys()]) - set(
        packed_mc_from_file().keys()) == set()

    return d


def pid_algorithms():
    algs = reco_unpackers()
    return collections.OrderedDict(
        [('RichPIDs',
          Algorithm(
              ChargedProtoParticleAddRichInfo,
              InputRichPIDLocation=algs['RichPIDs'].OutputName,
              ProtoParticleLocation=algs['ChargedProtos'].OutputName)),
         ('MuonPIDs',
          Algorithm(
              ChargedProtoParticleAddMuonInfo,
              InputMuonPIDLocation=algs['MuonPIDs'].OutputName,
              ProtoParticleLocation=algs['ChargedProtos'].OutputName)),
         ('CombinedPIDs',
          Algorithm(
              ChargedProtoCombineDLLsAlg,
              ProtoParticleLocation=algs['ChargedProtos'].OutputName))])
