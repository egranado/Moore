###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Initial versions of 2- and 3-body topological lines.

MVAs trained with VLoose HLT1 configuration, working points tuned by eye.

Functions `make_filtered_topo_twobody` and `make_unfiltered_topo_twobody`
return two body combinations with and without an MVA cut, for use in other
selections.
"""
from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder
from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from ...algorithms import ParticleCombinerWithPVs, ParticleFilterWithPVs, ParticleFilterWithTMVA, require_all
from ...framework import configurable
from ...standard_particles import make_has_rich_long_pions

all_lines = {}


@configurable
def make_input_particles(make_particles=make_has_rich_long_pions,
                         make_pvs=make_pvs,
                         trchi2_max=3,
                         mipchi2_min=4,
                         pt_min=0 * MeV,
                         p_min=0 * GeV):
    """Return particles used as input to all topo combiners."""
    code = require_all('PT > {pt_min}', 'P > {p_min}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           trchi2_max=trchi2_max,
                           mipchi2_min=mipchi2_min)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_two_body_particles(particles,
                            descriptors,
                            pvs,
                            amaxchild_pt_min=1500 * MeV,
                            apt_min=1000 * MeV,
                            amindoca_max=1 * mm,
                            vchi2pdof_max=10,
                            bpvvdchi2_min=16,
                            dira_min=0):
    """Return two-body vertices filtered by some rectangular cuts.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to use in the combination.
    descriptors : list of str
        Decay descriptors used to define the decay tree.
    pvs : DataHandle
        Container of primary vertices.
    """
    combination_code = require_all("AMAXCHILD(PT) > {amaxchild_pt_min}",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       amaxchild_pt_min=amaxchild_pt_min,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {dira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  dira_min=dira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_three_body_particles(particles,
                              descriptors,
                              pvs,
                              amaxchild_pt_min=1500 * MeV,
                              apt_min=2000 * MeV,
                              amindoca_max=1 * mm,
                              bpvvdchi2_min=16,
                              dira_min=0):
    """Return three-body vertices filtered by some rectangular cuts.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to use in the combination.
    descriptors : list of str
        Decay descriptors used to define the decay tree.
    pvs : DataHandle
        Container of primary vertices.
    """
    combination_code = require_all("AMAXCHILD(PT) > {amaxchild_pt_min}",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       amaxchild_pt_min=amaxchild_pt_min,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    vertex_code = require_all("BPVVALID()", "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVDIRA() > {dira_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  dira_min=dira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_filtered_two_body_particles(particles, make_pvs=make_pvs, MVACut=0.1):
    """Return two-body vertices filtered a BDT decision threshold.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to filter, holding two-body vertices.
    """
    weights_path = "$PARAMFILESROOT/data/Hlt2Topo2Body_BDTParams_Run3.xml"
    bdt_vars = {
        "TwoBody_DIRA_OWNPV":
        "BPVDIRA()",
        "TwoBody_DOCAMAX":
        "PFUNA(ADOCAMAX(''))",
        "log(TwoBody_ENDVERTEX_CHI2)":
        "math.log(VFASPF(VCHI2/VDOF))",
        "log(TwoBody_FDCHI2_OWNPV)":
        "math.log(BPVVDCHI2())",
        "1.*(TwoBody_M>2000)+1.*(TwoBody_M>4000)":
        "switch( (M>2000),1.,0.) +switch( (M>4000),1.,0.)",
        "log(TwoBody_MINIPCHI2)":
        "math.log(MIPCHI2DV(PRIMARY))",
        "TwoBody_Mcorr":
        "BPVCORRM()",
        "TwoBody_PT":
        "PT",
        "log(Track1_MINIPCHI2)":
        "math.log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "Track1_PT":
        "CHILD(PT,1)",
        "log(Track2_MINIPCHI2)":
        "math.log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "Track2_PT":
        "CHILD(PT,2)",
    }
    code = require_all(
        # Ensure no unphysical candidates are evaluated in the MVA
        # See Moore#167 for discussion
        "M > 0",
        "VALUE('LoKi::Hybrid::DictValue/TwoBodyMVA')>{MVACutTwoBody}").format(
            MVACutTwoBody=MVACut)
    pvs = make_pvs()
    return ParticleFilterWithTMVA(
        name="topo_two_body_mva_filter",
        particles=particles,
        pvs=pvs,
        mva_code=code,
        mva_name="TwoBodyMVA",
        xml_file=weights_path,
        bdt_vars=bdt_vars)


@configurable
def make_filtered_three_body_particles(particles,
                                       make_pvs=make_pvs,
                                       MVACut=0.1):
    """Return three-body vertices filtered a BDT decision threshold.

    Parameters
    ----------
    particles : DataHandle
        Input Particle container to filter, holding three-body vertices.
    """
    weights_path = "$PARAMFILESROOT/data/Hlt2Topo3Body_BDTParams_Run3.xml"
    bdt_vars = {
        "TwoBody_DIRA_OWNPV":
        "CHILD(BPVDIRA(),1)",
        "TwoBody_DOCAMAX":
        "CHILD(PFUNA(ADOCAMAX('')),1)",
        "log(TwoBody_ENDVERTEX_CHI2)":
        "math.log(CHILD(VFASPF(VCHI2/VDOF),1))",
        "log(TwoBody_FDCHI2_OWNPV)":
        "math.log(CHILD(BPVVDCHI2(),1))",
        "1.*(TwoBody_M>2000)+1.*(TwoBody_M>4000)":
        "switch( (CHILD(M,1)>2000),1.,0.) +switch( (CHILD(M,1)>4000),1.,0.)",
        "log(TwoBody_MINIPCHI2)":
        "math.log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "TwoBody_Mcorr":
        "CHILD(BPVCORRM(),1)",
        "TwoBody_PT":
        "CHILD(PT,1)",
        "log(Track1_MINIPCHI2)":
        "math.log(CHILD(CHILD(MIPCHI2DV(PRIMARY),1),1))",
        "Track1_PT":
        "CHILD(CHILD(PT,1),1)",
        "log(Track2_MINIPCHI2)":
        "math.log(CHILD(CHILD(MIPCHI2DV(PRIMARY),2),1))",
        "Track2_PT":
        "CHILD(CHILD(PT,2),1)",
        "log(1.-ThreeBody_DIRA_OWNPV)":
        "math.log(1.-BPVDIRA())",
        "ThreeBody_DOCAMAX":
        "PFUNA(ADOCAMAX(''))",
        "log(ThreeBody_ENDVERTEX_CHI2)":
        "math.log(VFASPF(VCHI2/VDOF))",
        "log(ThreeBody_FDCHI2_OWNPV)":
        "math.log(BPVVDCHI2())",
        "1.*(ThreeBody_M>2000)+1.*(ThreeBody_M>4000)":
        "switch( (M>2000),1.,0.) +switch( (M>4000),1.,0.)",
        "log(ThreeBody_MINIPCHI2)":
        "math.log(MIPCHI2DV(PRIMARY))",
        "ThreeBody_Mcorr":
        "BPVCORRM()",
        "ThreeBody_PT":
        "PT",
        "log(TrackBachelor_MINIPCHI2)":
        "math.log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "TrackBachelor_PT":
        "CHILD(PT,2)",
    }
    code = require_all(
        # Ensure no unphysical candidates are evaluated in the MVA
        # See Moore#167 for discussion
        "M > 0",
        "VALUE('LoKi::Hybrid::DictValue/ThreeBodyMVA')>{MVACutThreeBody}"
    ).format(MVACutThreeBody=MVACut)
    pvs = make_pvs()
    return ParticleFilterWithTMVA(
        name="topo_three_body_mva_filter",
        particles=particles,
        pvs=pvs,
        mva_code=code,
        mva_name="ThreeBodyMVA",
        xml_file=weights_path,
        bdt_vars=bdt_vars)


def line_prefilters():
    """Control flow filters common to all topological lines."""
    return [require_pvs(make_pvs())]


def make_unfiltered_topo_twobody(make_particles=make_input_particles,
                                 make_pvs=make_pvs):
    """Return two-body candidates used by the topological lines."""
    pvs = make_pvs()
    pions = make_particles()
    two_bodys = make_two_body_particles(
        particles=pions,
        descriptors=['B0 -> pi- pi+', 'B0 -> pi+ pi+', 'B0 -> pi- pi-'],
        pvs=pvs)
    return two_bodys


def make_filtered_topo_twobody():
    """Return two-body candidates used by the topological lines, filtered by the MVA."""
    two_bodys = make_unfiltered_topo_twobody()
    filtered = make_filtered_two_body_particles(particles=two_bodys)
    return filtered


def make_unfiltered_topo_threebody(make_particles=make_input_particles,
                                   make_pvs=make_pvs):
    """Return three-body candidates used by the topological lines."""
    pions = make_input_particles()
    pvs = make_pvs()
    # Explicitly pass the same makers we use for the 2+1 combination
    two_body = make_unfiltered_topo_twobody(
        make_particles=make_input_particles, make_pvs=make_pvs)
    three_body = make_three_body_particles(
        particles=[two_body, pions],
        descriptors=['B_s0 -> B0 pi+', 'B_s0 -> B0 pi-'],
        pvs=pvs)
    return three_body


def make_filtered_topo_threebody():
    """Return three-body candidates used by the topological lines, filtered by the MVA."""
    three_body = make_unfiltered_topo_threebody()
    filtered = make_filtered_three_body_particles(particles=three_body)
    return filtered


@register_line_builder(all_lines)
@configurable
def twobody_line(name='Hlt2Topo2BodyLine', prescale=1):
    candidates = make_filtered_topo_twobody()
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def threebody_line(name='Hlt2Topo3BodyLine', prescale=1):
    candidates = make_filtered_topo_threebody()
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
    )
