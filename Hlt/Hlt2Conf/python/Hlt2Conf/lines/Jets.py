###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
First version of Jet lines
The missing steps are summarised in the HltJetBuilderRun3, HltParticleFlowRun3 code
"""

from __future__ import absolute_import, division, print_function
import string, math

from GaudiKernel.SystemOfUnits import GeV

from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.reconstruction_objects import make_charged_protoparticles, reconstruction

from ..algorithms import require_all, ParticleFilter, ParticleCombiner
from ..framework import configurable
from PyConf.Algorithms import (ParticleMakerForParticleFlow, HltJetBuilderRun3,
                               HltParticleFlowRun3)

from PyConf.Tools import (LoKi__Hybrid__ProtoParticleFilter as
                          ProtoParticleFilter, LoKi__FastJetMaker as FastJet)

from Moore.config import HltLine, register_line_builder

from ..hacks import patched_hybrid_tool
from ..standard_particles import (
    make_photons,
    make_KsLL,
    make_KsDD,
    make_LambdaLL,
    make_merged_pi0s,
    make_resolved_pi0s,
)

from ..standard_particles import (
    get_long_track_selector,
    get_down_track_selector,
    standard_protoparticle_filter,
)

all_lines = {}


def get_fastjet():
    return FastJet(
        PtMin=5000,  #  FastJet: min pT
        RParameter=0.5,  #  FastJet: cone size
        Sort=2,  #  FastJet: sort by pt
        Strategy=1,  #  FastJet: strategy: 0 N3Dumb, 1 Best, 2 NlnN, ...,
        Type=2,  #  FastJet: anti-kt code:  0 kt, 1 cambridge, 2 anti-kt, ...
        Recombination=0,  #  FastJet: scheme : 0 E, 1 pT, 2 pT^2, ...
        JetID=98,  # LHCb: Jet PID number
    )


def build_jets(pflow):
    return HltJetBuilderRun3(Input=pflow, FastJet=get_fastjet()).Output


@configurable
def make_jets(pflow, pt_min=10 * GeV):
    jets = build_jets(pflow)

    code = require_all("ABSID == 'CELLjet'",
                       "PT > {pt_min}".format(pt_min=pt_min))

    return ParticleFilter(jets, Code=code)


@configurable
def make_dijets(jets, pt_min=17 * GeV, dphi=0.0):
    """Make two-jet combinations"""

    descriptors = ["CLUSjet -> CELLjet CELLjet"]

    combination_code = require_all(
        "abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > {dphi}".format(dphi=dphi))

    return ParticleCombiner(
        particles=[jets],
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut="ALL",
        ParticleCombiners={'': 'ParticleAdder'})


@configurable
def _make_pf_particles(
        make_protoparticles=make_charged_protoparticles,
        get_track_selector=get_long_track_selector,
        make_protoparticle_filter=standard_protoparticle_filter):
    chargedProtos = make_protoparticles()
    return ParticleMakerForParticleFlow(
        InputProtoParticles=chargedProtos,
        TrackSelector=get_track_selector(),
        ProtoParticleFilter=make_protoparticle_filter()).Output


@configurable
def make_particleflow():
    return HltParticleFlowRun3(Inputs=[
        _make_pf_particles(get_track_selector=get_long_track_selector),
        _make_pf_particles(get_track_selector=get_down_track_selector),
        make_KsDD(),
        make_KsLL(),
        make_LambdaLL(),
        make_merged_pi0s(),
        ParticleFilter(
            make_resolved_pi0s(), Code='ALL', CloneFilteredParticles=True),
        # FIXME use just make_resolved_pi0s() above
        # The HltParticleFlowRun3 MergingTransformer expects inputs of type
        # "Particles" (KeyedContainer<Particle, ...>) but make_resolved_pi0s()
        # produces a "Selection" (SharedObjectsContainer<Particle>), which
        # is not accepted. Instead, explicitly produce Particles.
        make_photons(),
    ]).Output


@register_line_builder(all_lines)
@configurable
def jetpt10_func_line(name='Hlt2JetsPt10Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=10 * GeV)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [jets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def jetpt20_func_line(name='Hlt2JetsPt20Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=20 * GeV)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [jets],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dijetpt10_func_line(name='Hlt2DiJetsPt10Line', prescale=1):
    pflow = make_particleflow()
    jets = make_jets(pflow=pflow, pt_min=10 * GeV)
    dijets = make_dijets(jets)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dijets],
        prescale=prescale,
    )
