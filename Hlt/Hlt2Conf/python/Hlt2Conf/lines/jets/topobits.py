###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Construct topo 2-body combinations as tag in put to HltJetBuilder
"""
from __future__ import absolute_import, division, print_function

from ...framework import configurable
from ...algorithms import (require_all, ParticleFilterWithPVs,
                           ParticleCombinerWithPVs)
from ...standard_particles import (make_has_rich_long_kaons, make_KsLL,
                                   make_KsDD, make_LambdaLL, make_LambdaDD)
from RecoConf.reconstruction_objects import make_pvs

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond


@configurable
def make_topo_kaons(particles,
                    pvs,
                    pt_min=200 * MeV,
                    p_min=3000 * MeV,
                    trchi2dof_max=3.0,
                    ipchi2_min=4.0):  # {
    """Filter kaons for input to Topo 2-body combinations.

    The Run 2 Topo lines appear to have configured everything through a
    'Common' settings dictionary.  The relevant values from Topo_pp_2018.py
    appear to be
            'TRK_PT_MIN'        : 200 * MeV,
            'TRK_P_MIN'         : 3000 * MeV,
            'TRK_CHI2_MAX'      : 3,
            'TRK_IPCHI2_MIN'    : 4,
    """
    code = require_all("PT > {pt_min}", "P > {p_min}",
                       "TRCHI2DOF < {trchi2dof_max}",
                       "MIPCHI2DV(PRIMARY) > {ipchi2_min}").format(
                           pt_min=pt_min,
                           p_min=p_min,
                           trchi2dof_max=trchi2dof_max,
                           ipchi2_min=ipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


# }


@configurable
def make_topo_v0s(particles, pvs, bpvltime_min=0.0 * picosecond):  # {
    """Filter V0s for input to Topo 2-body combinations.

    The Run 2 Topo lines appear to have configured everything through a
    'Common' settings dictionary.  The relevant values from Topo_pp_2018.py
    appear to be
            'TRK_LT_MIN'        : 0 * picosecond,
    This is an unnecessary pass-through for any standard V0 with a
    displacement cut.
    """
    ## TODO:  Eliminate this step if it remains redundant
    code = require_all("BPVLTIME('') > {bpvltime_min}").format(
        bpvltime_min=bpvltime_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


# }


def make_topo_ksll():
    return make_topo_v0s(make_KsLL(), make_pvs())


def make_topo_ksdd():
    return make_topo_v0s(make_KsDD(), make_pvs())


def make_topo_lambdall():
    return make_topo_v0s(make_LambdaLL(), make_pvs())


def make_topo_lambdadd():
    return make_topo_v0s(make_LambdaDD(), make_pvs())


@configurable
def make_topo_2body(apt_min=2000 * MeV,
                    adocachi2_max=1000,
                    am_max=10 * GeV,
                    an_ipchi2lt16_max=2,
                    bpvvdchi2_min=16,
                    bpveta_min=2.0,
                    bpveta_max=5.0):  # {
    """Make Topo 2-body candidates used as tagging input to jet builder.

    The Run 2 Topo lines appear to have configured everything through a
    'Common' settings dictionary.  The relevant values from Topo_pp_2018.py
    appear to be
            'CMB_PRT_PT_MIN'    : 2000 * MeV,
            'CMB_VRT_CHI2_MAX'  : 1000,
            'CMB_VRT_MCOR_MAX'  : 10000 * MeV
            'CMB_TRK_NLT16_MAX' : 2,
            'CMB_VRT_VDCHI2_MIN': 16,
            'CMB_VRT_ETA_MIN'   : 2,
            'CMB_VRT_ETA_MAX'   : 5,
    """
    kaons = make_topo_kaons(make_has_rich_long_kaons(), make_pvs())
    ksll = make_topo_ksll()
    ksdd = make_topo_ksdd()
    lambdall = make_topo_lambdall()
    lambdadd = make_topo_lambdadd()
    pids = ['K+', 'K-', 'KS0', 'Lambda0', 'Lambda~0']
    comboskp = ['K*(892)0 -> K+ ' + pid for pid in pids]
    comboskm = ['K*(892)0 -> K- ' + pid for pid in pids[1:]]
    descriptors = comboskp + comboskm
    combination_code = require_all(
        "APT > {apt_min}", "ACUTDOCACHI2({adocachi2_max}, '')",
        "AM < {am_max}",
        "ANUM((ABSID=='K+') & (MIPCHI2DV(PRIMARY) < 16)) < {an_ipchi2lt16_max}"
    ).format(
        apt_min=apt_min,
        adocachi2_max=adocachi2_max,
        am_max=am_max,
        an_ipchi2lt16_max=an_ipchi2lt16_max)
    vertex_code = require_all(
        "HASVERTEX", "CHI2VXNDOF < {adocachi2_max}",
        "BPVVDCHI2() > {bpvvdchi2_min}",
        "in_range({bpveta_min}, BPVETA(), {bpveta_max})").format(
            adocachi2_max=adocachi2_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpveta_min=bpveta_min,
            bpveta_max=bpveta_max)
    return ParticleCombinerWithPVs(
        particles=[kaons, ksll, ksdd, lambdall, lambdadd],
        pvs=make_pvs(),
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# }
