###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Run 3 versions of the Run 2 jet lines

The ultimate goal is to create a line that does the same thing as the previous
suite of Hlt2Lines/Jets lines.
"""
from __future__ import absolute_import, division, print_function

from ...framework import configurable
from ...algorithms import (require_all, ParticleFilter, ParticleFilterWithPVs,
                           ParticleCombiner)
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from RecoConf.reconstruction_objects import reconstruction
from .algorithms import ParticleFlow, JetBuilder
from .topobits import make_topo_2body
from ...standard_particles import make_long_muons

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV

all_lines = {}


@configurable
def make_sv_for_jb(particles,
                   pvs,
                   prod_pt_min=500 * MeV,
                   prod_ghostprob_max=0.2,
                   prod_ipchi2_min=16,
                   chi2pdof_max=10,
                   bpvvdchi2_min=25):  # {
    """Filter two-body secondary vertexes as tag input or HltJetBuilder

    The relevant keys from the Jets_pp_2018.py configuration appear to be
            'SV_TRK_PT'    : 500*MeV,
            'GHOSTPROB'    : 0.2,
            'SV_TRK_IPCHI2': 16,
            'SV_VCHI2'     : 10,
            'SV_FDCHI2'    : 25,
    """
    code = require_all(
        "MINTREE(ALL,PT) > {prod_pt_min}",
        "MINTREE(ISBASIC,TRGHOSTPROB) < {prod_ghostprob_max}",
        "MINTREE((ABSID=='K+'),MIPCHI2DV(PRIMARY)) > {prod_ipchi2_min}",
        "HASVERTEX", "CHI2VXNDOF < {chi2pdof_max}",
        "BPVVDCHI2() > {bpvvdchi2_min}").format(
            prod_pt_min=prod_pt_min,
            prod_ghostprob_max=prod_ghostprob_max,
            prod_ipchi2_min=prod_ipchi2_min,
            chi2pdof_max=chi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


# }


@configurable
def make_muons_for_jb(particles,
                      pt_min=1000 * MeV,
                      probnnmu_min=0.5,
                      ghostprob_max=0.2):  # {
    """Filter muons as tag input or HltJetBuilder

    The relevant keys from the Jets_pp_2018.py configuration appear to be
            'MU_PT'        : 1000*MeV,
            'MU_PROBNNMU'  : 0.5,
            'GHOSTPROB'    : 0.2,
    """
    code = require_all("PT > {pt_min}", "PROBNNmu > {probnnmu_min}",
                       "TRGHOSTPROB < {ghostprob_max}").format(
                           pt_min=pt_min,
                           probnnmu_min=probnnmu_min,
                           ghostprob_max=ghostprob_max,
                       )
    return ParticleFilter(particles, Code=code)


# }


@configurable
def make_jetbuilder(inputs,
                    inputtags,
                    jetptmin=5 * GeV,
                    jetinfo=False,
                    jetecpath=''):  # {
    """Configuration wrapper for JetBuilder

    Meant to emulate the configuration of the Run 2 line.  The Run 2
    Jets_pp_2018 configuration dictionary was
        'JetBuilder': {
            'Reco'      : 'TURBO',
            'JetPtMin'  : 5*GeV,
            'JetInfo'   : False,
            'JetEcPath' : ''
            },
    The 'Reco' element is not a property of the JetBuilder but rather used to
    select a set of inputs for the ParticleFlow algorithm.  That functionality
    has not been reproduced; only the configuration of the properties of the
    JetBuilder have been retained.
    """
    return JetBuilder(
        inputs=inputs,
        inputtags=inputtags,
        JetPtMin=jetptmin,
        JetInfo=jetinfo,
        JetEcPath=jetecpath)


# }


@configurable
def make_particleflow_forjets():
    ## TODO:  restore neutral protoparticles when CaloClusters can be remade
    inprots = [
        reconstruction()['ChargedProtos'],
        #reconstruction()['NeutralProtos']
    ]
    return ParticleFlow(inProts=inprots)


@configurable
def make_jets():
    pflow = make_particleflow_forjets()
    mutags = make_muons_for_jb(make_long_muons())
    svtags = make_sv_for_jb(make_topo_2body(), make_pvs())
    return make_jetbuilder(inputs=[pflow], inputtags=[mutags, svtags])


@configurable
def filter_jets_by_tag(pt_min=17 * GeV, tagtype=None):  # {
    """Filter containers of jets as final line output

    Filtering by a specific tag type relies on information added to the
    ExtraInfo by HltJetBuilder.
    """
    jets = make_jets()
    tags = {'SV': 9600, 'Mu': 9601, 'DH': 9602}
    tagFilter = "ALL"
    if tagtype: tagFilter = "INFO({tag}, -1) != -1".format(tag=tags[tagtype])

    code = require_all("ABSID == 'CELLjet'", "PT > {pt_min}",
                       tagFilter).format(pt_min=pt_min)
    return ParticleFilter(jets, Code=code)


# }


@configurable
def make_dijets_by_tagpair(
        tagpair=(None, None), prod_pt_min=17 * GeV, dphi=0.0):  # {
    """Make two-jet combinations
    """
    jets = filter_jets_by_tag(pt_min=prod_pt_min, tagtype=None)
    descriptors = ["CLUSjet -> CELLjet CELLjet"]
    tags = {'SV': 9600, 'Mu': 9601, 'DH': 9602}
    tagfilt0 = 'AALL'
    tagfilt1 = 'AALL'
    if tagpair[0]:
        tagfilt0 = "ACHILD(INFO({tag}, -1), 1) != -1".format(
            tag=tags[tagpair[0]])
    if tagpair[1]:
        tagfilt1 = "ACHILD(INFO({tag}, -1), 2) != -1".format(
            tag=tags[tagpair[1]])

    combination_code = require_all(
        tagfilt0, tagfilt1,
        "abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > {dphi}").format(dphi=dphi)

    return ParticleCombiner(
        particles=[jets],
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut="ALL",
        ParticleCombiners={'': 'ParticleAdder'})


# }


@configurable
def make_trijets(nsvjets_min=0, prod_pt_min=30 * GeV, dphi=0.0):  # {
    """Make Trijet combinations"""
    jets = filter_jets_by_tag(pt_min=prod_pt_min, tagtype=None)
    descriptors = ["CLUSjet -> CELLjet CELLjet CELLjet"]
    combination_code = require_all(
        "ANUM(INFO(9600, -1) != -1) >= {nsvjets_min}",
        "abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > {dphi}",
        "abs(ACHILD(PHI,1) - ACHILD(PHI,3)) > {dphi}",
        "abs(ACHILD(PHI,2) - ACHILD(PHI,3)) > {dphi}").format(
            nsvjets_min=nsvjets_min, dphi=dphi)

    return ParticleCombiner(
        particles=[jets],
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut="ALL",
        ParticleCombiners={'': 'ParticleAdder'})


# }


@configurable
def filter_displacedjets_by_tag(pt_min=17 * GeV,
                                tagtype='SV',
                                ndisplong_min=5,
                                prod_ipchi2_min=25,
                                pt_w_doca_min=0.2):  # {
    """Filter containers of jets with displacement cuts as final line output

    Filtering by a specific tag type relies on information added to the
    ExtraInfo by HltJetBuilder.
    """
    jets = make_jets()
    pvs = make_pvs()
    tags = {'SV': 9600, 'Mu': 9601, 'DH': 9602}
    tagFilter = "ALL"
    if tagtype: tagFilter = "INFO({tag}, -1) != -1".format(tag=tags[tagtype])

    code = require_all(
        "ABSID == 'CELLjet'", "PT > {pt_min}", tagFilter,
        "NINTREE(ISBASIC & HASTRACK & ISLONG & ({prod_ipchi2_min} < MIPCHI2DV(PRIMARY))) > {ndisplong_min}",
        "(SUMTREE(abs(BEAMLINEDOCA())*PT, ISBASIC & HASTRACK & ISLONG) / SUMTREE(PT, ISBASIC & HASTRACK & ISLONG)) > {pt_w_doca_min}"
    ).format(
        pt_min=pt_min,
        ndisplong_min=ndisplong_min,
        prod_ipchi2_min=prod_ipchi2_min,
        pt_w_doca_min=pt_w_doca_min)
    return ParticleFilterWithPVs(jets, pvs, Code=code)


# }


## Finaly, make the lines.
@register_line_builder(all_lines)
@configurable
def jetlowpt_line(name='Hlt2JetsJetLowPtLine', prescale=1e-04):
    jets = filter_jets_by_tag(pt_min=10 * GeV)
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def jetsv_line(name='Hlt2JetsJetSVLine', prescale=4.6e-03):
    jets = filter_jets_by_tag(tagtype='SV')
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsv_line(name='Hlt2JetsDiJetSVLine', prescale=2.2e-03):
    jets = make_dijets_by_tagpair(tagpair=('SV', None))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvsv_line(name='Hlt2JetsDiJetSVSVLine', prescale=1):
    jets = make_dijets_by_tagpair(tagpair=('SV', 'SV'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvmu_line(name='Hlt2JetsDiJetSVMuLine', prescale=1):
    jets = make_dijets_by_tagpair(tagpair=('SV', 'Mu'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetmumu_line(name='Hlt2JetsDiJetMuMuLine', prescale=1):
    jets = make_dijets_by_tagpair(tagpair=('Mu', 'Mu'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvlowpt_line(name='Hlt2JetsDiJetSVLowPtLine', prescale=4.1e-04):
    jets = make_dijets_by_tagpair(prod_pt_min=10 * GeV, tagpair=('SV', None))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvsvlowpt_line(name='Hlt2JetsDiJetSVSVLowPtLine', prescale=0.1):
    jets = make_dijets_by_tagpair(prod_pt_min=10 * GeV, tagpair=('SV', 'SV'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvmulowpt_line(name='Hlt2JetsDiJetSVMuLowPtLine', prescale=1.7e-02):
    jets = make_dijets_by_tagpair(prod_pt_min=10 * GeV, tagpair=('SV', 'Mu'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetmumulowpt_line(name='Hlt2JetsDiJetMuMuLowPtLine', prescale=1.7e-02):
    jets = make_dijets_by_tagpair(prod_pt_min=10 * GeV, tagpair=('Mu', 'Mu'))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def dijetsvhighpt_line(name='Hlt2JetsDiJetSVHighPtLine', prescale=1.0e-01):
    jets = make_dijets_by_tagpair(prod_pt_min=50 * GeV, tagpair=('SV', None))
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def trijet_line(name='Hlt2JetsTriJetLine', prescale=1):
    jets = make_trijets()
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def trijetsv_line(name='Hlt2JetsTriJetSVLine', prescale=1):
    jets = make_trijets(nsvjets_min=1, prod_pt_min=20 * GeV)
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def trijetsv_line(name='Hlt2JetsTriJetSVSVLine', prescale=1):
    jets = make_trijets(nsvjets_min=2, prod_pt_min=10 * GeV)
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


@register_line_builder(all_lines)
@configurable
def jetdispsv_line(name='Hlt2JetsJetDisplacedSVLine', prescale=1):
    jets = filter_displacedjets_by_tag()
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)


## ActiveLines from Jets_pp_2018.py that were not implemented
#         'Hlt2JetsDiJetDisplacedSVSVLowPt',
#         'Hlt2JetsSixJet'
