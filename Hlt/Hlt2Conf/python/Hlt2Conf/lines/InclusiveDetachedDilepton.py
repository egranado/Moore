###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive detached dilepton HLT2 lines.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV

from RecoConf.hlt1_tracking import require_pvs, require_gec
from Moore.config import HltLine, register_line_builder

from ..algorithms import require_all, ParticleFilter, ParticleCombiner, ParticleFilterWithTMVA, ParticleFilterWithPVs
from ..framework import configurable
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from ..standard_particles import make_detached_mumu, make_detached_ee, make_detached_mue, make_dimuon_base
from ..standard_particles import make_has_rich_long_pions
from Hlt2Conf.lines.b_to_open_charm.builders.basic_builder import make_selected_particles

all_lines = {}


# make sure we passed GEC and have PV in event
# Turn off GEC for now
def prefilters():
    #return [require_gec(), require_pvs(make_pvs())]
    return [require_pvs(make_pvs())]


#Take the standard detached dileptons, filter with an MVA and save the necessary related tracks
@configurable
def filter_dimuon(mvacut=0.1):

    dimuons = make_detached_mumu()
    pvs = make_pvs()

    filterdimuonmva_name = "DiMuonMVAFilter"

    MVACode = "VALUE('LoKi::Hybrid::DictValue/{{mva_name}}') > {mvacut}".format(
        mvacut=mvacut)
    XMLFile = "$PARAMFILESROOT/data/Upgrade_InclusiveDetachedDimuon_weights.xml"
    Key = "BDT"
    BDTVars = {
        "TwoBody_DIRA_OWNPV": "BPVDIRA()",
        "TwoBody_DOCAMAX": "PFUNA(ADOCAMAX(''))",
        "log(TwoBody_ENDVERTEX_CHI2)": "math.log(VFASPF(VCHI2/VDOF))",
        "log(TwoBody_FDCHI2_OWNPV)": "math.log(BPVVDCHI2())",
        "log(TwoBody_MINIPCHI2)": "math.log(MIPCHI2DV(PRIMARY))",
        "TwoBody_Mcorr": "BPVCORRM()",
        "TwoBody_PT": "PT",
        "log(Track1_MINIPCHI2)": "math.log(CHILD(MIPCHI2DV(PRIMARY),1))",
        "Track1_PT": "CHILD(PT,1)",
        "log(Track2_MINIPCHI2)": "math.log(CHILD(MIPCHI2DV(PRIMARY),2))",
        "Track2_PT": "CHILD(PT,2)",
    }
    MVA_name = "TwoBodyMVA"

    filterdimuonmva = ParticleFilterWithTMVA(
        name=filterdimuonmva_name,
        particles=dimuons,
        pvs=pvs,
        mva_code=MVACode,
        mva_name=MVA_name,
        xml_file=XMLFile,
        bdt_vars=BDTVars)

    return filterdimuonmva


@register_line_builder(all_lines)
def incldetdimuon_line(name="Hlt2InclDetDiMuonLine", prescale=1):

    dimuons = filter_dimuon()
    additional_tracks = make_selected_particles()

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [dimuons] +
        [additional_tracks],
        prescale=prescale,
    )
