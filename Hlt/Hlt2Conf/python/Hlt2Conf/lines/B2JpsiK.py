###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the prompt dimuon HLT2 lines
"""

from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from ..algorithms import require_all, ParticleFilterWithPVs, ParticleCombinerWithPVs
from ..framework import configurable

from Moore.config import HltLine, register_line_builder

# get the basic particles
from ..standard_particles import make_dimuon_base, make_has_rich_long_kaons

# get the dimuon filters
from .onia.builders.prefilters import make_prefilters
from .onia.builders.dimuon import make_detached_dimuon, make_jpsi

all_lines = {}


@configurable
def make_B2JpsiK(Jpsi,
                 kaons,
                 pvs,
                 massWind_B=200 * MeV,
                 maxVertexChi2=25,
                 lifetime=0.3 * picosecond):
    """Make the B --> Jpsi K candidate"""

    # mass window around the B mass
    combination_cut = require_all("ADAMASS('B+') < {massWind_B}").format(
        massWind_B=massWind_B)

    # require the Jpsi and K to come from the same vertex + lifetime cut
    mother_cut = require_all("VFASPF(VCHI2PDOF) < {maxVertexChi2}",
                             "BPVLTIME() > {lifetime}").format(
                                 maxVertexChi2=maxVertexChi2,
                                 lifetime=lifetime)

    return ParticleCombinerWithPVs(
        particles=[Jpsi, kaons],
        pvs=pvs,
        DecayDescriptors=["[B+ -> J/psi(1S) K+]cc"],
        CombinationCut=combination_cut,
        MotherCut=mother_cut)


###########
# Definition of the line
###########


@register_line_builder(all_lines)
@configurable
def B2JpsiK_line(name='Hlt2DiMuonBpToJpsiKpLine', prescale=1):
    """B+ --> Jpsi K+ line"""

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and require that they are detached
    detached_dimuons = make_detached_dimuon()

    # make the detached Jpsi candidates without pt requirements
    Jpsi = make_jpsi(
        #        name="JpsiMaker_B2JpsiK_line",
        minPt_Jpsi=0 * MeV)

    # get the basic kaons
    kaons = make_has_rich_long_kaons()

    # get the BJpsiK candidates
    B2JpsiK = make_B2JpsiK(Jpsi=Jpsi, kaons=kaons, pvs=pvs)

    return HltLine(
        name=name,
        algs=make_prefilters() + [B2JpsiK],
        prescale=prescale,
    )
