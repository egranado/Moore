###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Common prefilters used by all lines
"""
from __future__ import absolute_import, division, print_function

from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)
from Hlt2Conf.lines.topological_b import (make_filtered_topo_twobody,
                                          make_filtered_topo_threebody)


def b2oc_prefilters(require_GEC=False, require_topo=False):

    filters = upfront_reconstruction()
    if require_GEC:
        filters.append(require_gec())
    filters.append(require_pvs(make_pvs()))
    if require_topo:
        filters.append(topo_2or3body_node())
    return filters


def topo_2or3body_node():

    return CompositeNode(
        'Topo_2or3body',
        [make_filtered_topo_twobody(),
         make_filtered_topo_threebody()],
        combineLogic=NodeLogic.NONLAZY_OR)
