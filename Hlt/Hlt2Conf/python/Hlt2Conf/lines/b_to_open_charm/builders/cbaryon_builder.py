###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached charm baryon decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""
from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import (require_all, ParticleFilter, ParticleCombiner,
                                 ParticleCombinerWithPVs,
                                 ParticleFilterWithPVs)
from Hlt2Conf.framework import configurable

from . import basic_builder
from . import d_builder

###############################################################################
# Specific charm baryon decay builders, overrides default cuts where needed   #
###############################################################################


@configurable
def make_lc_to_pkpi(am_min=2236 * MeV, am_max=2336 * MeV, **decay_arguments):
    """
    Return a Lambda_c+ -> p K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    particles = [
        basic_builder.make_protons(),
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    descriptors = ["[Lambda_c+ -> p+ K- pi+]cc"]

    return basic_builder.make_threebody(
        particles,
        descriptors,
        name="B2OCLC2PKPICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xicp_to_pkpi(am_min=2418 * MeV, am_max=2518 * MeV, **decay_arguments):
    """
    Return a Xi_c+ -> p K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    particles = [
        basic_builder.make_protons(),
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    descriptors = ["[Xi_c+ -> p+ K- pi+]cc"]

    return basic_builder.make_threebody(
        particles,
        descriptors,
        name="B2OCXICP2PKPICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xic0_to_pkkpi(am_min=2421 * MeV, am_max=2521 * MeV,
                       **decay_arguments):
    """
    Return a Xi_c0 -> p K- K= pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    particles = [
        basic_builder.make_protons(),
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    descriptors = ["[Xi_c0 -> p+ K- K- pi+]cc"]

    return basic_builder.make_fourbody(
        particles,
        descriptors,
        name="B2OCXIC02PKKPICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_omegac0_to_pkkpi(am_min=2645 * MeV,
                          am_max=2745 * MeV,
                          **decay_arguments):
    """
    Return a Omega_c0 -> p K- K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    particles = [
        basic_builder.make_protons(),
        basic_builder.make_kaons(),
        basic_builder.make_pions()
    ]
    descriptors = ["[Omega_c0 -> p+ K- K- pi+]cc"]

    return basic_builder.make_fourbody(
        particles,
        descriptors,
        name="B2OCOMEGAC02PKKPICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xiccpp_to_xicppi(am_min=3571 * MeV,
                          am_max=3671 * MeV,
                          **decay_arguments):
    """
    Return a Xi_cc++ -> Xi_c+ pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->2body
                        maker
    """
    particles = [make_xicp_to_pkpi(), basic_builder.make_soft_bachelor_pions()]
    descriptors = ["[Xi_cc++ -> Xi_c+ pi+]cc"]

    return basic_builder.make_twobody(
        particles,
        descriptors,
        name="B2OCXICCPP2XICPPICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xiccp_to_xic0pi(am_min=3571 * MeV,
                         am_max=3671 * MeV,
                         **decay_arguments):
    """
    Return a Xi_cc+ -> Xi_c0 pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->2body
                        maker
    """
    particles = [
        make_xic0_to_pkkpi(),
        basic_builder.make_soft_bachelor_pions()
    ]
    descriptors = ["[Xi_cc+ -> Xi_c0 pi+]cc"]

    return basic_builder.make_twobody(
        particles,
        descriptors,
        name="B2OCXICCP2XIC0PICombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xiccpp_to_lcpkmpippip(am_min=3571 * MeV,
                               am_max=3671 * MeV,
                               **decay_arguments):
    """
    Return a Xi_cc++ -> Lambda_c+ K- pi+ pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    particles = [
        make_lc_to_pkpi(),
        basic_builder.make_soft_bachelor_kaons(),
        basic_builder.make_soft_bachelor_pions()
    ]
    descriptors = ["[Xi_cc++ -> Lambda_c+ K- pi+ pi+]cc"]

    return basic_builder.make_fourbody(
        particles,
        descriptors,
        name="B2OCXICCPP2LCPKMPIPPIPCombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xiccp_to_lcpkmpip(am_min=3571 * MeV,
                           am_max=3671 * MeV,
                           **decay_arguments):
    """
    Return a Xi_cc+ -> Lambda_c+ K- pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    particles = [
        make_lc_to_pkpi(),
        basic_builder.make_soft_bachelor_kaons(),
        basic_builder.make_soft_bachelor_pions()
    ]
    descriptors = ["[Xi_cc+ -> Lambda_c+ K- pi+]cc"]

    return basic_builder.make_threebody(
        particles,
        descriptors,
        name="B2OCXICCP2LCPKMPIPCombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_xiccp_to_dppk(am_min=3571 * MeV, am_max=3671 * MeV,
                       **decay_arguments):
    """
    Return a Xi_cc+ -> D+ p+ K- decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    particles = [
        d_builder.make_dplus_to_pippipkm(),
        basic_builder.make_soft_bachelor_kaons(),
        basic_builder.make_protons(),
    ]
    descriptors = ["[Xi_cc+ -> D+ p+ K-]cc"]

    return basic_builder.make_threebody(
        particles,
        descriptors,
        name="B2OCXICCP2DPPKCombiner",
        am_min=am_min,
        am_max=am_max,
        **decay_arguments)


@configurable
def make_tight_lc_to_pkpi(**decay_arguments):
    """ make lc with tighter P, PT, PID """
    lc = make_lc_to_pkpi(**decay_arguments)
    code = "(NINTREE( (ABSID=='pi+') & ((P<3*GeV) | (PT<0.3*GeV) | (PIDK>5))) == 0)"
    code = code + " & (NINTREE((ABSID=='K+')&((P<5*GeV) | (PT<0.5*GeV) | (PIDK<-5))) == 0)"
    code = code + " & (NINTREE((ABSID=='p+')&((P<8*GeV) | (PT<0.5*GeV) | (PIDp<-5))) == 0)"
    return ParticleFilter(particles=lc, Code=code)


@configurable
def make_tight_xicp_to_pkpi(**decay_arguments):
    """ make xicp with tighter P, PT, PID """
    xic = make_xicp_to_pkpi(**decay_arguments)
    code = "(NINTREE( (ABSID=='pi+') & ((P<3*GeV) | (PT<0.3*GeV) | (PIDK>5))) == 0)"
    code = code + " & (NINTREE((ABSID=='K+')&((P<5*GeV) | (PT<0.5*GeV) | (PIDK<-5))) == 0)"
    code = code + " & (NINTREE((ABSID=='p+')&((P<8*GeV) | (PT<0.5*GeV) | (PIDp<-5))) == 0)"
    return ParticleFilter(particles=xic, Code=code)


@configurable
def make_tight_xic0_to_pkkpi(**decay_arguments):
    """ make Xic0 with tighter P, PT, PID """
    xic = make_xic0_to_pkkpi(**decay_arguments)
    code = "(NINTREE((ABSID=='pi+')&((P<2*GeV) | (PT<0.25*GeV) | (PIDK>5))) ==0)"
    code = code + " & (NINTREE((ABSID=='K+')&((P<5*GeV) | (PT<0.5*GeV) | (PIDK<-5))) == 0)"
    code = code + " & (NINTREE((ABSID=='p+')&((P<8*GeV) | (PT<0.5*GeV) | (PIDp<-5))) == 0)"
    return ParticleFilter(particles=xic, Code=code)


@configurable
def make_tight_omegac0_to_pkkpi(**decay_arguments):
    """ make omegac0 with tighter P, PT, PID """
    omc = make_omegac0_to_pkkpi(**decay_arguments)
    code = "(NINTREE((ABSID=='pi+')&((P<2*GeV) | (PT<0.25*GeV) | (PIDK>5))) ==0)"
    code = code + " & (NINTREE((ABSID=='K+')&((P<5*GeV) | (PT<0.5*GeV) | (PIDK<-5))) == 0)"
    code = code + " & (NINTREE((ABSID=='p+')&((P<8*GeV) | (PT<0.5*GeV) | (PIDp<-5))) == 0)"
    return ParticleFilter(particles=omc, Code=code)
