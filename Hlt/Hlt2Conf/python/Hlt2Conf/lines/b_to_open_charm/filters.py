###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* B-meson BDT filters:
* _ b_bdt_filter: could be applied to ANY line
* _ b_to_dh_bdt_filter: to be applied to Dh lines (D0/D+/Ds+/... + pi/K/...)
* _ BDT .xml files are in https://gitlab.cern.ch/lhcb-datapkg/ParamFiles/data
"""
from __future__ import absolute_import, division, print_function

from Hlt2Conf.algorithms import require_all, ParticleFilterWithTMVA

import math


def b_bdt_filter(particles, pvs, MVACut=-0.2):
    BDTWeightsFile = "$PARAMFILESROOT/data/Hlt2B2OC_B_BDTParams_Run3.xml"
    BDTVars = {
        "log_B_PT": "math.log(PT)",
        "B_ETA": "ETA",
        "log_B_DIRA": "math.log(1.-BPVDIRA())",
        "log_B_ENDVERTEX_CHI2": "math.log(VFASPF(VCHI2/VDOF))",
        "log_B_IPCHI2_OWNPV": "math.log(BPVIPCHI2())",
        "log_B_IP_OWNPV": "math.log(BPVIP())"
    }
    code = require_all("VALUE('LoKi::Hybrid::DictValue/BeautyBDT')>{MVACut}"
                       ).format(MVACut=MVACut)
    return ParticleFilterWithTMVA(
        name="B2OCBBDTFilter",
        particles=particles,
        pvs=pvs,
        mva_code=code,
        mva_name="BeautyBDT",
        xml_file=BDTWeightsFile,
        bdt_vars=BDTVars)


def b_to_dh_bdt_filter(particles, pvs, MVACut=-0.2, with_neutrals=False):
    BDTWeightsFile = "$PARAMFILESROOT/data/Hlt2B2OC_B2DH_BDTParams_Run3.xml"
    if with_neutrals:
        # use as D the child of the child of the B: B -> D* (D neutal) h
        log_D_MINIPCHI2_var = "math.log(CHILD(CHILD(MIPCHI2DV(PRIMARY),1),1))"
    else:
        # use as D the direct child of the B: B -> D h
        log_D_MINIPCHI2_var = "math.log(CHILD(MIPCHI2DV(PRIMARY),1))"
    BDTVars = {
        "log_B_PT": "math.log(PT)",
        "B_ETA": "ETA",
        "log_B_DIRA": "math.log(1.-BPVDIRA())",
        "log_B_ENDVERTEX_CHI2": "math.log(VFASPF(VCHI2/VDOF))",
        "log_B_IPCHI2_OWNPV": "math.log(BPVIPCHI2())",
        "log_B_IP_OWNPV": "math.log(BPVIP())",
        "log_D_PT": "math.log(CHILD(PT,1))",
        "log_Bac_PT": "math.log(CHILD(PT,2))",
        "log_D_MINIPCHI2": log_D_MINIPCHI2_var,
        "log_Bac_MINIPCHI2": "math.log(CHILD(MIPCHI2DV(PRIMARY),2))"
    }
    code = require_all("VALUE('LoKi::Hybrid::DictValue/B2DHBDT')>{MVACut}"
                       ).format(MVACut=MVACut)
    return ParticleFilterWithTMVA(
        name="B2OCB2DHBDTFilter",
        particles=particles,
        pvs=pvs,
        mva_code=code,
        mva_name="B2DHBDT",
        xml_file=BDTWeightsFile,
        bdt_vars=BDTVars)


def b_to_d0hhh_bdt_filter(particles, pvs, MVACut=-0.9):
    BDTWeightsFile = "$PARAMFILESROOT/data/Hlt2B2OC_B2D0HHH_BDTGParams_Run3.xml"
    BDTVars = {
        "log_B_PT":
        "math.log(PT)",
        "B_ETA":
        "ETA",
        "log_B_DIRA_OWNPV":
        "math.log(1.0-BPVDIRA())",
        "log_B_ENDVERTEX_CHI2":
        "math.log(VFASPF(VCHI2/VDOF))",
        "log_B_MINIPCHI2":
        "math.log(BPVIPCHI2())",
        "log_B_IP_OWNPV":
        "math.log(BPVIP())",
        "log_D0_PT":
        "math.log(CHILD(PT,4))",
        "log_D0_MINIPCHI2":
        "math.log(CHILD(MIPCHI2DV(PRIMARY),4))",
        "log_D0_ENDVERTEX_CHI2":
        "math.log(CHILD(VFASPF(VCHI2/VDOF),4))",
        "log_Min_Bac_PT":
        "math.log(min(CHILD(PT,1),min(CHILD(PT,2),CHILD(PT,3))))",
        "log_Max_Bac_PT":
        "math.log(max(CHILD(PT,1),max(CHILD(PT,2),CHILD(PT,3))))",
        "log_Min_D0dau_PT":
        "math.log(min(CHILD(PT,4,1),CHILD(PT,4,2)))",
        "log_Max_D0dau_PT":
        "math.log(max(CHILD(PT,4,1),CHILD(PT,4,2)))",
        "log_Min_Bac_IPCHI2_OWNPV":
        "math.log(min(CHILD(MIPCHI2DV(PRIMARY),1),min(CHILD(MIPCHI2DV(PRIMARY),2),CHILD(MIPCHI2DV(PRIMARY),3))))",
        "log_Max_Bac_IPCHI2_OWNPV":
        "math.log(max(CHILD(MIPCHI2DV(PRIMARY),1),max(CHILD(MIPCHI2DV(PRIMARY),2),CHILD(MIPCHI2DV(PRIMARY),3))))",
        "log_Min_D0dau_IPCHI2_OWNPV":
        "math.log(min(CHILD(MIPCHI2DV(PRIMARY),4,1),CHILD(MIPCHI2DV(PRIMARY),4,2)))",
        "log_Max_D0dau_IPCHI2_OWNPV":
        "math.log(max(CHILD(MIPCHI2DV(PRIMARY),4,1),CHILD(MIPCHI2DV(PRIMARY),4,2)))"
    }
    code = require_all("VALUE('LoKi::Hybrid::DictValue/B2D0HHHBDT')>{MVACut}"
                       ).format(MVACut=MVACut)
    return ParticleFilterWithTMVA(
        name="B2OCB2D0HHHBDTFilter",
        particles=particles,
        pvs=pvs,
        mva_code=code,
        mva_name="B2D0HHHBDT",
        xml_file=BDTWeightsFile,
        bdt_vars=BDTVars)
