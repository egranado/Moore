###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
all_lines = {}


###########################################################
# Form the Xi_bc+ -> Lc+ D0,  Lc+ -> p K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToLcpD0_LcpToPKPi_D0ToKPi_line(
        name='Hlt2B2OC_XibcpToLcpD0_LcpToPKPi_D0ToKPi_Line', prescale=1):

    cmeson = d_builder.make_dzero_to_kpi()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, cmeson],
        descriptors=['Xi_bc+ -> Lambda_c+ D0', 'Xi_bc~- -> Lambda_c~- D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


######################################################################
# Form the Omega_bc0 -> Xi_c0 D0,  Xi_c0 -> p K K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
######################################################################
@register_line_builder(all_lines)
@configurable
def Ombc0ToXic0D0_Xic0ToPKKPi_D0ToKPi_line(
        name='Hlt2B2OC_Ombc0ToXic0D0_Xic0ToPKKPi_D0ToKPi_Line', prescale=1):

    cmeson = d_builder.make_dzero_to_kpi()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, cmeson],
        descriptors=['Omega_bc0 -> Xi_c0 D0', 'Omega_bc~0 -> Xi_c~0 D0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#########################################################################
# Form the Lambda_b0 -> Lambda_c+ D_s-,  Lambda_c+ -> pK- pi+, Ds->HHH
# Can we use only the CF Ds decay here?
#########################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpDsm_LcpToPKPi_DsmToHHH_line(
        name='Hlt2B2OC_LbToLcpDsm_LcpToPKPi_DsmToHHH_Line', prescale=1):
    cmeson = d_builder.make_dsplus_to_hhh()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson],
        descriptors=['[Lambda_b0 -> Lambda_c+ D_s-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#############################################################################
# Form the Lambda_b0 -> Lambda_c+ D-,  Lambda_c+ -> pK- pi+, D- -> K+ pi- pi-
#############################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpDm_LcpToPKPi_DmToPimPimKp_line(
        name='Hlt2B2OC_LbToLcpDm_LcpToPKPi_DmToPimPimKp_Line', prescale=1):
    cmeson = d_builder.make_dplus_to_pippipkm()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson],
        descriptors=['[Lambda_b0 -> Lambda_c+ D-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
