###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
all_lines = {}


###########################################################
# Form the Xi_bc0 -> Xi_c0 Pi- Pi+, Xi_c0 --> p K- K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToXic0PiPi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibcToXic0PiPi_Xic0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Xi_c0 pi- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> Lc+ K- Pi+, Lc+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToLcpKmPip_LcpToPKPi_line(
        name='Hlt2B2OC_XibcpToLcpKmPip_LcpToPKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_tightpid_bachelor_pions()
    bachelork = basic_builder.make_tightpid_bachelor_kaons()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelorpi, bachelork],
        descriptors=['[Xi_bc+ -> Lambda_c+ K- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> Xic+ Pi- Pi+, Xic+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToXicpPiPi_XicpToPKPi_line(
        name='Hlt2B2OC_XibcpToXicpPiPi_XicpToPKPi_Line', prescale=1):

    bachelor = basic_builder.make_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc+ -> Xi_c+ pi- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> Lc+ Pi- Pi+, Lc+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToLcpPiPi_LcpToPKPi_line(
        name='Hlt2B2OC_XibcpToLcpPiPi_LcpToPKPi_Line', prescale=1):

    bachelor = basic_builder.make_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc+ -> Lambda_c+ pi- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################################
# Form the Omega_bc0 -> Xi_c0 K- Pi+, Xi_c0 --> p K- K- pi+
##############################################################
@register_line_builder(all_lines)
@configurable
def Ombc0ToXic0KmPip_Xic0ToPKKPi_line(
        name='Hlt2B2OC_Ombc0ToXic0KmPip_Xic0ToPKKPi_Line', prescale=1):

    bachelork = basic_builder.make_tightpid_bachelor_kaons()
    bachelorpi = basic_builder.make_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Omega_bc0 -> Xi_c0 K- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
