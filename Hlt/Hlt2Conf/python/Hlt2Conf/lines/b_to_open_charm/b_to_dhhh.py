###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDhh lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import (require_all, ParticleFilter)

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

from Hlt2Conf.lines.b_to_open_charm.filters import b_to_d0hhh_bdt_filter

all_lines = {}


@register_line_builder(all_lines)
@configurable
def BuToD0PiPiPi_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0PiPiPi_D0ToKsLLHH_Line',
                                 prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+ pi- pi+', 'B- -> D0 pi+ pi- pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0PiPiPi_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0PiPiPi_D0ToKsDDHH_Line',
                                 prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+ pi- pi+', 'B- -> D0 pi+ pi- pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0KPiPi_D0ToKsLLHH_Line(name='Hlt2B2OC_BuToD0KPiPi_D0ToKsLLHH_Line',
                                prescale=1):
    bachelor_pions = basic_builder.make_bachelor_pions()
    bachelor_kaons = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor_pions, bachelor_kaons],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0KPiPi_D0ToKsDDHH_Line(name='Hlt2B2OC_BuToD0KPiPi_D0ToKsDDHH_Line',
                                prescale=1):
    bachelor_pions = basic_builder.make_bachelor_pions()
    bachelor_kaons = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor_pions, bachelor_kaons],
        descriptors=['B+ -> D0 K+ pi- pi+', 'B- -> D0 K- pi+ pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################################################
# Form the B+- -> D0 Pi Pi Pi and D0 K Pi Pi for ADS/GLW, all D0->hh modes needed
############################################################################################
@register_line_builder(all_lines)
@configurable
def BuToD0PiPiPi_D0ToHH_line(name='Hlt2B2OC_BuToD0PiPiPi_D0ToHH_Line',
                             MVACut=-0.7,
                             prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    d = d_builder.make_tight_dzero_to_hh()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi],
        descriptors=['B+ -> pi+ pi- pi+ D0', 'B- ->  pi- pi+ pi- D0'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    line_alg_filt = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_to_d0hhh_bdt_filter(line_alg_filt, pvs, MVACut)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0KPiPi_D0ToHH_line(name='Hlt2B2OC_BuToD0KPiPi_D0ToHH_Line',
                            MVACut=-0.7,
                            prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    d = d_builder.make_dzero_to_hh()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['B+ -> K+ pi- pi+ D0', 'B- -> K- pi+ pi- D0'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<-5), 1 ) <= {kpidfail})".format(kpidfail=0)
    line_alg_filt = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_to_d0hhh_bdt_filter(line_alg_filt, pvs, MVACut)

    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##################################################################################
# Form the B+- -> D0 KKPi, all D0->Kpi
###################################################################################
@register_line_builder(all_lines)
@configurable
def BuToD0KKPi_D0ToKPi_line(name='Hlt2B2OC_BuToD0KKPi_D0ToKPi_Line',
                            MVACut=-0.7,
                            prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    d = d_builder.make_dzero_to_kpi()
    #d = d_builder.make_dzero_to_hh()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['B+ -> K+ K- pi+ D0', 'B- -> K- K+ pi- D0'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<-5), 1 ) <= {kpidfail})".format(kpidfail=1)
    #line_alg = ParticleFilter(line, Code=track_code)
    line_alg_filt = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_to_d0hhh_bdt_filter(line_alg_filt, pvs, MVACut)

    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##################################################################################
# Form the B+- -> D0 Pbar P Pi,  only D0->KPi modes needed ?
###################################################################################
@register_line_builder(all_lines)
@configurable
def BuToD0PbarPPi_D0ToKPi_line(name='Hlt2B2OC_BuToD0PbarPPi_D0ToKPi_Line',
                               MVACut=-0.9,
                               prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    d = d_builder.make_dzero_to_kpi()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelorp],
        descriptors=['B+ -> p+ p~- pi+ D0', 'B- -> p~- p+ pi- D0'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    #track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<-5), 1 ) <= {ppidfail})".format(ppidfail=0)
    #line_alg = ParticleFilter(line, Code=track_code)
    line_alg_filt = ParticleFilter(line, Code=track_code)
    pvs = make_pvs()
    line_alg = b_to_d0hhh_bdt_filter(line_alg_filt, pvs, MVACut)

    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#####################################################
# Form the B0 -> D- PiPiPi, KPiPi, KKPi, and PbarPPi
#####################################################
@register_line_builder(all_lines)
@configurable
def BdToDmPiPiPi_DmToPimPimKp_line(
        name='Hlt2B2OC_BdToDmPiPiPi_DmToPimPimKp_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    d = d_builder.make_dplus_to_pippipkm()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi], descriptors=['[B0 -> pi+ pi- pi+ D-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmKPiPi_DmToPimPimKp_line(
        name='Hlt2B2OC_BdToDmKPiPi_DmToPimPimKp_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    d = d_builder.make_dplus_to_pippipkm()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['[B0 -> K+ pi- pi+ D-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmKKPi_DmToPimPimKp_line(name='Hlt2B2OC_BdToDmKKPi_DmToPimPimKp_Line',
                                 prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    d = d_builder.make_dplus_to_pippipkm()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['[B0 -> K+ K- pi+ D-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmPbarPPi_DmToPimPimKp_line(
        name='Hlt2B2OC_BdToDmPbarPPi_DmToPimPimKp_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    d = d_builder.make_dplus_to_pippipkm()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelorp],
        descriptors=['[B0 -> p+ p~- pi+ D-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


############################################################################################
# Form the B_d/s -> D- K- Pi+ Pi+ (Spectroscopy line)
############################################################################################
@register_line_builder(all_lines)
@configurable
def BdToDmKmPipPip_DmToPimPimKp_line(
        name='Hlt2B2OC_BdToDmKmPipPip_DmToPimPimKp_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    d = d_builder.make_dplus_to_pippipkm()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['[B0 -> K- pi+ pi+ D-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


############################################################################################
# Form the Bs -> Ds- PiPiPi, KPiPi, KKPi, and PbarPPi
# Only Ds -> KKPi
############################################################################################
@register_line_builder(all_lines)
@configurable
def BdToDsmPiPiPi_DsmToKmKpPim_line(
        name='Hlt2B2OC_BdToDsmPiPiPi_DsmToKmKpPim_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    d = d_builder.make_dsplus_to_kpkmpip()
    #d = d_builder.make_dsplus_to_hhh()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi], descriptors=['[B0 -> pi+ pi- pi+ D_s-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmKPiPi_DsmToKmKpPim_line(
        name='Hlt2B2OC_BdToDsmKPiPi_DsmToKmKpPim_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    #d = d_builder.make_dsplus_to_hhh()
    d = d_builder.make_dsplus_to_kpkmpip()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['[B0 -> K+ pi- pi+ D_s-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmKKPi_DsmToKmKpPim_line(
        name='Hlt2B2OC_BdToDsmKKPi_DsmToKmKpPim_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    #d = d_builder.make_dsplus_to_hhh()
    d = d_builder.make_dsplus_to_kpkmpip()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelork],
        descriptors=['[B0 -> K+ K- pi+ D_s-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmPbarPPi_DsmToKmKpPim_line(
        name='Hlt2B2OC_BdToDsmPbarPPi_DsmToKmKpPim_Line', prescale=1):
    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    #d = d_builder.make_dsplus_to_hhh()
    d = d_builder.make_dsplus_to_kpkmpip()
    line = b_builder.make_b2chhh(
        particles=[d, bachelorpi, bachelorp],
        descriptors=['[B0 -> p+ p~- pi+ D_s-]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


# TODO: WS equivalents
