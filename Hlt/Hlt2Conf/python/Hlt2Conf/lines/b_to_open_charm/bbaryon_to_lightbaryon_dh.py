###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.algorithms import (require_all, ParticleFilter)

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
all_lines = {}


###########################################################
# Form the Xi_bc0 -> D0 pK-,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi-
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToPD0K_D0ToKPi_line(name='Hlt2B2OC_Xibc0ToPD0K_D0ToKPi_Line',
                             prescale=1):

    bachelork = basic_builder.make_kaons(pid='PIDK > 0')
    bachelorp = basic_builder.make_tightpid_bachelor_protons(pid='PIDp > 0')
    cmeson = d_builder.make_tight_dzero_to_kpi()
    line_alg = b_builder.make_xibc(
        particles=[cmeson, bachelorp, bachelork],
        descriptors=['Xi_bc0 -> D0 p+ K-', 'Xi_bc~0 -> D0 p~- K+'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> D+ pK-,  D+ --> K- pi+ pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToPDpK_DpToKmPipPip_line(
        name='Hlt2B2OC_XibcpToPDpK_DpToKmPipPip_Line', prescale=1):

    bachelork = basic_builder.make_kaons(pid='PIDK > 0')
    bachelorp = basic_builder.make_tightpid_bachelor_protons(pid='PIDp > 0')
    cmeson = d_builder.make_dplus_to_pippipkm()
    line_alg = b_builder.make_xibc(
        particles=[cmeson, bachelorp, bachelork],
        descriptors=['[Xi_bc+ -> D+ p+ K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
