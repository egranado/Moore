###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDh lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters
from Hlt2Conf.lines.b_to_open_charm.filters import b_bdt_filter
from Hlt2Conf.lines.b_to_open_charm.filters import b_to_dh_bdt_filter

from Hlt2Conf.framework import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
all_lines = {}

##############################################
# From the BToDh_Builder
##############################################


@register_line_builder(all_lines)
@configurable
def BdToDsmK_DsmToHHH_line(name='Hlt2B2OC_BdToDsmK_DsmToHHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D_s- K+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmK_DsmToHHH_BDT_line(
        name='Hlt2B2OC_BdToDsmK_DsmToHHH_BDTGtm1d0_Line', MVACut=-1.,
        prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dsplus_to_hhh()
    b = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D_s- K+]cc'])
    pvs = make_pvs()
    line_alg = b_bdt_filter(b, pvs, MVACut)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmK_DsmToHHH_2or3bodyTOPO_line(
        name='Hlt2B2OC_BdToDsmK_DsmToHHH_2or3bodyTOPO_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D_s- K+]cc'])
    return HltLine(
        name=name,
        prescale=prescale,
        algs=b2oc_prefilters(require_topo=True) + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsmPi_DsmToHHH_line(name='Hlt2B2OC_BdToDsmPi_DsmToHHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D_s- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDmPi_DmToPimPimKp_line(name='Hlt2B2OC_BdToDmPi_DmToPimPimKp_Line',
                               MVACut=-0.2,
                               prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dplus_to_pippipkm()
    b = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B0 -> D- pi+]cc'])
    pvs = make_pvs()
    line_alg = b_to_dh_bdt_filter(b, pvs, MVACut)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsstmK_DsstmToDsmGamma_DsmToHHH_line(
        name='Hlt2B2OC_BdToDsstmK_DsstmToDsmGamma_DsmToHHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = b_builder.make_b2x(
        particles=[dsst, bachelor],
        descriptors=['B0 -> D*_s- K+', 'B0 -> D*_s+ K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDsstmPi_DsstmToDsmGamma_DsmToHHH_line(
        name='Hlt2B2OC_BdToDsstmPi_DsstmToDsmGamma_DsmToHHH_Line',
        MVACut=-1.,
        prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    b = b_builder.make_b2x(
        particles=[dsst, bachelor],
        descriptors=['B0 -> D*_s- pi+', 'B0 -> D*_s+ pi-'])
    pvs = make_pvs()
    line_alg = b_to_dh_bdt_filter(b, pvs, MVACut, with_neutrals=True)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# GLW/ADS lines
##############################################


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToHH_line(name='Hlt2B2OC_BuToD0K_D0ToHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# GGSZ lines
##############################################


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToKsLLHH_Line',
                             prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0Pi_D0ToKsDDHH_Line',
                             prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToKsLLHH_line(name='Hlt2B2OC_BuToD0K_D0ToKsLLHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_D0ToKsDDHH_line(name='Hlt2B2OC_BuToD0K_D0ToKsDDHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['B+ -> D0 K+', 'B- -> D0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# GGSZ lines for part reco D decays
##############################################
# Lines for partially reconstructed GGSZ lines with wider mass windows
# but (still to be implemented) tighter cuts


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_PartialD0ToKsLLHH_line(
        name='Hlt2B2OC_BuToD0Pi_PartialD0ToKsLLHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0Pi_PartialD0ToKsDDHH_line(
        name='Hlt2B2OC_BuToD0Pi_PartialD0ToKsDDHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 pi+', 'B- -> D0 pi-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_PartialD0ToKsLLHH_line(
        name='Hlt2B2OC_BuToD0K_PartialD0ToKsLLHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToD0K_PartialD0ToKsDDHH_line(
        name='Hlt2B2OC_BuToD0K_PartialD0ToKsDDHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), am_min=1000 * MeV)
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor],
        descriptors=['B+ -> D0 K+', 'B- -> D0 K-'],
        am_min=4500 * MeV)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# B->D*h GGSZ lines
##############################################


@register_line_builder(all_lines)
@configurable
def BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH_Line(
        name='Hlt2B2OC_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH_Line(
        name='Hlt2B2OC_BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH_Line(
        name='Hlt2B2OC_BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_merged_pi0s()
    dzerost = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH_Line(
        name='Hlt2B2OC_BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH_Line(
        name='Hlt2B2OC_BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 pi+', 'B- -> D*(2007)0 pi-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH_Line(
        name='Hlt2B2OC_BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dzerost = d_builder.make_dzerost_to_dzerogamma(dzero)

    line_alg = b_builder.make_b2x(
        particles=[dzerost, bachelor],
        descriptors=['B+ -> D*(2007)0 K+', 'B- -> D*(2007)0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
