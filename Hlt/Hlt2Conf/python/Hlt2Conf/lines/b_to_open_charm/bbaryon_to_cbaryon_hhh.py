###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.algorithms import (require_all, ParticleFilter)

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
all_lines = {}


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ Pi- Pi+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpPiPiPi_LcpToPKPi_line(name='Hlt2B2OC_LbToLcpPiPiPi_LcpToPKPi_Line',
                                 prescale=1):

    bachelor = basic_builder.make_soft_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line = b_builder.make_lb2lchhh(
        particles=[cbaryon, bachelor],
        descriptors=['[Lambda_b0 ->  pi- pi+ pi- Lambda_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ K- Pi+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpKPiPi_LcpToPKPi_line(name='Hlt2B2OC_LbToLcpKPiPi_LcpToPKPi_Line',
                                prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line = b_builder.make_lb2lchhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Lambda_b0 -> K- pi+ pi- Lambda_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)

    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ K- K+ Pi-, Lambda_c+ --> p K- pi+
#######################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpKKPi_LcpToPKPi_line(name='Hlt2B2OC_LbToLcpKKPi_LcpToPKPi_Line',
                               prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line = b_builder.make_lb2lchhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Lambda_b0 -> K- K+ pi- Lambda_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ p p~ Pi-, Lambda_c+ --> p K- pi+
######################################################################
@register_line_builder(all_lines)
@configurable
def LbToLcpPbarPPi_LcpToPKPi_line(
        name='Hlt2B2OC_LbToLcpPbarPPi_LcpToPKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    line = b_builder.make_lb2lchhh(
        particles=[cbaryon, bachelorp, bachelorpi],
        descriptors=['[Lambda_b0 -> p~- p+ pi- Lambda_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################################################


####################################################################
# Form the Xi_b0 -> Xi_c+ Pi- Pi+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpPiPiPi_XicpToPKPi_line(
        name='Hlt2B2OC_Xib0ToXicpPiPiPi_XicpToPKPi_Line', prescale=1):

    bachelor = basic_builder.make_soft_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_b0 -> pi- pi+ pi- Xi_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b0 -> Xi_c+ K- Pi+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpKPiPi_XicpToPKPi_line(
        name='Hlt2B2OC_Xib0ToXicpKPiPi_XicpToPKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Xi_b0 -> K- pi+ pi- Xi_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b0 -> Xi_c+ K- K+ Pi-, Xi_c+ --> p K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpKKPi_XicpToPKPi_line(
        name='Hlt2B2OC_Xib0ToXicpKKPi_XicpToPKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Xi_b0 -> K- K+ pi- Xi_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b0 -> Xi_c+ p p~ Pi-, Xi_c+ --> p K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpPbarPPi_XicpToPKPi_line(
        name='Hlt2B2OC_Xib0ToXicpPbarPPi_XicpToPKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelorp, bachelorpi],
        descriptors=['[Xi_b0 -> p~- p+ pi- Xi_c+]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################################################


####################################################################
# Form the Xi_b- -> Xi_c0 Pi- Pi+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0PiPiPi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibmToXic0PiPiPi_Xic0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_soft_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_b- -> pi- pi+ pi- Xi_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b- -> Xi_c0 K- Pi+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0KPiPi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibmToXic0KPiPi_Xic0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Xi_b- -> K- pi+ pi- Xi_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b- -> Xi_c0 K- K+ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0KKPi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibmToXic0KKPi_Xic0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Xi_b- -> K- K+ pi- Xi_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Xi_b- -> Xi_c0 p p~ Pi-, Xi_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0PbarPPi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibmToXic0PbarPPi_Xic0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi()
    line = b_builder.make_xib2xichhh(
        particles=[cbaryon, bachelorp, bachelorpi],
        descriptors=['[Xi_b- -> p~- p+ pi- Xi_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################################################


####################################################################
# Form the Omega_b- -> Omega_c0 Pi- Pi+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0PiPiPi_Omc0ToPKKPi_line(
        name='Hlt2B2OC_OmbmToOmc0PiPiPi_Omc0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_soft_tightpid_bachelor_pions()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line = b_builder.make_omegab2omegachhh(
        particles=[cbaryon, bachelor],
        descriptors=['[Omega_b- -> pi- pi+ pi- Omega_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Omega_b- -> Omega_c0 K- Pi+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0KPiPi_Omc0ToPKKPi_line(
        name='Hlt2B2OC_OmbmToOmc0KPiPi_Omc0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons(pid='PIDK > 5')
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line = b_builder.make_omegab2omegachhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Omega_b- -> K- pi+ pi- Omega_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Omega_b- -> Omega_c0 K- K+ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0KKPi_Omc0ToPKKPi_line(
        name='Hlt2B2OC_OmbmToOmc0KKPi_Omc0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelork = basic_builder.make_kaons()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line = b_builder.make_omegab2omegachhh(
        particles=[cbaryon, bachelork, bachelorpi],
        descriptors=['[Omega_b- -> K- K+ pi- Omega_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='K+') & (PIDK<5), 1 ) <= {kpidfail})".format(
        kpidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


####################################################################
# Form the Omega_b- -> Omega_c0 p p~ Pi-, Omega_c0 --> p K- K- pi+
####################################################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0PbarPPi_Omc0ToPKKPi_line(
        name='Hlt2B2OC_OmbmToOmc0PbarPPi_Omc0ToPKKPi_Line', prescale=1):

    bachelorpi = basic_builder.make_soft_tightpid_bachelor_pions()
    bachelorp = basic_builder.make_protons()
    cbaryon = cbaryon_builder.make_tight_omegac0_to_pkkpi()
    line = b_builder.make_omegab2omegachhh(
        particles=[cbaryon, bachelorp, bachelorpi],
        descriptors=['[Omega_b- -> p~- p+ pi- Omega_c0]cc'])
    track_code = "(NINGENERATION( ((ABSID=='pi+')|(ABSID=='K+')) & (PT<250*MeV), 1 ) <= {ntracks})".format(
        ntracks=1)
    track_code = track_code + "& (NINGENERATION( (ABSID=='p+') & (PIDp<5), 1 ) <= {ppidfail})".format(
        ppidfail=1)
    line_alg = ParticleFilter(line, Code=track_code)
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
