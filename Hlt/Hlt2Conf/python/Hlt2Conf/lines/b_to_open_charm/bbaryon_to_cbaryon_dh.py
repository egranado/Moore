###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
all_lines = {}


###########################################################
# Form the Xi_bc0 -> Lc+ D0 pi-,  Lc+ -> p K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi-
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPi_line(
        name='Hlt2B2OC_Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPi_Line', prescale=1):

    pions = basic_builder.make_pions()
    cmeson = d_builder.make_dzero_to_kpi()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, cmeson, pions],
        descriptors=[
            'Xi_bc0 -> Lambda_c+ D0 pi-', 'Xi_bc~0 -> Lambda_c~- D0 pi+'
        ])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


######################################################################
# Form the Omega_bc0 -> Xi_c0 D0,  Xi_c0 -> p K K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
######################################################################
@register_line_builder(all_lines)
@configurable
def Ombc0ToLcpD0K_LcpToPKPi_D0ToKPi_line(
        name='Hlt2B2OC_Ombc0ToLcpD0K_LcpToPKPi_D0ToKPi_Line', prescale=1):

    cmeson = d_builder.make_dzero_to_kpi()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    kaons = basic_builder.make_kaons()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, cmeson, kaons],
        descriptors=[
            'Omega_bc0 -> Lambda_c+ D0 K-', 'Omega_bc~0 -> Lambda_c~- D0 K+'
        ])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
