###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DD lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder

all_lines = {}


@register_line_builder(all_lines)
@configurable
def BuToDpDmPi_DpToHHH_line(name='Hlt2B2OC_BuToDpDmPi_DpToHHH_Line',
                            prescale=1):
    bachelor = basic_builder.make_bachelor_pions()
    d = d_builder.make_dplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[d, bachelor], descriptors=['[B+ -> D+ D- pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToD0D0Kst_D0ToHH_line(name='Hlt2B2OC_BdToD0D0Kst_D0ToHH_Line',
                            prescale=1):
    kst = basic_builder.make_kstar0()
    d = d_builder.make_dzero_to_hh()
    line_alg = b_builder.make_b2x(
        particles=[d, kst],
        descriptors=['B0 -> D0 D0 K*(892)0', 'B0 -> D0 D0 K*(892)~0'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


@register_line_builder(all_lines)
@configurable
def BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH_line(
        name='Hlt2B2OC_BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH_Line', prescale=1):
    bachelor = basic_builder.make_bachelor_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, dzero, bachelor],
        descriptors=['B0 -> D*(2010)- D0 K+', 'B0 -> D*(2010)+ D0 K-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
