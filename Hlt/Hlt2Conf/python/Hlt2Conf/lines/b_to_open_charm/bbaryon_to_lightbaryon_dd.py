###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder
all_lines = {}


#############################################################################
# Form the Xi_bc+ -> D0 D0 p,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ ONLY (CC picked up) - avoids many WS combinations
##############################################################################
@register_line_builder(all_lines)
@configurable
def XibcpToPD0D0_D0ToKPi_line(name='Hlt2B2OC_XibcpToPD0D0_D0ToKPi_Line',
                              prescale=1):

    cmeson = d_builder.make_dzero_to_kpi()
    protons = basic_builder.make_protons()
    line_alg = b_builder.make_xibc(
        particles=[cmeson, protons],
        descriptors=['Xi_bc+ -> D0 D0 p+', 'Xi_bc~- -> D0 D0 p~-'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
