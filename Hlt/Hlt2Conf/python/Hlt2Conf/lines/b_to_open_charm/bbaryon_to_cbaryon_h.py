###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from Hlt2Conf.lines.b_to_open_charm.prefilters import b2oc_prefilters

from Hlt2Conf.framework import configurable

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
all_lines = {}


##############################################
# Form the Lb->LcPi
##############################################
@register_line_builder(all_lines)
@configurable
def LbToLcpPi_LcpToPKPi_line(name='Hlt2B2OC_LbToLcpPi_LcpToPKPi_Line',
                             prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, bachelor],
        descriptors=['[Lambda_b0 -> Lambda_c+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Xib0 -> Xic+ Pi-
##############################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpPi_XicpToPKPi_line(name='Hlt2B2OC_Xib0ToXicpPi_XicpToPKPi_Line',
                                 prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, bachelor], descriptors=['[Xi_b0 -> Xi_c+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Xib- -> Xic0 Pi-
##############################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0Pi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibmToXic0Pi_Xic0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, bachelor], descriptors=['[Xi_b- -> Xi_c0 pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Omegab- -> Omegac0 Pi-
##############################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0Pi_Omc0ToPKKPi_line(
        name='Hlt2B2OC_OmbmToOmc0Pi_Omc0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, bachelor],
        descriptors=['[Omega_b- -> Omega_c0 pi-]cc'],
    )
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Lb->LcK
##############################################
@register_line_builder(all_lines)
@configurable
def LbToLcpK_LcpToPKPi_line(name='Hlt2B2OC_LbToLcpK_LcpToPKPi_Line',
                            prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, bachelor],
        descriptors=['[Lambda_b0 -> Lambda_c+ K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Xib0 -> Xic+ K-
##############################################
@register_line_builder(all_lines)
@configurable
def Xib0ToXicpK_XicpToPKPi_line(name='Hlt2B2OC_Xib0ToXicpK_XicpToPKPi_Line',
                                prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, bachelor], descriptors=['[Xi_b0 -> Xi_c+ K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Xib- -> Xic0 K-
##############################################
@register_line_builder(all_lines)
@configurable
def XibmToXic0K_Xic0ToPKKPi_line(name='Hlt2B2OC_XibmToXic0K_Xic0ToPKKPi_Line',
                                 prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xib(
        particles=[cbaryon, bachelor], descriptors=['[Xi_b- -> Xi_c0 K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################
# Form the Omegab- -> Omegac0 K-
##############################################
@register_line_builder(all_lines)
@configurable
def OmbmToOmc0K_Omc0ToPKKPi_line(name='Hlt2B2OC_OmbmToOmc0K_Omc0ToPKKPi_Line',
                                 prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, bachelor],
        descriptors=['[Omega_b- -> Omega_c0 K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> Xi_cc++ Pi-, Xi_cc++ --> Xic+ pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToXiccppPi_XiccppToXicpPi_XicpToPKPi_line(
        name='Hlt2B2OC_XibcpToXiccppPi_XiccppToXicpPi_XicpToPKPi_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xiccpp_to_xicppi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc+ -> Xi_cc++ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


################################################################
# Form the Xi_bc+ -> Xi_cc++ Pi-, Xi_cc++ --> Lc+ K- pi+ pi+
################################################################
@register_line_builder(all_lines)
@configurable
def XibcpToXiccppPi_XiccppToLcpKPiPi_LcpToPKPi_line(
        name='Hlt2B2OC_XibcpToXiccppPi_XiccppToLcpKPiPi_LcpToPKPi_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xiccpp_to_lcpkmpippip()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc+ -> Xi_cc++ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Xi_cc+ Pi-, Xi_cc+ --> Xic0 pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToXiccpPi_XiccpToXic0Pi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_Xibc0ToXiccpPi_XiccpToXic0Pi_Xic0ToPKKPi_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_xic0pi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Xi_cc+ Pi-, Xi_cc+ --> Lc+ K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToXiccpPi_XiccpToLcpKmPip_LcpToPKPi_line(
        name='Hlt2B2OC_Xibc0ToXiccpPi_XiccpToLcpKmPip_LcpToPKPi_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_lcpkmpip()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Xi_cc+ Pi-, Xi_cc+ --> D+ p K-
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToXiccpPi_XiccpToPDpKm_DpToPipPipKm_line(
        name='Hlt2B2OC_Xibc0ToXiccpPi_XiccpToPDpKm_DpToPipPipKm_Line',
        prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xiccp_to_dppk()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Xi_cc+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Xi_c+ Pi-, Xi_c+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToXicpPi_XicpToPKPi_line(
        name='Hlt2B2OC_Xibc0ToXicpPi_XicpToPKPi_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor], descriptors=['[Xi_bc0 -> Xi_c+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Lc+ K-, Lc+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToLcpK_LcpToPKPi_line(name='Hlt2B2OC_Xibc0ToLcpK_LcpToPKPi_Line',
                               prescale=1):

    bachelor = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Lambda_c+ K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc+ -> Xi_c0 Pi+, Xi_c0 --> p K- K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def XibcpToXic0Pi_Xic0ToPKKPi_line(
        name='Hlt2B2OC_XibcpToXic0Pi_Xic0ToPKKPi_Line', prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor], descriptors=['[Xi_bc+ -> Xi_c0 pi+]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


###########################################################
# Form the Xi_bc0 -> Lc+ Pi-, Lc+ --> p K- pi+
##########################################################
@register_line_builder(all_lines)
@configurable
def Xibc0ToLcpPi_LcpToPKPi_line(name='Hlt2B2OC_Xibc0ToLcpPi_LcpToPKPi_Line',
                                prescale=1):

    bachelor = basic_builder.make_bachelor_pions()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelor],
        descriptors=['[Xi_bc0 -> Lambda_c+ pi-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])


##############################################################
# Form the Omega_bc0 -> Xi_c+ K-, Xi_c+ --> p K- pi+
##############################################################
@register_line_builder(all_lines)
@configurable
def Ombc0ToXicpK_XicpToPKPi_line(name='Hlt2B2OC_Ombc0ToXicpK_XicpToPKPi_Line',
                                 prescale=1):

    bachelork = basic_builder.make_bachelor_kaons()
    cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xibc(
        particles=[cbaryon, bachelork],
        descriptors=['[Omega_bc0 -> Xi_c+ K-]cc'])
    return HltLine(
        name=name, prescale=prescale, algs=b2oc_prefilters() + [line_alg])
