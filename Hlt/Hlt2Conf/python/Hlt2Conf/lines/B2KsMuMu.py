###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the B2KsMuMu exclusive lines, with LL and DD Ks
"""

from __future__ import absolute_import, division, print_function
import math

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)

from ..algorithms import require_all, ParticleFilterWithPVs, ParticleCombinerWithPVs
from ..framework import configurable

from Moore.config import HltLine, register_line_builder

# get the basic particles
from ..standard_particles import make_dimuon_base, make_has_rich_long_pions, make_has_rich_down_pions, make_KsDD, make_KsLL

# get the dimuon filters
from .onia.builders.prefilters import make_prefilters
from .onia.builders.dimuon import make_dimuon, make_detached_dimuon_geo

all_lines = {}


@configurable
def make_b2ksmumu(name,
                  dimuons,
                  Ks,
                  pvs,
                  max_mass_B=7000 * MeV,
                  maxVertexChi2=20,
                  minCorrM=3500 * MeV,
                  maxCorrM=7000 * MeV,
                  maxTrackGhostProb_mu=0.4,
                  maxTrackGhostProb_pi=0.4):
    """Make the B --> Ks Mu Mu candidate"""

    # upper B mass
    combination_cut = require_all("AM < {max_mass_B}").format(
        max_mass_B=max_mass_B)

    # some additional cuts
    mother_cut = require_all(
        "BPVCORRM() > {minCorrM}",
        "BPVCORRM() < {maxCorrM}",
        "MAXTREE('mu-' == ABSID, TRGHOSTPROB) < {maxTrackGhostProb_mu}",
        "MAXTREE('pi+' == ABSID, TRGHOSTPROB) < {maxTrackGhostProb_pi}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
    ).format(
        minCorrM=minCorrM,
        maxCorrM=maxCorrM,
        maxTrackGhostProb_mu=maxTrackGhostProb_mu,
        maxTrackGhostProb_pi=maxTrackGhostProb_pi,
        maxVertexChi2=maxVertexChi2)

    return ParticleCombinerWithPVs(
        name=name,
        particles=[dimuons, Ks],
        pvs=pvs,
        DecayDescriptors=["B0 -> KS0 J/psi(1S)"],
        CombinationCut=combination_cut,
        MotherCut=mother_cut)


###########
# Definition of the lines
###########


@register_line_builder(all_lines)
@configurable
def b2ksmumull_line(name="Hlt2DiMuonB2KSMuMuLLLine", prescale=1):
    """B0 --> Ks Jpsi LL line"""

    # get the long pions
    pions_long = make_has_rich_long_pions()

    # form the Ks
    kaons_LL = make_KsLL()

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    filtered_dimuons = make_dimuon()

    # get the detached dimuons
    detached_dimuons = make_detached_dimuon_geo()

    # make the candidate
    B2KsMuMuLL = make_b2ksmumu(
        name="B2KsMuMuMaker_B2KsMuMuLLLine",
        dimuons=detached_dimuons,
        Ks=kaons_LL,
        pvs=pvs)

    return HltLine(
        name=name,
        algs=make_prefilters() + [B2KsMuMuLL],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def b2ksmumudd_line(name="Hlt2DiMuonB2KSMuMuDDLine", prescale=1):
    """B0 --> Ks Jpsi DD line"""

    # get the downstream pions
    pions_down = make_has_rich_down_pions()

    # fprm the DD Ks
    kaons_DD = make_KsDD()

    # get the PVs
    pvs = make_pvs()

    # get the basic dimuons and filter them
    filtered_dimuons = make_dimuon()

    # get the detached dimuons
    detached_dimuons = make_detached_dimuon_geo()

    # make the candidate
    B2KsMuMuDD = make_b2ksmumu(
        name="B2KsMuMuMaker_B2KsMuMuDDLine",
        dimuons=detached_dimuons,
        Ks=kaons_DD,
        pvs=pvs)

    return HltLine(
        name=name,
        algs=make_prefilters() + [B2KsMuMuDD],
        prescale=prescale,
    )
