###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Some generic lines

mainly to test consistency combiners and filters and estimate timing
"""

from __future__ import absolute_import, division, print_function
import math

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction, make_tracks

from ..algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs, ParticleFilter, N3BodyCombinerWithPVs
from PyConf.Algorithms import (
    BasicsPropertyDumper,
    ChargedBasicsFilter,
    CompositePropertyDumper,
    FilterDesktop,
    FourBodyCombiner,
    LHCb__Converters__RangeToVectorParticle as RangeToVector,
    LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertex as
    Vertex_v2_to_v1,
    LHCb__Converters__RecVertices__LHCbRecVerticesToVectorV2RecVertex as
    Vertex_v1_to_v2,
    LHCb__Converters__Track__v2__fromV1TrackV2Track as v2TrackConverter,
    LHCb__VectorToSelectionParticle as VectorToSelection,
    LegacyParticlePropertyDumper,
    ParticleSelectionFilter,
    ThOrFourBodyCombiner,
    ThOrThreeBodyCombiner,
    ThOrTwoBodyCombiner,
    ThreeBodyCombiner,
    TwoBodyCombiner,
)

from ..framework import configurable
from ..standard_particles import (
    make_long_pions, make_long_kaons, make_long_cb_muons, make_long_cb_pions,
    make_long_muons, make_long_cb_kaons, make_has_rich_long_pions,
    make_has_rich_long_kaons)

from Functors.math import in_range
from Functors import (ALL, SUM, DOCACHI2, DOCA, NONE, MAXDOCACHI2CUT, P, PT,
                      BPVIPCHI2, MINIPCUT, MINIP, MINIPCHI2, MINIPCHI2CUT,
                      FILTER, ISMUON, PID_MU, PID_K, PID_E, PID_P, PID_PI,
                      COMPOSITEMASS, CHI2NDOF, MAX, BPVFDCHI2, BPVDIRA)
from Functors.utils import pack_dict as __transform_functor_dict

combiner_lines = {}
filter_lines = {}


def make_v2_pvs():
    v2_tracks = v2TrackConverter(
        InputTracksName=make_tracks()).OutputTracksName
    v2pvs = Vertex_v1_to_v2(
        InputVertices=make_pvs(), InputTracks=v2_tracks).OutputVertices
    return v2pvs


def make_pvs_from_v2_pvs():
    v1pvs = Vertex_v2_to_v1(
        InputVerticesName=make_v2_pvs(), InputTracksName=make_tracks())
    return v1pvs


@configurable
def filter_particles_thor(particles,
                          make_pvs=make_v2_pvs,
                          trchi2_max=3,
                          mipchi2_min=4,
                          pt_min=250 * MeV,
                          p_min=2 * GeV,
                          pid=None,
                          filterAlg=ChargedBasicsFilter,
                          name="FilterParticles"):
    """Return maker for particles filtered by thresholds common to charm decay product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """

    v2pvs = make_pvs()
    location_pvs = v2pvs.location

    code = (PT > pt_min) & \
           (P > p_min) & \
           (MINIPCHI2(Vertices=location_pvs) > mipchi2_min)
    # TODO(AP): Cut value is reasonable for Run 2, but removes basically
    # everything in the upgrade sample
    # (TRCHI2 < trchi2_max)

    if pid is not None:
        code = code & (pid)
    code = FILTER(code)
    return filterAlg(name=name, Input=particles, Cut=code).Output


@configurable
def select_pions(particles, pid=(PID_K < 5)):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return filter_particles_thor(particles, pid=pid, name="ThOrPionsCB")


@configurable
def select_kaons(particles, pid=(PID_K > 5)):
    """Return maker for kaons filtered by thresholds common to charm decay product selections."""
    return filter_particles_thor(particles, pid=pid, name="ThOrKaonsCB")


@configurable
def select_LHCb_pions(particles, pid=(PID_K < 5)):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    v = RangeToVector(Input=particles).Output
    s = VectorToSelection(Input=v).Output
    return filter_particles_thor(
        s, pid=pid, filterAlg=ParticleSelectionFilter, name="ThOrPions")


@configurable
def select_ISMUON_pions(particles):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return ChargedBasicsFilter(
        Input=particles, Cut=FILTER(ISMUON), name="ThOrISMUONPionsCB")


@configurable
def select_LHCb_ISMUON_pions(particles):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    v = RangeToVector(Input=particles).Output
    s = VectorToSelection(Input=v).Output
    return ParticleSelectionFilter(
        Input=s, Cut=FILTER(ISMUON), name="ThOrISMUONPions")


@configurable
def select_legacy_ismuon_pions(particles):
    return ParticleFilter(
        particles,
        name="LoKiISMUONPions",
        Code="ISMUON",
        ParticleCombiners={"": "ParticleVertexFitter"},
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
    )


@configurable
def combine_ThOr_dplus_hhh(particles,
                           decay_descriptors,
                           make_pvs=make_v2_pvs,
                           am_min=1789 * MeV,
                           am_max=1949 * MeV,
                           amaxchild_pt_min=1000 * MeV,
                           trk_2of3_pt_min=400 * MeV,
                           aptsum_min=3000 * MeV,
                           amaxchild_ipchi2_min=50,
                           trk_2of3_ipchi2_min=10,
                           vchi2pdof_max=6,
                           bpvvdchi2_min=150,
                           bpvltime_min=0.4 * picosecond,
                           acos_bpvdira_max=10. * mrad):
    '''combining with the thor particle combiner (acting on Particle v2)'''

    v2pvs = make_pvs()
    location_pvs = v2pvs.location

    combination_code = (in_range(am_min, COMPOSITEMASS, am_max)) & \
        (MAX(PT) > amaxchild_pt_min) & \
        (SUM( PT > trk_2of3_pt_min ) > 1 ) & \
        (SUM(PT) > aptsum_min ) & \
        (MAX(MINIPCHI2(Vertices=location_pvs))> amaxchild_ipchi2_min ) & \
        (SUM(MINIPCHI2(Vertices=location_pvs) > trk_2of3_ipchi2_min ) > 1)

    cos_bpvdira_min = math.cos(acos_bpvdira_max)

    vertex_code = FILTER((BPVFDCHI2(Vertices=location_pvs) > bpvvdchi2_min) &
                         (BPVDIRA(Vertices=location_pvs) > cos_bpvdira_min) &
                         (CHI2NDOF < vchi2pdof_max))

    # BPVLTIME() > bpvltime_min &
    # BPVVALID() & \
    # TODO implement BPVVALID if needed

    return ThOrThreeBodyCombiner(
        Inputs=particles,
        DecayDescriptors=decay_descriptors,
        Comb3Cut=combination_code,
        MotherCut=vertex_code).Particles


@configurable
def combine_Shiny_dplus_hhh(particles,
                            decay_descriptors,
                            make_pvs=make_v2_pvs,
                            trchi2_max=3,
                            mipchi2_min=4,
                            pt_min=250 * MeV,
                            p_min=2 * GeV,
                            am_min=1789 * MeV,
                            am_max=1949 * MeV,
                            amaxchild_pt_min=1000 * MeV,
                            trk_2of3_pt_min=400 * MeV,
                            aptsum_min=3000 * MeV,
                            amaxchild_ipchi2_min=50,
                            trk_2of3_ipchi2_min=10,
                            vchi2pdof_max=6,
                            bpvvdchi2_min=150,
                            bpvltime_min=0.4 * picosecond,
                            acos_bpvdira_max=10. * mrad):
    '''combining with the even newer shiny combiny'''
    pvs = make_pvs()
    location_pvs = pvs.location

    # daughtercuts = (PT > pt_min) & \
    #                (P > p_min) &\
    #    (MINIPCHI2(Vertices=location_pvs) > mipchi2_min)
    # daughtercuts_k = daughtercuts & (PID_K > 5)
    # daughtercuts_pi = daughtercuts & (PID_K < 5)

    combination_code = (in_range(am_min, COMPOSITEMASS, am_max)) & \
        (MAX(PT) > amaxchild_pt_min) & \
        (SUM( PT > trk_2of3_pt_min ) > 1 ) & \
        (SUM(PT) > aptsum_min ) &\
        (MAX(MINIPCHI2(Vertices=location_pvs)) > amaxchild_ipchi2_min ) &\
        (SUM(MINIPCHI2(Vertices=location_pvs) > trk_2of3_ipchi2_min ) > 1)

    cos_bpvdira_min = math.cos(acos_bpvdira_max)

    # vertex_code = ( CHI2NDOF < vchi2pdof_max )
    vertex_code = ( CHI2NDOF < vchi2pdof_max ) &\
    ( BPVFDCHI2(Vertices=location_pvs) > bpvvdchi2_min ) & \
    ( BPVDIRA(Vertices=location_pvs) > cos_bpvdira_min )
    # BPVLTIME() > bpvltime_min &
    #     # BPVVALID() & \
    # TODO implement BPVVALID if needed

    return ThreeBodyCombiner(
        Inputs=particles,
        # DaughterCuts=__transform_functor_dict({
        #     "K+": daughtercuts_k,
        #     "pi+": daughtercuts_pi,
        # }),
        DecayDescriptors=decay_descriptors,
        Comb123Cut=combination_code,
        ParticleCombiner="ParticleVertexFitter",
        DistanceCalculator="LoKi::TrgDistanceCalculator",
        MotherCut=vertex_code).OutputParticles


################### stolen from d2hhh
@configurable
def filter_particles(particles,
                     pvs,
                     trchi2_max=3,
                     mipchi2_min=4,
                     pt_min=250 * MeV,
                     p_min=2 * GeV,
                     pid=None):
    """Return maker for particles filtered by thresholds common to charm decay product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(
        particles,
        pvs,
        Code=code,
        ParticleCombiners={"": "ParticleVertexFitter"},
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
    )


@configurable
def make_pions(particles, pvs, pid='PIDK < 5'):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@configurable
def make_kaons(particles, pvs, pid='PIDK > 5'):
    """Return maker for kaons filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@configurable
def make_dplus(particles,
               descriptors,
               pvs,
               am_min=1789 * MeV,
               am_max=1949 * MeV,
               amaxchild_pt_min=1000 * MeV,
               trk_2of3_pt_min=400 * MeV,
               aptsum_min=3000 * MeV,
               amaxchild_ipchi2_min=50,
               trk_2of3_ipchi2_min=10,
               vchi2pdof_max=6,
               bpvvdchi2_min=150,
               bpvltime_min=0.4 * picosecond,
               acos_bpvdira_max=10. * mrad,
               combiner=ParticleCombinerWithPVs,
               **kwargs):
    """Return Dplus maker with selection tailored for three-body hadronic final states.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "ANUM( PT > {trk_2of3_pt_min} ) > 1",
        "(APT1 + APT2 + APT3) > {aptsum_min}",
        "AMAXCHILD(MIPCHI2DV(PRIMARY)) > {amaxchild_ipchi2_min}",
        "ANUM( MIPCHI2DV(PRIMARY) > {trk_2of3_ipchi2_min} ) > 1").format(
            am_min=am_min,
            am_max=am_max,
            amaxchild_pt_min=amaxchild_pt_min,
            trk_2of3_pt_min=trk_2of3_pt_min,
            aptsum_min=aptsum_min,
            amaxchild_ipchi2_min=amaxchild_ipchi2_min,
            trk_2of3_ipchi2_min=trk_2of3_ipchi2_min)

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    # vertex_code = "BPVVDCHI2() > {}".format(bpvltime_min)

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}",
        "BPVVALID()",
        "BPVVDCHI2() > {bpvvdchi2_min}",
        #"BPVLTIME() > {bpvltime_min}",
        "BPVDIRA() > {cos_bpvdira_min}").format(
            vchi2pdof_max=vchi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min,
            #  bpvltime_min=bpvltime_min,
            cos_bpvdira_min=cos_bpvdira_min)

    return combiner(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code,
        ParticleCombiners={"": "ParticleVertexFitter"},
        DistanceCalculators={"": "LoKi::TrgDistanceCalculator"},
        **kwargs)


######### end of stolen d2hhh code


@register_line_builder(filter_lines)
def HLT_filter_line(name="Hlt2_ThOrCBFilterISMUONLine"):
    cbpions = select_ISMUON_pions(make_long_cb_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), cbpions])


@register_line_builder(filter_lines)
def HLT_filter_line(name="Hlt2_LoKiFilterISMUONLine"):
    lokipions = select_legacy_ismuon_pions(make_has_rich_long_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), lokipions])


@register_line_builder(filter_lines)
def HLT_filter_line(name="Hlt2_ThOrFilterISMUONLine"):
    thorpions = select_LHCb_ISMUON_pions(make_has_rich_long_pions())
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), thorpions])


### ThOr combiner lines
@register_line_builder(combiner_lines)
def HLT_ThOr_dplus_hhh_line(name="Hlt2_ThOr_Dp2KmPipPipLine"):
    kaons = select_kaons(make_long_cb_kaons())
    pions = select_pions(make_long_cb_pions())
    dplus = combine_ThOr_dplus_hhh(
        particles=[kaons, pions], decay_descriptors=['[D+ -> K- pi+ pi+]cc'])
    return HltLine(
        name=name,
        algs=upfront_reconstruction() +
        [make_v2_pvs(), require_pvs(make_pvs()), dplus])


### Shiny combiner lines
@register_line_builder(combiner_lines)
def HLT_Shiny_dplus_hhh_line(name="Hlt2_Shiny_Dp2KmPipPipLine"):
    kaons = make_kaons(make_has_rich_long_kaons(), make_pvs())
    pions = make_pions(make_has_rich_long_pions(), make_pvs())
    dplus = combine_Shiny_dplus_hhh(
        particles=[kaons, pions], decay_descriptors=['[D+ -> K- pi+ pi+]cc'])
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), dplus])


# lhcb particle combiner lines
@register_line_builder(combiner_lines)
def dplus2Kpipi_line(name='Hlt2CharmHadDToKmPipPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = make_dplus(
        particles=[kaons, pions],
        descriptors=['[D+ -> K- pi+ pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dplus],
        prescale=prescale,
    )


@register_line_builder(combiner_lines)
@configurable
def dplus2Kpipi_line_n3(name='Hlt2CharmHadDToKmPipPipN3Line', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = make_dplus(
        particles=[kaons, pions],
        descriptors=['[D+ -> K- pi+ pi+]cc'],
        pvs=pvs,
        combiner=N3BodyCombinerWithPVs,
        Combination12Cut="AALL")
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dplus],
        prescale=prescale,
    )
