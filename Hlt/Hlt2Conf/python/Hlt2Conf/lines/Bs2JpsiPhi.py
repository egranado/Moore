###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Bs -> J/psi Phi HLT2 lines.
"""
from __future__ import absolute_import, division, print_function

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import MeV

from RecoConf.hlt1_tracking import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from ..algorithms import require_all, ParticleFilter, ParticleCombiner
from ..framework import configurable
from ..standard_particles import make_mass_constrained_jpsi2mumu, make_phi2kk

all_lines = {}


# make sure we passed GEC and have PV in event
def prefilters():
    return [require_pvs(make_pvs())]


@configurable
def make_phi(am_min=980 * MeV,
             am_max=1060 * MeV,
             pt=500. * MeV,
             vchi2=10,
             tr_chi2pdof=5,
             pid_k=0):
    phis = make_phi2kk()
    code = require_all("in_range({am_min}, M, {am_max})", "PT > {pt}",
                       "VFASPF(VCHI2) < {vchi2}",
                       "MAXTREE('K+'==ABSID, TRCHI2DOF) < {tr_chi2pdof}",
                       "MINTREE('K+'==ABSID, PIDK) > {pid_k}").format(
                           am_min=am_min,
                           am_max=am_max,
                           pt=pt,
                           vchi2=vchi2,
                           tr_chi2pdof=tr_chi2pdof,
                           pid_k=pid_k)
    return ParticleFilter(particles=phis, Code=code)


@configurable
def make_bs2jpsiphi(am_min=5050 * MeV,
                    am_max=5650 * MeV,
                    am_min_vtx=5050 * MeV,
                    am_max_vtx=5650 * MeV,
                    vtx_chi2pdof=20):
    phi = make_phi()
    jpsi = make_mass_constrained_jpsi2mumu()
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)
    vertex_code = require_all("in_range({am_min_vtx}, M, {am_max_vtx})",
                              "VFASPF(VCHI2PDOF) < {vtx_chi2pdof}").format(
                                  am_min_vtx=am_min_vtx,
                                  am_max_vtx=am_max_vtx,
                                  vtx_chi2pdof=vtx_chi2pdof)
    return ParticleCombiner(
        particles=[phi, jpsi],
        DecayDescriptors=['B_s0 -> J/psi(1S) phi(1020)'],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@register_line_builder(all_lines)
@configurable
def bs2jpsiphi_line(name='Hlt2BsToJpsiPhiLine', prescale=1):
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [make_bs2jpsiphi()],
        prescale=prescale,
    )
