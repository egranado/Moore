###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import HltLine, register_line_builder
from Hlt2Conf.standard_particles import make_long_pions, make_long_electrons_no_brem, make_photons
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.inclusive_radiative_b.builders import basic_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import gamma_ee_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import hh_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import hhh_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import b_builder
from Hlt2Conf.lines.inclusive_radiative_b.builders import b_tmva_builder
all_lines = {}


@register_line_builder(all_lines)
def btohhhgammaee_inclusive_line(name="Hlt2BToHHHGammaEE_Inclusive_Line",
                                 prescale=1):
    pvs = make_pvs()
    electrons = basic_builder.filter_electrons(make_long_electrons_no_brem(),
                                               pvs)
    hadrons = basic_builder.filter_basic_hadrons(make_long_pions(), pvs)
    last_hadrons = basic_builder.filter_third_hadrons(make_long_pions(), pvs)
    photons = gamma_ee_builder.make_gamma_ee(electrons, pvs)
    kstars = hh_builder.make_hh(hadrons, pvs)
    dstars = hhh_builder.make_hhh(kstars, last_hadrons, pvs)
    presel_b = b_builder.make_presel_b(dstars, photons, pvs,
                                       ["[B+ -> D*(2010)+ gamma]cc"])
    b = b_tmva_builder.make_b(presel_b, pvs, "HHHgammaEE", 0.05)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [b],
        prescale=prescale,
    )
