###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of inclusive_radiative_b basic particles: hadrons, photons, electrons
"""
import math
from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable


@configurable
def filter_electrons(particles, pvs, pt_min=0.1 * GeV):
    """Returns electrons for inclusive_radiative_b selection """
    code = require_all('PT > {pt_min}').format(pt_min=pt_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def filter_photons(particles, pvs, pt_min=2.0 * GeV, p_min=5.0 * GeV):
    """Returns photons for inclusive_radiative_b selection"""
    code = require_all('PT > {pt_min}', 'P > {p_min}').format(
        pt_min=pt_min, p_min=p_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def filter_basic_hadrons(particles,
                         pvs,
                         pt_min=1.0 * GeV,
                         pt_max=25.0 * GeV,
                         ipchi2_min=7.4,
                         trchi2_max=2.5,
                         trgp_max=0.2,
                         param1=1.0,
                         param2=1.0,
                         param3=1.1):
    """Returns basic hadrons with the inclusive_radiative_b selections"""
    log_ipchi2_min = math.log(ipchi2_min)
    difficult_cut = require_all(
        'in_range({pt_min}, PT, {pt_max})',
        'log(BPVIPCHI2()) > ({param1} / ((PT / GeV - {param2}) ** 2) + ({param3} / {pt_max}) * ({pt_max} - PT) + {log_ipchi2_min})'
    )
    code = require_all(
        'TRCHI2DOF < {trchi2_max}', 'TRGHOSTPROB < {trgp_max}',
        '((PT > {pt_max}) & (BPVIPCHI2() > {ipchi2_min})) | (' + difficult_cut
        + ')').format(
            pt_min=pt_min,
            pt_max=pt_max,
            ipchi2_min=ipchi2_min,
            log_ipchi2_min=log_ipchi2_min,
            trchi2_max=trchi2_max,
            trgp_max=trgp_max,
            param1=param1,
            param2=param2,
            param3=param3)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def filter_third_hadrons(particles,
                         pvs,
                         pt_min=0.2 * GeV,
                         p_min=3.0 * GeV,
                         trchi2_max=3.0,
                         ipchi2_min=4.0):
    """Returns third hadron used in HHH inclusive_radiative_b selections"""
    code = require_all("PT > {pt_min}", "P > {p_min}",
                       "TRCHI2DOF < {trchi2_max}",
                       "MIPCHI2DV(PRIMARY) > {ipchi2_min}").format(
                           pt_min=pt_min,
                           p_min=p_min,
                           trchi2_max=trchi2_max,
                           ipchi2_min=ipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)
