###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 single high-PT muon line.
"""

from __future__ import absolute_import
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from Moore.config import HltLine, register_line_builder
from Hlt2Conf.algorithms import require_all, ParticleFilter
from Hlt2Conf.standard_particles import make_ismuon_long_muon
from RecoConf.reco_objects_from_file import upfront_reconstruction

all_lines = {}


@configurable
def make_highpt_muons(name='HighPtMuonMaker',
                      muons=make_ismuon_long_muon,
                      min_pt=10. * GeV,
                      max_trchi2=5):

    # Filter on the PT and trkchi2
    code = require_all('PT > {min_pt}',
                       #'TRCHI2DOF < {max_trchi2}',
                       ).format(
                           min_pt=min_pt, max_trchi2=max_trchi2)

    return ParticleFilter(particles=muons(), name=name, Code=code)


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_line(name='Hlt2SingleHighPtMuonLine', prescale=1):
    """High PT single muon line"""

    high_pt_muons = make_highpt_muons()

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons],
        prescale=prescale,
    )
