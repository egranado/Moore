###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function

from Hlt2Conf.framework import configurable
#from PyConf import configurable

from Hlt2Conf.algorithms import require_all, ParticleFilter, ParticleFilterWithPVs, ParticleCombiner

from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction, reconstruction

from Hlt2Conf.lines.jets.algorithms import ParticleFlow, JetBuilder
from Hlt2Conf.lines.jets.topobits import make_topo_2body
from Hlt2Conf.standard_particles import make_long_muons
from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV

all_lines = {}


@configurable
def make_sv_for_jb(particles,
                   pvs,
                   prod_pt_min=500 * MeV,
                   prod_ghostprob_max=0.2,
                   prod_ipchi2_min=16,
                   chi2pdof_max=10,
                   bpvvdchi2_min=25):
    """Filter two-body secondary vertexes as tag input or HltJetBuilder

    The relevant keys from the Jets_pp_2018.py configuration appear to be
            'SV_TRK_PT'    : 500*MeV,
            'GHOSTPROB'    : 0.2,
            'SV_TRK_IPCHI2': 16,
            'SV_VCHI2'     : 10,
            'SV_FDCHI2'    : 25,
    """
    code = require_all(
        "MINTREE(ALL,PT) > {prod_pt_min}",
        "MINTREE(ISBASIC,TRGHOSTPROB) < {prod_ghostprob_max}",
        "MINTREE((ABSID=='K+'),MIPCHI2DV(PRIMARY)) > {prod_ipchi2_min}",
        "HASVERTEX", "CHI2VXNDOF < {chi2pdof_max}",
        "BPVVDCHI2() > {bpvvdchi2_min}").format(
            prod_pt_min=prod_pt_min,
            prod_ghostprob_max=prod_ghostprob_max,
            prod_ipchi2_min=prod_ipchi2_min,
            chi2pdof_max=chi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def make_muons_for_jb(particles,
                      pt_min=1000 * MeV,
                      probnnmu_min=0.5,
                      ghostprob_max=0.2):
    """Filter muons as tag input or HltJetBuilder
    The relevant keys from the Jets_pp_2018.py configuration appear to be
            'MU_PT'        : 1000*MeV,
            'MU_PROBNNMU'  : 0.5,
            'GHOSTPROB'    : 0.2,
    """
    code = require_all("PT > {pt_min}", "PROBNNmu > {probnnmu_min}",
                       "TRGHOSTPROB < {ghostprob_max}").format(
                           pt_min=pt_min,
                           probnnmu_min=probnnmu_min,
                           ghostprob_max=ghostprob_max,
                       )
    return ParticleFilter(particles, Code=code)


@configurable
def make_jetbuilder(inputs,
                    inputtags,
                    jetptmin=5 * GeV,
                    jetinfo=False,
                    jetecpath=''):
    """Configuration wrapper for JetBuilder
    Meant to emulate the configuration of the Run 2 line.  The Run 2
    Jets_pp_2018 configuration dictionary was
        'JetBuilder': {
            'Reco'      : 'TURBO',
            'JetPtMin'  : 5*GeV,
            'JetInfo'   : False,
            'JetEcPath' : ''
            },
    The 'Reco' element is not a property of the JetBuilder but rather used to
    select a set of inputs for the ParticleFlow algorithm.  That functionality
    has not been reproduced; only the configuration of the properties of the
    JetBuilder have been retained.                                                                            """
    return JetBuilder(
        inputs=inputs,
        inputtags=inputtags,
        JetPtMin=jetptmin,
        JetInfo=jetinfo,
        JetEcPath=jetecpath)


@configurable
def make_particleflow_forjets():
    ## TODO:  restore neutral protoparticles when CaloClusters can be remade
    inprots = [
        reconstruction()['ChargedProtos'],
        #reconstruction()['NeutralProtos']
    ]
    return ParticleFlow(inProts=inprots)


@configurable
def make_jets():
    pflow = make_particleflow_forjets()
    mutags = make_muons_for_jb(make_long_muons())
    svtags = make_sv_for_jb(make_topo_2body(), make_pvs())
    return make_jetbuilder(inputs=[pflow], inputtags=[mutags, svtags])


#Valid tagtypes are 'SV', 'Mu' and 'DH'
@configurable
def filter_jets_by_tag(pt_min=17 * GeV, tagtype=None):
    """Filter containers of jets as final line output
    Filtering by a specific tag type relies on information added to the
    ExtraInfo by HltJetBuilder.
    """
    jets = make_jets()
    tags = {'SV': 9600, 'Mu': 9601, 'DH': 9602}
    tagFilter = "ALL"
    if tagtype: tagFilter = "INFO({tag}, -1) != -1".format(tag=tags[tagtype])

    code = require_all("ABSID == 'CELLjet'", "PT > {pt_min}",
                       tagFilter).format(pt_min=pt_min)
    return ParticleFilter(jets, Code=code)


@configurable
def make_dijets_by_tagpair(
        tagpair=(None, None), prod_pt_min=17 * GeV, dphi=0.0):
    """Make two-jet combinations
    """
    jets = filter_jets_by_tag(pt_min=prod_pt_min, tagtype=None)
    descriptors = ["CLUSjet -> CELLjet CELLjet"]
    tags = {'SV': 9600, 'Mu': 9601, 'DH': 9602}
    tagfilt0 = 'AALL'
    tagfilt1 = 'AALL'
    if tagpair[0]:
        tagfilt0 = "ACHILD(INFO({tag}, -1), 1) != -1".format(
            tag=tags[tagpair[0]])
    if tagpair[1]:
        tagfilt1 = "ACHILD(INFO({tag}, -1), 2) != -1".format(
            tag=tags[tagpair[1]])

    combination_code = require_all(
        tagfilt0, tagfilt1,
        "abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > {dphi}").format(dphi=dphi)

    return ParticleCombiner(
        particles=[jets],
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut="ALL",
        ParticleCombiners={'': 'ParticleAdder'})


@register_line_builder(all_lines)
@configurable
def lowptjet_line(name='Hlt2LowPtSingleJetLine', prescale=0.001):
    jets = filter_jets_by_tag(pt_min=10 * GeV)
    return HltLine(
        name=name, algs=upfront_reconstruction() + [jets], prescale=prescale)
