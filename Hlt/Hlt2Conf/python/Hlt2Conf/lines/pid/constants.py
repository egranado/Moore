###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

JPSI_M = 3096 * MeV
D0_M = 1865 * MeV
DSTAR_M = 2010 * MeV
LAMBDA_M = 1116 * MeV
OMEGA_M = 1672.45 * MeV
