###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for ``J/psi -> mu+ mu-`` with positive muon tagged.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder

from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from Hlt2Conf.standard_particles import (
    make_long_muons,
    make_ismuon_long_muon,
)
from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs
from Hlt2Conf.algorithms import ParticleCombinerWithPVs
from constants import *

all_lines = {}


def make_tag_muons(particles,
                   pvs,
                   trchi2dof_max=4.0,
                   trghostprob_max=0.2,
                   p_min=3 * GeV,
                   pt_min=1.2 * GeV,
                   mipchi2dv_min=20.0):
    code = require_all('Q > 0', 'ISMUON', 'TRCHI2DOF <{trchi2dof_max}',
                       'ISLONG', 'TRGHOSTPROB < {trghostprob_max}',
                       'P > {p_min} ', 'PT >{pt_min} ',
                       'MIPCHI2DV(PRIMARY)>{mipchi2dv_min}').format(
                           trchi2dof_max=trchi2dof_max,
                           trghostprob_max=trghostprob_max,
                           p_min=p_min,
                           pt_min=pt_min,
                           mipchi2dv_min=mipchi2dv_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_probe_muons(particles,
                     pvs,
                     trchi2dof_max=4.0,
                     p_min=3 * GeV,
                     pt_min=0 * MeV,
                     mipchi2dv_min=20.0):
    code = require_all('Q < 0', 'TRCHI2DOF < {trchi2dof_max}', 'ISLONG',
                       'P > {p_min}', 'PT >{pt_min}',
                       'MIPCHI2DV(PRIMARY)> {mipchi2dv_min}').format(
                           trchi2dof_max=trchi2dof_max,
                           p_min=p_min,
                           pt_min=pt_min,
                           mipchi2dv_min=mipchi2dv_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_jpsis(tag_muons,
               probe_muons,
               pvs,
               am_min=JPSI_M - 210 * MeV,
               am_max=JPSI_M + 160 * MeV,
               m_min=JPSI_M - 200 * MeV,
               m_max=JPSI_M + 150 * MeV,
               achi2doca_max=5.0):
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "ACHI2DOCA(1,2) < {achi2doca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       achi2doca_max=achi2doca_max)
    mother_code = require_all("in_range({m_min},  M, {m_max})").format(
        m_min=m_min, m_max=m_max)

    return ParticleCombinerWithPVs(
        particles=[tag_muons, probe_muons],
        pvs=pvs,
        DecayDescriptors=["[J/psi(1S) -> mu+ mu-]cc"],
        CombinationCut=combination_code,
        MotherCut=mother_code)


def make_detached_jpsis(particles,
                        pvs,
                        vfaspf_max=15.0,
                        pt_min=1.0 * GeV,
                        bpvvdchi2_min=150.0,
                        mipchi2dv_min=5.0,
                        bpvdira=0.995):
    vertex_code = require_all("VFASPF(VCHI2)<{vfaspf_max}", "PT > {pt_min}",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "MIPCHI2DV(PRIMARY) > {mipchi2dv_min}",
                              "BPVDIRA() > {bpvdira}").format(
                                  vfaspf_max=vfaspf_max,
                                  pt_min=pt_min,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  mipchi2dv_min=mipchi2dv_min,
                                  bpvdira=bpvdira)

    return ParticleFilterWithPVs(particles, pvs, Code=vertex_code)


@register_line_builder(all_lines)
def detJpsiMuMuPosTagged_line(name="Hlt2DetJpsiToMuMuPosTaggedLine",
                              prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs)
    probe_muons = make_probe_muons(make_long_muons(), pvs)
    jpsis = make_jpsis(tag_muons, probe_muons, pvs)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [detached_jpsis],
        prescale=prescale,
    )
