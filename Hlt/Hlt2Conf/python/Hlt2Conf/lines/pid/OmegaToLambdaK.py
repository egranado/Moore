###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for ``Omega+ -> Lambda_0 K+, Lambda_0 -> p pi-`` LL and DD.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Moore.config import HltLine, register_line_builder

from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from Hlt2Conf.standard_particles import (make_long_pions, make_down_pions,
                                         make_long_kaons, make_down_kaons,
                                         make_long_protons, make_down_protons)
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from Hlt2Conf.algorithms import ParticleFilterWithPVs

all_lines = {}


def filter_long_particles(particles, pvs, trchi2_max=4, mipchi2_min=36):
    code = require_all('TRCHI2DOF < {trchi2_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           trchi2_max=trchi2_max, mipchi2_min=mipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def filter_down_particles(particles,
                          pvs,
                          p_min=3.0 * GeV,
                          pt_min=0.175 * GeV,
                          trchi2_max=4):
    code = require_all('P > {p_min}', 'PT > {pt_min}',
                       'TRCHI2DOF < {trchi2_max}').format(
                           p_min=p_min, pt_min=pt_min, trchi2_max=trchi2_max)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def filter_kaons(particles, pvs, mipchi2_min=16):
    code = require_all('MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
        mipchi2_min=mipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_l0lls(protons,
               pions,
               pvs,
               am_min=(1116. - 50.) * MeV,
               am_max=(1116. + 50.) * MeV,
               m_min=1110. * MeV,
               m_max=1120. * MeV,
               bpvvdz_min=-100 * mm,
               bpvvdz_max=500 * mm,
               bpvltime_min=0.002 * ns,
               vchi2_max=30):
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})",
        "in_range({bpvvdz_min}, BPVVDZ(), {bpvvdz_max})",
        "BPVLTIME() > {bpvltime_min}", "VFASPF(VCHI2) < {vchi2_max}").format(
            m_min=m_min,
            m_max=m_max,
            bpvvdz_min=bpvvdz_min,
            bpvvdz_max=bpvvdz_max,
            bpvltime_min=bpvltime_min,
            vchi2_max=vchi2_max)

    return ParticleCombinerWithPVs(
        particles=[protons, pions],
        pvs=pvs,
        DecayDescriptors=["[Lambda0 -> p+ pi-]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


# TODO: Add Lambda flight cut from it's production vertex
def make_l0dds(protons,
               pions,
               pvs,
               am_min=(1116. - 80.) * MeV,
               am_max=(1116. + 80.) * MeV,
               m_min=1110. * MeV,
               m_max=1120. * MeV,
               bpvvdz_min=300 * mm,
               bpvvdz_max=2275 * mm,
               bpvltime_min=0.0045 * ns,
               vchi2_max=30):
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})",
        "in_range({bpvvdz_min}, BPVVDZ(), {bpvvdz_max})",
        "BPVLTIME() > {bpvltime_min}", "VFASPF(VCHI2) < {vchi2_max}").format(
            m_min=m_min,
            m_max=m_max,
            bpvvdz_min=bpvvdz_min,
            bpvvdz_max=bpvvdz_max,
            bpvltime_min=bpvltime_min,
            vchi2_max=vchi2_max)

    return ParticleCombinerWithPVs(
        particles=[protons, pions],
        pvs=pvs,
        DecayDescriptors=["[Lambda0 -> p+ pi-]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_omegallls(l0s,
                   kaons,
                   pvs,
                   am_min=(1672.45 - 42.5) * MeV,
                   am_max=(1672.45 + 42.5) * MeV,
                   apt_min=0.5 * GeV,
                   m_min=1640. * MeV,
                   m_max=1705. * MeV,
                   bpvipchi2_max=25.,
                   vchi2_max=10.,
                   bpvdira_min=0.9994,
                   bpvvdchi2_min=10,
                   bpvltime_min=0.002 * ns):
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {apt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       apt_min=apt_min)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})", "BPVIPCHI2() < {bpvipchi2_max}",
        "VFASPF(VCHI2) < {vchi2_max}", "BPVDIRA() > {bpvdira_min}",
        "BPVVDCHI2() > {bpvvdchi2_min}", "BPVLTIME() > {bpvltime_min}").format(
            m_min=m_min,
            m_max=m_max,
            bpvipchi2_max=bpvipchi2_max,
            vchi2_max=vchi2_max,
            bpvdira_min=bpvdira_min,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvltime_min=bpvltime_min)

    return ParticleCombinerWithPVs(
        particles=[l0s, kaons],
        pvs=pvs,
        DecayDescriptors=["[Omega~+ -> Lambda0 K+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_omegaddds(l0s,
                   kaons,
                   pvs,
                   am_min=(1672.45 - 42.5) * MeV,
                   am_max=(1672.45 + 42.5) * MeV,
                   apt_min=0.5 * GeV,
                   m_min=1640. * MeV,
                   m_max=1705. * MeV,
                   vchi2_max=10.,
                   bpvdira_min=0.9994,
                   bpvvdchi2_min=10,
                   bpvltime_min=0.002 * ns):
    combination_code = require_all("in_range({am_min}, AM, {am_max})",
                                   "APT > {apt_min}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       apt_min=apt_min)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})", "VFASPF(VCHI2) < {vchi2_max}",
        "BPVDIRA() > {bpvdira_min}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVLTIME() > {bpvltime_min}").format(
            m_min=m_min,
            m_max=m_max,
            vchi2_max=vchi2_max,
            bpvdira_min=bpvdira_min,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvltime_min=bpvltime_min)

    return ParticleCombinerWithPVs(
        particles=[l0s, kaons],
        pvs=pvs,
        DecayDescriptors=["[Omega~+ -> Lambda0 K+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@register_line_builder(all_lines)
def omegatol0k_l0toppilll_line(name="Hlt2PIDOmegaToL0K_L0ToPPiLLLLine",
                               prescale=1):
    pvs = make_pvs()
    protons = filter_long_particles(make_long_protons(), pvs)
    pions = filter_long_particles(make_long_pions(), pvs)
    kaons = filter_kaons(make_long_kaons(), pvs)
    l0lls = make_l0lls(protons, pions, pvs)
    omegallls = make_omegallls(l0lls, kaons, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [omegallls],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def omegatol0k_l0toppiddd_line(name="Hlt2PIDOmegaToL0K_L0ToPPiDDDLine",
                               prescale=1):
    pvs = make_pvs()
    protons = filter_down_particles(make_down_protons(), pvs)
    pions = filter_down_particles(make_down_pions(), pvs)
    kaons = filter_kaons(make_down_kaons(), pvs)
    l0dds = make_l0dds(protons, pions, pvs)
    omegaddds = make_omegaddds(l0dds, kaons, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [omegaddds],
        prescale=prescale,
    )
