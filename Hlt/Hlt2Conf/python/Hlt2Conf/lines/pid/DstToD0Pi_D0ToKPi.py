###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for ``D*(2010)+ -> D0 pi+`` with ``D0 -> K- pi+``.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Moore.config import HltLine, register_line_builder

from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from Hlt2Conf.standard_particles import (
    make_long_kaons,
    make_long_pions,
)
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from Hlt2Conf.algorithms import ParticleFilterWithPVs

all_lines = {}


def filter_particles(particles,
                     pvs,
                     pt_min=0.25 * GeV,
                     p_min=2.0 * GeV,
                     trchi2_max=3,
                     mipchi2_min=16):
    code = require_all('PT > {pt_min}', 'P > {p_min}',
                       'TRCHI2DOF < {trchi2_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           trchi2_max=trchi2_max,
                           mipchi2_min=mipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_d0s(kaons,
             pions,
             pvs,
             am_min=(1865. - 85.) * MeV,
             am_max=(1865. + 85.) * MeV,
             apt_min=1500 * MeV,
             amindoca_max=0.1 * mm,
             m_min=(1865. - 75.) * MeV,
             m_max=(1865. + 75.) * MeV,
             vchi2pdof_max=10,
             bpvvdchi2_min=49,
             bpvdira_min=0.9999):
    combination_code = require_all("in_range({am_min},  AM, {am_max})",
                                   "APT > {apt_min}",
                                   "AMINDOCA('') < {amindoca_max}").format(
                                       am_min=am_min,
                                       am_max=am_max,
                                       apt_min=apt_min,
                                       amindoca_max=amindoca_max)

    vertex_code = require_all(
        "in_range({m_min},  M, {m_max})", "CHI2VXNDOF < {vchi2pdof_max}",
        "BPVVDCHI2() > {bpvvdchi2_min}", "BPVDIRA() > {bpvdira_min}").format(
            m_min=m_min,
            m_max=m_max,
            vchi2pdof_max=vchi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvdira_min=bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=[kaons, pions],
        pvs=pvs,
        DecayDescriptors=["[D0 -> K- pi+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_dsts(d0s,
              pions,
              pvs,
              am_min=(2010. - 82.5) * MeV,
              am_max=(2010. + 82.5) * MeV,
              m_min=(2010. - 77.5) * MeV,
              m_max=(2010. + 77.5) * MeV,
              vchi2pdof_max=15):
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all("in_range({m_min}, M, {m_max})",
                              "CHI2VXNDOF < {vchi2pdof_max}").format(
                                  m_min=m_min,
                                  m_max=m_max,
                                  vchi2pdof_max=vchi2pdof_max)

    return ParticleCombinerWithPVs(
        particles=[d0s, pions],
        pvs=pvs,
        DecayDescriptors=["[D*(2010)+ -> D0 pi+]cc"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@register_line_builder(all_lines)
def dsttod0pi_d0tokpi_line(name="Hlt2PIDDstToD0Pi_D0ToKPiLine", prescale=1):
    pvs = make_pvs()
    kaons = filter_particles(make_long_kaons(), pvs)
    pions = filter_particles(make_long_pions(), pvs)
    slow_pions = filter_particles(
        make_long_pions(),
        pvs,
        pt_min=0.1 * GeV,
        p_min=1.0 * GeV,
        mipchi2_min=0.)
    d0s = make_d0s(kaons, pions, pvs)
    dsts = make_dsts(d0s, slow_pions, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [dsts],
        prescale=prescale,
    )
