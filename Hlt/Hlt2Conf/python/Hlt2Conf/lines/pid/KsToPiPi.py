###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for ``Ks0 -> pi+ pi-`` LL and DD.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Moore.config import HltLine, register_line_builder

from RecoConf.reco_objects_from_file import make_pvs, upfront_reconstruction

from Hlt2Conf.standard_particles import (make_long_pions, make_down_pions)
from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from Hlt2Conf.algorithms import ParticleFilterWithPVs

all_lines = {}


def filter_long_pions(particles, pvs, trchi2_max=3, mipchi2_min=36):
    code = require_all('TRCHI2DOF < {trchi2_max}',
                       'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
                           trchi2_max=trchi2_max, mipchi2_min=mipchi2_min)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def filter_down_pions(particles,
                      pvs,
                      p_min=3.0 * GeV,
                      pt_min=0.175 * GeV,
                      trchi2_max=3):
    code = require_all('P > {p_min}', 'PT > {pt_min}',
                       'TRCHI2DOF < {trchi2_max}').format(
                           p_min=p_min, pt_min=pt_min, trchi2_max=trchi2_max)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


def make_ks0lls(pions,
                pvs,
                am_min=(497.6 - 50.) * MeV,
                am_max=(497.6 + 50.) * MeV,
                m_min=(497.6 - 30.) * MeV,
                m_max=(497.6 + 30.) * MeV,
                bpvvdz_max=2200 * mm,
                vchi2_min=0,
                vchi2_max=16,
                vchi2pdof_max=30,
                bpvvdchi2_min=25,
                bpvltime_min=0.002 * ns):
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})", "BPVVDZ() < {bpvvdz_max}",
        "in_range({vchi2_min}, VFASPF(VCHI2), {vchi2_max})",
        "CHI2VXNDOF < {vchi2pdof_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVLTIME() > {bpvltime_min}").format(
            m_min=m_min,
            m_max=m_max,
            bpvvdz_max=bpvvdz_max,
            vchi2_min=vchi2_min,
            vchi2_max=vchi2_max,
            vchi2pdof_max=vchi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvltime_min=bpvltime_min)

    return ParticleCombinerWithPVs(
        particles=[pions],
        pvs=pvs,
        DecayDescriptors=["KS0 -> pi+ pi-"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def make_ks0dds(pions,
                pvs,
                am_min=(497.6 - 80.) * MeV,
                am_max=(497.6 + 80.) * MeV,
                m_min=(497.6 - 30.) * MeV,
                m_max=(497.6 + 30.) * MeV,
                bpvvdz_min=400 * mm,
                bpvvdz_max=2200 * mm,
                vchi2_min=0,
                vchi2_max=16,
                bpvvdchi2_min=25,
                bpvipchi2_max=150):
    combination_code = require_all("in_range({am_min}, AM, {am_max})").format(
        am_min=am_min, am_max=am_max)

    vertex_code = require_all(
        "in_range({m_min}, M, {m_max})",
        "in_range({bpvvdz_min}, BPVVDZ(), {bpvvdz_max})",
        "in_range({vchi2_min}, VFASPF(VCHI2), {vchi2_max})",
        "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVIPCHI2() < {bpvipchi2_max}").format(
            m_min=m_min,
            m_max=m_max,
            bpvvdz_min=bpvvdz_min,
            bpvvdz_max=bpvvdz_max,
            vchi2_min=vchi2_min,
            vchi2_max=vchi2_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvipchi2_max=bpvipchi2_max)

    return ParticleCombinerWithPVs(
        particles=[pions],
        pvs=pvs,
        DecayDescriptors=["KS0 -> pi+ pi-"],
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@register_line_builder(all_lines)
def kstopipill_line(name="Hlt2PIDKsToPiPiLLLine", prescale=0.0005):
    pvs = make_pvs()
    pions = filter_long_pions(make_long_pions(), pvs)
    ks0lls = make_ks0lls(pions, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [ks0lls],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def kstopipidd_line(name="Hlt2PIDKsToPiPiDDLine", prescale=0.0005):
    pvs = make_pvs()
    pions = filter_down_pions(make_down_pions(), pvs)
    ks0dds = make_ks0dds(pions, pvs)

    return HltLine(
        name=name,
        algs=upfront_reconstruction() + [ks0dds],
        prescale=prescale,
    )
