###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the PID calibration HLT2 lines
"""

from . import DetJpsiMuMuNegTagged
from . import DetJpsiMuMuPosTagged
from . import DstToD0Pi_D0ToKPi
from . import KsToPiPi
from . import OmegaToLambdaK

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(DetJpsiMuMuNegTagged.all_lines)
all_lines.update(DetJpsiMuMuPosTagged.all_lines)
all_lines.update(DstToD0Pi_D0ToKPi.all_lines)
all_lines.update(KsToPiPi.all_lines)
all_lines.update(OmegaToLambdaK.all_lines)
