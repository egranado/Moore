###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D+(s) -> h- h+ h+ HLT2 lines.

Final states built are:

1. D+ -> pi- pi+ pi+ and its charge conjugate
2. D+ ->  K- pi+ pi+ and its charge conjugate
3. D+_s ->  pi- pi+ pi+ and its charge conjugate
4. D+ -> K- K+ pi+ and its charge conjugate
5. D+ -> K- K+ K+ and its charge conjugate
6. D+_s -> K- K+ K+ and its charge conjugate
7. D+_s -> pi- K+ K+ and its charge conjugate
8. D+_s -> K- K+ pi+ and its charge conjugate
9. D+ -> pi- pi+ K+ and its charge conjugate
10. D+_s -> pi- pi+ K+ and its charge conjugate
11. D+ -> pi- K+ K+ and its charge conjugate
12. D+_s -> K- pi+ pi+ and its charge conjugate
"""
## TODO(EP): Include Hlt1 TOS

from __future__ import absolute_import, division, print_function
import math

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad, picosecond

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)

from ..algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs
from ..framework import configurable
from ..standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons

all_lines = {}


@configurable
def filter_particles(particles,
                     pvs,
                     trchi2_max=3,
                     mipchi2_min=4,
                     pt_min=250 * MeV,
                     p_min=2 * GeV,
                     pid=None):
    """Return maker for particles filtered by thresholds common to charm decay product selections.

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min)
    if pid is not None:
        code += ' & ({})'.format(pid)
    return ParticleFilterWithPVs(particles, pvs, Code=code)


@configurable
def make_pions(particles, pvs, pid='PIDK < 5'):
    """Return maker for pions filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@configurable
def make_kaons(particles, pvs, pid='PIDK > 5'):
    """Return maker for kaons filtered by thresholds common to charm decay product selections."""
    return filter_particles(particles, pvs, pid=pid)


@configurable
def make_dplus(particles,
               descriptors,
               pvs,
               am_min=1789 * MeV,
               am_max=1949 * MeV,
               amaxchild_pt_min=1000 * MeV,
               trk_2of3_pt_min=400 * MeV,
               aptsum_min=3000 * MeV,
               amaxchild_ipchi2_min=50,
               trk_2of3_ipchi2_min=10,
               vchi2pdof_max=6,
               bpvvdchi2_min=150,
               bpvltime_min=0.4 * picosecond,
               acos_bpvdira_max=10. * mrad):
    """Return Dplus maker with selection tailored for three-body hadronic final states.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min},  AM, {am_max})",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "ANUM( PT > {trk_2of3_pt_min} ) > 1",
        "(APT1 + APT2 + APT3) > {aptsum_min}",
        "AMAXCHILD(MIPCHI2DV(PRIMARY)) > {amaxchild_ipchi2_min}",
        "ANUM( MIPCHI2DV(PRIMARY) > {trk_2of3_ipchi2_min} ) > 1").format(
            am_min=am_min,
            am_max=am_max,
            amaxchild_pt_min=amaxchild_pt_min,
            trk_2of3_pt_min=trk_2of3_pt_min,
            aptsum_min=aptsum_min,
            amaxchild_ipchi2_min=amaxchild_ipchi2_min,
            trk_2of3_ipchi2_min=trk_2of3_ipchi2_min)

    cos_bpvdira_min = math.cos(acos_bpvdira_max)
    vertex_code = require_all("CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
                              "BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVLTIME() > {bpvltime_min}",
                              "BPVDIRA() > {cos_bpvdira_min}").format(
                                  vchi2pdof_max=vchi2pdof_max,
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvltime_min=bpvltime_min,
                                  cos_bpvdira_min=cos_bpvdira_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def charm_prefilters():
    """Return a list of prefilters common to charm HLT2 lines."""
    return [require_gec(), require_pvs(make_pvs())]


@register_line_builder(all_lines)
@configurable
def dplus2Kpipi_line(name='Hlt2CharmHadDToKmPipPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = make_dplus(
        particles=[kaons, pions],
        descriptors=['[D+ -> K- pi+ pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dplus2KKpi_line(name='Hlt2CharmHadDToKmKpPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dplus = make_dplus(
        particles=[kaons, pions], descriptors=['[D+ -> K- K+ pi+]cc'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipipi_line(name='Hlt2CharmHadDToPimPipPipLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs, pid='PIDK < 3')
    dplus = make_dplus(
        particles=[pions], descriptors=['[D+ -> pi- pi+ pi+]cc'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dplus2pipiK_line(name='Hlt2CharmHadDToPimPipKpLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs)
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dplus = make_dplus(
        particles=[pions, kaons],
        descriptors=['[D+ -> pi- pi+ K+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dplus2KKK_line(name='Hlt2CharmHadDToKmKpKpLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dplus = make_dplus(
        particles=[kaons],
        trk_2of3_ipchi2_min=4,
        amaxchild_ipchi2_min=10,
        vchi2pdof_max=10,
        descriptors=['[D+ -> K- K+ K+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dplus2piKK_line(name='Hlt2CharmHadDToPimKpKpLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs)
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dplus = make_dplus(
        particles=[pions, kaons], descriptors=['[D+ -> pi- K+ K+]cc'], pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2KKpi_line(name='Hlt2CharmHadDsToKmKpPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dsplus = make_dplus(
        particles=[kaons, pions],
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        acos_bpvdira_max=14.1,
        descriptors=['[D_s+ -> K- K+ pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2pipipi_line(name='Hlt2CharmHadDsToPimPipPipLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dsplus = make_dplus(
        particles=[pions],
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        aptsum_min=3200 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        descriptors=['[D_s+ -> pi- pi+ pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2pipiK_line(name='Hlt2CharmHadDsToPimPipKpLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs)
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dsplus = make_dplus(
        particles=[pions, kaons],
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        acos_bpvdira_max=14.1,
        descriptors=['[D_s+ -> pi- pi+ K+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2KKK_line(name='Hlt2CharmHadDsToKmKpKpLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dsplus = make_dplus(
        particles=[kaons],
        trk_2of3_ipchi2_min=4,
        amaxchild_ipchi2_min=10,
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        aptsum_min=3000 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        acos_bpvdira_max=14.1,
        vchi2pdof_max=10,
        descriptors=['[D_s+ -> K- K+ K+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2piKK_line(name='Hlt2CharmHadDsToPimKpKpLine', prescale=1):
    pvs = make_pvs()
    pions = make_pions(make_has_rich_long_pions(), pvs)
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    dsplus = make_dplus(
        particles=[pions, kaons],
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        acos_bpvdira_max=14.1,
        descriptors=['[D_s+ -> pi- K+ K+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def dsplus2Kpipi_line(name='Hlt2CharmhadDsToKmPipPipLine', prescale=1):
    pvs = make_pvs()
    kaons = make_kaons(make_has_rich_long_kaons(), pvs)
    pions = make_pions(make_has_rich_long_pions(), pvs)
    dsplus = make_dplus(
        particles=[kaons, pions],
        am_min=1879 * MeV,
        am_max=2059 * MeV,
        bpvvdchi2_min=100,
        bpvltime_min=0.2 * picosecond,
        acos_bpvdira_max=14.1,
        descriptors=['[D_s+ -> K- pi+ pi+]cc'],
        pvs=pvs)
    return HltLine(
        name=name,
        algs=upfront_reconstruction() + charm_prefilters() + [dsplus],
        prescale=prescale,
    )
