###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q charged hadrons taking into account RICHes acceptance and radiator thresholds.
Use track reduced chi square and ghost probability used in Run 2
Separate detached and prompt categories. Prompt to be cheched for rate.
Use DLL so far for PID response.
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs
from Hlt2Conf.framework import configurable

from Hlt2Conf.standard_particles import (
    make_has_rich_long_pions, make_has_rich_long_kaons,
    make_has_rich_long_protons, make_has_rich_down_pions)

####################################
# Charged hadron selections        #
####################################


@configurable
def make_charged_hadrons(
        make_particles=make_has_rich_long_pions,
        make_pvs=make_pvs,
        name="onia_has_rich_long_pions",
        pt_min=250. * MeV,  #TBC with Reco
        p_min=2.5 * GeV,
        p_max=150. * GeV,
        eta_min=2.,
        eta_max=5.,
        trchi2dof_max=4,  #TBC with Reco
        trghostprob_max=0.4,  #TBC with Reco
        mipchi2dvprimary_min=None,
        pid=None):

    code = require_all('PT > {pt_min}', 'in_range({p_min}, P, {p_max})',
                       'in_range({eta_min}, ETA, {eta_max})',
                       'TRCHI2DOF < {trchi2dof_max}',
                       'TRGHOSTPROB < {trghostprob_max}').format(
                           pt_min=pt_min,
                           p_min=p_min,
                           p_max=p_max,
                           eta_min=eta_min,
                           eta_max=eta_max,
                           trchi2dof_max=trchi2dof_max,
                           trghostprob_max=trghostprob_max)

    if pid is not None:
        code += ' & ({})'.format(pid)

    if mipchi2dvprimary_min is not None:
        code += '& (MIPCHI2DV(PRIMARY) > {mipchi2dvprimary_min})'.format(
            mipchi2dvprimary_min=mipchi2dvprimary_min)

    return ParticleFilterWithPVs(
        make_particles(), make_pvs(), name=name, Code=code)


####################################
# Detached                         #
####################################


@configurable
def make_detached_pions(
        name="onia_detached_pions",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDK < 0.'):
    """
    Return B&Q detached pions.
    """
    return make_charged_hadrons(
        name=name,
        make_particles=make_has_rich_long_pions,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_kaons(
        name="onia_detached_kaons",
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDK > 0.'):
    """
    Return B&Q detached kaons.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_long_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


@configurable
def make_detached_protons(
        name="onia_detached_protons",
        p_min=10. * GeV,
        mipchi2dvprimary_min=3.,  #TBC
        pid='PIDp > 0.'):
    """
    Return B&Q detached protons.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_long_protons,
        name=name,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid)


####################################
# Prompt                           #
####################################


@configurable
def make_prompt_pions(name="onia_prompt_pions", pid='PIDK < 0.'):
    """
    Return B&Q prompt pions.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_long_pions, name=name, pid=pid)


@configurable
def make_prompt_kaons(name="onia_prompt_kaons", pid='PIDK > 0.'):
    """
    Return B&Q prompt kaons.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_long_kaons, name=name, pid=pid)


@configurable
def make_prompt_protons(name="onia_prompt_protons",
                        p_min=10. * GeV,
                        pid='PIDp > 0.'):
    """
    Return B&Q prompt protons.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_long_protons,
        name=name,
        p_min=p_min,
        pid=pid)


####################################
# Downstream tracks                #
####################################
def make_down_pions(name="onia_down_pions", pid=None):
    """
    Return B&Q down hadrons with pion mass hypothesis.
    """
    return make_charged_hadrons(
        make_particles=make_has_rich_down_pions, name=name, pid=pid)
