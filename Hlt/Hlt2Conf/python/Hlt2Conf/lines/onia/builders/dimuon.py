###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q dimuon combinations.
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleFilterWithPVs
from Hlt2Conf.framework import configurable

from Hlt2Conf.standard_particles import make_dimuon_base

# mass window [MeV] around the Jpsi and Psi2s,
# applied to Jpsi and Psi2s lines [2018: 120 MeV]
_MASSWINDOW_JPSI = 120 * MeV
_MASSWINDOW_PSI2S = 120 * MeV
_MASSMIN_UPSILON = 7900 * MeV

# ProbNNmu requirements on the muons
# of the Jpsi and Psi2s lines [2018: 0.1]
_PROBNNMU_JPSI = 0.1
_PROBNNMU_PSI2S = 0.1
_PROBNNMU_UPSILON = 0.1


@configurable
def make_dimuon(
        make_particles=make_dimuon_base,
        make_pvs=make_pvs,
        name="onia_prompt_dimuon",
        minMass_dimuon=0 * MeV,
        minPt_dimuon=600 * MeV,
        minPt_muon=300 * MeV,
        maxVertexChi2=25,
        #maxTrackChi2_muon=10, # to be decided if we want to keep TrackChi2 cuts in the trigger
        maxTrackGhostProb_muon=0.4,  # this has to be reoptimised for the Upgrade
        minProbNN_muon=0.1,
        bpvdls_min=None):

    code = require_all(
        'MM > {minMass_dimuon}',
        'PT > {minPt_dimuon}',
        'MINTREE("mu-" == ABSID, PT) > {minPt_muon}',
        'VFASPF(VCHI2PDOF) < {maxVertexChi2}',
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #'MAXTREE("mu-" == ABSID, TRCHI2DOF) < {maxTrackChi2_muon}',
        'CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)',
        'CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)',
        'MINTREE("mu-" == ABSID, PROBNNmu) > {minProbNN_muon}'
    ).format(
        minMass_dimuon=minMass_dimuon,
        minPt_dimuon=minPt_dimuon,
        minPt_muon=minPt_muon,
        maxVertexChi2=maxVertexChi2,
        # to be decided if we want to keep TrackChi2 cuts in the trigger
        #maxTrackChi2_muon=maxTrackChi2_muon,
        maxTrackGhostProb_muon=maxTrackGhostProb_muon,
        minProbNN_muon=minProbNN_muon)

    if bpvdls_min is not None:
        code += '& (BPVDLS() > {bpvdls_min})'.format(bpvdls_min=bpvdls_min)

    return ParticleFilterWithPVs(
        make_particles(), make_pvs(), name=name, Code=code)


@configurable
def make_detached_dimuon(name="onia_detached_dimuon",
                         minProbNN_muon=0.1,
                         bpvdls_min=3.):
    """
    Make the detached dimuon.
    Used in Run 1-2 by many dimuon exclusive lines, previously named DetachedCommonFilter
    """

    return make_dimuon(
        name=name, minProbNN_muon=minProbNN_muon, bpvdls_min=bpvdls_min)


@configurable
def make_detached_dimuon_geo(name="onia_detached_dimuon_geo",
                             make_particles=make_dimuon_base,
                             make_pvs=make_pvs,
                             minIPChi2_muon=25,
                             minDecayLenghtSig=9):
    """
    Make the detached dimuon, with only geometrical requirements.
    Used in Run 2 by the HLT2DiMuonB2KSMuMu lines, previously named DetachedDiMuonFilter
    """

    code = require_all(
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > {minIPChi2_muon}",
        "BPVDLS() > {minDecayLenghtSig}",
    ).format(
        minIPChi2_muon=minIPChi2_muon,
        minDecayLenghtSig=minDecayLenghtSig,
    )

    return ParticleFilterWithPVs(
        make_particles(), make_pvs(), name=name, Code=code)


@configurable
def make_detached_jpsi(name="onia_detached_jpsi",
                       massWind_Jpsi=_MASSWINDOW_JPSI,
                       minPt_Jpsi=0 * MeV):

    code = require_all('ADMASS("J/psi(1S)") < {massWind_Jpsi}',
                       'PT > {minPt_Jpsi}').format(
                           massWind_Jpsi=massWind_Jpsi, minPt_Jpsi=minPt_Jpsi)

    dimuon = make_detached_dimuon(minProbNN_muon=_PROBNNMU_JPSI)

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_detached_psi2s(name="onia_detached_psi2s",
                        massWind_Psi2S=_MASSWINDOW_PSI2S,
                        minPt_Psi2S=0 * MeV):

    code = require_all('ADMASS("psi(2S)") < {massWind_Psi2S}',
                       'PT > {minPt_Psi2S}').format(
                           massWind_Psi2S=massWind_Psi2S,
                           minPt_Psi2S=minPt_Psi2S)

    dimuon = make_detached_dimuon(minProbNN_muon=_PROBNNMU_PSI2S)

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_jpsi(name="onia_jpsi",
              massWind_Jpsi=_MASSWINDOW_JPSI,
              minPt_Jpsi=0 * MeV):

    code = require_all('ADMASS("J/psi(1S)") < {massWind_Jpsi}',
                       'PT > {minPt_Jpsi}').format(
                           massWind_Jpsi=massWind_Jpsi, minPt_Jpsi=minPt_Jpsi)

    dimuon = make_dimuon(minProbNN_muon=_PROBNNMU_JPSI)

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_psi2s(name="onia_psi2s",
               massWind_Psi2S=_MASSWINDOW_PSI2S,
               minPt_Psi2S=0 * MeV,
               maxPt_Psi2S=100000 * GeV):

    code = require_all('ADMASS("psi(2S)") < {massWind_Psi2S}',
                       'PT > {minPt_Psi2S}', 'PT < {maxPt_Psi2S}').format(
                           massWind_Psi2S=massWind_Psi2S,
                           minPt_Psi2S=minPt_Psi2S,
                           maxPt_Psi2S=maxPt_Psi2S)

    dimuon = make_dimuon(minProbNN_muon=_PROBNNMU_PSI2S)

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_upsilon(name="onia_upsilon",
                 minMass_Upsilon=_MASSMIN_UPSILON,
                 minPt_Upsilon=0 * MeV):

    code = require_all('MM > {minMass_Upsilon}',
                       'PT > {minPt_Upsilon}').format(
                           minMass_Upsilon=minMass_Upsilon,
                           minPt_Upsilon=minPt_Upsilon)

    dimuon = make_dimuon(minProbNN_muon=_PROBNNMU_UPSILON)

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_z(name="onia_z", minMass_Z=40000 * MeV, minPt_Z=0 * MeV):

    code = require_all('MM > {minMass_Z}', 'PT > {minPt_Z}').format(
        minMass_Z=minMass_Z, minPt_Z=minPt_Z)

    dimuon = make_dimuon()

    return ParticleFilterWithPVs(dimuon, make_pvs(), name=name, Code=code)


@configurable
def make_soft_detached_dimuon(name="DiMuonDetachedSoftFilter",
                              make_particles=make_dimuon_base,
                              make_pvs=make_pvs,
                              minIP_muon=0.3 * mm,
                              maxTrackGhostProb_muon=0.4,
                              minIPChi2_muon=9,
                              maxIPChi2_muon=10000000000000,
                              minProbNN_muon=0.05,
                              minRho=3,
                              maxVz=650 * mm,
                              maxIPdistRatio=1. / 60.,
                              maxMass_dimuon=1000 * MeV,
                              maxVertexChi2=25,
                              maxDOCA=0.3,
                              minVDz=0.,
                              minBPVDira=0.,
                              maxCosAngle=0.999998):
    """
    Make detached soft dimuon.
    Used by the HLT2DiMuonSoft line.
    Ref: http://cds.cern.ch/record/2297352/files/LHCb-PUB-2017-023.pdf
    """
    code = require_all(
        "MINTREE('mu-' == ABSID, MIPDV(PRIMARY)) > {minIP_muon}",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),1)",
        "CHILDCUT((TRGHOSTPROB < {maxTrackGhostProb_muon}),2)",
        "MINTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) > {minIPChi2_muon}",
        "MAXTREE('mu-' == ABSID, MIPCHI2DV(PRIMARY)) < {maxIPChi2_muon}",
        "MINTREE('mu+'==ABSID,PROBNNmu) > {minProbNN_muon}",
        "VFASPF(sqrt(VX*VX+VY*VY)) > {minRho}",
        "VFASPF(VZ) < {maxVz}",
        "(MIPDV(PRIMARY)/BPVVDZ()) < {maxIPdistRatio}",
        "MM < {maxMass_dimuon}",
        "VFASPF(VCHI2PDOF) < {maxVertexChi2}",
        "DOCAMAX < {maxDOCA}",
        "BPVVDZ() > {minVDz}",
        "BPVDIRA() > {minBPVDira}",
        "PCUTA(ALV(1,2) < {maxCosAngle})",
    ).format(
        minIP_muon=minIP_muon,
        maxTrackGhostProb_muon=maxTrackGhostProb_muon,
        minIPChi2_muon=minIPChi2_muon,
        maxIPChi2_muon=maxIPChi2_muon,
        minProbNN_muon=minProbNN_muon,
        minRho=minRho,
        maxVz=maxVz,
        maxIPdistRatio=maxIPdistRatio,
        maxMass_dimuon=maxMass_dimuon,
        maxVertexChi2=maxVertexChi2,
        maxDOCA=maxDOCA,
        minVDz=minVDz,
        minBPVDira=minBPVDira,
        maxCosAngle=maxCosAngle,
    )

    return ParticleFilterWithPVs(
        make_particles(), make_pvs(), name=name, Code=code)
