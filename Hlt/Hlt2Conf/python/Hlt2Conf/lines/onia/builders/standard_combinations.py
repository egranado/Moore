###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q standard combinations as defined in Table 2 of the B&Q physics requirements document.
"""
from GaudiKernel.SystemOfUnits import MeV, picosecond

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms import require_all, ParticleCombinerWithPVs
from Hlt2Conf.framework import configurable

from .charged_hadrons import make_detached_pions, make_detached_kaons
from .dimuon import make_detached_jpsi


@configurable
def make_BpToJpsiKp(make_detached_jpsi=make_detached_jpsi,
                    make_detached_kaons=make_detached_kaons,
                    make_pvs=make_pvs,
                    adamass=200 * MeV,
                    vfaspf_max=25,
                    bpvltime_max=0.3 * picosecond):
    """
    Make the B&Q [B+ --> Jpsi K+]cc candidate.
    """
    jpsi = make_detached_jpsi()
    kaons = make_detached_kaons()
    pvs = make_pvs()

    combination_cut = require_all("ADAMASS('B+') < {adamass}").format(
        adamass=adamass)

    mother_cut = require_all("VFASPF(VCHI2PDOF) < {vfaspf_max}",
                             "BPVLTIME() < {bpvltime_max}").format(
                                 vfaspf_max=vfaspf_max,
                                 bpvltime_max=bpvltime_max)

    return ParticleCombinerWithPVs(
        particles=[jpsi, kaons],
        pvs=pvs,
        DecayDescriptors=["[B+ -> J/psi(1S) K+]cc"],
        CombinationCut=combination_cut,
        MotherCut=mother_cut)


@configurable
def make_BcToJpsiPi(make_detached_jpsi=make_detached_jpsi,
                    make_detached_pions=make_detached_pions,
                    make_pvs=make_pvs,
                    amass_min=5800 * MeV,
                    amass_max=7000 * MeV,
                    mass_min=6000 * MeV,
                    mass_max=6750 * MeV,
                    vfaspf_max=16,
                    bpvipchi2_max=25):
    """
    Make the [Bc+ --> Jpsi pi+]cc candidate. Combination and mother cuts from S34 Bc2JpsiHBDTLine.
    """
    jpsi = make_detached_jpsi()
    pions = make_detached_pions()
    pvs = make_pvs()

    combination_cut = require_all(
        "in_range({amass_min}, AM, {amass_max})").format(
            amass_min=amass_min, amass_max=amass_max)

    mother_cut = require_all("in_range({mass_min}, M, {mass_max})",
                             "VFASPF(VCHI2PDOF) < {vfaspf_max}",
                             "BPVIPCHI2() < {bpvipchi2_max}").format(
                                 mass_min=mass_min,
                                 mass_max=mass_max,
                                 vfaspf_max=vfaspf_max,
                                 bpvipchi2_max=bpvipchi2_max)

    return ParticleCombinerWithPVs(
        particles=[jpsi, pions],
        pvs=pvs,
        DecayDescriptors=["[B_c+ -> J/psi(1S) pi+]cc"],
        CombinationCut=combination_cut,
        MotherCut=mother_cut)
