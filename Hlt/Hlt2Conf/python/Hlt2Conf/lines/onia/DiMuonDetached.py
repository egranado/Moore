###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of detached dimuon lines
"""

from Moore.config import HltLine, register_line_builder

# get the prefilters
from .builders.prefilters import make_prefilters

# get the combination
from .builders.dimuon import make_detached_jpsi, make_detached_psi2s, make_soft_detached_dimuon
from .builders.charged_hadrons import make_charged_hadrons, make_down_pions

all_lines = {}


@register_line_builder(all_lines)
def JpsiToMuMuDetached_line(name='Hlt2JpsiToMuMuDetachedLine', prescale=1):
    extra_outputs = [
        ("charged_long_hadrons", make_charged_hadrons()),
        ("charged_down_pions", make_down_pions()),
    ]
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_detached_jpsi()],
        prescale=prescale,
        extra_outputs=extra_outputs)


@register_line_builder(all_lines)
def Psi2SToMuMuDetached_line(name='Hlt2Psi2SToMuMuDetachedLine', prescale=1):
    extra_outputs = [
        ("charged_long_hadrons", make_charged_hadrons()),
        ("charged_down_pions", make_down_pions()),
    ]
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_detached_psi2s()],
        prescale=prescale,
        extra_outputs=extra_outputs)


@register_line_builder(all_lines)
def DiMuonSoft_line(name='Hlt2DiMuonSoftLine', prescale=1):
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_soft_detached_dimuon()],
        prescale=prescale)
