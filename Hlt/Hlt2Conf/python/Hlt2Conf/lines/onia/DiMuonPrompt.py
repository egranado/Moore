###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of prompt dimuon lines
"""
from GaudiKernel.SystemOfUnits import MeV

from Moore.config import HltLine, register_line_builder

# get the prefilters
from .builders.prefilters import make_prefilters

# get the combination
from .builders.dimuon import make_jpsi, make_psi2s, make_upsilon, make_z

all_lines = {}


@register_line_builder(all_lines)
def JpsiToMuMu_line(name='Hlt2JpsiToMuMuLine', prescale=1):
    return HltLine(
        name=name, algs=make_prefilters() + [make_jpsi()], prescale=prescale)


@register_line_builder(all_lines)
def Psi2SToMuMu_line(name='Hlt2Psi2SToMuMuLine', prescale=1):
    return HltLine(
        name=name, algs=make_prefilters() + [make_psi2s()], prescale=prescale)


@register_line_builder(all_lines)
def JpsiToMuMuHighPt_line(name='Hlt2DiMuonJPsiHighPTLine', prescale=1):
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_jpsi(minPt_Jpsi=2000 * MeV)],
        prescale=prescale)


#@register_line_builder(all_lines)
#def Psi2SToMuMuLowPt_line(name='Hlt2DiMuonPsi2SLowPTLine', prescale=1):
#    """ [psi(2S) -> mu+ mu-] line"""
#    return HltLine(
#        name=name,
#        algs=make_prefilters() + [make_psi2s(maxPt_Psi2S=2500 * MeV)],
#        prescale=prescale)


@register_line_builder(all_lines)
def UpsilonToMuMu_line(name='Hlt2DiMuonUpsilonLine', prescale=1):
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_upsilon()],
        prescale=prescale)


@register_line_builder(all_lines)
def ZToMuMu_line(name='Hlt2DiMuonZLine', prescale=1):
    return HltLine(
        name=name, algs=make_prefilters() + [make_z()], prescale=prescale)
