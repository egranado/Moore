###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of Bc+ lines
"""

from Moore.config import HltLine, register_line_builder

# get the prefilters
from .builders.prefilters import make_prefilters

# get the combination
from .builders.standard_combinations import make_BcToJpsiPi

all_lines = {}


@register_line_builder(all_lines)
def Bc2ToJpsiPi_line(name='Hlt2BcpToJpsiPipLine', prescale=1):
    """[B_c+ --> Jpsi pi+]CC line"""
    return HltLine(
        name=name,
        algs=make_prefilters() + [make_BcToJpsiPi()],
        prescale=prescale)
