###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Rare Charm Makers that are used for the Rare charm HLT2 lines. For the definition of the lines see RareCharmLines.py

These are makers for HLT2 lines of rare charm decays with two leptons in the final state and their (prescaled) hadronic control channels. The lines have been transported from the Run1/2 to the new upgrade framework. Cuts will need retuning. Lines can be found in RareCharmLines.py

*******************************
Remarks:

- A dedicated four-body Dz combiner exists for the modes with at least one electron in the final state, where the cut on the Dz FDCHI2 is tighened to FDCHI2>49 wrt. to the muonic modes, where FDCHI2>16 is applied. This is taken from Run1/2 but might need retuning or can be equalized in the best case. Three body decays share a cut FDCHI<20 for hee and hmumu. In general, all cuts can be set individually for hhmumu and hhel modes, however, at the moment, all other cuts are shared between the two combiners. The same is true for the baryonic decays.
- Four-body and baryonic decays share the same preselection for the final state particles via make_selected_rarecharm_particles. The three body decays have a tighter IP cut (*tightSel versions of the maker, but they had a slightly looser momentum daughter cut (p>2GeV) during Run1/2, which is now set to 3GeV. To be checked if this is fine. The IPCHI2 cut in the preselction was set to IPCHI2>3, which we already applied in the stripping (instead of IPCHI2>2 before). PID cuts in the selection have to be checked (also for two-body decays)
- Track_Chi2 cut is commented out at the moment because this quantity is not available in the input objects.
- In order to avoid duplication of combinatorics of the charge-symmetric final state of the D0, only combinations labelled D0 are made. This leads to the second 'unphysical' decay descriptor for the D*-. See https://gitlab.cern.ch/lhcb/Moore/issues/64.
- before the four-body candidates are formed, intermediate two-particle objects are created to reduce combinatorics. These are generically denoted as Jpsi, phi, rho, eta. These have nothing to do with the physical meson states and there is no requirement on the dilepton mass. For same-sign lepton cominations, the two hadron-lepton combinations are allowed to be displaced. For this reason, the intermediate states are labelled with B0, B~0, B_s0 B_s~0 ,... , ie. intermediate states with a finite lifetime (otherwise the vertex fit of the combiner requires the particles to come from the same vertex).
*******************************
"""

from __future__ import absolute_import, division, print_function
from math import *

from Moore.config import HltLine, register_line_builder

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, mrad

from RecoConf.hlt1_tracking import require_pvs, require_gec
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)

from ..algorithms import require_all, ParticleCombinerWithPVs, ParticleFilterWithPVs, ParticleCombiner
from ..framework import configurable
from ..standard_particles import make_has_rich_long_pions, make_has_rich_long_kaons, make_ismuon_long_muon, make_long_electrons_no_brem, make_long_protons

# Charged pion mass in MeV
_PION_M = 139.57061 * MeV  # +/- 0.00024
"""****************************************"""
"""Common cuts"""
"""****************************************"""
"""Cuts for all rare charm particles for multibody and baryonic decays. At the moment, these cuts are shared among 3 and 4 body and baryonic decays. The two body lines have tighter cuts."""
"""****************************************"""

_TrkPt_Xll = 300.0 * MeV
_TrkP_Xll = 3000.0 * MeV
_TrkPVIPChi2_Xll = 3.0  # changed from 2 to 3 as in the stripping it was 3
_TrkPVIPChi2_Xll_tight = 5
_TrkChi2_Xll = 4.0
"""****************************************"""
"""Two body lines"""
"""****************************************"""

_TrackChi2_LL = 5
_TrkminP_LL = 4000 * MeV
_TrkminPT_LL = 750 * MeV
_TrkminIPChi2_LL = 3

_XmaxPT_LL = 1100 * MeV
_XmaxIPChi2_LL = 8

_DMassWin_LL = 70. * MeV
_DMassWinSig_LL = 300. * MeV
_D0MinPT_LL = 1800. * MeV
_doca_LL = 1.0 * mm

_DVChi2_LL = 10
_DDira_LL = 0.9997
_DMinFlightChi2_LL = 20
_DMaxIPChi2_LL = 15
"""****************************************"""
"""Two muon combinations"""
"""****************************************"""

_Pair_AMINDOCA_MAX_mumuX = 0.3 * mm  ###!!was 0.1, rather tight? compare to D combiner, the minDOCA cut is then implicitely fulfilled. Set 0.3
_Pair_BPVVDCHI2_MIN_mumuX = 9.0
_Pair_BPVVD_MIN_mumuX = 0.0 * mm

_Pair_AMASS_MAX_mumuX = 2100.0 * MeV
_Pair_SumAPT_MIN_mumuX = 0.0 * MeV  # seems also a bit useless..
_Pair_BPVCORRM_MAX_mumuX = 3500.0 * MeV
"""****************************************"""
"""Two electron combination"""
"""****************************************"""

_Pair_AMINDOCA_MAX_eeX = 0.3 * mm  ###!!was 0.1 rather tight? compare to D combiner, the minDOCA cut is then implicitely fulfilled. Set 0.3
_Pair_BPVVDCHI2_MIN_eeX = 9.0  # 0.0 potentially dangerous for electrons? Was set to 20 for e- in Run2, for the moment take the same cut as for muons.
_Pair_BPVVD_MIN_eeX = 0.0 * mm

_Pair_AMASS_MAX_eeX = 2100.0 * MeV  #added (was hardcoded in Run2)
_Pair_SumAPT_MIN_eeX = 0.0 * MeV  # seems also a bit useless..
_Pair_BPVCORRM_MAX_eeX = 3500.0 * MeV
"""****************************************"""
"""electron-muon combination"""
"""****************************************"""

_Pair_AMINDOCA_MAX_mueX = 0.3 * mm  ###!!was 0.1 rather tight? compare to D combiner, the minDOCA cut is then implicitely fulfilled. Set 0.3
_Pair_BPVVDCHI2_MIN_mueX = 9.0  # 0.0 potentially dangerous for electrons? Was set to 20 for e- in Run2, for the moment take the same cut as for muons.
_Pair_BPVVD_MIN_mueX = 0.0 * mm

_Pair_AMASS_MAX_mueX = 2100.0 * MeV  #added (was hardcoded in Run2)
_Pair_SumAPT_MIN_mueX = 0.0 * MeV  # seems also a bit useless..
_Pair_BPVCORRM_MAX_mueX = 3500.0 * MeV
"""****************************************"""
"""hadron-lepton combination"""
"""****************************************"""

_Pair_AMINDOCA_MAX_hlX = 0.1 * mm  # tighter cut on the dimuon objects, as no DOCA cut on the D is placed
"""****************************************"""
"""Dzero combiner cuts for D02HHLL"""
"""****************************************"""

_TrkPVIPChi2MAX_HHll = 9.0  # !!In stripping, one of the daughters has to fullfill MIPCHI2>9. Added this cut for now.
_Sum_SqrtTrkPVIPChi2_HHll = 8
_TrkPtMAX_HHll = 0.0 * MeV
_PairMinDoca_HHll = 0.2 * mm
_PairMaxDoca_HHll = 0.3 * mm
_VtxChi2_HHll = 15.0
_DIPChi2_HHll = 25.0
_DSumPt_HHll = 3000.0 * MeV  ##suspicious, check if variable makes sense. Kills also a lot of signal, in stripping Dpt>2000
_DDira_HHll = 0.9999
_MCOR_MAX_HHll = 3500.0 * MeV
_DPTMIN_Hll = 2000 * MeV  #newly added, was present in the stripping

_WideMass_M_MIN_HHll = 1650.0 * MeV
_WideMass_M_MAX_HHll = 2100.0 * MeV

_Mass_M_MIN_Control_HHll = 1765.0 * MeV
_Mass_M_MAX_Control_HHll = 1965.0 * MeV

#cuts that are different between muon und electron modes

_VtxPVDispChi2_HHll = 16.0
_VtxPVDispChi2_HHel = 49.0  # TODO try to lower that cut
"""****************************************"""
"""D combiner cuts for D02HLL"""
"""****************************************"""
""" for the _TrkChi2_HLL,_TrkPt_HLL,_TrkP,_HLLm_TrkPVIPChi2_HLL I take a common filter with the cuts listed at the very top for all rare charm decays (plus the _TrkPVIPChi2_Xll_tight). Can three body decays life with these? """

_TrkChi2_HLL = 3.0  #not used at the moment, see common cuts at the beginning of the file
_TrkPt_HLL = 300.0 * MeV  #not used at the moment, see common cuts at the beginning of the file
_TrkP_HLL = 2000.0 * MeV  #not used at the moment, see common cuts at the beginning of the file
_TrkPVIPChi2_HLL = 5.0  #not used at the moment, see common cuts at the beginning of the file
"""****************************************"""

_DMassWin_Hll = 200.0 * MeV
_DMAXDOCA_Hll = 0.15 * mm
_DMassLow_Hll = 1763.0 * MeV
_DimuonMass_Hll = 250.0 * MeV
_DVCHI2DOF_Hll = 5
_DIPCHI2_Hll = 25
_BPVVD_Hll = 20.0
_DDIRA_Hll = 0.9999
_Sum_SqrtTrkPVIPChi2_Hll = 8
"""****************************************"""
""" combiner cuts for lambdac"""
"""****************************************"""

_TrkPVIPChi2MAX_lambdac = 9.0  # !!unitless.In stripping, one of the daughters has to fullfill MIPCHI2>9. Added this cut for now.
_TrkPtMAX_lambdac = 0.0 * MeV
_PairMinDoca_lambdac = 0.1 * mm
_PairMaxDoca_lambdac = 0.25 * mm
_VtxPVDispChi2_lambdac = 20.0
_VtxChi2_lambdac = 20.0
_IPChi2_lambdac = 36.0
_SumPt_lambdac = 500.0 * MeV
_Dira_lambdac = 0.9999
_WideMass_M_MIN_lambdac = 1700.0 * MeV
_WideMass_M_MAX_lambdac = 2300.0 * MeV
_MCOR_MAX_lambdac = 3500.0 * MeV
_Sum_SqrtTrkPVIPChi2_lambdac = 8

_VtxPVDispChi2_lambdac_mumu = 20.0
_VtxPVDispChi2_lambdac_el = 49.0
"""****************************************"""
"""Dstar combiner cuts"""
"""****************************************"""

_Q_AM_MIN = 130.0 * MeV - _PION_M
_Q_M_MIN = 130.0 * MeV - _PION_M
_Q_AM_MAX = 180.0 * MeV - _PION_M
_Q_M_MAX = 170.0 * MeV - _PION_M
_TagVCHI2PDOF_MAX = 25.0
_Trk_Slowpi_TRCHI2DOF_MAX = 5  #cuts on slowpi are not yet included
_Trk_Slowpi_PT_MIN = 120 * MeV
"""****************************************"""


@configurable
def make_selected_rarecharm_particles(make_particles=make_has_rich_long_pions,
                                      make_pvs=make_pvs,
                                      trchi2_max=_TrkChi2_Xll,
                                      mipchi2_min=_TrkPVIPChi2_Xll,
                                      pt_min=_TrkPt_Xll,
                                      p_min=_TrkP_Xll,
                                      pid_cut=None):
    """Return maker for particles filtered by thresholds common to rare decay charm decay product selections. Some cuts for the multibudy decays have to be looser compared to the "standard two-body charm selection" At the momement the same cuts are applied to the three body lines, which had a tighther IP and looser momentum p cut

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        'PT > {pt_min}',
        'P > {p_min}',
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
        'MIPCHI2DV(PRIMARY) > {mipchi2_min}').format(
            pt_min=pt_min,
            p_min=p_min,
            trchi2_max=trchi2_max,
            mipchi2_min=mipchi2_min)
    if pid_cut is not None:
        code += ' & ({})'.format(pid_cut)
    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_selected_rarecharm_slowpions(make_particles=make_has_rich_long_pions,
                                      make_pvs=make_pvs,
                                      trchi2_max=_Trk_Slowpi_TRCHI2DOF_MAX,
                                      pt_min=_Trk_Slowpi_PT_MIN):
    """Return slow pions for rare charm decays

    Parameters
    ----------
    make_particles
        Particle maker function.
    make_pvs : callable
        Primary vertex maker function.

    Remaining parameters define thresholds for the selection.
    """
    code = require_all(
        'PT > {pt_min}'
        # TODO(AP): Cut value is reasonable for Run 2, but removes basically
        # everything in the upgrade sample
        # 'TRCHI2 < {trchi2_max}',
    ).format(
        pt_min=pt_min, trchi2_max=trchi2_max)

    return ParticleFilterWithPVs(make_particles(), make_pvs(), Code=code)


@configurable
def make_rarecharm_pions(pid_cut='PIDK < 3'):
    """Kept the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return pions filtered by thresholds common to rare charm decay product selections."""

    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_rarecharm_kaons(pid_cut='PIDK > 3'):
    """left the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return kaons filtered by thresholds common to rare charm decay product selections."""

    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_rarecharm_muons(pid_cut='PIDmu > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon, pid_cut=pid_cut)


@configurable
def make_rarecharm_electrons(pid_cut='PIDe > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem, pid_cut=pid_cut)


@configurable
def make_rarecharm_pions_tightSel(pid_cut='PIDK < 3'):
    """Kept the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return pions filtered by thresholds common to rare charm decay product selections."""

    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        mipchi2_min=_TrkPVIPChi2_Xll_tight)


@configurable
def make_rarecharm_kaons_tightSel(pid_cut='PIDK > 3'):
    """left the cuts from the two body decays, we had none in Run2 I think. This one has to be checked.
    Return kaons filtered by thresholds common to rare charm decay product selections."""

    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons,
        pid_cut=pid_cut,
        mipchi2_min=_TrkPVIPChi2_Xll_tight)


@configurable
def make_rarecharm_muons_tightSel(pid_cut='PIDmu > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        mipchi2_min=_TrkPVIPChi2_Xll_tight)


@configurable
def make_rarecharm_electrons_tightSel(pid_cut='PIDe > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem,
        pid_cut=pid_cut,
        mipchi2_min=_TrkPVIPChi2_Xll_tight)


@configurable
def make_rarecharm_protons(pid_cut='PIDp > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_long_protons, pid_cut=pid_cut)


@configurable
def make_rarecharm_noPID_pions(pid_cut=None):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions, pid_cut=pid_cut)


@configurable
def make_rarecharm_noPID_kaons(pid_cut=None):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons, pid_cut=pid_cut)


@configurable
def make_rarecharm_pions_forTwoBodyDecays(pid_cut='PIDK < 3'):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_pions,
        pid_cut=pid_cut,
        pt_min=_TrkminPT_LL,
        p_min=_TrkminP_LL,
        mipchi2_min=_TrkminIPChi2_LL,
        trchi2_max=_TrackChi2_LL)


@configurable
def make_rarecharm_kaons_forTwoBodyDecays(pid_cut='PIDK > 3'):
    return make_selected_rarecharm_particles(
        make_particles=make_has_rich_long_kaons,
        pid_cut=pid_cut,
        pt_min=_TrkminPT_LL,
        p_min=_TrkminP_LL,
        mipchi2_min=_TrkminIPChi2_LL,
        trchi2_max=_TrackChi2_LL)


@configurable
def make_rarecharm_muons_forTwoBodyDecays(pid_cut='PIDmu > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_ismuon_long_muon,
        pid_cut=pid_cut,
        pt_min=_TrkminPT_LL,
        p_min=_TrkminP_LL,
        mipchi2_min=_TrkminIPChi2_LL,
        trchi2_max=_TrackChi2_LL)


@configurable
def make_rarecharm_electrons_forTwoBodyDecays(pid_cut='PIDe > 0'):
    return make_selected_rarecharm_particles(
        make_particles=make_long_electrons_no_brem,
        pid_cut=pid_cut,
        pt_min=_TrkminPT_LL,
        p_min=_TrkminP_LL,
        mipchi2_min=_TrkminIPChi2_LL,
        trchi2_max=_TrackChi2_LL)


"""
Make two lepton combinations with very loose selection to reduce combinatorics
"""


@configurable
def make_rarecharm_TwoMuons(particles,
                            pvs,
                            amass=_Pair_AMASS_MAX_mumuX,
                            apt=_Pair_SumAPT_MIN_mumuX,
                            amindoca_max=_Pair_AMINDOCA_MAX_mumuX,
                            bpvcorrm_max=_Pair_BPVCORRM_MAX_mumuX,
                            bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_mumuX,
                            bpvvd_min=_Pair_BPVVD_MIN_mumuX):
    """
    add a dimuon object (generically denoted as JPsi, however obviously no cut on the dimuon mass)
    to allow for cuts on the dimoun combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons if needed. Set them to the same values at the moment.
    """

    descriptors = [
        'J/psi(1S) -> mu+ mu-', 'phi(1020) -> mu+ mu+', 'rho(770)0 -> mu- mu-'
    ]

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVCORRM() < {bpvcorrm_max}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvcorrm_max=bpvcorrm_max,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_TwoPions(particles,
                            pvs,
                            amass=_Pair_AMASS_MAX_mumuX,
                            apt=_Pair_SumAPT_MIN_mumuX,
                            amindoca_max=_Pair_AMINDOCA_MAX_mumuX,
                            bpvcorrm_max=_Pair_BPVCORRM_MAX_mumuX,
                            bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_mumuX,
                            bpvvd_min=_Pair_BPVVD_MIN_mumuX):
    """
    add a dipion object (generically denoted as JPsi,Phi,Rho, however obviously no cut on the mass) for the HHpipi control modes
    to allow for cuts on the dimoun combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons if needed. Cuts are set to the default values of the mumu combinations.
    """

    descriptors = [
        'J/psi(1S) -> pi+ pi-', 'phi(1020) -> pi+ pi+', 'rho(770)0 -> pi- pi-'
    ]

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVCORRM() < {bpvcorrm_max}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvcorrm_max=bpvcorrm_max,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_TwoElectrons(particles,
                                pvs,
                                amass=_Pair_AMASS_MAX_eeX,
                                apt=_Pair_SumAPT_MIN_eeX,
                                amindoca_max=_Pair_AMINDOCA_MAX_eeX,
                                bpvcorrm_max=_Pair_BPVCORRM_MAX_eeX,
                                bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_eeX,
                                bpvvd_min=_Pair_BPVVD_MIN_eeX):
    """
    add a dielectron object (generically denoted as JPsi,Phi,Rho, however obviously no cut on the dielectron mass)
    to allow for cuts on the dielectron combination and to speed up the code
    PID and pt cuts could directly be applied in make_rarecharm_electrons if needed.
    """

    descriptors = [
        'J/psi(1S) -> e+ e-', 'phi(1020) -> e+ e+', 'rho(770)0 -> e- e-'
    ]

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVCORRM() < {bpvcorrm_max}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvcorrm_max=bpvcorrm_max,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        pvs=pvs,
        particles=particles,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_ElectronMuon(particles,
                                pvs,
                                amass=_Pair_AMASS_MAX_mueX,
                                apt=_Pair_SumAPT_MIN_mueX,
                                amindoca_max=_Pair_AMINDOCA_MAX_mueX,
                                bpvcorrm_max=_Pair_BPVCORRM_MAX_mueX,
                                bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_mueX,
                                bpvvd_min=_Pair_BPVVD_MIN_mueX):
    """
    add a muon-electron object (generically denoted as JPsi, however, obviously no cut on the dilepton mass)
    to allow for cuts on the dilepton combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_electrons if needed.
    """

    #muons = particles
    descriptors = [
        'J/psi(1S) -> mu+ e-', 'phi(1020) -> e+ mu-', 'rho(770)0 -> mu+ e+',
        'eta -> mu- e-'
    ]

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVCORRM() < {bpvcorrm_max}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvcorrm_max=bpvcorrm_max,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_PionMuon(  # for LNV D->hhmumu with two seperated pi-mu vertices
        particles,
        pvs,
        amass=_Pair_AMASS_MAX_mumuX,
        apt=_Pair_SumAPT_MIN_mumuX,
        amindoca_max=_Pair_AMINDOCA_MAX_hlX,
        bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_mumuX,
        bpvvd_min=_Pair_BPVVD_MIN_mumuX):
    """
    add a combined pi-muon pbject object (generically denoted as JPsi and Phi, depending on the charge combination, however, obviously no cut on the combination mass)
    to allow for cuts on the diparticle combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_pions if needed.
    """

    descriptors = ['B0 -> pi+ mu-', 'B~0 -> pi- mu+']

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_KaonMuon(  # for LNV D->hhmumu with two seperated pi-mu vertices
        particles,
        pvs,
        amass=_Pair_AMASS_MAX_mumuX,
        apt=_Pair_SumAPT_MIN_mumuX,
        amindoca_max=_Pair_AMINDOCA_MAX_hlX,
        bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_mumuX,
        bpvvd_min=_Pair_BPVVD_MIN_mumuX):
    """
    add a combined pi-muon pbject object (generically denoted as JPsi, however obviously no cut on the combination mass)
    to allow for cuts on the diparticle combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_pions if needed.
    """

    descriptors = ['B0L -> K+ mu-', 'B0H -> K- mu+']

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_PionElectron(  # for LNV D->hhmumu with two seperated pi-mu vertices
        particles,
        pvs,
        amass=_Pair_AMASS_MAX_eeX,
        apt=_Pair_SumAPT_MIN_eeX,
        amindoca_max=_Pair_AMINDOCA_MAX_hlX,
        bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_eeX,
        bpvvd_min=_Pair_BPVVD_MIN_eeX):
    """
    add a combined pi-muon pbject object (generically denoted as JPsi, however obviously no cut on the combination mass)
    to allow for cuts on the diparticle combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_electrons and make_rarecharm_pions if needed.
    """

    descriptors = ['B_s0 -> pi+ e-', 'B_s~0 -> pi- e+']

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_KaonElectron(  # for LNV D->hhmumu with two seperated pi-mu vertices
        particles,
        pvs,
        amass=_Pair_AMASS_MAX_eeX,
        apt=_Pair_SumAPT_MIN_eeX,
        amindoca_max=_Pair_AMINDOCA_MAX_hlX,
        bpvvdchi2_min=_Pair_BPVVDCHI2_MIN_eeX,
        bpvvd_min=_Pair_BPVVD_MIN_eeX):
    """
    add a combined pi-muon pbject object (generically denoted as JPsi, however obviously no cut on the combination mass)
    to allow for cuts on the diparticle combination and to speed the code up;
    PID and pt cuts could directly be applied in make_rarecharm_muons and make_rarecharm_pions if needed.
    """

    descriptors = ['B_s0L -> K+ e-', 'B_s0H -> K- e+']

    combination_code = require_all("AM < {amass}",
                                   "AMINDOCA('') < {amindoca_max}",
                                   "(APT1+APT2) > {apt}").format(
                                       amass=amass,
                                       amindoca_max=amindoca_max,
                                       apt=apt)

    vertex_code = require_all("BPVVDCHI2() > {bpvvdchi2_min}",
                              "BPVVD() > {bpvvd_min}").format(
                                  bpvvdchi2_min=bpvvdchi2_min,
                                  bpvvd_min=bpvvd_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_d02hhmumu_dzeros(particles,
                                    descriptors,
                                    pvs,
                                    am_min=_WideMass_M_MIN_HHll,
                                    am_max=_WideMass_M_MAX_HHll,
                                    amaxchild_pt_min=_TrkPtMAX_HHll,
                                    apt_min=_DSumPt_HHll,
                                    amindoca_max=_PairMinDoca_HHll,
                                    amaxdoca_max=_PairMaxDoca_HHll,
                                    vchi2pdof_max=_VtxChi2_HHll,
                                    bpvcorrm_max=_MCOR_MAX_HHll,
                                    bpvvdchi2_min=_VtxPVDispChi2_HHll,
                                    bpvdira_min=_DDira_HHll,
                                    BPVIPCHI2=_DIPChi2_HHll,
                                    bpvipchi2_min=_DIPChi2_HHll,
                                    trkipchi2_max=_TrkPVIPChi2MAX_HHll,
                                    sumipchi2=_Sum_SqrtTrkPVIPChi2_HHll,
                                    dpt_min=_DPTMIN_Hll):
    """Return D0 maker with selection tailored for four-body dimuon final states.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min}-100*MeV,  AM, {am_max}+ 100*MeV)",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "(APT1+APT2+APT3+APT4) > {apt_min}", "AMINDOCA('') < {amindoca_max}",
        "AMAXDOCA('') < {amaxdoca_max}",
        "AMAXCHILD( MIPCHI2DV(PRIMARY) ) > {trkipchi2_max}").format(
            am_min=am_min,
            am_max=am_max,
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            amindoca_max=amindoca_max,
            amaxdoca_max=amaxdoca_max,
            trkipchi2_max=trkipchi2_max)

    vertex_code = require_all(
        "in_range({am_min},  M, {am_max})",
        "VFASPF(VCHI2PDOF) < {vchi2pdof_max}", "BPVVALID()",
        "BPVCORRM() < {bpvcorrm_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}", "BPVIPCHI2() < {bpvipchi2_min}",
        "PT > {dpt_min}",
        "( SUMTREE( ( (ID=='K+') | (ID=='K-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        #"SUMTREE(  math.sqrt(BPVIPCHI2()) , ( (ID=='K+') | (ID=='K-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-')) )  > {trkipchi2_max}").format(
        am_min=am_min,
        am_max=am_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvcorrm_max=bpvcorrm_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        bpvipchi2_min=bpvipchi2_min,
        dpt_min=dpt_min,
        sumipchi2=sumipchi2)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_d02hhel_dzeros(particles,
                                  descriptors,
                                  pvs,
                                  am_min=_WideMass_M_MIN_HHll,
                                  am_max=_WideMass_M_MAX_HHll,
                                  amaxchild_pt_min=_TrkPtMAX_HHll,
                                  apt_min=_DSumPt_HHll,
                                  amindoca_max=_PairMinDoca_HHll,
                                  amaxdoca_max=_PairMaxDoca_HHll,
                                  vchi2pdof_max=_VtxChi2_HHll,
                                  bpvcorrm_max=_MCOR_MAX_HHll,
                                  bpvvdchi2_min=_VtxPVDispChi2_HHel,
                                  bpvdira_min=_DDira_HHll,
                                  BPVIPCHI2=_DIPChi2_HHll,
                                  bpvipchi2_min=_DIPChi2_HHll,
                                  trkipchi2_max=_TrkPVIPChi2MAX_HHll,
                                  sumipchi2=_Sum_SqrtTrkPVIPChi2_HHll,
                                  dpt_min=_DPTMIN_Hll):
    """Return D0 maker with selection tailored for four-body semileptonic decays with at least one electron in the final state.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min}-100*MeV,  AM, {am_max}+ 100*MeV)",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "(APT1+APT2+APT3+APT4) > {apt_min}", "AMINDOCA('') < {amindoca_max}",
        "AMAXDOCA('') < {amaxdoca_max}",
        "AMAXCHILD( MIPCHI2DV(PRIMARY) ) > {trkipchi2_max}").format(
            am_min=am_min,
            am_max=am_max,
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            amindoca_max=amindoca_max,
            amaxdoca_max=amaxdoca_max,
            trkipchi2_max=trkipchi2_max)

    vertex_code = require_all(
        "in_range({am_min},  M, {am_max})",
        "VFASPF(VCHI2PDOF) < {vchi2pdof_max}", "BPVVALID()",
        "BPVCORRM() < {bpvcorrm_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}", "BPVIPCHI2() < {bpvipchi2_min}",
        "PT > {dpt_min}",
        "( SUMTREE( ( (ID=='K+') | (ID=='K-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        am_min=am_min,
        am_max=am_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvcorrm_max=bpvcorrm_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        bpvipchi2_min=bpvipchi2_min,
        trkipchi2_max=trkipchi2_max,
        sumipchi2=sumipchi2,
        dpt_min=dpt_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_d02hhll_sslepton_dzeros(particles,
                                           descriptors,
                                           pvs,
                                           am_min=_WideMass_M_MIN_HHll,
                                           am_max=_WideMass_M_MAX_HHll,
                                           amaxchild_pt_min=_TrkPtMAX_HHll,
                                           apt_min=_DSumPt_HHll,
                                           vchi2pdof_max=_VtxChi2_HHll,
                                           bpvcorrm_max=_MCOR_MAX_HHll,
                                           bpvvdchi2_min=_VtxPVDispChi2_HHll,
                                           bpvdira_min=_DDira_HHll,
                                           BPVIPCHI2=_DIPChi2_HHll,
                                           bpvipchi2_min=_DIPChi2_HHll,
                                           trkipchi2_max=_TrkPVIPChi2MAX_HHll,
                                           sumipchi2=_Sum_SqrtTrkPVIPChi2_HHll,
                                           dpt_min=_DPTMIN_Hll):
    """Return D0 maker with selection tailored for four body D decays with SS lepton final states with no DOCA cut, ie possible displaced hadron-lepton pairs.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        "in_range({am_min}-100*MeV,  AM, {am_max}+ 100*MeV)",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "(APT1+APT2+APT3+APT4) > {apt_min}",
        "AMAXCHILD( MIPCHI2DV(PRIMARY) ) > {trkipchi2_max}").format(
            am_min=am_min,
            am_max=am_max,
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            trkipchi2_max=trkipchi2_max)

    vertex_code = require_all(
        "in_range({am_min},  M, {am_max})",
        "VFASPF(VCHI2PDOF) < {vchi2pdof_max}", "BPVVALID()",
        "BPVCORRM() < {bpvcorrm_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}", "BPVIPCHI2() < {bpvipchi2_min}",
        "PT > {dpt_min}",
        "( SUMTREE( ( (ID=='K+') | (ID=='K-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        am_min=am_min,
        am_max=am_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvcorrm_max=bpvcorrm_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        bpvipchi2_min=bpvipchi2_min,
        trkipchi2_max=trkipchi2_max,
        dpt_min=dpt_min,
        sumipchi2=sumipchi2)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_d02ll_dzeros(particles,
                                descriptors,
                                pvs,
                                adamass=_DMassWinSig_LL,
                                amaxchild_pt_min=_XmaxPT_LL,
                                apt_min=_D0MinPT_LL,
                                amaxdoca_max=_doca_LL,
                                vchi2pdof_max=_DVChi2_LL,
                                bpvvdchi2_min=_DMinFlightChi2_LL,
                                bpvdira_min=_DDira_LL,
                                bpvipchi2_min=_DMaxIPChi2_LL,
                                maxIPChi2_min=_XmaxIPChi2_LL):

    combination_code = require_all(
        "ADAMASS('D0') < {adamass}", "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "APT > {apt_min}", "AMAXDOCA('') < {amaxdoca_max}").format(
            adamass=adamass,
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            amaxdoca_max=amaxdoca_max)

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}", "BPVVALID()",
        "BPVVDCHI2() > {bpvvdchi2_min}", "BPVDIRA() > {bpvdira_min}",
        "BPVIPCHI2() < {bpvipchi2_min}",
        "INGENERATION(BPVIPCHI2() > {maxIPChi2_min}, 1)").format(
            vchi2pdof_max=vchi2pdof_max,
            bpvvdchi2_min=bpvvdchi2_min,
            bpvdira_min=bpvdira_min,
            bpvipchi2_min=bpvipchi2_min,
            maxIPChi2_min=maxIPChi2_min)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_d2hll_ds(particles,
                            descriptors,
                            pvs,
                            adamass=_DMassWin_Hll,
                            amaxdoca_max=_DMAXDOCA_Hll,
                            am_min=_DMassLow_Hll,
                            am23=_DimuonMass_Hll,
                            vfaspf=_DVCHI2DOF_Hll,
                            bpvipchi2_max=_BPVVD_Hll,
                            bpvvdchi2_min=_BPVVD_Hll,
                            bpvdira_min=_DDIRA_Hll,
                            sumipchi2=_Sum_SqrtTrkPVIPChi2_Hll):
    """Return D0 maker with selection tailored for two-body leptonic final states.

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """

    combination_code = require_all("ADAMASS('D+') < {adamass}",
                                   "AMAXDOCA('') < {amaxdoca_max}",
                                   "AM  > {am_min}", "AM23 > {am23}").format(
                                       adamass=adamass,
                                       amaxdoca_max=amaxdoca_max,
                                       am_min=am_min,
                                       am23=am23)

    vertex_code = require_all(
        "VFASPF(VCHI2/VDOF) < {vfaspf}", "BPVVALID()",
        "BPVIPCHI2()  < {bpvipchi2_max}", " BPVVDCHI2() > {bpvvdchi2_min}",
        " BPVDIRA() > {bpvdira_min}",
        "( SUMTREE( ( (ID=='K+') | (ID=='K-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        vfaspf=vfaspf,
        bpvipchi2_max=bpvipchi2_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        sumipchi2=sumipchi2)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pmumu(
        particles,
        descriptors,
        pvs,
        am_min=_WideMass_M_MIN_lambdac,
        am_max=_WideMass_M_MAX_lambdac,
        amaxchild_pt_min=_TrkPtMAX_lambdac,
        apt_min=_SumPt_lambdac,
        amindoca_max=_PairMinDoca_lambdac,
        amaxdoca_max=_PairMaxDoca_lambdac,
        vchi2pdof_max=_VtxChi2_lambdac,
        bpvcorrm_max=_MCOR_MAX_lambdac,
        bpvvdchi2_min=_VtxPVDispChi2_lambdac_mumu,
        bpvdira_min=_Dira_lambdac,
        BPVIPCHI2=_IPChi2_lambdac,
        bpvipchi2_min=_IPChi2_lambdac,
        trkipchi2_max=_TrkPVIPChi2MAX_lambdac,
        sumipchi2=_Sum_SqrtTrkPVIPChi2_lambdac,
):
    """Return lamdba maker decaying to proton and two leptons

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """

    combination_code = require_all(
        "AM < 2400",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "(APT1+APT2+APT3) > {apt_min}",
        "AMINDOCA('') < {amindoca_max}",
        "AMAXDOCA('') < {amaxdoca_max}",  #.format(
        "AMAXCHILD( MIPCHI2DV(PRIMARY) ) > {trkipchi2_max}").format(
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            amindoca_max=amindoca_max,
            amaxdoca_max=amaxdoca_max,
            trkipchi2_max=trkipchi2_max)

    vertex_code = require_all(
        "in_range({am_min},  M, {am_max})",
        "VFASPF(VCHI2PDOF) < {vchi2pdof_max}", "BPVVALID()",
        "BPVCORRM() < {bpvcorrm_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}", "BPVIPCHI2() < {bpvipchi2_min}",
        "( SUMTREE( ( (ID=='p+') | (ID=='p~-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        am_min=am_min,
        am_max=am_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvcorrm_max=bpvcorrm_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        bpvipchi2_min=bpvipchi2_min,
        trkipchi2_max=trkipchi2_max,
        sumipchi2=sumipchi2)

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_rarecharm_lambdacs2pel(particles,
                                descriptors,
                                pvs,
                                am_min=_WideMass_M_MIN_lambdac,
                                am_max=_WideMass_M_MAX_lambdac,
                                amaxchild_pt_min=_TrkPtMAX_lambdac,
                                apt_min=_SumPt_lambdac,
                                amindoca_max=_PairMinDoca_lambdac,
                                amaxdoca_max=_PairMaxDoca_lambdac,
                                vchi2pdof_max=_VtxChi2_lambdac,
                                bpvcorrm_max=_MCOR_MAX_lambdac,
                                bpvvdchi2_min=_VtxPVDispChi2_lambdac_el,
                                bpvdira_min=_Dira_lambdac,
                                BPVIPCHI2=_IPChi2_lambdac,
                                bpvipchi2_min=_IPChi2_lambdac,
                                sumipchi2=_Sum_SqrtTrkPVIPChi2_lambdac,
                                trkipchi2_max=_TrkPVIPChi2MAX_lambdac):
    """Return lamdba maker decaying to proton and two leptons

    Parameters
    ----------
    particles : list
        Input particles used in the combination.
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """

    combination_code = require_all(
        "AM < 2400",
        "AMAXCHILD(PT) > {amaxchild_pt_min}",
        "(APT1+APT2+APT3) > {apt_min}",
        "AMINDOCA('') < {amindoca_max}",
        "AMAXDOCA('') < {amaxdoca_max}",  #.format(
        "AMAXCHILD( MIPCHI2DV(PRIMARY) ) > {trkipchi2_max}").format(
            amaxchild_pt_min=amaxchild_pt_min,
            apt_min=apt_min,
            amindoca_max=amindoca_max,
            amaxdoca_max=amaxdoca_max,
            trkipchi2_max=trkipchi2_max)

    vertex_code = require_all(
        "in_range({am_min},  M, {am_max})",
        "VFASPF(VCHI2PDOF) < {vchi2pdof_max}", "BPVVALID()",
        "BPVCORRM() < {bpvcorrm_max}", "BPVVDCHI2() > {bpvvdchi2_min}",
        "BPVDIRA() > {bpvdira_min}", "BPVIPCHI2() < {bpvipchi2_min}",
        "( SUMTREE( ( (ID=='p+') | (ID=='p~-') | (ID=='pi+') | (ID=='pi-') | (ID=='mu+') | (ID=='mu-')  | (ID=='e+') | (ID=='e-') ), sqrt(BPVIPCHI2()) ) >  {sumipchi2})"
    ).format(
        am_min=am_min,
        am_max=am_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvcorrm_max=bpvcorrm_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvdira_min=bpvdira_min,
        bpvipchi2_min=bpvipchi2_min,
        trkipchi2_max=trkipchi2_max,
        sumipchi2=sumipchi2
    )  #not used right now in the vertex code, replaced by a cut in the combination

    return ParticleCombinerWithPVs(
        particles=particles,
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


@configurable
def make_dstars(dzeros,
                soft_pions,
                descriptors,
                pvs,
                q_am_min=_Q_AM_MIN,
                q_am_max=_Q_AM_MAX,
                q_m_min=_Q_M_MIN,
                q_m_max=_Q_M_MAX,
                vchi2pdof_max=_TagVCHI2PDOF_MAX):
    """Return D*+ maker for tagging a D0 with a soft pion.

    Parameters
    ----------
    dzeros
        D0 particles.
    soft_pions
        Soft pion particles.ok top
    descriptors : list
        Decay descriptors to be reconstructed.
    pvs
        Primary vertices used for computing PV-related information.

    Remaining parameters define thresholds for the selection.
    """

    combination_code = require_all(
        "in_range({q_am_min}, (AM - AM1 - AM2), {q_am_max})").format(
            q_am_min=q_am_min,
            q_am_max=q_am_max,
        )

    vertex_code = require_all(
        "CHI2VXNDOF < {vchi2pdof_max}",
        "in_range({q_m_min}, (M - M1 - M2), {q_m_max})").format(
            q_m_min=q_m_min, q_m_max=q_m_max, vchi2pdof_max=vchi2pdof_max)

    return ParticleCombinerWithPVs(
        particles=[dzeros, soft_pions],
        pvs=pvs,
        DecayDescriptors=descriptors,
        CombinationCut=combination_code,
        MotherCut=vertex_code)


def charm_prefilters():
    """Return a list of prefilters common to charm HLT2 lines."""
    return [require_gec(), require_pvs(make_pvs())]
