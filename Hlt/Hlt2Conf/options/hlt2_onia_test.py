###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/Hlt2_onia_test.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from Hlt2Conf.lines.onia import all_lines

# When running from Upgrade MC, must use the post-juggling locations of the raw event
raw_event_format = 4.3

# a list of available samples for testing
#decay_descriptor = 30000000  # MinBias
#decay_descriptor = 18112001  # inclusive Upsilon
#decay_descriptor = 24142001  # inclusive Jpsi
#decay_descriptor = 28142001  # Psi2s --> mu mu
decay_descriptor = 12143001  # B --> Jpsi K
#decay_descriptor = 28144041  # Chic --> Jpsi mu mu
#decay_descriptor = 11114100  # B --> Ks mu mu
#decay_descriptor = 13144011  # Bs --> Jpsi phi

# FT decoder version to use
ftdec_v = -1

# set the input files and the detector conditions
input_files = []
DDDBTag = ""
CONDDBTag = ""

# output file name
outputfile_name = ""

if decay_descriptor == 30000000:

    #input_files = [
    #    # MinBias 30000000
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
    #    'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
    #]
    #
    #ftdec_v = 4
    #DDDBTag = 'dddb-20171126'
    #CONDDBTag = 'sim-20171127-vc-md100'
    #inputFileType = 'ROOT'
    #outputfile_name = "MinBias_30000000_HLT2.dst"

    # HLT1-filtered
    input_files = [
        # MinBias 30000000
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/30000000/LDST
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000001_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000002_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000003_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000004_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000005_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000006_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000007_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000008_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000010_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000012_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000014_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000016_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000017_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000018_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000019_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000020_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000021_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000022_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000023_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000025_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000026_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000028_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000029_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000030_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000031_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000032_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000033_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000034_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000035_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000036_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000037_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000038_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000039_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000040_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000041_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000043_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000044_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000045_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000047_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000048_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000049_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000050_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000051_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000052_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000054_1.ldst',
        'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000055_1.ldst',
    ]

    ftdec_v = 4
    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "MinBias_30000000_HLT1filtered_HLT2.dst"

elif decay_descriptor == 18112001:
    input_files = [
        # inclusive Upsilon, 18112001
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/18112001/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000016_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000017_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000020_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000024_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000026_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000027_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000028_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000029_2.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070550/0000/00070550_00000030_2.ldst",
    ]

    # HLT1-filtered
    #input_files = [
    #    # inclusive Upsilon, 18112001
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/18112001/LDST
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000001_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000002_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000003_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000004_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000005_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000006_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000007_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000008_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076733/0000/00076733_00000043_1.ldst",
    #]

    ftdec_v = 2
    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = "inclUpsilon_18112001_HLT2.dst"

elif decay_descriptor == 24142001:

    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/inclJpsi/inclJpsi_24142001_BrunelReco.ldst"
    ]

    # HLT1-filtered
    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/inclJpsi/inclJpsi_24142001_BrunelReco_HLT1filtered.dst"
    #]

    ftdec_v = 2
    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "inclJpsi_24142001_HLT2.dst"

elif decay_descriptor == 28142001:

    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Psi2s/Psi2s_28142001_BrunelReco.ldst"
    #]

    # HLT1-filtered
    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Psi2s/Psi2s_28142001_BrunelReco_HLT1filtered.dst"
    ]

    ftdec_v = 2
    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "Psi2s_28142001_HLT2.dst"

elif decay_descriptor == 12143001:

    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_2.ldst",
    ]

    # HL1-filtered
    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2JpsiK/B2JpsiK_12143001_BrunelReco_HLT1filtered.dst",
    #]

    ftdec_v = 2
    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "B2JpsiK_12143001_HLT2.dst"

elif decay_descriptor == 28144041:

    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_2.ldst",
    ]

    # HLT1-filtered
    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/Chic2JpsiMuMu/Chic2JpsiMuMu_28144041_BrunelReco_HLT1filtered.dst",
    #    ]

    ftdec_v = 2
    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "ChicJpsiDiMuMu_28144040_HLT2.dst"

elif decay_descriptor == 11114100:

    #input_files = [
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_1.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_2.ldst",
    #]

    # HLT1-filtered
    input_files = [
        "root://eoslhcb.cern.ch//eos/lhcb/wg/BandQ/trigger/Upgrade/MC_samples/B2KsMuMu/B2KsMuMu_11114100_BrunelReco_HLT1filtered.dst",
    ]

    ftdec_v = 2
    DDDBTag = 'dddb-20171122'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = "ROOT"
    outputfile_name = "B2KsMuMu_28144040_HLT2.dst"

elif decay_descriptor == 13144011:

    #input_files = [
    #    # Bs2JpsiPhi, 13144011
    #    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/13144011/LDST
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000011_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000019_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000024_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000035_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000046_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000054_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000068_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000072_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000087_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000093_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000109_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000110_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000111_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000112_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000122_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000125_2.ldst",
    #    "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00070705/0000/00070705_00000126_2.ldst",
    #    ]

    # HLT1-filtered
    input_files = [
        # Bs2JpsiPhi, 13144011
        # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/13144011/LDST
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000001_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000002_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000003_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000004_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000005_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000006_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000007_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000064_1.ldst",
        "root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00076706/0000/00076706_00000068_1.ldst",
    ]

    ftdec_v = 4
    DDDBTag = 'dddb-20171126'
    CONDDBTag = 'sim-20171127-vc-md100'
    inputFileType = 'ROOT'
    outputfile_name = ""

# set the options
options.evt_max = 5000
options.input_files = input_files
options.data_type = 'Upgrade'
options.dddb_tag = DDDBTag
options.conddb_tag = CONDDBTag
options.simulation = True
options.input_type = inputFileType
options.input_raw_format = raw_event_format


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)

# setup the output file
#from PyConf.environment import setupOutput
#
#setupOutput(outputfile_name, 'ROOT')
