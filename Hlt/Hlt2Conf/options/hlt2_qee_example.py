###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Example options file for testing QEE Hlt2 lines on signal MC.
    Run like any other options file:

    ./Moore/run gaudirun.py Moore/Hlt/Hlt2Conf/options/hlt2_qee_example.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
# TODO stateProvider_with_simplified_geom must go away from option files
from Hlt2Conf.lines.qee import all_lines
# Add other lines here...

input_files = [
    # W+jet 42311011 upgrade sample. See https://its.cern.ch/jira/browse/LHCBGAUSS-1837
    # Not HLT1 filtered.
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000010_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000011_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000012_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000013_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000014_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000015_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000016_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000017_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000018_2.xdst",
    "root://x509up_u1000@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00100893/0000/00100893_00000020_2.xdst"
]

options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event
options.input_files = input_files
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20190223'
options.conddb_tag = 'sim-20180530-vc-mu100'
options.simulation = True
options.scheduler_legacy_mode = True
options.evt_max = 1000

# Write the output file
options.output_file = 'hlt2_qee_lines.dst'
options.output_type = 'ROOT'


def make_lines():
    return [builder() for builder in all_lines.values()]


# Uncomment the following to increase the output verbosity
from Gaudi.Configuration import DEBUG
#options.output_level = DEBUG

public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
