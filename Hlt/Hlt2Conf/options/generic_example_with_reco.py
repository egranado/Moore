###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore
from Hlt2Conf.lines.generic import new_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt1_tracking import default_ft_decoding_version
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles


def make_lines():
    return [builder() for builder in new_lines.values()]


default_ft_decoding_version.global_bind(value=4)
# global_reco.global_bind(from_file=False)
public_tools = [stateProvider_with_simplified_geom()]


with reconstruction.bind(from_file=False),\
     make_charged_protoparticles.bind(enable_muon_id=True):
    run_moore(options, make_lines, public_tools)
