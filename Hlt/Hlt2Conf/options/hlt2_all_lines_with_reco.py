###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines with on-the-fly reconstruction.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_all_lines.py
"""

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.protoparticles import make_charged_protoparticles

options.evt_max = 100
from Moore.config import add_line_to_registry

from Hlt2Conf.lines import (
    B2JpsiK,
    Bs2JpsiPhi,
    D02HH,
    D2HHH,
    D02HHHH,
    b_to_open_charm,
    RareCharmLines,
    Chic2JpsiMuMu,
    B2KsMuMu,
    InclusiveDetachedDilepton,
    inclusive_radiative_b,
    onia,
    topological_b,
    Jets,
)

from Hlt2Conf.lines.jets import Hlt2Run2Jets

__all__ = [
    'all_lines',
    'modules',
]


def _get_all_lines(modules):
    all_lines = {}
    for module in modules:
        try:
            lines = module.all_lines
        except AttributeError:
            raise AttributeError(
                'line module {} does not define mandatory `all_lines`'.format(
                    module.__name__))
        for name, maker in lines.items():
            if name.lower().find("gamma") != -1 or name.lower().find(
                    "pi0") != -1:
                # ResolvedPi0Maker and PhotonMaker are not functional.
                print("{} not added ".format(name))
                continue
            add_line_to_registry(all_lines, name, maker)
    return all_lines


modules = [
    #Hlt2Run2Jets, # crash, segfault
    B2JpsiK,
    Bs2JpsiPhi,
    D02HH,
    D2HHH,
    D02HHHH,
    b_to_open_charm,
    RareCharmLines,
    Chic2JpsiMuMu,
    B2KsMuMu,
    InclusiveDetachedDilepton,
    inclusive_radiative_b,
    onia,
    topological_b,
    # Jets, # FIXME some jet lines accept >50% of events
]

all_lines = _get_all_lines(modules)
print("Number of HLT2 lines {}".format(len(all_lines)))


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False),\
     make_charged_protoparticles.bind(enable_muon_id=True):
    run_moore(options, make_lines, public_tools)
