###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_example.py
"""
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
# TODO stateProvider_with_simplified_geom must go away from option files

###############################################################################
# B2OC: configure which lines to test
###############################################################################

# Run on ALL B2OC lines:
from Hlt2Conf.lines.b_to_open_charm import all_lines

# Run on a specific subset of lines: (uncomment ONE line)
#from Hlt2Conf.lines.b_to_open_charm.b_to_dh import all_lines
#from Hlt2Conf.lines.b_to_open_charm.b_to_dd import all_lines
#from Hlt2Conf.lines.b_to_open_charm.lb_to_lc import all_lines

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line

from Hlt2Conf.lines.b_to_open_charm.b_to_dh import BdToDsmK_DsmToHHH_BDT_line

###############################################################################
# configure input data set (uncomment the "root:// .... " lines)
###############################################################################
# TIP: to go from a BK path to a root-URL
# 1) lb-run LHCbDirac dirac-bookkeeping-get-files -B <BK path> # to get list of LFN's
# 2) lb-run LHCbDirac dirac-dms-lfn-accessURL --Protocol="xroot" -l <an LFN> # to get root-URL
# Update sample BK paths are:
# Without HLT1:
#   /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/<EVT NUMBER>/LDST
# After HLT1:
#   /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/<EVT NUMBER>/LDST

input_files = [
    # MinBias 30000000 BEFORE Hlt1
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/30000000/LDST
    # 'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00069155/0000/00069155_00000878_2.ldst'
    # MinBias 30000000 AFTER Hlt1
    # evt+std://MC/Upgrade/Beam7000GeV-Upgrade-MagUp-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/30000000/LDST
    # 'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/Upgrade/LDST/00077381/0000/00077381_00000801_1.ldst'

    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/30000000/LDST
    # https://gitlab.cern.ch/lhcb-b2oc/b2ocupgrade/-/blob/master/Moore_input_files/min_bias.ldst has a large number
    # of input files (>100) in a "ready to plug" format, here only the first 5 are listed
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000001_1.ldst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000002_1.ldst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000003_1.ldst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000004_1.ldst',
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075219/0000/00075219_00000005_1.ldst',

    # B2OC upgrade MC:
    # Bu2DK_D2KsHH, 12165106
    # evt+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/12165106/LDST
    # 'root://door05.pic.es:1094/pnfs/pic.es/data/lhcb/MC/Upgrade/LDST/00070352/0000/00070352_00000047_2.ldst'
    # evt+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/12165106/LDST
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075817/0000/00075817_00000003_1.ldst'

    # B2OC upgrade MC:
    # DsK, 13264031
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/13264031/LDST
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000001_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000003_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000004_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000005_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000006_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000007_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000009_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000010_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000011_1.ldst
    # /lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000012_1.ldst
    'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000001_1.ldst'
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000003_1.ldst',
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000004_1.ldst',
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000005_1.ldst',
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000006_1.ldst',
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000007_1.ldst',
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000009_1.ldst',
    # 'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000010_1.ldst',
    # 'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000011_1.ldst',
    # 'root://ccxrootdlhcb.in2p3.fr//pnfs/in2p3.fr/data/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000012_1.ldst'

    # B2OC upgrade MC:
    # DDK, 11196010
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/11196010/LDST
    # /lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst'

    # B2OC upgrade MC:
    # DDK, 11196010
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/11196010/LDST
    # /lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst'

    # B2OC upgrade MC:
    # DDK, 11196010
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/11196010/LDST
    # /lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00074194/0000/00074194_00000109_2.ldst'

    # B2OC upgrade MC:
    # D*Kpi, 11164611
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim10-Up03-OldP8Tuning/Reco-Up02/11164611/XDST
    # /lhcb/MC/Upgrade/XDST/00102881/0000//00102881_00000001_2.xdst
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDST/00102881/0000/00102881_00000001_2.xdst'

    # MC sample from hlt_example
    # D*-tagged D0 to KK, 27163002
    # sim+std://MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/27163002/LDST
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00075807/0000/00075807_00000009_1.ldst'
    # 'root://lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/MC/Upgrade/LDST/00070317/0000/00070317_00000012_2.ldst'

    # B2OC upgrade MC:
    # B2DD 11296013
    # /MC/Upgrade/Beam7000GeV-Upgrade-MagDown-Nu7.6-25ns-Pythia8/Sim09c-Up02/Reco-Up01/Trig0x52000000/11296013/LDST
    # /lhcb/MC/Upgrade/LDST/00075825/0000/00075825_00000002_1.ldst
    # 'root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/LDST/00075825/0000/00075825_00000002_1.ldst'
]

options.input_files = input_files
options.input_type = 'ROOT'
options.input_raw_format = 4.3
# When running from Upgrade MC, must use the post-juggling locations of the raw
# event

# Write the output file
options.output_file = 'hlt2_b2oc_lines.dst'
options.output_type = 'ROOT'

###############################################################################
# Set a reasonable number of events
###############################################################################

options.evt_max = 1000
options.simulation = True
options.data_type = 'Upgrade'
options.dddb_tag = 'dddb-20171126'
options.conddb_tag = 'sim-20171127-vc-md100'


def make_lines():
    lines = [builder() for builder in all_lines.values()]
    lines.append(twobody_line())
    lines.append(threebody_line())
    lines.append(
        BdToDsmK_DsmToHHH_BDT_line(
            name='Hlt2B2OC_BdToDsmK_DsmToHHH_GenBDTGt0d0_Line', MVACut=0.))
    return lines


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
