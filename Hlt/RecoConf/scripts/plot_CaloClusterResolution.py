###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to plot ECAL cluster resolution

author: Sasha Zenaiev (oleksandr.zenaiev@cern.ch)
date:   03/02/2020

"""

from ROOT import (TH2F, TFile, TCanvas, gDirectory, TGraphErrors, gStyle,
                  gROOT, TLegend, TPolyLine, EColor)
from array import array
import sys
import os

# cosmetics
color = [
    EColor.kBlack, EColor.kBlue, EColor.kRed, EColor.kMagenta,
    EColor.kGreen + 2, EColor.kYellow + 1, EColor.kAzure + 4,
    EColor.kOrange + 2, EColor.kRed - 7, EColor.kBlue - 9, EColor.kRed + 3,
    EColor.kViolet - 7
]  #, EColor.kSpring + 5]
markerSize = 1

# skip converted gammas
flagNoConv = True

# produce plots for several values of matchFraction (reasonable values seem to be from ~0.8 to ~1.0, bt check that there is no unexpected dependence on them)
list_matchFractions = [
    0.50, 0.60, 0.70, 0.80, 0.90, 0.95, 0.975, 1.00, 1.025, 1.05, 1.10, 1.20
]
#list_matchFractions = [0.95]

# axes ranges on plots
plotRange = {
    "xMinDep": 0.40,
    "xMaxDep": 1.25,
    "yMinDep": -5,
    "yMaxDep": 45,
}


def style():
    gStyle.SetOptStat(0000000000)
    gROOT.SetBatch()


def SetGraphsCosmetics(graphs, markerSize, markerStyle, color):
    for g in graphs:
        g.SetMarkerStyle(markerStyle)
        g.SetMarkerSize(markerSize)
        g.SetMarkerColor(color)
        g.SetLineColor(color)


def plot_resolution(nameFileIn, nameDirOut, matchFraction, nentries, inputName,
                    prefixTitle):
    nbins_eRes = 8
    min_eRes = 0.1
    max_eRes = 0.5
    bins_energy = [
        min_eRes + (max_eRes - min_eRes) * x / nbins_eRes
        for x in range(0, nbins_eRes + 1)
    ]

    leg = TLegend(0.14, 0.55, 0.87, 0.89)
    leg.SetNColumns(3)
    leg.AddEntry(0, "", "")
    leg.AddEntry(0, "Mean[#DeltaE/E,%]", "")
    leg.AddEntry(0, "RMS[#DeltaE/E,%]", "")

    g_eRes_mean = []
    g_eRes_rms = []
    g_xRes_mean = []
    g_xRes_rms = []
    g_yRes_mean = []
    g_yRes_rms = []
    g_Res_mean = []
    g_Res_rms = []

    fIn = TFile.Open(nameFileIn)

    for iName, name in enumerate(inputName):
        tupleName = name[0]
        shortName = name[1]

        g_eRes_mean.append(TGraphErrors())
        g_eRes_rms.append(TGraphErrors())
        g_xRes_mean.append(TGraphErrors())
        g_xRes_rms.append(TGraphErrors())
        g_yRes_mean.append(TGraphErrors())
        g_yRes_rms.append(TGraphErrors())
        g_Res_mean.append(TGraphErrors())
        g_Res_rms.append(TGraphErrors())
        SetGraphsCosmetics(
            graphs=[
                g_eRes_mean[-1], g_xRes_mean[-1], g_yRes_mean[-1],
                g_Res_mean[-1]
            ],
            markerSize=markerSize,
            color=color[iName],
            markerStyle=20)
        SetGraphsCosmetics(
            graphs=[
                g_eRes_rms[-1], g_xRes_rms[-1], g_yRes_rms[-1], g_Res_rms[-1]
            ],
            markerSize=2 * markerSize,
            color=color[iName],
            markerStyle=24)

        # energy resolution
        c_eRes = TCanvas("c_eRes_{}_{}".format(name, matchFraction))
        c_eRes.Divide(nbins_eRes)
        tree = fIn.Get(tupleName)
        #fIn.ls()
        #print(hex(id(tree)))
        for i in range(0, nbins_eRes):
            tree.Draw(
                "((energyCluster - energy) / energy) >> h_eRes(1, -10, 10)",
                "maxMatchFraction > {} && 1.0 / sqrt(energy / 1000.0) > {} && 1.0 / sqrt(energy / 1000.0) < {} && (flagConv == 0 || {})"
                .format(matchFraction, bins_energy[i], bins_energy[i + 1],
                        int(not flagNoConv)), "", nentries)
            h_eRes = gDirectory.Get("h_eRes")
            x = bins_energy[i] + (bins_energy[i + 1] - bins_energy[i]) / 2
            g_eRes_mean[-1].SetPoint(i, x, 100 * h_eRes.GetMean())
            g_eRes_mean[-1].SetPointError(i, 0, 100 * h_eRes.GetMeanError())
            g_eRes_rms[-1].SetPoint(i, x, 100 * h_eRes.GetRMS())
            g_eRes_rms[-1].SetPointError(i, 0, 100 * h_eRes.GetRMSError())
        c_eRes_summary = TCanvas("c_eRes_summary_{}_{}".format(
            name, matchFraction))
        c_eRes_summary.cd()
        hrange_eRes_summary = TH2F("", "", 1, min_eRes, max_eRes, 1, -10.0,
                                   20.0)
        hrange_eRes_summary.GetXaxis().SetTitle("1/#sqrt{E/GeV}")
        hrange_eRes_summary.GetYaxis().SetTitle("#DeltaE/E (%)")
        hrange_eRes_summary.GetYaxis().SetTitleOffset(1.0)
        hrange_eRes_summary.Draw()
        c_eRes_summary.cd()
        g_eRes_mean[-1].Draw("p")
        g_eRes_rms[-1].Draw("p")

        # x resolution
        tree.Draw(
            "(xCluster - xExtrap) >> h_xRes(1, -1e6, 1e6)",
            "maxMatchFraction > {} && (flagConv == 0 || {})".format(
                matchFraction, int(not flagNoConv)), "", nentries)
        h_xRes = gDirectory.Get("h_xRes")
        #print("h_xRes entries: {}".format(int(h_xRes.GetEntries())))
        g_Res_mean[-1].SetPoint(g_Res_mean[-1].GetN(), 0.1 + iName * 0.1,
                                h_xRes.GetMean())
        g_Res_mean[-1].SetPointError(g_Res_mean[-1].GetN() - 1, 0.0,
                                     h_xRes.GetMeanError())
        g_Res_rms[-1].SetPoint(g_Res_rms[-1].GetN(), 0.1 + iName * 0.1,
                               h_xRes.GetRMS())
        g_Res_rms[-1].SetPointError(g_Res_rms[-1].GetN() - 1, 0.0,
                                    h_xRes.GetRMSError())

        # y resolution
        tree.Draw(
            "(yCluster - yExtrap) >> h_yRes(1, -1e6, 1e6)",
            "maxMatchFraction > {} && (flagConv == 0 || {})".format(
                matchFraction, int(not flagNoConv)), "", nentries)
        h_yRes = gDirectory.Get("h_yRes")
        #print("h_yRes entries: {}".format(int(h_yRes.GetEntries())))
        g_Res_mean[-1].SetPoint(g_Res_mean[-1].GetN(), 1.1 + iName * 0.1,
                                h_yRes.GetMean())
        g_Res_mean[-1].SetPointError(g_Res_mean[-1].GetN() - 1, 0.0,
                                     h_yRes.GetMeanError())
        g_Res_rms[-1].SetPoint(g_Res_rms[-1].GetN(), 1.1 + iName * 0.1,
                               h_yRes.GetRMS())
        g_Res_rms[-1].SetPointError(g_Res_rms[-1].GetN() - 1, 0.0,
                                    h_yRes.GetRMSError())

        # E resolution
        tree.Draw(
            "(energyCluster - energy) / energy * 100 >> h_eRes(1, -1e6, 1e6)",
            "maxMatchFraction > {} && (flagConv == 0 || {})".format(
                matchFraction, int(not flagNoConv)), "", nentries)
        h_eRes = gDirectory.Get("h_eRes")
        #print("h_eRes entries: {}".format(int(h_eRes.GetEntries())))
        g_Res_mean[-1].SetPoint(g_Res_mean[-1].GetN(), 2.1 + iName * 0.1,
                                h_eRes.GetMean())
        g_Res_mean[-1].SetPointError(g_Res_mean[-1].GetN() - 1, 0.0,
                                     h_eRes.GetMeanError())
        g_Res_rms[-1].SetPoint(g_Res_rms[-1].GetN(), 2.1 + iName * 0.1,
                               h_eRes.GetRMS())
        g_Res_rms[-1].SetPointError(g_Res_rms[-1].GetN() - 1, 0.0,
                                    h_eRes.GetRMSError())

        leg.AddEntry(0, "{}".format(shortName), "")
        leg.AddEntry(
            g_eRes_mean[-1], "{:.3f}+-{:.3f}".format(
                round(h_eRes.GetMean(), 3), round(h_eRes.GetMeanError(), 3)),
            "pe")
        leg.AddEntry(
            g_eRes_rms[-1], "{:.3f}+-{:.3f}".format(
                round(h_eRes.GetRMS(), 3), round(h_eRes.GetRMSError(), 3)),
            "pe")

    c_eRes_summary.cd()
    leg.Draw()

    gStyle.SetPadGridX(1)
    c_Res_summary = TCanvas("c_eRes_summary_{}".format(matchFraction))
    c_Res_summary.cd()
    hrange_Res_summary = TH2F(
        "", prefixTitle + ", matchFraction > {}, flagConv = {}".format(
            matchFraction, not flagNoConv), 3, 0, 3, 10, plotRange["yMinDep"],
        plotRange["yMaxDep"])
    hrange_Res_summary.GetXaxis().SetBinLabel(1, "#Deltax (mm)")
    hrange_Res_summary.GetXaxis().SetBinLabel(2, "#Deltay (mm)")
    hrange_Res_summary.GetXaxis().SetBinLabel(3, "#DeltaE/E (%)")
    hrange_Res_summary.GetXaxis().SetLabelSize(
        hrange_Res_summary.GetXaxis().GetLabelSize() * 1.6)
    hrange_Res_summary.GetYaxis().SetTitleOffset(1.3)
    hrange_Res_summary.GetYaxis().SetTitle("mm, %")
    hrange_Res_summary.Draw()
    pl = TPolyLine(2, array('f', [0.0, 3.0]), array('f', [0.0, 0.0]))
    pl.Draw()
    for iName, name in enumerate(inputName):
        g_Res_mean[iName].Draw("p")
        #g_Res_mean[iName].Print()
        g_Res_rms[iName].Draw("p")
        #g_Res_rms[iName].Print()
    leg.Draw()
    nameFileOut = nameDirOut + '/Res_summary_matchFraction{}'.format(
        matchFraction)
    c_Res_summary.SaveAs(nameFileOut + '.png')
    c_Res_summary.SaveAs(nameFileOut + '.pdf')
    gStyle.SetPadGridX(0)

    fIn.Close()

    return [g_Res_mean, g_Res_rms]


def make_plots(nameFileIn, nameDirOut, list_matchFractions, nentries,
               inputName, prefixTitle):
    x_mean = [[] for i in range(nalg)]
    x_mean_err = [[] for i in range(nalg)]
    x_rms = [[] for i in range(nalg)]
    x_rms_err = [[] for i in range(nalg)]
    y_mean = [[] for i in range(nalg)]
    y_mean_err = [[] for i in range(nalg)]
    y_rms = [[] for i in range(nalg)]
    y_rms_err = [[] for i in range(nalg)]
    e_mean = [[] for i in range(nalg)]
    e_mean_err = [[] for i in range(nalg)]
    e_rms = [[] for i in range(nalg)]
    e_rms_err = [[] for i in range(nalg)]

    for matchFraction in list_matchFractions:
        graphs_mean, graphs_rms = plot_resolution(
            nameFileIn=nameFileIn,
            nameDirOut=nameDirOut,
            matchFraction=matchFraction,
            nentries=nentries,
            inputName=list(zip(trees, labels)),
            prefixTitle=prefixTitle)

        for a in range(0, nalg):
            x_mean[a].append(graphs_mean[a].GetY()[0])
            x_mean_err[a].append(graphs_mean[a].GetErrorY(0))
            x_rms[a].append(graphs_rms[a].GetY()[0])
            x_rms_err[a].append(graphs_rms[a].GetErrorY(0))
            y_mean[a].append(graphs_mean[a].GetY()[1])
            y_mean_err[a].append(graphs_mean[a].GetErrorY(1))
            y_rms[a].append(graphs_rms[a].GetY()[1])
            y_rms_err[a].append(graphs_rms[a].GetErrorY(1))
            e_mean[a].append(graphs_mean[a].GetY()[2])
            e_mean_err[a].append(graphs_mean[a].GetErrorY(2))
            e_rms[a].append(graphs_rms[a].GetY()[2])
            e_rms_err[a].append(graphs_rms[a].GetErrorY(2))

    np = len(x_mean[0])
    #print('np = {}'.format(np))
    g_x_mean = []
    g_x_rms = []
    g_y_mean = []
    g_y_rms = []
    g_e_mean = []
    g_e_rms = []
    for a in range(0, nalg):
        g_x_mean.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', x_mean[a]), array('f', [0.0] * np),
                         array('f', x_mean_err[a])))
        g_x_rms.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', x_rms[a]), array('f', [0.0] * np),
                         array('f', x_rms_err[a])))
        SetGraphsCosmetics([g_x_mean[-1]], 1, 20, color[a])
        SetGraphsCosmetics([g_x_rms[-1]], 2, 24, color[a])

        g_y_mean.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', y_mean[a]), array('f', [0.0] * np),
                         array('f', y_mean_err[a])))
        g_y_rms.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', y_rms[a]), array('f', [0.0] * np),
                         array('f', y_rms_err[a])))
        SetGraphsCosmetics([g_y_mean[-1]], 1, 20, color[a])
        SetGraphsCosmetics([g_y_rms[-1]], 2, 24, color[a])

        g_e_mean.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', e_mean[a]), array('f', [0.0] * np),
                         array('f', e_mean_err[a])))
        g_e_rms.append(
            TGraphErrors(np, array('f', list_matchFractions),
                         array('f', e_rms[a]), array('f', [0.0] * np),
                         array('f', e_rms_err[a])))
        SetGraphsCosmetics([g_e_mean[-1]], 1, 20, color[a])
        SetGraphsCosmetics([g_e_rms[-1]], 2, 24, color[a])

    # x
    c_matchFractionDependence = TCanvas()
    c_matchFractionDependence.Divide(2, 2)
    hr_x = TH2F("", "", 1, plotRange["xMinDep"], plotRange["xMaxDep"], 1,
                plotRange["yMinDep"], plotRange["yMaxDep"])
    hr_x.GetXaxis().SetTitle('matchFraction')
    hr_x.GetYaxis().SetTitle('#Deltax, mm')
    c_matchFractionDependence.cd(1)
    hr_x.Draw()
    plx = TPolyLine(2, array('f',
                             [plotRange["xMinDep"], plotRange["xMaxDep"]]),
                    array('f', [0.0, 0.0]))
    plx.Draw()
    for g in g_x_mean:
        g.Draw("lp")
        #g.Print()
    for g in g_x_rms:
        g.Draw("lp")
    leg_x = TLegend(0.08, 0.12, 0.92, 0.88)
    leg_x.SetNColumns(2)
    leg_x.AddEntry(0, "Mean", "")
    leg_x.AddEntry(0, "RMS", "")
    for a in range(0, nalg):
        leg_x.AddEntry(g_e_mean[a], labels[a], "pe")
        leg_x.AddEntry(g_e_rms[a], labels[a], "pe")

    # y
    hr_y = TH2F("", "", 1, plotRange["xMinDep"], plotRange["xMaxDep"], 1,
                plotRange["yMinDep"], plotRange["yMaxDep"])
    hr_y.GetXaxis().SetTitle('matchFraction')
    hr_y.GetYaxis().SetTitle('#Deltay, mm')
    c_matchFractionDependence.cd(2)
    hr_y.Draw()
    ply = TPolyLine(2, array('f',
                             [plotRange["xMinDep"], plotRange["xMaxDep"]]),
                    array('f', [0.0, 0.0]))
    ply.Draw()
    for g in g_y_mean:
        g.Draw("lp")
        #g.Print()
    for g in g_y_rms:
        g.Draw("lp")

    # E
    hr_e = TH2F("", "", 1, plotRange["xMinDep"], plotRange["xMaxDep"], 1,
                plotRange["yMinDep"], plotRange["yMaxDep"])
    hr_e.GetXaxis().SetTitle('matchFraction')
    hr_e.GetYaxis().SetTitle('#DeltaE/E, %')
    c_matchFractionDependence.cd(3)
    hr_e.Draw()
    ple = TPolyLine(2, array('f',
                             [plotRange["xMinDep"], plotRange["xMaxDep"]]),
                    array('f', [0.0, 0.0]))
    ple.Draw()
    for g in g_e_mean:
        g.Draw("lp")
        #g.Print()
    for g in g_e_rms:
        g.Draw("lp")

    c_matchFractionDependence.cd(4)
    leg_x.Draw()

    c_matchFractionDependence.SaveAs(nameDirOut + '/Res_matchFractionDep.pdf')
    c_matchFractionDependence.SaveAs(nameDirOut + '/Res_matchFractionDep.png')


if __name__ == '__main__':

    # what to plot (mandatory)
    if len(sys.argv) < 2:
        print(
            'Usage: <script> <particle> <inputfile> (<particle> could be "gamma" or "pi0"'
        )
        sys.exit(1)
    particle = sys.argv[1]
    if particle not in ["gamma", "pi0"]:
        print(
            'Usage: <script> <particle> <inputfile> (<particle> could be "gamma" or "pi0"'
        )
        sys.exit(1)

    # input ROOT tuple (optional, default is from the reco test which creates it)
    nameFileIn = 'outputfile_calo_res_' + particle + '.root'
    if len(sys.argv) == 3:
        nameFileIn = sys.argv[2]

    style()

    # created plots go to the same directory where input file
    nameDirOut = os.path.dirname(nameFileIn)
    if nameDirOut == "":
        nameDirOut = "./"
    nameDirOut = nameDirOut + '/plots_' + particle
    #nameDirOut = './Moore/build.x86_64-centos7-gcc9-opt/Hlt/RecoConf/tests_tmp'
    if not os.path.isdir(nameDirOut):
        os.mkdir(nameDirOut)

    if particle == 'gamma':
        prefixTitle = '#gamma from B_{d}#rightarrowK^{*}#gamma'
        #trees = ['CaloClusterResolutionRaw/clustersRaw', 'CaloClusterResolutionOverlap/clustersOverlap', 'CaloClusterResolutionOverlapFast/clustersOverlapFast']
        #labels = ['Raw', 'Overlap', 'OverlapFast']
        trees = [
            'CaloClusterResolutionNoOverlap/clustersNoOverlap',
            'CaloClusterResolutionOverlapDef/clustersOverlapDef',
            'CaloClusterResolutionOverlapFast/clustersOverlapFast',
            'CaloClusterResolutionOverlapNoCor/clustersOverlapNoCor',
            'CaloHypoResolutionNoOverlap/photonsNoOverlap',
            'CaloHypoResolutionOverlapDef/photonsOverlapDef',
            'CaloHypoResolutionOverlapFast/photonsOverlapFast',
            'CaloHypoResolutionOverlapNoCor/photonsOverlapNoCor'
        ]
        labels = [
            'clustersNoOverlap', 'clustersOverlapDef', 'clustersOverlapFast',
            'clustersOverlapNoCor', 'photonsNoOverlap', 'photonsOverlapDef',
            'photonsOverlapFast', 'photonsOverlapNoCor'
        ]
        #nameFileIn = '/home/lzenaiev/stack/calores-gamma-tuples/outputfile.root-BIG'

    elif particle == 'pi0':
        prefixTitle = '#pi^{0} from B_{d}#rightarrow#pi^{+}#pi^{-}#pi^{0}'
        trees = [
            'CaloHypoResolution-clusDef-pi0Def/pi0-clusDef-pi0Def',
            'CaloHypoResolution-clusSOFast-pi0Def/pi0-clusSOFast-pi0Def',
            'CaloHypoResolution-clusSONoCor-pi0Def/pi0-clusSONoCor-pi0Def',
            'CaloHypoResolution-clusSONoCor-pi0SOFast/pi0-clusSONoCor-pi0SOFast',
            'CaloHypoResolution-clusSONoCor-pi0SONoCor/pi0-clusSONoCor-pi0SONoCor',
            'CaloHypoResolution-clusSONoCor-pi0NoSO/pi0-clusSONoCor-pi0NoSO',
            'CaloHypoResolution-clusNoSO-pi0Def/pi0-clusNoSO-pi0Def',
            'CaloHypoResolution-clusNoSO-pi0NoSO/pi0-clusNoSO-pi0NoSO',
            'CaloHypoResolution-clusDef-pi0NoCor/pi0-clusDef-pi0NoCor',
        ]
        labels = [
            'clusDef-pi0Def', 'clusSOFast-pi0Def', 'clusSONoCor-pi0Def',
            'clusSONoCor-pi0SOFast', 'clusSONoCor-pi0SONoCor',
            'clusSONoCor-pi0NoSO', 'clusNoSO-pi0Def', 'clusNoSO-pi0NoSO',
            'clusDef-pi0NoCor'
        ]
        #nameFileIn = '/home/lzenaiev/stack/calores-pi0-tuples/outputfile.root-BIG-NEW'  # 135K
        plotRange["yMinDep"] = -10
        plotRange["yMaxDep"] = 60

    nalg = len(trees)
    print('nameFileIn: {}'.format(nameFileIn))
    print('nameDirOut: {}'.format(nameDirOut))
    print('nalg: {}'.format(nalg))
    nentries = 1000000000000
    make_plots(nameFileIn, nameDirOut, list_matchFractions, nentries,
               list(zip(trees, labels)), prefixTitle)
