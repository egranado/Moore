###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import require_gec
from RecoConf.hlt1_allen import make_allen_tracks, make_dumped_raw_banks
from RecoConf.mc_checking import tracker_dumper, pv_dumper, get_track_checkers
from Configurables import RunAllen

dumpBinaries = False
outputDir = "dump/"


def hlt1_reco_allen_tracks():

    allen_tracks = make_allen_tracks()

    types_and_locations_for_checkers = {
        "Velo": allen_tracks["Velo"],
        "Upstream": allen_tracks["Upstream"],
        "Forward": allen_tracks["Forward"],
    }
    data = get_track_checkers(types_and_locations_for_checkers)

    if dumpBinaries:
        data.append(make_dumped_raw_banks(output_dir=outputDir + "banks"))
        data.append(
            tracker_dumper(
                dump_to_root=False,
                dump_to_binary=True,
                bin_output_dir=outputDir + "MC_info/tracks"))
        data.append(pv_dumper(output_dir=outputDir + "MC_info/PVs"))

    return Reconstruction('allen_reco', data, [require_gec()])


run_allen_reconstruction(options, hlt1_reco_allen_tracks, dumpBinaries)
