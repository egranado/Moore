###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt1_tracking import (require_gec, make_hlt1_tracks,
                                    make_VeloClusterTrackingSIMD_hits)
from RecoConf.mc_checking import monitor_tracking_efficiency, make_links_tracks_mcparticles, make_links_lhcbids_mcparticles_tracking_system
from RecoConf.mc_checking_categories import get_mc_categories, get_hit_type_mask
from RecoConf.hlt1_muonid import make_fitted_tracks_with_muon_id

from Hlt1Conf.algorithms import Filter

from Functors import ISMUON

from PyConf.Algorithms import (
    LHCb__Converters__Track__v1__fromV2TrackV1Track as
    trackV1FromV2TrackV1Track,
    LHCb__Converters__Track__v2__fromPrFittedForwardTrackWithMuonID as
    trackV2FromPrFittedForwardTrackWithMuonID)


def hlt1_reco_muonid_efficiency():
    # get the fitted tracks with muon ID
    tracks_with_muon_id = make_fitted_tracks_with_muon_id()

    # appliy selection to tracks to select only tracks which are isMuon
    full_sel = ISMUON
    track_filter = Filter(tracks_with_muon_id,
                          full_sel)['PrFittedForwardWithMuonID']

    # convert result to v1 (which is what the PrChecker needs)
    velo_hits = make_VeloClusterTrackingSIMD_hits()
    v2_tracks = trackV2FromPrFittedForwardTrackWithMuonID(
        FittedTracks=track_filter)
    v1_tracks = trackV1FromV2TrackV1Track(InputTracksName=v2_tracks, )

    # make links to lhcb id for mc matching
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    # make links between tracks and mcparticles for mc matching
    links_to_tracks_muon_id = make_links_tracks_mcparticles(
        InputTracks={"v1": v1_tracks.OutputTracksName},
        LinksToLHCbIDs=links_to_lhcbids)

    # build the PrChecker algorihm for muon_id track
    pr_checker_for_muon_id = monitor_tracking_efficiency(
        TrackType="MuonMatch",
        InputTracks={"v1": v1_tracks.OutputTracksName},
        #InputTracks={"v1": v1_tracks.OutputTracksName},
        LinksToTracks=links_to_tracks_muon_id,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("MuonMatch"),
        HitTypesToCheck=get_hit_type_mask("BestLong"),
    )

    # build the PrChecker algorihm for forward track
    forward_tracks = make_hlt1_tracks()['Forward']
    links_to_forward_tracks = make_links_tracks_mcparticles(
        InputTracks=forward_tracks, LinksToLHCbIDs=links_to_lhcbids)

    pr_checker_for_forward_track = monitor_tracking_efficiency(
        TrackType="Forward",
        InputTracks=forward_tracks,
        LinksToTracks=links_to_forward_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("MuonMatch"),
        HitTypesToCheck=get_hit_type_mask("BestLong"),
    )

    return Reconstruction(
        'muonideff', [pr_checker_for_forward_track, pr_checker_for_muon_id],
        [require_gec()])


options.histo_file = "PrChecker_MuonID_MiniBias.root"
run_reconstruction(options, hlt1_reco_muonid_efficiency)
