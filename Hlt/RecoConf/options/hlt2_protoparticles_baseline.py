###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple options running proto particle making and dumping output to text files for reference checking."""
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import require_gec
from RecoConf.protoparticles import make_charged_protoparticles as make_charged_protoparticles_from
from RecoConf.hlt2_global_reco import reconstruction
from PyConf.Algorithms import PrintProtoParticles


def standalone_hlt2_protoparticles():
    # The current muon ID is not thread safe, but in this single-threaded test
    # we can run it safely, so maxmise test coverage by doing so
    with make_charged_protoparticles_from.bind(enable_muon_id=True):
        reco = reconstruction()
        # Need to schedule upfront reconstruction explicitly as charged proto particle making is not functional.
        data = reco["UpfrontReconstruction"]
        charged_protos = reco["ChargedProtos"]
        neutral_protos = reco["NeutralProtos"]

    print_charged = PrintProtoParticles(
        name="PrintChargedProtos",
        Input=charged_protos,
    )
    print_neutral = PrintProtoParticles(
        name="PrintNeutralProtos",
        Input=neutral_protos,
    )
    data += [print_charged, print_neutral]

    prefilters = [require_gec()]
    return Reconstruction('hlt2_protoparticles', data, prefilters)


options.evt_max = 10
# Run with serial processing to allow muon ID
options.n_event_slots = 1
options.n_threads = 1
run_reconstruction(options, standalone_hlt2_protoparticles)
