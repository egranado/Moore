###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_allen_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import require_gec
from RecoConf.hlt1_allen import make_allen_tracks, make_allen_pvs
from RecoConf.mc_checking import monitor_IPresolution
from Configurables import NTupleSvc


def hlt1_reco_allen_IPresolution():
    allen_tracks = make_allen_tracks()
    allen_pvs = make_allen_pvs()
    pr_checker = monitor_IPresolution(allen_tracks["Forward"]["v1"], allen_pvs,
                                      allen_tracks["Velo"]["v1"])

    return Reconstruction('IPresolution', [pr_checker], [require_gec()])


options.histo_file = "Hlt1ForwardTracking_IPresolution_Allen.root"
run_allen_reconstruction(options, hlt1_reco_allen_IPresolution)
