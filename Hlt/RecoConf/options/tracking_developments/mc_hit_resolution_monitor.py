###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_moore, run_reconstruction
from Moore.config import Reconstruction
from PyConf.application import default_raw_event


def monitor_vphits(make_raw=default_raw_event):
    from PyConf.Algorithms import PrVPHitsMonitor
    from Hlt2Conf.data_from_file import mc_unpackers
    from RecoConf.hlt1_tracking import require_gec, make_velo_full_clusters, make_VeloClusterTrackingSIMD_hits
    from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system
    vphits = make_VeloClusterTrackingSIMD_hits()
    vpclusters = make_velo_full_clusters()
    mcvphits = mc_unpackers()["MCVPHits"]
    links = make_links_lhcbids_mcparticles_tracking_system()
    monitor = PrVPHitsMonitor(
        VPHitsLocation=vphits,
        VPMCHitsLocation=mcvphits,
        VPClusterLocation=vpclusters,
        LHCbIdLinkLocation=links,
        performStudy=True)
    return Reconstruction("monitor", [monitor], [require_gec()])


options.histo_file = "VPHitsMonitor.root"
run_reconstruction(options, monitor_vphits)
