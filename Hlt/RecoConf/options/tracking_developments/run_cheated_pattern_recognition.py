###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.mc_checking import make_default_IdealStateCreator


def run_cheated():
    from Hlt2Conf.data_from_file import mc_unpackers
    from PyConf.application import make_data_with_FetchDataFromFile
    from PyConf.Algorithms import PrCheatedLongTracking, TrackBestTrackCreator, MCParticle2MCHitAlg
    from PyConf.Tools import IdealStateCreator
    from RecoConf.hlt1_tracking import make_PrStoreFTHit_hits, get_track_master_fitter, FromV2TrackV1Track
    from RecoConf.mc_checking import make_links_lhcbids_mcparticles_tracking_system, make_links_tracks_mcparticles
    from PyConf.control_flow import CompositeNode, NodeLogic

    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()

    cheated_long_tracking = PrCheatedLongTracking(
        LHCbIdLinkLocation=links_to_hits,
        FTHitsLocation=make_PrStoreFTHit_hits(),
        MCParticleLocation=mc_unpackers()["MCParticles"],
        MCVerticesLocation=mc_unpackers()["MCVertices"],
        MCPropertyLocation=make_data_with_FetchDataFromFile(
            "/Event/MC/TrackInfo"),
        IdealStateCreator=make_default_IdealStateCreator(),
        AddIdealStates=True)

    cheated_long_tracks_v2 = cheated_long_tracking.OutputName
    cheated_long_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=cheated_long_tracks_v2).OutputTracksName

    best_tracks = TrackBestTrackCreator(
        TracksInContainers=[cheated_long_tracks_v1],
        Fitter=get_track_master_fitter(),
        DoNotRefit=False,
        AddGhostProb=False,
        FitTracks=True).TracksOutContainer

    # add MCLinking to the (fitted) V1 tracks
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=best_tracks, LinksToLHCbIDs=links_to_hits)

    # Now it's showtime: resolutions
    from PyConf.Algorithms import TrackResChecker
    from PyConf.Tools import VisPrimVertTool
    mcpart = mc_unpackers()["MCParticles"]
    mchead = make_data_with_FetchDataFromFile('/Event/MC/Header')
    res_checker = TrackResChecker(
        TracksInContainer=best_tracks,
        MCParticleInContainer=mc_unpackers()["MCParticles"],
        LinkerInTable=links_to_tracks,
        VisPrimVertTool=VisPrimVertTool(MCHeader=mchead, MCParticles=mcpart))

    # the last producer of data needs to be added to the control flow.
    data = [res_checker]

    return Reconstruction('hlt2_cheated_pattern_reco', data)


run_reconstruction(options, run_cheated)
