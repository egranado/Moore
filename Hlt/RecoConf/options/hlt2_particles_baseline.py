###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Simple options file to demonstrate how to create basic particles and their combination from the output of the reconstruction."""
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.hlt1_tracking import make_hlt1_tracks, require_gec, require_pvs
from RecoConf.protoparticles import make_charged_protoparticles as make_charged_protoparticles_from
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt2_global_reco import (
    reconstruction,
    make_charged_protoparticles,
    make_neutral_protoparticles,
    make_pvs,
    upfront_reconstruction,
)
from Hlt2Conf.algorithms import (
    ParticleCombiner,
    ParticleCombinerWithPVs,
    ParticleFilter,
    ParticleFilterWithPVs,
    require_all,
)
from Hlt2Conf.standard_particles import (
    _make_V0LL,
    _make_particles,
    standard_protoparticle_filter,
)
from PyConf.Algorithms import (
    LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertices as
    FromVectorLHCbRecVertices,
    PrintProtoParticles,
)


def make_KsLL(pions, pvs):
    descriptors = ["KS0 -> pi+ pi-"]
    return _make_V0LL(
        particles=[pions], descriptors=descriptors, pname='KS0', pvs=pvs)


def make_JPsi(muons):
    return ParticleCombiner(
        name="jpsi_combiner",
        particles=muons,
        DecayDescriptors=['J/psi(1S) -> mu+ mu-'],
        CombinationCut='AALL',
        MotherCut=require_all("VFASPF(VCHI2PDOF) < 25"))


def standalone_hlt2_particles(print_protos=False):
    with make_charged_protoparticles_from.bind(enable_muon_id=True):
        reco = reconstruction()
        # Need to schedule upfront reconstruction explicitly as charged proto particle making is not functional.
        data = reco["UpfrontReconstruction"]
        pvs = reco["PVs"]

        pions = _make_particles(
            species='pion', make_protoparticles=make_charged_protoparticles)
        with standard_protoparticle_filter.bind(Code='PP_ISMUON'):
            muons = _make_particles(
                species='muon',
                make_protoparticles=make_charged_protoparticles)

    # Must be specified explicitly in the data flow as the DVAlgorithm base
    # class is not functional, and the Python wrapper we have is not clever
    # enough to add PVs to the dataflow tree
    data += [pvs]
    # Make V0s
    long_detached_pions = ParticleFilterWithPVs(
        pions,
        pvs,
        name="filter_pions",
        # Relatively tight cuts to get rid of warnings from failed IPCHi2 calculations.
        Code=require_all("PT>500.", "P>2000.", "PIDK<0", "BPVVALID()",
                         "BPVIPCHI2()>9"))
    long_muons = ParticleFilter(
        muons,
        name="filter_muons",
        Code=require_all("PT>200.", "ISMUON", "PROBNNmu>0.5"))

    ksll = make_KsLL(long_detached_pions, pvs)
    jpsi = make_JPsi(long_muons)
    data += [ksll, jpsi]

    charged_protos = make_charged_protoparticles()
    neutral_protos = make_neutral_protoparticles()
    if (print_protos):
        data += [
            PrintProtoParticles(
                name="PrintChargedProtos", Input=charged_protos)
        ]
        data += [
            PrintProtoParticles(
                name="PrintNeutralProtos", Input=neutral_protos)
        ]
    prefilters = [require_gec(), require_pvs(pvs)]
    return Reconstruction('hlt2_particles', data, prefilters)


public_tools = [stateProvider_with_simplified_geom()]
run_reconstruction(options, standalone_hlt2_particles, public_tools)
