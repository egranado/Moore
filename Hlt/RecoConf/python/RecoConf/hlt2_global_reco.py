###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Methods defining ProtoParticle makers, used as input to Particle selections.

The 'global' reconstruction is that which produces the final output of the
'full' HLT2 reconstruction: charged and neutral ProtoParticle containers.
"""
from .hlt1_tracking import make_reco_pvs
from .hlt2_tracking import make_hlt2_tracks
from .rich_reconstruction import make_all_rich_pids, default_rich_reco_options, make_merged_rich_pids
from .calorimeter_reconstruction import make_calo
from .hlt2_muonid import make_muon_ids
from .protoparticles import (
    make_charged_protoparticles as make_charged_protoparticles_from,
    make_neutral_protoparticles as make_neutral_protoparticles_from,
)
from PyConf.Algorithms import (
    LHCb__Converters__RecVertex__v2__fromVectorLHCbRecVertices as
    FromVectorLHCbRecVertices)


# TODO make things configurable, if needed
def reconstruction():
    """Return reconstruction objects.

    Note it is advised to use this function if more than one object is needed,
    rather than the accessors below  as it makes the configuration slower.
    """
    # Tracks
    hlt2_tracks = make_hlt2_tracks()
    best_tracks = hlt2_tracks["Best"]
    velo_tracks = hlt2_tracks["Velo"]

    # PVs
    pvs_v2 = make_reco_pvs(velo_tracks)
    pvs = FromVectorLHCbRecVertices(
        InputVerticesName=pvs_v2,
        InputTracksName=velo_tracks["v1"]).OutputVerticesName

    # RICH
    richRecConfs = make_all_rich_pids(best_tracks, default_rich_reco_options())
    # Merge them for now to be compatible with Brunel.
    rich_pids = make_merged_rich_pids(richRecConfs)

    # Calo
    calo_pids = make_calo(best_tracks["v1"], pvs)

    # Muons
    muon_pids = make_muon_ids(best_tracks)

    charged_protos = make_charged_protoparticles_from(
        best_tracks,
        rich_pids=rich_pids,
        calo_pids=calo_pids,
        muon_pids=muon_pids)
    neutral_protos = make_neutral_protoparticles_from(calo_pids=calo_pids)

    return {
        "ChargedProtos": charged_protos["location"],
        "NeutralProtos": neutral_protos,
        "Tracks": best_tracks["v1"],
        "PVs": pvs,
        "UpfrontReconstruction": charged_protos["sequence"],
    }


def make_charged_protoparticles():
    """Return a DataHandle to the container of charged ProtoParticles.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """
    return reconstruction()["ChargedProtos"]


def upfront_reconstruction():
    """Return sequence to create charged ProtoParticles.

    Be aware that the node needs to be configures with the mode `forceOrder=True`.

    The charged ProtoParticle making is not functional, and so the sequence of algorithms
    which produces the full information at this location needs to be scheduled explicitly
    by including the `upfront_reconstruction` method in the file in the control flow.
    """

    return reconstruction()["UpfrontReconstruction"]


def make_neutral_protoparticles():
    """Return a DataHandle to the container of neutral ProtoParticles.

    The neutral ProtoParticle making is functional, there is no need to
    schedule the upfront reconstruction explicitly.
    """

    return reconstruction()["NeutralProtos"]


def make_pvs():
    """Return a DataHandle to the container of PVs
    """

    return reconstruction()["PVs"]


def make_tracks():
    """Return a DataHandle to the container of all tracks
    """
    return reconstruction()["Tracks"]
