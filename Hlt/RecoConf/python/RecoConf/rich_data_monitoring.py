###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.utilities import ConfigurationError

from PyConf.Algorithms import (
    Rich__Future__Rec__Moni__DetectorHits as DetectorHits,
    Rich__Future__SmartIDClustering as RichClustering,
    Rich__Future__Rec__Moni__PixelClusters as ClusterMoni,
    Rich__Future__Rec__Moni__SIMDRecoStats as RecoStats,
    Rich__Future__Rec__Moni__TrackSelEff as TrackSelEff,
    Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PhotAngles,
    Rich__Future__Rec__Moni__TrackRadiatorMaterial as TkMaterial,
    Rich__Future__Rec__Moni__DLLs as DLLs,  #
    Rich__Future__Rec__Moni__MassHypoRings as MassRings,
    Rich__Future__Rec__Moni__PhotonYield as PhotonYield,
    Rich__Future__Rec__Moni__GeometricalEfficiencies as GeomEffs,
    Rich__Future__Rec__Moni__PixelBackgrounds as PixBackgrds)

from PyConf.Tools import TrackSelector

from .rich_reconstruction import (get_radiator_bool_opts,
                                  get_detector_bool_opts)

###############################################################################


def default_rich_monitoring_options():
    """
    Returns a dict of the default RICH data monitoring options
    """

    opts = {
        "TightTrackSelection": {
            "MinP": 10 * GeV,
            "MinPt": 0.5 * GeV,
            "MaxChi2": 2,
            "MaxGhostProb": 0.1
        }
    }

    return opts


###############################################################################


@configurable
def default_rich_monitors(moni_set="Standard"):
    """
    Returns the set of monitors to activate

    Args:
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    monitors = {
        # Activates all montiors
        "Expert": [
            "RecoStats", "RichHits", "PixelClusters", "RichMaterial",
            "PixelBackgrounds", "TrackSelEff", "PhotonCherenkovAngles", "DLLs",
            "PhotonYields", "GeomEffs", "MassRings"
        ],
        # The default set of monitors
        "Standard": [
            "RecoStats", "RichHits", "PixelClusters", "RichMaterial",
            "PixelBackgrounds", "TrackSelEff", "PhotonCherenkovAngles", "DLLs",
            "PhotonYields", "GeomEffs", "MassRings"
        ],
        # For monitoring at the pit
        "OnlineMonitoring": [
            "RecoStats", "RichHits", "PixelClusters", "TrackSelEff",
            "PhotonCherenkovAngles", "DLLs"
        ],
        "None": []
    }

    if moni_set not in monitors.keys():
        raise ConfigurationError("Unknown histogram set " + moni_set)

    return monitors[moni_set]


###############################################################################


@configurable
def make_rich_pixel_monitors(conf, reco_opts, moni_opts, moni_set="Standard"):
    """
    Returns a set of RICH pixel only level monitors (i.e. tracking free).

    Args:
        conf       (dict): Reconstruction configuration (data) to run monitoring on
        reco_opts  (dict): Reconstruction options
        moni_opts  (dict): Data monitoring options
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    # get the list of monitors to activate
    monitors = default_rich_monitors(moni_set)

    # The dict of configured monitors to return
    results = {}

    # RICH hits
    key = "RichHits"
    if key in monitors:
        results[key] = DetectorHits(
            name="RichRecPixelQC", DecodedDataLocation=conf["RichDecodedData"])

    # Rich Clustering checks
    key = "PixelClusters"
    if key in monitors:
        # Run custom monitoring specific clustering here, to be decoupled
        # from whatever the reco uses (likely no clustering).
        clustering = RichClustering(
            name="RichClusteringForMoni",
            # Force clustering on for both RICHes
            ApplyPixelClustering=(True, True),
            # input data
            DecodedDataLocation=conf["RichDecodedData"])
        # ... and now the monitor for these clusters
        results[key] = ClusterMoni(
            name="RichRecPixelClusters",
            RichPixelClustersLocation=clustering.RichPixelClustersLocation)

    return results


###############################################################################


@configurable
def make_rich_track_monitors(conf, reco_opts, moni_opts, moni_set="Standard"):
    """
    Returns a set of RICH track level monitors

    Args:
        conf       (dict): Reconstruction data configuration to run monitoring for
        reco_opts  (dict): Reconstruction options
        moni_opts  (dict): Data monitoring options
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    # get the list of monitors to activate
    monitors = default_rich_monitors(moni_set)

    # The dict of configured monitors to return
    results = {}

    # The track name for this configuration
    track_name = conf["TrackName"]

    # The detector and radiator options
    rad_opts = get_radiator_bool_opts(reco_opts, track_name)
    det_opts = get_detector_bool_opts(reco_opts, track_name)

    # Basic reco statistics
    key = "RecoStats"
    if key in monitors:
        results[key] = RecoStats(
            name="RichRecoStats" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackToSegmentsLocation=conf["SelectedTrackToSegments"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"])

    # RICH track selection efficiencies
    key = "TrackSelEff"
    if key in monitors:
        results[key] = TrackSelEff(
            name="RichTkSelEff" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TracksLocation=conf["InputTracks"],
            RichPIDsLocation=conf["RichPIDs"])

    # RICH cherenkov photon angles
    key = "PhotonCherenkovAngles"
    if key in monitors:
        # Standard monitor
        results[key] = PhotAngles(
            name="RiCKRes" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            SummaryTracksLocation=conf["SummaryTracks"],
            PhotonToParentsLocation=conf["PhotonToParents"])
        # Tight monitor
        tight_sel = moni_opts["TightTrackSelection"]
        results[key + "Tight"] = PhotAngles(
            name="RiCKRes" + track_name + "Tight",
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSelector=TrackSelector(
                MinPCut=tight_sel["MinP"],
                MinPtCut=tight_sel["MinPt"],
                MaxChi2Cut=tight_sel["MaxChi2"],
                MaxGhostProbCut=tight_sel["MaxGhostProb"]),
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            SummaryTracksLocation=conf["SummaryTracks"],
            PhotonToParentsLocation=conf["PhotonToParents"])

    # Photon Yields
    key = "PhotonYields"
    if key in monitors:
        results["Emitted" + key] = PhotonYield(
            name="RiTkEmittedYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            MaximumYields=(100, 800, 800),
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["EmittedYields"])
        results["Detectable" + key] = PhotonYield(
            name="RiTkDetectableYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["DetectableYields"])
        results["Signal" + key] = PhotonYield(
            name="RiTkSignalYields" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            PhotonYieldLocation=conf["SignalYields"])

    # Geom. Effs.
    key = "GeomEffs"
    if key in monitors:
        results[key] = GeomEffs(
            name="RiTkGeomEffs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            GeomEffsLocation=conf["GeomEffs"])

    # Rich material monitor
    key = "RichMaterial"
    if key in monitors:
        results[key] = TkMaterial(
            name="RiTkMaterial" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"])

    # Rich likelihood pixel backgrounds
    key = "PixelBackgrounds"
    if key in monitors:
        results[key] = PixBackgrds(
            name="RichRecPixBkgs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            RichSIMDPixelSummariesLocation=conf["RichSIMDPixels"],
            PixelBackgroundsLocation=conf["RichPixelBackgrounds"])

    # RICH DLL values
    key = "DLLs"
    if key in monitors:
        results[key] = DLLs(
            name="RichDLLs" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            RichPIDsLocation=conf["RichPIDs"])

    # RICH mass rings
    key = "MassRings"
    if key in monitors:
        results[key] = MassRings(
            name="RichMassRings" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            TrackSegmentsLocation=conf["TrackSegments"],
            MassHypothesisRingsLocation=conf["EmittedCKRings"])

    return results
