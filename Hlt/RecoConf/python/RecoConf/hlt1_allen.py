###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable

from PyConf.application import default_raw_event, make_odin

from PyConf.Algorithms import (LHCb__Converters__Track__v1__fromV2TrackV1Track
                               as FromV2TrackV1Track, AllenVeloToV2Tracks,
                               AllenUTToV2Tracks, AllenForwardToV2Tracks,
                               AllenPVsToRecVertexV2, AllenDecReportsToTES,
                               DumpRawBanks, RunAllen, TransposeRawBanks)
from PyConf.Algorithms import HltDecReportsDecoder


def make_transposed_raw_banks(make_raw=default_raw_event):
    return TransposeRawBanks(RawEventLocations=[make_raw()]).AllenRawInput


@configurable
def make_dumped_raw_banks(transposed_raw_banks=make_transposed_raw_banks,
                          odin_location=make_odin,
                          output_dir="dump/banks"):
    return DumpRawBanks(
        BanksLocation=transposed_raw_banks(),
        ODINLocation=odin_location(),
        OutputDirectory=output_dir)


def make_allen_output(odin_location=make_odin,
                      transposed_raw_banks=make_transposed_raw_banks,
                      filter_hlt1=False):
    return RunAllen(
        AllenRawInput=transposed_raw_banks(),
        ODINLocation=odin_location(),
        ParamDir="${ALLEN_PROJECT_ROOT}/input/detector_configuration/down/",
        FilterHLT1=filter_hlt1).AllenOutput


def make_allen_dec_reports(odin_location=make_odin,
                           transposed_raw_banks=make_transposed_raw_banks,
                           filter_hlt1=False,
                           dump_to_file=False):
    return RunAllen(
        AllenRawInput=transposed_raw_banks(),
        ODINLocation=odin_location(),
        ParamDir="${ALLEN_PROJECT_ROOT}/input/detector_configuration/down/",
        FilterHLT1=filter_hlt1).DecReportsLocation


def make_allen_velo_tracks():
    allen_output = make_allen_output()
    velo_tracks_v2 = AllenVeloToV2Tracks(AllenOutput=allen_output).OutputTracks
    velo_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=velo_tracks_v2).OutputTracksName

    return {"v2": velo_tracks_v2, "v1": velo_tracks_v1}


def make_allen_veloUT_tracks():
    allen_output = make_allen_output()
    veloUT_tracks_v2 = AllenUTToV2Tracks(AllenOutput=allen_output).OutputTracks
    veloUT_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=veloUT_tracks_v2).OutputTracksName

    return {"v2": veloUT_tracks_v2, "v1": veloUT_tracks_v1}


def make_allen_forward_tracks():
    allen_output = make_allen_output()
    forward_tracks_v2 = AllenForwardToV2Tracks(
        AllenOutput=allen_output).OutputTracks
    forward_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_tracks_v2).OutputTracksName

    return {"v2": forward_tracks_v2, "v1": forward_tracks_v1}


def make_allen_forward_muon_tracks():
    allen_output = make_allen_output()
    forward_muon_tracks_v2 = AllenForwardToV2Tracks(
        AllenOutput=allen_output).OutputMuonTracks
    forward_muon_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=forward_muon_tracks_v2).OutputTracksName

    return {"v2": forward_muon_tracks_v2, "v1": forward_muon_tracks_v1}


def make_allen_tracks():
    velo_tracks = make_allen_velo_tracks()
    velo_ut_tracks = make_allen_veloUT_tracks()
    forward_tracks = make_allen_forward_tracks()

    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "Forward": forward_tracks,
    }


def make_allen_pvs():
    allen_output = make_allen_output()
    return AllenPVsToRecVertexV2(AllenOutput=allen_output).OutputPVs


def make_allen_raw_dec_reports(filterHLT1=False):
    allen_output = make_allen_output(filter_hlt1=filterHLT1)
    allen_dec_reports_raw = AllenDecReportsToTES(
        AllenOutput=allen_output).OutputDecReports
    return allen_dec_reports_raw


def decode_allen_dec_reports():
    allen_dec_reports_raw = make_allen_raw_dec_reports()
    return HltDecReportsDecoder(
        RawEventLocations=allen_dec_reports_raw,
        SourceID=1).OutputHltDecReportsLocation
