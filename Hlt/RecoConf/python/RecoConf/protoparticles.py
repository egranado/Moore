###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Functions to create charged and neutral protoparticles from reconstruction output."""
from PyConf import configurable
from PyConf.Algorithms import (
    FunctionalChargedProtoParticleMaker,
    ChargedProtoParticleAddRichInfo,
    FutureChargedProtoParticleAddEcalInfo,
    FutureChargedProtoParticleAddHcalInfo,
    FutureChargedProtoParticleAddBremInfo,
    ChargedProtoParticleAddMuonInfo,
    ChargedProtoCombineDLLsAlg,
    ANNGlobalPID__ChargedProtoANNPIDAlg,
    FutureNeutralProtoPAlg,
)
from PyConf.Tools import (
    LoKi__Hybrid__TrackSelector as TrackSelector,
    CaloFutureElectron,
    CaloFutureHypoEstimator,
    CaloFutureHypo2CaloFuture,
    FutureGammaPi0XGBoostTool,
    FutureGammaPi0SeparationTool,
    FutureNeutralIDTool,
    CaloFutureElectron,
)


@configurable
def make_neutral_protoparticles(calo_pids):
    """Create neutral ProtoParticles from Calorimeter reconstruction output.

    Args:
        calo_pids: dictionary containing all necessary inputs to create neutral ProtoParticles.

    Returns:
        DataHandle to the container of neutral ProtoParticles

    """
    hypo2Calo = CaloFutureHypo2CaloFuture(
        EcalDigitsLocation=calo_pids["digitsEcal"],
        HcalDigitsLocation=calo_pids["digitsHcal"])

    gamma_pi0_xgb = FutureGammaPi0XGBoostTool(
        DigitLocation=calo_pids["digitsEcal"])

    hypo_estimator = CaloFutureHypoEstimator(
        ClusterMatchLocation=calo_pids["clusterMatch"],
        ElectronMatchLocation=calo_pids["electronMatch"],
        BremMatchLocation=calo_pids["bremMatch"],
        Hypo2Calo=hypo2Calo,
        Pi0Separation=FutureGammaPi0SeparationTool(),
        Pi0SeparationXGB=gamma_pi0_xgb,
        NeutralID=FutureNeutralIDTool(),
        Electron=CaloFutureElectron(),
    )
    neutral_protos = FutureNeutralProtoPAlg(
        CaloFutureHypoEstimator=hypo_estimator,
        MergedPi0s=calo_pids["mergedPi0s"],
        Photons=calo_pids["photons"],
        SplitPhotons=calo_pids["splitPhotons"],
    )

    return neutral_protos.ProtoParticleLocation


@configurable
def make_charged_protoparticles(tracks,
                                rich_pids,
                                calo_pids,
                                muon_pids,
                                track_type="",
                                enable_muon_id=False):
    """Create charged ProtoParticles from tracking and particle identification reconstruction outputs.

    Args:
        tracks: input tracks to create charged protos
        rich_pids: input rich_pids created from tracks
        calo_pids: input rich_pids from Calorimeter reconstruction
        muon_pids: input muon_pids created from tracks
        enable_muon_id: Running the MuonID is currently not thread-safe therefore the default is False but the option exists to run it in single threaded mode.

    Returns:
        dict with DataHandle to the container of charged ProtoParticles and the sequence of algorithms to create them.

    Note:
        The charged ProtoParticle making is not functional, and so the sequence of algorithms
        which produces the full information at this location needs to be scheduled explicitly
        by including it in the control flow.

    """

    if track_type == "":
        trackselector = TrackSelector(
            Code="TrLONG | TrDOWNSTREAM | TrUPSTREAM")
        charged_protos = FunctionalChargedProtoParticleMaker(
            Inputs=[tracks["v1"]], TrackSelector=trackselector)
    else:
        charged_protos = FunctionalChargedProtoParticleMaker(
            Inputs=[tracks["v1"]])

    add_rich = ChargedProtoParticleAddRichInfo(
        ProtoParticleLocation=charged_protos.Output,
        InputRichPIDLocation=rich_pids)

    hypo2Calo = CaloFutureHypo2CaloFuture(
        EcalDigitsLocation=calo_pids["digitsEcal"],
        HcalDigitsLocation=calo_pids["digitsHcal"])

    gamma_pi0_xgb = FutureGammaPi0XGBoostTool(
        DigitLocation=calo_pids["digitsEcal"])

    hypo_estimator = CaloFutureHypoEstimator(
        ClusterMatchLocation=calo_pids["clusterMatch"],
        ElectronMatchLocation=calo_pids["electronMatch"],
        BremMatchLocation=calo_pids["bremMatch"],
        Hypo2Calo=hypo2Calo,
        Pi0Separation=FutureGammaPi0SeparationTool(),
        Pi0SeparationXGB=gamma_pi0_xgb,
        NeutralID=FutureNeutralIDTool(),
        Electron=CaloFutureElectron(),
    )

    add_ecal = FutureChargedProtoParticleAddEcalInfo(
        CaloFutureElectron=CaloFutureElectron(),
        CaloFutureHypoEstimator=hypo_estimator,
        ProtoParticleLocation=charged_protos.Output,
        InputInEcalLocation=calo_pids["inEcal"],
        InputElectronMatchLocation=calo_pids["electronMatch"],
        InputEcalChi2Location=calo_pids["ecalChi2"],
        InputEcalELocation=calo_pids["ecalE"],
        InputClusterChi2Location=calo_pids["clusChi2"],
        InputEcalPIDeLocation=calo_pids["ecalPIDe"],
        InputEcalPIDmuLocation=calo_pids["ecalPIDmu"],
        InputClusterMatchLocation=calo_pids["clusterMatch"],
    )

    add_hcal = FutureChargedProtoParticleAddHcalInfo(
        CaloFutureHypoEstimator=hypo_estimator,
        ProtoParticleLocation=charged_protos.Output,
        InputInHcalLocation=calo_pids["inHcal"],
        InputHcalELocation=calo_pids["hcalE"],
        InputHcalPIDeLocation=calo_pids["hcalPIDe"],
        InputHcalPIDmuLocation=calo_pids["hcalPIDmu"],
    )

    add_brem = FutureChargedProtoParticleAddBremInfo(
        CaloFutureHypoEstimator=hypo_estimator,
        ProtoParticleLocation=charged_protos.Output,
        InputInBremLocation=calo_pids["inBrem"],
        InputBremMatchLocation=calo_pids["bremMatch"],
        InputBremChi2Location=calo_pids["bremChi2"],
        InputBremPIDeLocation=calo_pids["bremPIDe"],
    )
    add_muon = ChargedProtoParticleAddMuonInfo(
        ProtoParticleLocation=charged_protos.Output,
        InputMuonPIDLocation=muon_pids)
    add_comb_dll = ChargedProtoCombineDLLsAlg(
        ProtoParticleLocation=charged_protos.Output)

    # Set up ann pids
    annVersion = "MCUpTuneV1"
    if track_type == "":
        track_types = ["Long", "Downstream", "Upstream"]
    else:
        track_types = [track_type]
    pid_types = ["Electron", "Muon", "Pion", "Kaon", "Proton", "Ghost"]
    ann_algos = []
    for track_type in track_types:
        for pid_type in pid_types:
            ann_pid = ANNGlobalPID__ChargedProtoANNPIDAlg(
                name="ChargedProtoANNPID" + track_type + pid_type,
                ProtoParticleLocation=charged_protos.Output.location,
                NetworkVersion=annVersion,
                PIDType=pid_type,
                TrackType=track_type,
            )
            ann_algos += [ann_pid]
    # Not functional and we need to have a certain order
    protos_sequence = [add_rich, add_ecal, add_hcal, add_brem]
    # MuonChi2MatchTool is not thread-safe, so we cannot run this by default at the moment.
    if enable_muon_id:
        protos_sequence.append(add_muon)
    protos_sequence += [add_comb_dll] + ann_algos

    return {"location": charged_protos.Output, "sequence": protos_sequence}
