###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.application import default_raw_event

from PyConf.Algorithms import MuonRawToCoord, PrepareMuonHits, MuonIDAlgLite
from PyConf.Tools import CommonMuonTool, MakeMuonTool, DLLMuonTool, MVATool
from .hlt1_muonid import make_muon_hits as make_hlt1_muon_hits


@configurable
def make_muon_hits(make_raw=default_raw_event):
    coords = MuonRawToCoord(
        RawEventLocation=make_raw(["Muon"])).MuonCoordLocation
    hits = PrepareMuonHits(CoordLocation=coords).Output
    return hits


@configurable
def make_muon_ids(tracks, make_hits=make_hlt1_muon_hits):
    muon_id_alg = MuonIDAlgLite(
        TracksLocations=tracks["v1"],
        HitHandler=make_hits(),
        # CommonMuonTool calls MuonChi2MatchTool which is not thread-safe, to be fixed.
        CommonMuonTool=CommonMuonTool(),
        MakeMuonTool=MakeMuonTool(),
        DLLMuonTool=DLLMuonTool(),
        MVATool=MVATool())
    return muon_id_alg.MuonIDLocation
