###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.application import default_raw_event
from PyConf.Algorithms import (
    MuonRawToHits,
    MuonIDHlt1AlgPr,
    MakeZip__PrFittedForwardTracks__PrMuonPIDs,
)
from RecoConf.hlt1_tracking import make_hlt1_fitted_tracks, make_hlt1_tracks


@configurable
def make_muon_hits(make_raw=default_raw_event):
    return MuonRawToHits(RawEventLocation=make_raw(["Muon"])).HitContainer


@configurable
def make_muon_id(tracks, make_muon_hits=make_muon_hits):
    return MuonIDHlt1AlgPr(
        InputTracks=tracks["Pr"], InputMuonHits=make_muon_hits()).OutputMuonPID


def make_tracks_with_muon_id(tracks, muon_ids):
    return MakeZip__PrFittedForwardTracks__PrMuonPIDs(
        Input1=tracks["Pr"], Input2=muon_ids).Output


def make_fitted_tracks_with_muon_id():
    tracks = make_hlt1_fitted_tracks(make_hlt1_tracks())
    muon_ids = make_muon_id(make_hlt1_tracks()["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(tracks, muon_ids)
    return {'PrFittedForwardWithMuonID': tracks_with_muon_id}
