###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.tonic import configurable
from PyConf.control_flow import CompositeNode, NodeLogic
from .hlt1_tracking import (
    require_gec, make_reco_pvs, make_pvs, make_hlt1_tracks,
    make_VeloKalman_fitted_tracks, make_PrForwardTracking_tracks,
    make_PatPV3DFuture_pvs, all_hlt1_forward_track_types,
    make_TrackEventFitter_fitted_tracks, make_hlt1_fitted_tracks)
from RecoConf.hlt1_muonmatch import make_tracks_with_muonmatch_ipcut
from .hlt1_muonid import make_muon_id, make_tracks_with_muon_id
from .hlt2_muonid import make_muon_ids as make_muon_id_hlt2
from .hlt2_tracking import make_hlt2_tracks, make_TrackBestTrackCreator_tracks
from .calorimeter_reconstruction import (make_calo, make_calo_resolution_gamma,
                                         make_calo_resolution_pi0,
                                         make_calo_raw_ecalclusters)
from .calorimeter_mc_checking import (
    monitor_calo_cluster_resolution, monitor_calo_future_cluster_resolution,
    monitor_calo_photon_resolution, monitor_calo_pi0_resolution,
    monitor_calo_efficiency)
from PyConf.Algorithms import CaloFutureDigit2MCLinks2Table
from PyConf.Algorithms import CaloClusterMCTruth
from PyConf.Algorithms import TracksToSelection, TrackSelectionMerger
from .mc_checking import get_track_checkers, get_fitted_tracks_checkers, get_best_tracks_checkers
from .reconstruction_objects import reconstruction
from .protoparticles import make_charged_protoparticles, make_neutral_protoparticles
from PyConf.application import default_raw_event
from .rich_reconstruction import (make_all_rich_pids, make_merged_rich_pids,
                                  make_rich_pids, make_rich_pixels,
                                  default_rich_reco_options)
from .rich_data_monitoring import (make_rich_pixel_monitors,
                                   make_rich_track_monitors,
                                   default_rich_monitoring_options)
from .rich_mc_checking import make_rich_checkers, default_rich_checking_options

from .calo_data_monitoring import monitor_calo_clusters

from GaudiKernel.SystemOfUnits import MeV, mm
from Moore.config import Reconstruction
from Hlt2Conf.data_from_file import mc_unpackers
from PyConf.application import make_data_with_FetchDataFromFile


def reco_prefilters():
    return [require_gec()]


@configurable
def standalone_hlt1_reco(do_mc_checking=False):
    """ Run the Hlt1 reconstruction, i.e. tracking and muon id, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
        Returns:
            Reconstruction: Data and control flow of Hlt1 reconstruction.

    """
    hlt1_tracks = make_hlt1_tracks()
    pvs = make_pvs()
    fitted_tracks = make_VeloKalman_fitted_tracks(hlt1_tracks)
    muon_ids = make_muon_id(hlt1_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_tracks, muon_ids)

    data = [pvs, fitted_tracks["Pr"], tracks_with_muon_id]

    if do_mc_checking:
        types_and_locations_for_checkers = {
            "Velo": hlt1_tracks["Velo"],
            "Upstream": hlt1_tracks["Upstream"],
            "Forward": hlt1_tracks["Forward"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    return Reconstruction('hlt1_reco', data, reco_prefilters())


def standalone_hlt1_reco_velo_only():
    """ Run the default Hlt1 Velo reconstruction

    Returns:
        Reconstruction: Data and control flow of Hlt1 reconstruction.
    """
    return Reconstruction('hlt1_velo_reco', [make_hlt1_tracks()["Velo"]["Pr"]],
                          reco_prefilters())


@configurable
def standalone_hlt1_muonmatching_reco(do_mc_checking=False,
                                      velo_track_min_ip=0.4 * mm,
                                      tracking_min_pt=80. * MeV):
    all_tracks = make_tracks_with_muonmatch_ipcut(
        velo_track_min_ip=velo_track_min_ip, tracking_min_pt=tracking_min_pt)
    fitted_tracks = make_hlt1_fitted_tracks(all_tracks)
    muon_ids = make_muon_id(all_tracks["Forward"])
    tracks_with_muon_id = make_tracks_with_muon_id(fitted_tracks, muon_ids)
    data = [fitted_tracks["Pr"], tracks_with_muon_id]
    if do_mc_checking:
        types_and_locations_for_checkers = {
            "Velo": all_tracks["Velo"],
            "Upstream": all_tracks["Upstream"],
            "Forward": all_tracks["Forward"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    return Reconstruction('hlt1_muonmatching_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_full_track_reco(light_reco=False, do_mc_checking=False):
    """ Run the Hlt2 track reconstruction, i.e. pattern recognition and track fit, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
        Returns:
            Reconstruction: Data and control flow of Hlt2 track reconstruction.
    """
    hlt2_tracks = make_hlt2_tracks(light_reco=light_reco)

    track_version = "v1"

    best_tracks = {}
    if light_reco:
        best_tracks["BestLong"] = hlt2_tracks["BestLong"]
        best_tracks["BestDownstream"] = hlt2_tracks["BestDownstream"]
    else:
        best_tracks["Best"] = hlt2_tracks["Best"]

    pvs = make_pvs()

    data = [best_tracks[key][track_version] for key in best_tracks.keys()]
    data += [pvs]

    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"]
        }
        data += get_track_checkers(types_and_locations_for_checkers)
        if light_reco:
            data += get_fitted_tracks_checkers(best_tracks)
        else:
            data += get_best_tracks_checkers(best_tracks["Best"])

    return Reconstruction('hlt2_track_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_reco(do_mc_checking=False, do_data_monitoring=False):
    """ Run the Hlt2 reconstruction up to ProtoParticle making, MC checking optional and data monitoring optional
        Args:
            do_mc_checking (bool): Enable MC checking.
            do_data_monitoring (bool): Enable data monitoring.
        Returns:
            Reconstruction: Data and control flow of Hlt2 reconstruction.
    """

    hlt2_tracks = make_hlt2_tracks(light_reco=False)

    best_tracks = hlt2_tracks["Best"]
    pvs = make_pvs()
    data = [best_tracks["v1"], pvs]
    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"]
        }
        data += get_track_checkers(types_and_locations_for_checkers)
        data += get_best_tracks_checkers(best_tracks)

    # Add RICH
    rich_reco = add_hlt2_rich(
        light_reco=False,
        best_tracks=best_tracks,
        do_mc_checking=do_mc_checking,
        do_data_monitoring=do_data_monitoring)
    data += [rich_reco["rich_pids"]]
    if rich_reco["monitoring"] != None:
        data += rich_reco["monitoring"]
    if rich_reco["mcchecking"] != None:
        data += rich_reco["mcchecking"]

    # Add Calo
    calo = make_calo(best_tracks["v1"], pvs)
    data += [
        calo["ecalSplitClusters"], calo["ecalPIDmu"], calo["hcalPIDe"],
        calo["hcalPIDmu"], calo["ecalPIDe"], calo["bremPIDe"], calo["clusChi2"]
    ]

    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "CaloElectrons": calo['electronTracks'],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    # Add monitoring
    if do_data_monitoring:
        data += monitor_calo_clusters(calo)

    # Add Muon
    muon_pids = make_muon_id_hlt2(best_tracks)
    # MuonChi2MatchTool is not thread-safe, so we cannot run this by default at the moment.
    #data += [muon_pids]

    # Add proto particles
    charged_protos = make_charged_protoparticles(
        best_tracks,
        rich_pids=rich_reco["rich_pids"],
        calo_pids=calo,
        muon_pids=muon_pids)

    data += charged_protos["sequence"]
    neutral_protos = make_neutral_protoparticles(calo_pids=calo)
    data += [neutral_protos]

    return Reconstruction('hlt2_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_light_reco(do_mc_checking=False, do_data_monitoring=False):

    hlt2_tracks = make_hlt2_tracks(light_reco=True)

    best_track_types = ["BestLong", "BestDownstream"]
    best_tracks = {}
    data = []
    for track_type in best_track_types:
        best_tracks[track_type] = hlt2_tracks[track_type]
        data += [best_tracks[track_type]["v1"]]

    pvs = make_pvs()
    data += [pvs]

    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "Velo": hlt2_tracks["Velo"],
            "VeloFull": hlt2_tracks["Velo"],
            "Upstream": hlt2_tracks["Upstream"],
            "Forward": hlt2_tracks["Forward"],
            "ForwardHlt1": hlt2_tracks["ForwardFastFitted"],
            "Seed": hlt2_tracks["Seed"],
            "Match": hlt2_tracks["Match"],
            "Downstream": hlt2_tracks["Downstream"],
        }
        data += get_track_checkers(types_and_locations_for_checkers)
        data += get_fitted_tracks_checkers(best_tracks)

    # Add RICH
    tracks = {
        "Long": best_tracks["BestLong"],
        "Downstream": best_tracks["BestDownstream"]
    }
    rich_reco = add_hlt2_rich(
        best_tracks=tracks,
        light_reco=True,
        do_mc_checking=do_mc_checking,
        do_data_monitoring=do_data_monitoring,
        track_types=tracks.keys())

    data += [
        rich_reco["rich_pids"][key] for key in rich_reco["rich_pids"].keys()
    ]
    if rich_reco["monitoring"] != None:
        data += rich_reco["monitoring"]
    if rich_reco["mcchecking"] != None:
        data += rich_reco["mcchecking"]

    trackSelections = {}
    for track_type in best_track_types:
        trackSelections[track_type] = TracksToSelection(
            InputLocation=best_tracks[track_type]["v1"]).OutputLocation

    all_best_tracks = {}
    all_best_tracks["v1"] = TrackSelectionMerger(InputLocations=[
        trackSelections[track_type] for track_type in best_track_types
    ]).OutputLocation

    calo = make_calo(all_best_tracks["v1"], pvs)

    data += [
        calo["ecalSplitClusters"], calo["ecalPIDmu"], calo["hcalPIDe"],
        calo["hcalPIDmu"], calo["ecalPIDe"], calo["bremPIDe"], calo["clusChi2"]
    ]
    if (do_mc_checking):
        types_and_locations_for_checkers = {
            "CaloElectrons": calo['electronTracks'],
        }
        data += get_track_checkers(types_and_locations_for_checkers)

    if do_data_monitoring:
        data += monitor_calo_clusters(calo)

    # Add Muon
    muon_pids = make_muon_id_hlt2(all_best_tracks)
    # MuonChi2MatchTool is not thread-safe, so we cannot run this by default at the moment.
    #data += [muon_pids]

    charged_protos = {}
    for key in tracks.keys():
        charged_protos[key] = make_charged_protoparticles(
            tracks=tracks[key],
            rich_pids=rich_reco["rich_pids"][key],
            calo_pids=calo,
            muon_pids=muon_pids,
            track_type=key)
        data += charged_protos[key]["sequence"]

    neutral_protos = make_neutral_protoparticles(calo_pids=calo)
    data += [neutral_protos]

    return Reconstruction('hlt2_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_calo_resolution_gamma(light_reco=False):
    # if running on XDIGI files
    hlt2_tracks = make_hlt2_tracks(light_reco=light_reco)
    pvs = make_pvs()

    # if running on LDST files
    # with reconstruction.bind(from_file=True):
    #     reco = reconstruction
    #     best_tracks = reco["Tracks"]
    #     pvs = reco["PVs"]

    if light_reco:
        best_tracks = {
            "BestLong": hlt2_tracks["BestLong"]["v1"],
            "BestDownstream": hlt2_tracks["BestDownstream"]["v1"]
        }

        trackSelections = {}
        for track_type in best_tracks.keys():
            trackSelections[track_type] = TracksToSelection(
                InputLocation=best_tracks[track_type]).OutputLocation

        all_best_tracks = TrackSelectionMerger(InputLocations=[
            trackSelections[track_type] for track_type in best_tracks.keys()
        ]).OutputLocation
    else:
        all_best_tracks = hlt2_tracks["Best"]["v1"]

    calo = make_calo_resolution_gamma(all_best_tracks, pvs)

    tableMCCaloDigits = CaloFutureDigit2MCLinks2Table(
        CaloDigits=calo['digitsEcal'],
        MCParticles=mc_unpackers()["MCParticles"],
        Link=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/Ecal/Digits')).Output
    # produce one table with relations (CellID,MCparticle) which can be used for all clusters
    # NB use clusters BEFORE overlap as now these are "future" clusters
    # eventually could switch to using clusters after shower overlap at this stage
    tableMCCaloClusters = CaloClusterMCTruth(
        Input=tableMCCaloDigits,
        Clusters=calo["clusters"]["ecalClustersNoOverlap"]).Output

    data = []
    # clusters resolution
    data += monitor_calo_cluster_resolution(
        'OverlapDef', calo["clusters"]["ecalClustersOverlap"],
        tableMCCaloClusters)
    data += monitor_calo_cluster_resolution(
        'OverlapNoCor', calo["clusters"]["ecalClustersOverlapNoCor"],
        tableMCCaloClusters)
    # NB notice _future_ in the next line, eventually should switch to _new_ everywhere
    data += monitor_calo_future_cluster_resolution(
        'NoOverlap', calo["clusters"]["ecalClustersNoOverlap"],
        tableMCCaloClusters)
    data += monitor_calo_cluster_resolution(
        'OverlapFast', calo["clusters"]["ecalClustersOverlapFast"],
        tableMCCaloClusters)
    # photons resolution
    data += monitor_calo_photon_resolution('OverlapDef', calo["photons"],
                                           tableMCCaloClusters)
    data += monitor_calo_photon_resolution(
        'OverlapNoCor', calo["photonsOverlapNoCor"], tableMCCaloClusters)
    data += monitor_calo_photon_resolution(
        'NoOverlap', calo["photonsNoOverlap"], tableMCCaloClusters)
    data += monitor_calo_photon_resolution(
        'OverlapFast', calo["photonsOverlapFast"], tableMCCaloClusters)
    # print MC tree for B0 decays
    #data += [PrintMCTree(ParticleNames=['B0', 'B~0'])]

    return Reconstruction('hlt2_calo_resolution', data, reco_prefilters())


def standalone_hlt2_calo_resolution_pi0():
    with reconstruction.bind(from_file=True):
        reco = reconstruction()
        best_tracks = reco["Tracks"]
        pvs = reco["PVs"]
    calo = make_calo_resolution_pi0(best_tracks, pvs)
    tableMCCaloDigits = CaloFutureDigit2MCLinks2Table(
        CaloDigits=calo['digitsEcal'],
        MCParticles=mc_unpackers()["MCParticles"],
        Link=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/Ecal/Digits')).Output
    # produce one table with relations (CellID,MCparticle) which can be used for all clusters
    # NB use clusters BEFORE overlap as now these are "future" clusters
    # eventually could switch to using clusters after shower overlap at this stage
    tableMCCaloClusters = CaloClusterMCTruth(
        Input=tableMCCaloDigits,
        Clusters=calo["clusters"]["ecalClustersNoOverlap"]).Output

    data = []
    data += monitor_calo_pi0_resolution(
        '-clusDef-pi0Def', calo["clusDef-pi0Def"], tableMCCaloClusters)
    data += monitor_calo_pi0_resolution(
        '-clusSOFast-pi0Def', calo["clusSOFast-pi0Def"], tableMCCaloClusters)
    data += monitor_calo_pi0_resolution(
        '-clusSONoCor-pi0Def', calo["clusSONoCor-pi0Def"], tableMCCaloClusters)
    data += monitor_calo_pi0_resolution('-clusSONoCor-pi0NoSO',
                                        calo["clusSONoCor-pi0NoSO"],
                                        tableMCCaloClusters)
    data += monitor_calo_pi0_resolution('-clusSONoCor-pi0SOFast',
                                        calo["clusSONoCor-pi0SOFast"],
                                        tableMCCaloClusters)
    data += monitor_calo_pi0_resolution('-clusSONoCor-pi0SONoCor',
                                        calo["clusSONoCor-pi0SONoCor"],
                                        tableMCCaloClusters)
    data += monitor_calo_pi0_resolution(
        '-clusNoSO-pi0Def', calo["clusNoSO-pi0Def"], tableMCCaloClusters)
    data += monitor_calo_pi0_resolution(
        '-clusNoSO-pi0NoSO', calo["clusNoSO-pi0NoSO"], tableMCCaloClusters)
    data += monitor_calo_pi0_resolution(
        '-clusDef-pi0NoCor', calo["clusDef-pi0NoCor"], tableMCCaloClusters)
    # print MC tree for B0 decays
    #data += [PrintMCTree(ParticleNames=['B0', 'B~0'])]

    return Reconstruction('hlt2_calo_resolution_pi0', data, reco_prefilters())


def standalone_hlt2_calo_efficiency():
    # make ecal clusters
    calo = make_calo_raw_ecalclusters()
    # check reconstruction efficiency
    data = monitor_calo_efficiency(calo["ecaldigits"], calo["ecalclusters"])
    return Reconstruction('hlt2_calo_efficiency', data)


def standalone_hlt2_reco_calo_from_dst(do_mc_checking=False):

    data = []

    # read tracks from input file
    with reconstruction.bind(from_file=True):
        reco = reconstruction()
    best_tracks = reco["Tracks"]
    pvs = reco["PVs"]

    calo = make_calo(best_tracks, pvs)

    data += [
        calo["ecalSplitClusters"], calo["ecalPIDmu"], calo["hcalPIDe"],
        calo["hcalPIDmu"], calo["ecalPIDe"], calo["bremPIDe"], calo["clusChi2"]
    ]

    return Reconstruction('hlt2_standalone_calo_reco', data, reco_prefilters())


@configurable
def standalone_hlt2_reco_brunel(do_mc_checking=False,
                                do_data_monitoring=False):
    """ Run the Hlt2 track reconstruction as it was defined in Brunel, MC checking optional
        Args:
            do_mc_checking (bool): Enable MC checking.
            do_data_monitoring (bool): Enable data monitoring.
        Returns:
            Reconstruction: Data and control flow of Hlt2 track reconstruction.

    """

    def get_Brunel_track_list_for_TrackBestTrackCreator():
        return [
            "Velo", "ForwardFastFitted", "Forward", "Upstream", "Downstream",
            "Match", "Seed"
        ]

    # we modify the velo tracks consistently for the pvs and the best tracks
    with all_hlt1_forward_track_types.bind(make_forward_tracks=make_PrForwardTracking_tracks),\
         make_hlt1_fitted_tracks.bind(make_forward_fitted_tracks=make_TrackEventFitter_fitted_tracks),\
         make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),\
         require_gec.bind(cut=0),\
         make_TrackBestTrackCreator_tracks.bind(get_tracklist=get_Brunel_track_list_for_TrackBestTrackCreator,do_not_refit=True):
        return standalone_hlt2_reco(
            do_mc_checking=do_mc_checking,
            do_data_monitoring=do_data_monitoring)


@configurable
def add_hlt2_rich(best_tracks,
                  light_reco=False,
                  do_mc_checking=False,
                  do_data_monitoring=False,
                  track_types=['Long', 'Downstream', 'Upstream']):
    """
    Returns the nodes to add for the RICH HLT2 reco
    """

    nodes = []
    reco_opts = default_rich_reco_options()
    if light_reco:
        selections = {}
        richRecConfs = {}
        for key in track_types:
            selections[key] = TracksToSelection(
                InputLocation=best_tracks[key]["v1"]).OutputLocation
            richRecConfs[key] = make_rich_pids(
                track_name=key,
                input_tracks=selections[key],
                options=reco_opts)
        pidMerge = {
            key: richRecConfs[key]["RichPIDs"]
            for key in richRecConfs.keys()
        }
    else:
        richRecConfs = make_all_rich_pids(
            best_tracks, reco_opts, track_types=track_types)
        # Merge them for now to be compatible with Brunel.
        pidMerge = make_merged_rich_pids(richRecConfs)

    # RICH data monitoring
    moni_nodes = None
    if do_data_monitoring:
        moni_nodes = []
        # data monitoring options
        moni_opts = default_rich_monitoring_options()
        # Rich pixel level data monitoring
        richPixelConf = make_rich_pixels(options=reco_opts)
        richPixelMonitors = make_rich_pixel_monitors(
            conf=richPixelConf, reco_opts=reco_opts, moni_opts=moni_opts)
        moni_nodes += [
            richPixelMonitors[key] for key in richPixelMonitors.keys()
        ]
        # Rich track level data monitoring
        for key, conf in richRecConfs.iteritems():
            riTkMoni = make_rich_track_monitors(
                conf=conf, reco_opts=reco_opts, moni_opts=moni_opts)
            moni_nodes += [riTkMoni[key] for key in riTkMoni.keys()]
        nodes += [
            CompositeNode(
                name='RICH_data_monitoring',
                children=moni_nodes,
                combineLogic=NodeLogic.NONLAZY_OR,
                forceOrder=False)
        ]

    # RICH MC checking
    mc_nodes = None
    if do_mc_checking:
        mc_nodes = []
        # Default MC checking options
        check_opts = default_rich_checking_options()
        for key, conf in richRecConfs.iteritems():
            # MC checkers require original 'v1' container before filtering
            # To Be removed...
            if light_reco:
                conf["OriginalV1Tracks"] = best_tracks[key]["v1"]
            else:
                conf["OriginalV1Tracks"] = best_tracks["v1"]
            riMCcheck = make_rich_checkers(
                conf=conf, reco_opts=reco_opts, check_opts=check_opts)
            mc_nodes += [riMCcheck[key] for key in riMCcheck.keys()]
        nodes += [
            CompositeNode(
                name='RICH_mc_checking',
                children=mc_nodes,
                combineLogic=NodeLogic.NONLAZY_OR,
                forceOrder=False)
        ]

    return {
        "rich_pids": pidMerge,
        "monitoring": moni_nodes,
        "mcchecking": mc_nodes
    }
