###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import CaloClusterResolution, CaloFutureClusterResolution, CaloHypoResolution
from PyConf.Algorithms import CaloFutureDigit2MCLinks2Table, CaloClusterMCTruth
from PyConf.Algorithms import CaloClusterEfficiency
from PyConf.application import make_data_with_FetchDataFromFile
from Hlt2Conf.data_from_file import mc_unpackers


def monitor_calo_future_cluster_resolution(tuplePrefix, clusters,
                                           tableMCCaloClusters):
    # look only at photons from Bd->K*gamma
    # NB it works for "future" clusters
    # (currently clusters before shower overlap,
    # eventually all clusters will be "future")
    return [
        CaloFutureClusterResolution(
            input=clusters,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='clusters' + tuplePrefix,
            PDGID=[22],
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name='CaloClusterResolution' + tuplePrefix)
    ]


def monitor_calo_cluster_resolution(tuplePrefix, clusters,
                                    tableMCCaloClusters):
    # look only at photons from Bd->K*gamma
    # NB it works for legacy clusters
    # (currently clusters after shower overlap,
    # eventually it should be out of use and removed)
    return [
        CaloClusterResolution(
            input=clusters,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='clusters' + tuplePrefix,
            PDGID=[22],
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name='CaloClusterResolution' + tuplePrefix)
    ]


def monitor_calo_photon_resolution(tuplePrefix, photons, tableMCCaloClusters):
    # look only at photons from Bd->K*gamma
    # NB photons are CaloHypos
    return [
        CaloHypoResolution(
            input=photons,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='photons' + tuplePrefix,
            PDGID=[22],
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name="CaloHypoResolution" + tuplePrefix)
    ]


def monitor_calo_pi0_resolution(tuplePrefix, pi0, tableMCCaloClusters):
    # look only at pi0 from Bd->pi+pi-pi0
    # NB pi0 are CaloHypos
    return [
        CaloHypoResolution(
            input=pi0,
            inputRelations=tableMCCaloClusters,
            tuplePrefix='pi0' + tuplePrefix,
            PDGID=[111],  # brother 211 & -211
            PDGIDMother=[511, -511],
            minMatchFraction=0.0,
            minEnergy=0.0,
            name="CaloHypoResolution" + tuplePrefix)
    ]


def monitor_calo_efficiency(digits, clusters):

    # get MCParticles
    mcparts = mc_unpackers()["MCParticles"]

    # get Digit2MC table
    tableMCCaloDigits = CaloFutureDigit2MCLinks2Table(
        CaloDigits=digits,
        MCParticles=mcparts,
        Link=make_data_with_FetchDataFromFile(
            '/Event/Link/Raw/Ecal/Digits')).Output

    # get Clusterv22MC table
    tableMCCaloClusters = CaloClusterMCTruth(
        Input=tableMCCaloDigits, Clusters=clusters).Output

    # chek reconstruction efficiency
    return [
        CaloClusterEfficiency(
            inputRelations=tableMCCaloClusters,
            inputMCParticles=mcparts,
            recoClusters=clusters,
            minMCfraction=0.9,
            minMatchFraction=0.9,
            minET=50.0,
            minEndVtxZ=7000.,
            name="CaloRecoEff")
    ]
