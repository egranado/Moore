###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import GeV

from PyConf import configurable
from PyConf.utilities import ConfigurationError

from Hlt2Conf.data_from_file import mc_unpackers

from .mc_checking import (make_links_lhcbids_mcparticles_tracking_system,
                          make_links_tracks_mcparticles)

from .rich_reconstruction import (get_radiator_bool_opts,
                                  get_detector_bool_opts)

from .rich_data_monitoring import default_rich_monitoring_options

from PyConf.Algorithms import (
    DataPacking__Unpack_LHCb__MCRichDigitSummaryPacker_ as RichSumUnPack,
    Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels,
    Rich__Future__Rec__MC__Moni__PIDQC as PIDQC,
    Rich__Future__Rec__MC__Moni__SIMDPhotonCherenkovAngles as MCCKAngles,
    Rich__Future__Rec__MC__Moni__CherenkovResolution as MCCKReso)

from PyConf.Tools import TrackSelector

###############################################################################


def default_rich_checking_options():
    """
    Returns a dict of the default RICH MC checking options
    """

    # start with the data monitoring options
    opts = default_rich_monitoring_options()

    # Append anything MC specific ...
    opts["MomentumRanges"] = {
        "2to100": [2 * GeV, 100 * GeV],
        "2to10": [2 * GeV, 10 * GeV],
        "10to70": [10 * GeV, 70 * GeV],
        "70to100": [70 * GeV, 100 * GeV]
    }

    return opts


###############################################################################


@configurable
def default_rich_checkers(moni_set="Standard"):
    """
    Returns the set of MC checkers to activate

    Args:
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    monitors = {
        "Expert":  # Activates all montiors
        ["PIDPerformance", "PhotonCherenkovAngles", "CherenkovResolution"],
        "Standard":  # The default set of monitors
        ["PIDPerformance", "PhotonCherenkovAngles"],
        "OnlineMonitoring":  # For monitoring at the pit
        ["PIDPerformance", "PhotonCherenkovAngles"],
        "None": []
    }

    if moni_set not in monitors.keys():
        raise ConfigurationError("Unknown histogram set " + moni_set)

    return monitors[moni_set]


###############################################################################


@configurable
def make_rich_checkers(conf, reco_opts, check_opts, moni_set="Standard"):
    """
    Returns a set of RICH MC checkers

    Args:
        conf       (dict): Reconstruction configuration (data) to run monitoring on
        reco_opts  (dict): Reconstruction options
        check_opts (dict): MC checking options
        moni_set (string): Monitor set to activate

    Returns:
        dict of activated monitoring algorithms
    """

    # Momentum selections for performance plots
    momentumCuts = check_opts["MomentumRanges"]

    # The track name for this configuration
    track_name = conf["TrackName"]

    # The detector and radiator options
    det_opts = get_detector_bool_opts(reco_opts, track_name)
    rad_opts = get_radiator_bool_opts(reco_opts, track_name)

    # get the list of checkers to activate
    checkers = default_rich_checkers(moni_set)

    # MCParticles
    mcps = mc_unpackers()["MCParticles"]

    # Track linker stuff
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
    links_to_tracks = make_links_tracks_mcparticles(
        InputTracks=conf["OriginalV1Tracks"], LinksToLHCbIDs=links_to_lhcbids)

    # Make the RICH Track -> MCParticle relations table
    tkMCPRels = TkToMCPRels(
        name="RichTkToMCPRelations",
        TracksLocation=conf["OriginalV1Tracks"],
        MCParticlesLinkLocation=links_to_tracks,
        MCParticlesLocation=mcps)

    # RICH digit MC summaries
    richSummaries = mc_unpackers()["MCRichDigitSummaries"]

    # The dict of configured monitors to return
    results = {}

    # PID performance
    key = "PIDPerformance"
    if key in checkers:
        # loop over momentum cuts
        for cutname, cuts in momentumCuts.iteritems():
            # Make a PID monitor for this selection
            results[key + cutname] = PIDQC(
                name="RichPIDMon" + track_name + cutname,
                Detectors=det_opts,
                Radiators=rad_opts,
                TrackSelector=TrackSelector(MinPCut=cuts[0], MaxPCut=cuts[1]),
                TracksLocation=conf["InputTracks"],
                RichPIDsLocation=conf["RichPIDs"],
                TrackToMCParticlesRelations=tkMCPRels.
                TrackToMCParticlesRelations)

    # MC CK angles
    key = "PhotonCherenkovAngles"
    if key in checkers:
        results[key] = MCCKAngles(
            name="RiCKMCRes" + track_name + cutname,
            Detectors=det_opts,
            Radiators=rad_opts,
            TracksLocation=conf["InputTracks"],
            TrackSegmentsLocation=conf["TrackSegments"],
            TrackToMCParticlesRelations=tkMCPRels.TrackToMCParticlesRelations,
            PhotonToParentsLocation=conf["PhotonToParents"],
            SummaryTracksLocation=conf["SummaryTracks"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            RichPixelClustersLocation=conf["RichClusters"],
            RichDigitSummariesLocation=richSummaries,
            RichSIMDPixelSummariesLocation=conf["RichSIMDPixels"])

    # Expected track CK resolutions
    key = "CherenkovResolution"
    if key in checkers:
        results[key] = MCCKReso(
            name="RiTkMCRes" + track_name,
            Detectors=det_opts,
            Radiators=rad_opts,
            RichDigitSummariesLocation=richSummaries,
            TrackToMCParticlesRelations=tkMCPRels.TrackToMCParticlesRelations,
            SummaryTracksLocation=conf["SummaryTracks"],
            CherenkovAnglesLocation=conf["SignalCKAngles"],
            PhotonToParentsLocation=conf["PhotonToParents"],
            CherenkovPhotonLocation=conf["CherenkovPhotons"],
            RichSIMDPixelSummariesLocation=conf["RichSIMDPixels"],
            TrackSegmentsLocation=conf["TrackSegments"],
            TracksLocation=conf["InputTracks"],
            RichPixelClustersLocation=conf["RichClusters"],
            CherenkovResolutionsLocation=conf["CherenkovResolutions"])

    return results
