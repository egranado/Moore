###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of minimum bias lines.

Each line has a very minimal set of restrictions on what must be present in the
event, such that the selected events are biased in a very limited way,
typically meant with respect to 'heavy flavour events'.
"""
import cppyy

from Configurables import VoidFilter as _VoidFilter

from PyConf.Algorithms import VoidFilter
from PyConf.application import make_odin
from Moore.config import HltLine

from RecoConf.hlt1_tracking import make_hlt1_tracks

from Functors import EVENTTYPE, SIZE
from Functors import math as fmath


def odin_nobias_filter(make_odin=make_odin):
    nobias_bit = cppyy.gbl.LHCb.ODIN.NoBias

    def input_transform(InputLocation):
        return {"Cut": fmath.test_bit(EVENTTYPE(InputLocation), nobias_bit)}

    odin = make_odin()
    return VoidFilter(InputLocation=odin, input_transform=input_transform)


def low_multiplicity_velo_line(name="Hlt1MicroBiasLowMultVeloLine",
                               prescale=1,
                               nvelo_tracks_min=5):
    """Accepts NoBias events with at least some number of VELO tracks."""

    def input_transform(InputLocation):
        return {"Cut": SIZE(InputLocation) > nvelo_tracks_min}

    nobias = odin_nobias_filter()
    velo_tracks = make_hlt1_tracks()["Velo"]["Pr"]
    ntracks_filter = VoidFilter(
        InputLocation=velo_tracks, input_transform=input_transform)

    return HltLine(name=name, algs=[nobias, ntracks_filter], prescale=prescale)


def no_bias_line(name="Hlt1NoBiasLine", prescale=1):
    """Accepts all crossings marked with the NoBias ODIN bit."""
    nobias = odin_nobias_filter()

    return HltLine(
        name=name,
        algs=[nobias],
        prescale=prescale,
    )
