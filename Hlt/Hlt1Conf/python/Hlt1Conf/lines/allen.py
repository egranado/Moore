###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import HltLine

from RecoConf.hlt1_allen import make_allen_dec_reports


def allen_line(name='Hlt1Allen', prescale=1):

    allen = make_allen_dec_reports()

    return HltLine(name=name, algs=[allen], prescale=prescale)
