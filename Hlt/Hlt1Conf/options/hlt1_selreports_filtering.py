###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run all HLT1 lines with each decision algorithm replaced by a TOS filter.

Some lines are modified so that the select all input objects. The decision
algorithms of all lines are filters which TOS on the line's SelReport, as
produced from a previous run of Moore (which was otherwise configured
identically). This checks that the objects which made the SelReports can be
selected by them.
"""
from PyConf.Algorithms import (
    HltSelReportsDecoder,
    TOSFilter__v1__Track,
    TOSFilter__v1__RecVertex,
)
from PyConf.application import default_raw_event

from Moore import options, run_moore
from Moore.config import HltLine
from Moore.selreports import convert_output

from Hlt1Conf.settings import all_lines as _all_lines
from Hlt1Conf.algorithms import CombineTracks, Filter
from Hlt1Conf.lines.track_muon import (
    make_fitted_tracks_with_muon_id,
    one_track_muon_mva_line,
)
from Hlt1Conf.lines.track_mva import (
    make_tracks_mva_tracks,
    one_track_mva_line,
    two_track_mva_line,
)

from Functors import ALL, FILTER


def make_selreports():
    return HltSelReportsDecoder(
        RawEventLocations=default_raw_event(),
        # '1' to decode HLT1 SelReports
        SourceID=1,
    )


def tos_filter_for(alg):
    """Return a configured TOS filter for this algorithm."""
    assert len(alg.outputs) == 1
    output = alg.outputs[alg.outputs.keys()[0]]
    output_type = output.type
    if output_type.startswith("std::vector<LHCb::Event::v1::Track"):
        return TOSFilter__v1__Track
    elif output_type.startswith("std::vector<LHCb::RecVertex"):
        return TOSFilter__v1__RecVertex
    else:
        raise ValueError("No TOS filter available for {}".format(output_type))


def one_track_line(name='Hlt1TrackMVALine'):
    """A one-track line that selects all tracks."""
    track_filter = Filter(make_tracks_mva_tracks(),
                          ALL)['PrFittedForwardWithPVs']
    return HltLine(name, [track_filter])


def two_track_line(name='Hlt1TwoTrackMVALine'):
    """A two-track line that selects all vertices."""
    children = Filter(make_tracks_mva_tracks(), ALL)
    combination_filter = CombineTracks(
        NBodies=2,
        VertexCut=ALL,
        InputTracks=children['PrFittedForwardWithPVs'],
        CombinationCut=ALL)
    return HltLine(name, [combination_filter])


def one_track_muon_line(name='Hlt1TrackMuonMVALine'):
    """A one-track muon line that selects all tracks."""
    track_filter = Filter(make_fitted_tracks_with_muon_id(),
                          ALL)['PrFittedForwardWithMuonID']
    return HltLine(name, [track_filter])


def all_lines():
    # Test the TOS filter on a track maker with and without muon ID and a
    # vertex maker
    with one_track_mva_line.substitute(one_track_line), \
         two_track_mva_line.substitute(two_track_line), \
         one_track_muon_mva_line.substitute(one_track_muon_line):
        # Have the run all lines otherwise the ANNSvc will be configured
        # incompletely
        all_lines = _all_lines()

    lines_to_run = []
    reports_maker = make_selreports()
    for line in all_lines:
        # No need to modify lines that don't produce output (as we can't
        # TIS/TOS on them anyway) nor lumi lines
        if not line.produces_output() or 'Lumi' in line.name:
            lines_to_run.append(line)
            continue

        line_output = line.output_producer
        v1_objects = convert_output(line_output)

        # Get the correct TOSFilter class, then instantiate it
        cls = tos_filter_for(v1_objects)
        tos_filter = cls(
            InputContainer=v1_objects,
            SelReports=reports_maker.OutputHltSelReportsLocation,
            DecisionPattern=line.decision_name,
        )
        lines_to_run.append(
            HltLine(
                name=line.name,
                # Exclude the prescaler child otherwise we'll get a circular
                # control flow error due to duplicate algorithms
                algs=line.node.children[1:] + [tos_filter],
            ))

    return lines_to_run


run_moore(options, all_lines)
